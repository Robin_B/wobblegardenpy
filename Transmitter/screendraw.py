import win32gui,  win32ui,  win32con, win32api
import timeit
import serial
import serial.tools
import serial.tools.list_ports
from msvcrt import getch
hwnd = win32gui.GetDesktopWindow()
from wobbleforest import wobbleforest as wf
print hwnd
 
import pygame
from pygame.locals import *

# you can use this to capture only a specific window
#l, t, r, b = win32gui.GetWindowRect(hwnd)
#w = r - l
#h = b - t

# get complete virtual screen including all monitors
SM_XVIRTUALSCREEN = 76
SM_YVIRTUALSCREEN = 77
SM_CXVIRTUALSCREEN = 78
SM_CYVIRTUALSCREEN = 79 
w = vscreenwidth = win32api.GetSystemMetrics(SM_CXVIRTUALSCREEN)
h = vscreenheigth = win32api.GetSystemMetrics(SM_CYVIRTUALSCREEN)
l = vscreenx = win32api.GetSystemMetrics(SM_XVIRTUALSCREEN)
t = vscreeny = win32api.GetSystemMetrics(SM_YVIRTUALSCREEN)
r = l + w
b = t + h
print l, t, r, b, ' -> ', w, h

panelWidth = 64;

captureWidth = w;
captureHeight = h;

captureOffsetX = 200;
captureOffsetY = 200;

springPixelOffsetX = 400;
springPixelOffsetY = 400;

pixels = wf.getPixels()
zoom = 1

def capture2():
	global panels, captureHeight, captureWidth, panelWidth
	hwndDC = win32gui.GetWindowDC(hwnd)
	mfcDC  = win32ui.CreateDCFromHandle(hwndDC)
	saveDC = mfcDC.CreateCompatibleDC()
	 
	saveBitMap = win32ui.CreateBitmap()
	saveBitMap.CreateCompatibleBitmap(mfcDC, w, h)
	saveDC.SelectObject(saveBitMap)
	saveDC.BitBlt((0, 0), (captureWidth, captureHeight),  mfcDC,  (captureOffsetX, captureOffsetY),  win32con.SRCCOPY)
	#saveBitMap.SaveBitmapFile(saveDC,  'screencapture.bmp')
	for y in range(captureHeight):
	 	for x in range(panelWidth):
	 		addPixel(saveDC.GetPixel((x + p * panelWidth) * 2, y * 2))
	 		# addPixel(saveDC.GetPixel(x + p * panelWidth, y))


 	# pixel = saveDC.GetPixel(1, 1)

	saveDC.DeleteDC()
	win32gui.DeleteObject(saveBitMap.GetHandle())


def capture():
	global panels, captureHeight, captureWidth, panelWidth
	hwndDC = win32gui.GetWindowDC(hwnd)
	mfcDC  = win32ui.CreateDCFromHandle(hwndDC)
	saveDC = mfcDC.CreateCompatibleDC()
	 
	saveBitMap = win32ui.CreateBitmap()
	saveBitMap.CreateCompatibleBitmap(mfcDC, w, h)
	saveDC.SelectObject(saveBitMap)
	saveDC.BitBlt((0, 0), (captureWidth, captureHeight),  mfcDC,  (captureOffsetX, captureOffsetY),  win32con.SRCCOPY)
	#saveBitMap.SaveBitmapFile(saveDC,  'screencapture.bmp')
	for i, p in enumerate(pixels):
		# print "getting pixel from ", int((p.x + springPixelOffsetX) * zoom), int((p.y + springPixelOffsetY) * zoom)
		pixel = saveDC.GetPixel(int((p.x + springPixelOffsetX) * zoom), int((p.y + springPixelOffsetY) * zoom))
		b = (pixel>>16)&0xFF
		g = (pixel>>8)&0xFF
		r = (pixel>>0)&0xFF
		# print "pixel col: ", pixel, r, g, b
		wf.setPixel(i, (r, g, b))
	# for p in range(panels):
	# 	for y in range(captureHeight):
	# 	 	for x in range(panelWidth):
	# 	 		addPixel(saveDC.GetPixel((x + p * panelWidth), y))
		 		# addPixel(saveDC.GetPixel(x + p * panelWidth, y))


 	# pixel = saveDC.GetPixel(1, 1)

	saveDC.DeleteDC()
	win32gui.DeleteObject(saveBitMap.GetHandle())

def startGame():
	pass

def loop():
	capture()

def keyboardHandler(events):
	pass

def init():
    wf.initialize(startGame, loop, keyboardHandler)
    wf.run()

init()