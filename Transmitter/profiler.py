import pstats
import platform
import datetime

with open('perfstats/'+platform.node()+'___' + '{date:%Y%m%d_%H%M%S}'.format( date=datetime.datetime.now() ) + '.txt', 'w+') as stream:
	p = pstats.Stats('stats', stream=stream)
	p.strip_dirs().sort_stats('time').print_stats(.2)

	p.strip_dirs().sort_stats('time').print_callers(.2)

