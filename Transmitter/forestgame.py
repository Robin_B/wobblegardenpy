﻿from wobbleforest import wobbleforest as wf
import time
import random
import colorsys
import math
import noise
import pygame
from pygame.locals import *

#region #################################################### SNOW ###############################################

snow = []
dt = 0
dtPrevious = 0

class Snow:
    active = True
    distOnRing = 0
    limitToWater = False
    tailPos = []
    tailLength = 1
    previousPos = 0

    def __init__(self, i, speed, color, targetX = -100000, targetY = -1, jumpChance = 0.4, movementNoise = 0.1, active = True):
        self.i = i
        self.speed = speed
        self.color = color
        self.targetX = targetX
        self.targetY = targetY
        self.friction = 0.1
        self.jumpChance = jumpChance
        self.movementNoise = movementNoise
        self.active = active
        #r = Runner(i, speed, color, jumpChance, distOnRing = 0, targetX = -100000, targetY = -1, movementNoise = 0.5, active=True)
        # TODO: cleanup runners once in a while

    def update(self):
        global dt
        if not self.active: return
        advance = (self.speed * (1 + self.movementNoise))*dt
        self.speed *= (1 - self.friction * dt)
        myRing = pixels[int(self.i)].ringId
        myAngle = rings[myRing].angle + (int(self.i) - rings[myRing].firstLEDindex) * 22.5
        myNewAngle = rings[myRing].angle + (int(self.i + advance) - rings[myRing].firstLEDindex) * 22.5
        self.i += advance
        # print "updating runner, myring:",myRing,"myAngle", myAngle,"mypos", self.i
        while self.i - rings[myRing].firstLEDindex >= 16: self.i -=16
        while self.i - rings[myRing].firstLEDindex < 0: self.i +=16
        self.movementNoise *= (1 - dt/5)
        if (self.movementNoise < 0.1):
            self.targetX = -100000
        #print "movement noise:", self.movementNoise
        self.distOnRing += abs(advance)
        if (int(myAngle - 30)/60 != int(myNewAngle - 30)/60):
            #jump?
            changeAngle = (max(int(myAngle - 30)/60, int(myNewAngle - 30)/60) * 60 + 30) % 360

            jumpChance = self.jumpChance
            if self.targetX != -100000:
                targetAngle = (90 + math.degrees(math.atan2(self.targetY - pixels[int(self.i)].y, self.targetX - pixels[int(self.i)].x))) % 360
                # print "Runner wants to jump, his angles are", myAngle, myNewAngle
                # print "switchangle:", changeAngle, "target:", targetAngle
                if abs(targetAngle - changeAngle) < 60 and self.movementNoise < random.random():
                    jumpChance = 1 
                else:
                    jumpChance = 0

            if random.random() < jumpChance:
                neighborId = (changeAngle/60) % 6
                newRing = rings[myRing].neighbours[neighborId]
                if newRing == -1:
                    return
                self.distOnRing = 0
                newAngle = (myNewAngle + 180) % 360
                newAngleExact = (neighborId * 60 + 30 + 180) % 360
                realNewAngle = 2*newAngleExact - newAngle # reversing direction
                self.speed *= -1
                newRingAngle = rings[newRing].angle
                newIndex = (realNewAngle - newRingAngle) / 22.5
                #newIndex = (newAngleExact - newRingAngle) / 22.5
                self.i = rings[newRing].firstLEDindex + newIndex
                while self.i - rings[newRing].firstLEDindex >= 16: self.i -=16
                while self.i - rings[newRing].firstLEDindex < 0: self.i +=16        

        self.color = wf.hsv(random.random(),0,random.random() * 0.5 + 0.5)

    def draw(self):
        if not self.active: return
        #wf.addPixel(int(self.i), self.color)
        if int(self.i) != self.previousPos:
            self.tailPos = [self.previousPos] + self.tailPos[0:(self.tailLength - 1)]
            #print "new tail:", self.tailPos
            self.previousPos = int(self.i)

        wf.addRingFloatPixel(self.i, self.color)

        #draw tail
        for i, p in enumerate(self.tailPos):
            wf.addPixel(p, wf.dimColor(self.color, 0.2 - i * (0.2/self.tailLength)))

    def getPos(self):
        return (pixels[int(self.i)].x, pixels[int(self.i)].y)


def addSnow(i, speed, color, targetX = -100000, targetY = -1, jumpChance = 0.6):
    snow.append(Snow(i, speed, color, targetX, targetY, jumpChance))

def snowBlast(ringId):
    #blast snow away from ring
    for s in snow:
        if not s.active: continue
        sPos = s.getPos()
        dx = sPos[0] - rings[ringId].x
        dy = sPos[1] - rings[ringId].y
        dist = math.sqrt(dx ** 2 + dy ** 2)
        if dist > 0:
            s.targetX = sPos[0] + dx 
            s.targetY = sPos[1] + dy 
            s.speed += 1000 / dist
            print "Snow blast to flake, new target:", sPos, "->",s.targetX, s.targetY, s.speed


def updateSnow():
    for s in snow: 
        s.update()

def drawSnow():
    for s in snow:
        s.draw()

#endregion


#region ################################################### RUNNER ###############################################

runners = []
fish = []
dt = 0
dtPrevious = 0

class Runner:
    active = True
    distOnRing = 0
    limitToWater = False
    tailPos = []
    tailLength = 2
    previousPos = 0

    def __init__(self, i, speed, color, targetX = -100000, targetY = -1, jumpChance = 0.4, movementNoise = 0.1, active = True):
        self.i = i
        self.speed = speed
        self.color = color
        self.targetX = targetX
        self.targetY = targetY
        self.jumpChance = jumpChance
        self.movementNoise = movementNoise
        self.active = active
        #r = Runner(i, speed, color, jumpChance, distOnRing = 0, targetX = -100000, targetY = -1, movementNoise = 0.5, active=True)
        # TODO: cleanup runners once in a while

    def update(self):
        global dt
        if not self.active: return

        if self.limitToWater:
            advance = (self.speed * (1 + self.movementNoise*5))*dt
        else:
            advance = (self.speed * (1 + self.movementNoise))*dt
        myRing = pixels[int(self.i)].ringId
        myAngle = rings[myRing].angle + (int(self.i) - rings[myRing].firstLEDindex) * 22.5
        myNewAngle = rings[myRing].angle + (int(self.i + advance) - rings[myRing].firstLEDindex) * 22.5
        self.i += advance
        # print "updating runner, myring:",myRing,"myAngle", myAngle,"mypos", self.i
        while self.i - rings[myRing].firstLEDindex >= 16: self.i -=16
        while self.i - rings[myRing].firstLEDindex < 0: self.i +=16
        self.movementNoise *= (1 - dt/5)
        if (self.movementNoise < 0.1):
            self.targetX = -100000
        #print "movement noise:", self.movementNoise
        self.distOnRing += abs(advance)
        if (int(myAngle - 30)/60 != int(myNewAngle - 30)/60 and self.distOnRing > 3):
            #jump?
            changeAngle = (max(int(myAngle - 30)/60, int(myNewAngle - 30)/60) * 60 + 30) % 360

            jumpChance = self.jumpChance
            if self.targetX != -100000:
                targetAngle = (90 + math.degrees(math.atan2(self.targetY - pixels[int(self.i)].y, self.targetX - pixels[int(self.i)].x))) % 360
                # print "Runner wants to jump, his angles are", myAngle, myNewAngle
                # print "switchangle:", changeAngle, "target:", targetAngle
                if abs(targetAngle - changeAngle) < 60 and self.movementNoise < random.random():
                    jumpChance *= 3
                else:
                    jumpChance *= 0.1

            if random.random() < jumpChance:
                # neighborId = max(int(myAngle - 30)/60, int(myNewAngle - 30)/60) % 6
                neighborId = (changeAngle/60) % 6
                # print "Runner jumps, his angles are", myAngle, myNewAngle, neighborId
                # print "neighbours are", rings[myRing].neighbours
                newRing = rings[myRing].neighbours[neighborId]
                if newRing == -1 or (self.limitToWater and (rings[newRing].cell == None or rings[newRing].cell.cellType != waterType)):
                    return
                self.distOnRing = 0
                newAngle = (myNewAngle + 180) % 360
                newAngleExact = (neighborId * 60 + 30 + 180) % 360
                realNewAngle = 2*newAngleExact - newAngle # reversing direction
                self.speed *= -1
                newRingAngle = rings[newRing].angle
                newIndex = (realNewAngle - newRingAngle) / 22.5
                #newIndex = (newAngleExact - newRingAngle) / 22.5
                self.i = rings[newRing].firstLEDindex + newIndex
                while self.i - rings[newRing].firstLEDindex >= 16: self.i -=16
                while self.i - rings[newRing].firstLEDindex < 0: self.i +=16        

    def draw(self):
        if not self.active: return
        #wf.addPixel(int(self.i), self.color)
        if int(self.i) != self.previousPos:
            self.tailPos = [self.previousPos] + self.tailPos[0:(self.tailLength - 1)]
            #print "new tail:", self.tailPos
            self.previousPos = int(self.i)

        wf.addRingFloatPixel(self.i, self.color)

        #draw tail
        for i, p in enumerate(self.tailPos):
            wf.addPixel(p, wf.dimColor(self.color, 0.2 - i * (0.2/self.tailLength)))


def addRunner(i, speed, color, targetX = -100000, targetY = -1, jumpChance = 0.6):
    runners.append(Runner(i, speed, color, targetX, targetY, jumpChance))


def addFish(i, speed):
    f = Runner(i, speed, (255, 55, 20 + random.random() * 5))
    f.limitToWater = True
    f.tailLength = 5
    print "adding fish:", f, "limitToWater:", f.limitToWater
    fish.append(f)

def updateRunners():
    for r in runners: 
        r.update()

def updateFish():
    for f in fish: 
        f.update()

def drawRunners():
    for r in runners:
        r.draw()

def drawFish():
    for f in fish:
        f.draw()

#endregion

#region ################################################### Cell ###############################################

trees = []
water = []
treeType = 1
waterType = 2

class Cell:
    baseColor = [0, 20, 0]
    currentColor = [0,0,0]

    def __init__(self, ringId, cellType, baseColor):
        self.ringId = ringId
        self.cellType = cellType
        self.firstLEDindex = wf.rings[ringId].firstLEDindex
        if baseColor is None:
            self.baseColor = [random.random() * 2, random.random() * 5 + 10, random.random() * 5]
        else:
            self.baseColor = baseColor
        rings[ringId].cell = self

    def update(self):
        pass

    def draw(self):
        global gameTime, tick
        for i in range(16):
            #col[1] += math.sin(time.time() + i) * (15.0 + i/8.0)
            #print "Cell col", col
            if self.cellType == treeType:
                self.currentColor = self.baseColor[:]
                self.currentColor[1] *= 0.3+noise.pnoise2(pixels[self.firstLEDindex + i].x , pixels[self.firstLEDindex + i].y + (time.time()-gameTime)*0.3)
            elif self.cellType == waterType:
                # col[2] *= 0.3+noise.pnoise2(pixels[self.firstLEDindex + i].x/50 + (time.time()-gameTime)*2.2,pixels[self.firstLEDindex + i].y/50 + (time.time()-gameTime)*2.3)
                #col[2] *= 1+noise.pnoise2(pixels[self.firstLEDindex + i].x/150 + (time.time()-gameTime)*0.2,pixels[self.firstLEDindex + i].y/150 + (time.time()-gameTime)*0.5)*2 +noise.pnoise2(pixels[self.firstLEDindex + i].x/170 - (time.time()-gameTime)*0.3,pixels[self.firstLEDindex + i].y/160 - (time.time()-gameTime)*0.4)
                # n = 2+noise.pnoise2(pixels[self.firstLEDindex + i].x/150 + (time.time()-gameTime)*0.2,pixels[self.firstLEDindex + i].y/150 + (time.time()-gameTime)*0.5)*2 +noise.pnoise2(pixels[self.firstLEDindex + i].x/170 - (time.time()-gameTime)*0.3,pixels[self.firstLEDindex + i].y/160 - (time.time()-gameTime)*0.4)
                #if ((tick + i) % 3 == 0):
                n = 2+noise.pnoise2(pixels[self.firstLEDindex + i].x/150 + (time.time()-gameTime)*0.2,pixels[self.firstLEDindex + i].y/150 + (time.time()-gameTime)*0.5)*2
                # col[2] = max(2, col[2])
                self.currentColor = wf.hsv(0.5 + n/10, n / 2, 0.01)
                #print col
            wf.setPixel(self.firstLEDindex + i, self.currentColor)


def addTree(ringId):
    trees.append(Cell(ringId, treeType, [random.random() * 2, random.random() * 5 + 20, random.random() * 2]))

def addWater(ringId):
    water.append(Cell(ringId, waterType, [0, 0, 15]))

def drawTrees():
    for t in trees:
        t.draw()

def drawWater():
    for w in water:
        w.draw()

#endregion

#region ################################################### Effects ###############################################
effects = []

class Effect:
    active = True
    limitToWater = False

    def __init__(self, x, y, speed, color, maxRadius, bandSize = -1):
        self.x = x
        self.y = y
        self.startTime = time.time()
        self.speed = speed
        self.color = color
        self.maxRadius = maxRadius
        self.bandSize = bandSize
        self.active = True
        # TODO: cleanup runners once in a while

    def draw(self):
        if not self.active: return
        currentR = (time.time() - self.startTime) * self.speed
        if currentR > self.maxRadius + self.bandSize:
            self.active = False
            return
        currentSqR = currentR * currentR
        maxR = self.maxRadius * self.maxRadius
        sqBand = self.bandSize * self.bandSize
        skipped = 0
        i = 0
        while i < len(pixels):
            p = pixels[i]
            sqDist = wf.getSqDist(self.x, self.y, p.x, p.y)
            if i % 16 == 0 and (sqDist > currentSqR + 1450*10 or sqDist < currentSqR - sqBand - 1450*2):
                #skip the entire ring
                skipped+=1
                i+=16
                continue
            if sqDist < min(maxR, currentSqR) and (self.bandSize == -1 or sqDist > currentSqR - sqBand) :
                wf.addPixel(i, wf.dimColor(self.color, 1.1 - min(1, (currentSqR - sqDist)/20000)))
            i+=1
        # print "skipped rings:", skipped


def addEffect(x, y, speed, color, maxRadius, bandSize = -1):
    for e in effects:
        if not e.active:
            e.x = x
            e.y = y
            e.speed = speed
            e.color = color
            e.maxRadius = maxRadius
            e.bandSize = bandSize
            e.startTime = time.time()
            e.active = True
            return
    effects.append(Effect(x, y, speed, color, maxRadius, bandSize))

def drawEffects():
    for e in effects:
        e.draw()

#endregion
       
#region ################################################### Ball ###############################################

balls = []

class Ball:
    def __init__(self, x, y, dx, dy, radius, drag, color, index):
        self.x = x
        self.y = y
        self.dx = dx
        self.dy = dy
        self.radius = radius
        self.drag = drag
        self.color = color
        self.index = index

    def hitWalls(self, newX, newY):
        lines = wf.lines
        for index, l in enumerate(lines):
            d1 = l.dist(self.x, self.y)
            d2 = l.dist(newX, newY)
            if (d1 < 0) != (d2 < 0): # opposite signs?
                self.dx, self.dy = l.reflectVector(self.dx, self.dy)
                if index == 0 or index == 4 or index == 5:
                    self.goalHit(index)
                return True
        return False

    def goalHit(self, line):
        print "hit line", line
        if line == 0:
            col = 0.3
        if line == 4:
            col = 0.6
        if line == 5:
            col = 1
        addEffect(self.x, self.y, speed = 350, color = wf.hsv(col, 1, 0.1), maxRadius = 450, bandSize = 170)


    def update(self):
        global dt
        newX = self.x + self.dx * dt
        newY = self.y + self.dy * dt
        if not self.hitWalls(newX, newY):
            # don't move if hit walls so we can check for more lines next tick
            self.x = newX
            self.y = newY

    def addSpringForce(self, springIndex, amount):
        dist = math.sqrt(wf.getSqDist(self.x, self.y, rings[springIndex].x, rings[springIndex].y))
        if (dist < 100):
            if dist < 1: dist = 1
            self.dx += (self.x - rings[springIndex].x)/(dist * dist / 50) * 100
            self.dy += (self.y - rings[springIndex].y)/(dist * dist / 50) * 100
            # cap speed
            speed = math.sqrt(self.dx * self.dx + self.dy * self.dy)
            if speed > 200:
                self.dx = 200 * self.dx / speed
                self.dy = 200 * self.dy / speed

    def checkCollision(self, otherBall):
        if self == otherBall: return
        dist = math.sqrt(wf.getSqDist(self.x, self.y, otherBall.x, otherBall.y))
        if dist <= self.radius + otherBall.radius:
            # print "Balls are colliding! speed before: ", self.dx, self.dy, dist
            collisionX = self.x - otherBall.x
            collisionY = self.y - otherBall.y
            collXNormalized = collisionX / dist
            collYNormalized = collisionY / dist
            # print "collision normalized: ", collXNormalized, collYNormalized

            myCollisionSpeed = self.dx * collXNormalized + self.dy * collYNormalized
            otherCollisionSpeed = otherBall.dx * collXNormalized + otherBall.dy * collYNormalized

            # print "collision speeds: ", myCollisionSpeed, otherCollisionSpeed

            self.x = otherBall.x + collXNormalized * (self.radius + otherBall.radius)
            self.y = otherBall.y + collYNormalized * (self.radius + otherBall.radius)

            self.dx += (otherCollisionSpeed - myCollisionSpeed) * collXNormalized
            self.dy += (otherCollisionSpeed - myCollisionSpeed) * collYNormalized

            otherBall.dx += (myCollisionSpeed - otherCollisionSpeed) * collXNormalized
            otherBall.dy += (myCollisionSpeed - otherCollisionSpeed) * collYNormalized

            wf.clackSound.play()
            # print "Speed after: ", self.dx, self.dy


    def draw(self):
        wf.drawDisk(self.x, self.y, self.radius + 5, self.color)

def addBall(x, y, dx, dy, radius, drag, color):
    balls.append(Ball(x, y, dx, dy, radius, drag, color, len(balls)))

def drawGoals():

    # 25 34 35
    wf.drawLEDsOnRing(25, 170, 50, wf.hsv(1, 1, 0.1))
    wf.drawLEDsOnRing(35, 170, 50, wf.hsv(1, 1, 0.1))
    wf.drawLEDsOnRing(34, 170, 60, wf.hsv(1, 1, 0.1))

    # 4 3 8
    wf.drawLEDsOnRing(4, 50, 50, wf.hsv(0.3, 1, 0.1))
    wf.drawLEDsOnRing(3, 50, 50, wf.hsv(0.3, 1, 0.1))
    wf.drawLEDsOnRing(8, 50, 60, wf.hsv(0.3, 1, 0.1))

    # 23 20 13
    wf.drawLEDsOnRing(23, 290, 50, wf.hsv(0.6, 1, 0.1))
    wf.drawLEDsOnRing(22, 290, 50, wf.hsv(0.6, 1, 0.1))
    wf.drawLEDsOnRing(13, 290, 60, wf.hsv(0.6, 1, 0.1))
    pass

def drawBalls():
    for b in balls:
        b.draw()

    if len(balls) > 0:
        drawGoals()

def updateBalls():
    for i, b in enumerate(balls):
        for j in range(i + 1, len(balls)):
            b.checkCollision(balls[j])

        b.update()

#endregion

#region ################################################### Main ###############################################


gameTime = 0

pixels = wf.getPixels()
rings = wf.getRings()
tick = 0
oldTouchData = []

def drawBackground():
    wf.fadeAllLeds(0.9)

def getCellType(i):
    if rings[i] != None and rings[i].cell != None:
        return rings[i].cell.cellType
    return None

def handleInput():
    springData = wf.getSpringData()
    for index, amount in enumerate(springData):
        if amount > 0:
            #wf.drawDisk(rings[index].x, rings[index].y, amount + 24)
            #addEffect(rings[index].x, rings[index].y, speed = 350, color = wf.hsv(index/14.0 % 1.0, 1, min(0.1, amount / 10)), maxRadius = 100, bandSize = 20)
            # addEffect(rings[index].x, rings[index].y, speed = 350, color = wf.hsv(index/14.0 % 1.0, 1, 0.1), maxRadius = 50, bandSize = 170)
            addEffect(rings[index].x, rings[index].y, speed = 350, color = wf.hsv(index/14.0 % 1.0, 1, 0.6), maxRadius = 350, bandSize = 100)
            print index
            for b in balls:
                b.addSpringForce(index, amount)

            for r in runners:
                r.targetX = rings[index].x
                r.targetY = rings[index].y
                r.movementNoise = 1

            snowBlast(index)

        if amount > 1:
            if getCellType(index) == waterType:
                for f in fish:
                    f.targetX = rings[index].x
                    f.targetY = rings[index].y
                    f.movementNoise = 0.5 + random.random()*0.5


    touchData = wf.getTouchData()
    for index, t in enumerate(touchData):
        if t != oldTouchData[index]:
            #value changed!
            if t > 0:
                #just touched
                addEffect(rings[index].x, rings[index].y, speed = 350, color = wf.hsv(index/14.0 % 1.0, 1, 0.1), maxRadius = 30, bandSize = 170)
            else:
                #just released
                pass



def keyboardHandler(events):
    for event in events:
        if event.type == KEYUP:
            if event.key == K_e:
                addEffect(100, 100, 350, (255, 0, 0), 450, 170)
        


def loop():
    global dtPrevious, dt, tick
    dt = time.time() - dtPrevious
    dtPrevious = time.time()
    tick += 1
    drawBackground()
    handleInput()
    updateRunners()
    updateFish()
    updateBalls()
    updateSnow()
    drawWater()
    drawFish()
    drawTrees()
    drawRunners()
    drawSnow()
    drawEffects()
    drawBalls()
    # drawDisk(math.sin((time.time() - gameTime) * 5) * 50 + 30, math.cos((time.time() - gameTime)* 5) * 50 + 100, math.sin((time.time() - gameTime) *4)*40 + 50)


def startGame():
    global gameTime, dtPrevious, oldTouchData
    gameTime = time.time()    

    version = 3
    # addBall(50, 50, 100, 100, radius = 25, drag = 0, color = 0.7)
    # addBall(150, 150, 100, -140, radius = 25, drag = 0, color = 0.1)
    # addBall(10, 150, -100, 40, radius = 25, drag = 0, color = 0.4)

    if version == 1:
        for x in range(10):
            #addRunner(100, 30 + random.random() * 10, wf.hsv(random.random(),1,1)) 
            pass

        for x in range(36):
            #addTree(x)
            neighborCount = sum([i >= 0 for i in rings[x].neighbours])
            if neighborCount == 6 or True:
                addWater(x)
                if (x % 3 == 0): addFish(x * 16, 2 + random.random() * 3)
            else:
                addTree(x)  
                #addRunner(x * 16, 30 + random.random() * 10, wf.hsv(random.random(),1,1)) 

    elif version == 2:
        for x in range(10):
            #addRunner(100, 30 + random.random() * 10, wf.hsv(random.random(),1,1)) 
            pass

        for x in range(36):
            #addTree(x)
            neighborCount = sum([i >= 0 for i in rings[x].neighbours])
            if neighborCount == 6:
                addWater(x)
                if (x % 2 == 0): addFish(x * 16, 2 + random.random() * 3)
            else:
                addTree(x)
                addRunner(x * 16, 30 + random.random() * 10, wf.hsv(random.random(),1,1)) 
    elif version == 3:
        for x in range(10):
            addRunner(100, 30 + random.random() * 10, wf.hsv(random.random(),1,0.3)) 
            pass

    elif version == 4:
        for x in range(10):
            #addRunner(100, 30 + random.random() * 10, wf.hsv(random.random(),1,1)) 
            pass

        for x in range(36):
            #addTree(x)
            neighborCount = sum([i >= 0 for i in rings[x].neighbours])
            if neighborCount == 6:
                addWater(x)
                if (x % 4 == 0): addFish(x * 16, 2 + random.random() * 3)
            else:
                addTree(x)
                addRunner(x * 16, 30 + random.random() * 10, wf.hsv(random.random(),1,1)) 

    elif version == 5:
        for x in range(36):
            addSnow(x * 16, 30 + random.random() * 10, wf.hsv(random.random(),0,random.random() * 0.5 + 0.5)) 

    dtPrevious = time.time()
    oldTouchData = [0 for i in range(0, 36)]


def init():
    wf.initialize(startGame, loop, keyboardHandler)
    wf.run()

init()
# help(noise)

#endregion