﻿from wobbleforest import wobbleforest as wf
# from wobbleforestunified import wobbleforestunified as wf
import time
import random
import math
import noise
import pygame
import os
import cProfile
from pygame.locals import *

# if run on a RPi uncomment this
# from gpiozero import Button 
# and comment this class: 
class Button:
    is_pressed = False
    def __init__(self, i, bounce_time):
        pass
    pass

buttons = [Button(2, bounce_time = 0.1), Button(3, bounce_time = 0.1), Button(4, bounce_time = 0.1), Button(17, bounce_time = 0.1), Button(27, bounce_time = 0.1), Button(22, bounce_time = 0.1)]
nextGame = -1
currentGame = 8 # start with this game!
interactiveThings = []
lastSpringWobble = [0 for x in range(36)]

#region ################################################### RUNNER ###############################################

runners = []
activeRunnerCount = 0
friends = []
fish = []
enemies = []
dt = 0
dtPrevious = 0

TYPE_OTHER = 0
TYPE_FRIEND = 1
TYPE_NEXTLEVEL = 2

class Runner:
    active = True
    distOnRing = 0
    limitToWater = False
    tailPos = []
    tailLength = 2
    previousPos = 0
    maxAge = 0
    repelledByTarget = False

    def __init__(self, i, speed, color, targetX = -100000, targetY = -1, jumpChance = 0.4, maxAge = 0, movementNoise = 0.1, active = True):
        self.i = i
        self.previousPos = i
        self.tailPos = [i for k in range(self.tailLength)]
        self.ringId = pixels[int(self.i)].ringId
        self.speed = speed
        self.color = color
        self.targetX = targetX
        self.targetY = targetY
        self.jumpChance = jumpChance
        self.movementNoise = movementNoise
        self.active = active
        self.attachedToPlayer = -1
        self.type = TYPE_OTHER
        self.maxAge = maxAge
        # TODO: cleanup runners once in a while

    def update(self):
        global dt, activeRunnerCount
        if not self.active: return

        if self.maxAge > 0 and time.time() > self.maxAge:
            self.active = False
            return
        
        activeRunnerCount += 1

        if self.limitToWater:
            advance = (self.speed * (1 + self.movementNoise*5))*dt
        else:
            advance = (self.speed * (1 + self.movementNoise))*dt
        myRing = pixels[int(self.i)].ringId
        self.ringId = myRing
        myAngle = rings[myRing].angle + (int(self.i) - rings[myRing].firstLEDindex) * 22.5
        myNewAngle = rings[myRing].angle + (int(self.i + advance) - rings[myRing].firstLEDindex) * 22.5
        self.i += advance
        # print "updating runner, myring:",myRing,"myAngle", myAngle,"mypos", self.i
        while self.i - rings[myRing].firstLEDindex >= 16: self.i -=16
        while self.i - rings[myRing].firstLEDindex < 0: self.i +=16
        self.movementNoise *= (1 - dt/5)
        if self.movementNoise < 0.1 and self.attachedToPlayer <= -1 and currentGame != 5:
            self.targetX = -100000
            #print "resetting target"

        self.distOnRing += abs(advance)
        if (int(myAngle - 30)/60 != int(myNewAngle - 30)/60 and self.distOnRing > 3):
            #jump?
            changeAngle = (max(int(myAngle - 30)/60, int(myNewAngle - 30)/60) * 60 + 30) % 360

            jumpChance = self.jumpChance
            if self.targetX != -100000:
                targetAngle = (90 + math.degrees(math.atan2(self.targetY - pixels[int(self.i)].y, self.targetX - pixels[int(self.i)].x))) % 360
                if self.repelledByTarget:
                    targetAngle = (targetAngle + 180) % 360
                # print "Runner wants to jump, his angles are", myAngle, myNewAngle
                # print "switchangle:", changeAngle, "target:", targetAngle
                if abs(targetAngle - changeAngle) < 60 and self.movementNoise < random.random():
                    jumpChance *= 3
                else:
                    jumpChance *= 0.1

            if random.random() < jumpChance:
                neighborId = (changeAngle/60) % 6
                # print "Runner jumps, his angles are", myAngle, myNewAngle, neighborId

                newRing = rings[myRing].neighbours[neighborId]
                
                if self.type == TYPE_FRIEND:
                    if isLava(newRing): # don't jump into lava!
                        return
                    if self.attachedToPlayer >= 0 and (not isPlayer(newRing) and isPlayer(self.ringId)): # don't jump away from player
                        return

                if newRing == -1 or (self.limitToWater and (rings[newRing].cell == None or rings[newRing].cell.cellType != waterType)):
                    return
                self.distOnRing = 0
                newAngle = (myNewAngle + 180) % 360
                newAngleExact = (neighborId * 60 + 30 + 180) % 360
                realNewAngle = 2*newAngleExact - newAngle # reversing direction
                self.speed *= -1
                newRingAngle = rings[newRing].angle
                newIndex = (realNewAngle - newRingAngle) / 22.5
                self.i = rings[newRing].firstLEDindex + newIndex
                self.ringId = newRing
                while self.i - rings[newRing].firstLEDindex >= 16: self.i -=16
                while self.i - rings[newRing].firstLEDindex < 0: self.i +=16        

    def draw(self):
        if not self.active: return
        #wf.addPixel(int(self.i), self.color)
        if int(self.i) != self.previousPos:
            self.tailPos = [self.previousPos] + self.tailPos[0:(self.tailLength - 1)]
            #print "new tail:", self.tailPos
            self.previousPos = int(self.i)

        if self.type == TYPE_NEXTLEVEL:
            #self.color = wf.hsv(time.time() - int(time.time()), 1, 0.5)
            self.color = wf.hsv256(int((time.time() - int(time.time())) * 255), 255, 128)

        wf.addRingFloatPixel(self.i, self.color)

        #draw tail
        for i, p in enumerate(self.tailPos):
            wf.addPixel(p, wf.dimColor(self.color, 0.2 - i * (0.2/self.tailLength)))

    def checkCollision(self):
        # check if runner collides with any player
        for hw in handwalkers:
            if hw.alive:
                for foot in hw.currentLegs:
                    if foot == self.ringId:
                        hw.hitEnemy()

    def updateFriend(self):
        global hw_caughtFriendCount, TYPE_NEXTLEVEL
        # set target
        if self.attachedToPlayer >= 0:
            if not handwalkers[self.attachedToPlayer].alive:
                self.attachedToPlayer = -2 # float away
                self.targetX = -100000
                self.movementNoise = 0.2
                self.jumpChance = 1
            
            else: 
                # move to random foot
                playerLegs = handwalkers[self.attachedToPlayer].currentLegs
                if len(playerLegs) > 0:
                    rndLeg = playerLegs[random.randint(0, len(playerLegs) - 1)]
                    self.targetX = rings[rndLeg].x
                    self.targetY = rings[rndLeg].y
                    self.movementNoise = 0
                    self.jumpChance = 1
                else:
                    print "Player legs 0, that shouldn't happen!"
                self.targetX
        else:
            #check if a player is here
            for index, hw in enumerate(handwalkers):
                if hw.alive:
                    for foot in hw.currentLegs:
                        if foot == self.ringId:
                            self.attachedToPlayer = index
                            self.movementNoise = 0
                            self.jumpChance = 1
                            hw_caughtFriendCount += 1
                            if self.type == TYPE_FRIEND:
                                for x in range(3):
                                    for i in range(8):
                                        addRunner(wf.rings[self.ringId].firstLEDindex + i, 70, (0, 255, 0), maxAge = time.time() + 0.5 * random.random())
                                
                                # s = pygame.mixer.Sound(file='audio/14/' + str(self.ringId) + '.wav')
                                # s.play()
                                wf.playSound('audio/14/' + str(self.ringId) + '.wav')
                                print "player caught friend!"
                            self.active = False 

        self.update()


def addRunner(i, speed, color, targetX = -100000, targetY = -1, jumpChance = 0.6, maxAge = 0):
    for r in runners:
        if not r.active:
            r.i = i
            r.previousPos = i
            r.ringId = pixels[int(r.i)].ringId
            r.tailPos = [i for k in range(r.tailLength)]
            r.speed = speed
            r.color = color
            r.targetX = targetX
            r.targetY = targetY
            r.jumpChance = jumpChance
            r.movementNoise = 0.1
            r.active = True
            r.attachedToPlayer = -1
            r.type = TYPE_OTHER
            r.maxAge = maxAge
            return

    runners.append(Runner(i, speed, color, targetX, targetY, jumpChance, maxAge))

def addEnemy(i, speed):
    enemies.append(Runner(i, speed, (255, 0, 0)))

def addFish(i, speed):
    f = Runner(i, speed, (255, 55, 20 + random.random() * 5))
    f.limitToWater = True
    f.tailLength = 5
    print "adding fish:", f, "limitToWater:", f.limitToWater
    fish.append(f)

def addNextLevelGate(ringId):
    rings = wf.getRings()
        
    f = Runner(wf.getRandomLEDonRing(ringId), 30 + random.random() * 5, (0, 55, 0), rings[ringId].x, rings[ringId].y)
    f.jumpChance = 0
    f.movementNoise = 0
    f.attachedToPlayer = -1
    f.type = TYPE_NEXTLEVEL

    f.tailLength = 5
    print "adding nextLevelGate:", f
    friends.append(f)

def addFriend(ringId):
    rings = wf.getRings()
    f = Runner(wf.getRandomLEDonRing(ringId), 30 + random.random() * 5, (0, 55, 0), rings[ringId].x, rings[ringId].y)
    f.jumpChance = 0
    f.movementNoise = 0
    f.attachedToPlayer = -1
    f.type = TYPE_FRIEND

    f.tailLength = 5
    print "adding friend:", f
    friends.append(f)

def addRandomFriend():
    global hw_nextLevelGatePos
    # add friend not in lava and not on player pos

    pos = random.randint(0, 35)
    while isLava(pos) or isPlayer(pos) or hw_nextLevelGatePos == pos:
        pos = random.randint(0, 35)
    addFriend(pos)

def allFriendsCaught():
    for f in friends:
        if f.attachedToPlayer < 0 and f.active:
            return False
    return True

def updateRunners():
    global activeRunnerCount
    activeRunnerCount = 0
    for r in runners: 
        r.update()

def updateEnemies():
    for e in enemies: 
        e.update()
        e.checkCollision()

def updateFish():
    for f in fish: 
        f.update()

def updateFriends():
    for f in friends: 
        f.updateFriend()

def drawFriends():
    for f in friends:
        f.draw()

def drawEnemies():
    for e in enemies:
        e.draw()

def drawRunners():
    for r in runners:
        r.draw()

def drawFish():
    for f in fish:
        f.draw()

#endregion

#region ################################################### Cell ###############################################

trees = []
water = []
treeType = 1
waterType = 2

class Cell:
    baseColor = [0, 20, 0]
    currentColor = [0,0,0]

    def __init__(self, ringId, cellType, baseColor):
        self.ringId = ringId
        self.cellType = cellType
        self.firstLEDindex = wf.rings[ringId].firstLEDindex
        if baseColor is None:
            self.baseColor = [random.random() * 2, random.random() * 5 + 10, random.random() * 5]
        else:
            self.baseColor = baseColor
        rings[ringId].cell = self

    def update(self):
        pass

    def draw(self):
        global gameTime, tick
        for i in range(16):
            #col[1] += math.sin(time.time() + i) * (15.0 + i/8.0)
            #print "Cell col", col
            if self.cellType == treeType:
                self.currentColor = self.baseColor[:]
                self.currentColor[1] *= 0.3+noise.pnoise2(pixels[self.firstLEDindex + i].x , pixels[self.firstLEDindex + i].y + (time.time()-gameTime)*0.3)
            elif self.cellType == waterType:
                # col[2] *= 0.3+noise.pnoise2(pixels[self.firstLEDindex + i].x/50 + (time.time()-gameTime)*2.2,pixels[self.firstLEDindex + i].y/50 + (time.time()-gameTime)*2.3)
                #col[2] *= 1+noise.pnoise2(pixels[self.firstLEDindex + i].x/150 + (time.time()-gameTime)*0.2,pixels[self.firstLEDindex + i].y/150 + (time.time()-gameTime)*0.5)*2 +noise.pnoise2(pixels[self.firstLEDindex + i].x/170 - (time.time()-gameTime)*0.3,pixels[self.firstLEDindex + i].y/160 - (time.time()-gameTime)*0.4)
                # n = 2+noise.pnoise2(pixels[self.firstLEDindex + i].x/150 + (time.time()-gameTime)*0.2,pixels[self.firstLEDindex + i].y/150 + (time.time()-gameTime)*0.5)*2 +noise.pnoise2(pixels[self.firstLEDindex + i].x/170 - (time.time()-gameTime)*0.3,pixels[self.firstLEDindex + i].y/160 - (time.time()-gameTime)*0.4)
                #if ((tick + i) % 3 == 0):
                n = 2+noise.pnoise2(pixels[self.firstLEDindex + i].x/150 + (time.time()-gameTime)*0.2,pixels[self.firstLEDindex + i].y/150 + (time.time()-gameTime)*0.5)*2
                # col[2] = max(2, col[2])
                #self.currentColor = wf.hsv(0.5 + n/10, n / 2, 0.01)
                self.currentColor = wf.hsv256(int(108 + n*20), 255, 35)

                #print col
            wf.setPixel(self.firstLEDindex + i, self.currentColor)


def addTree(ringId):
    trees.append(Cell(ringId, treeType, [random.random() * 2, random.random() * 5 + 20, random.random() * 2]))

def addWater(ringId):
    water.append(Cell(ringId, waterType, [0, 0, 15]))


def drawTrees():
    for t in trees:
        t.draw()

def drawWater():
    for w in water:
        w.draw()

#endregion

#region ################################################### Lava ###############################################

lava = []

class Lava:
    MODE_STATIC = 0
    MODE_LINEAR = 1
    MODE_PATH = 2

    active = True
    hexX = 0
    hexY = 0
    baseColor = [20, 20, 0]
    currentColor = [0,0,0]
    deadly = True
    cellType = 4
    mode = MODE_STATIC
    linearDirection = wf.ringTopRight
    pathStepTimer = 2
    nextPathIndex = 0
    nextnextPathIndex = 1
    nextPathRingId = 0
    nextnextPathRingId = 0
    pathStepTime = 1
    firstLEDindex = 0
    path = []

    def __init__(self, ringId, mode = 0):
        self.ringId = ringId
        self.firstLEDindex = wf.rings[ringId].firstLEDindex
        rings[ringId].cell = self
        self.pathStepTimer = self.pathStepTime + time.time()
        self.mode = mode
        self.active = True

    def __init__(self, hexX, hexY, mode):
        self.hexX = hexX
        self.hexY = hexY
        self.ringId = wf.getClosestSpringHex(hexX, hexY)
        if self.ringId > -1:
            self.firstLEDindex = wf.rings[self.ringId].firstLEDindex
            rings[self.ringId].cell = self
        self.pathStepTimer = self.pathStepTime + time.time()
        self.mode = mode
        self.active = True

    def setPosition(self, newRingRelative):
        newPosX = self.hexX + wf.neighboursHexX[newRingRelative]
        newPosY = self.hexY + wf.neighboursHexY[newRingRelative]
        newRing = wf.getClosestSpringHex(newPosX, newPosY)

        #print "Hex old", self.hexX, self.hexY, "new:", newPosX, newPosY

        if self.ringId > -1:
            rings[self.ringId].cell = None
            #newRing = wf.rings[self.ringId].neighbours[newRingRelative]
        
        #print "lava moving from", self.ringId, "to", newRing, "(rel pos:", newRingRelative,")"
        self.ringId = newRing
        self.hexX = newPosX
        self.hexY = newPosY

        if newRing == -1:
            print "Lava left screen"
        else:
            self.firstLEDindex = wf.rings[newRing].firstLEDindex
            rings[newRing].cell = self
            
    def getNextPathRingId(self):
        nextX = self.hexX + wf.neighboursHexX[self.path[self.nextPathIndex]]
        nextY = self.hexY + wf.neighboursHexY[self.path[self.nextPathIndex]]
        return wf.getClosestSpringHex(nextX, nextY)
        #next = wf.rings[self.ringId].neighbours[self.path[self.nextPathIndex]]

    def getNextNextPathRingId(self):
        nextX = self.hexX + wf.neighboursHexX[self.path[self.nextPathIndex]] + wf.neighboursHexX[self.path[self.nextnextPathIndex]]
        nextY = self.hexY + wf.neighboursHexY[self.path[self.nextPathIndex]] + wf.neighboursHexY[self.path[self.nextnextPathIndex]]
        return wf.getClosestSpringHex(nextX, nextY)
        #next = wf.rings[self.ringId].neighbours[self.path[self.nextPathIndex]]

    def update(self):
        if not self.active: return
        # movement code
        if self.mode == self.MODE_PATH and len(self.path) > 0:
            if time.time() > self.pathStepTimer:
                # next step in path
                self.pathStepTimer = self.pathStepTime + time.time()
                self.setPosition(self.path[self.nextPathIndex])
                self.nextPathIndex += 1
                if self.nextPathIndex == len(self.path):
                    self.nextPathIndex = 0
                self.nextnextPathIndex += 1
                if self.nextnextPathIndex == len(self.path):
                    self.nextnextPathIndex = 0

                self.nextPathRingId = self.getNextPathRingId()
                self.nextnextPathRingId = self.getNextNextPathRingId()

        
        # kill player!
        if self.deadly and self.ringId > -1:
            for hw in handwalkers:
                if hw.alive:
                    for foot in hw.currentLegs:
                        if foot == self.ringId:
                            hw.die(self.ringId)

    def drawWarn(self):
        global gameTime, tick
        if not self.active or self.nextPathRingId == -1: return

        if self.mode == self.MODE_PATH:
            # warn next position
            ledIndex = wf.rings[self.nextPathRingId].firstLEDindex
            for i in range(16):
                n = 2+noise.pnoise2(pixels[self.firstLEDindex + i].x/80 + (time.time()-gameTime)*5, pixels[self.firstLEDindex + i].y/80 + (time.time()-gameTime)*2)*2
                col = wf.hsv((n/15 + 0.87) % 1.0, 1, 0.1 * (1 + math.sin(time.time() * 30)))
                if math.sin(time.time() * 30) > 0:
                    wf.setPixel(ledIndex + i, col)
        
        if self.nextnextPathIndex == -1: return
        if self.mode == self.MODE_PATH:
            # warn nextnext position
            ledIndex = wf.rings[self.nextnextPathRingId].firstLEDindex
            for i in range(16):
                n = 2+noise.pnoise2(pixels[self.firstLEDindex + i].x/80 + (time.time()-gameTime)*5, pixels[self.firstLEDindex + i].y/80 + (time.time()-gameTime)*2)*2
                col = wf.hsv((n/15 + 0.87) % 1.0, 1, 0.1 * (1 + math.sin(time.time() * 30)))
                if  math.sin(time.time() * 30) > 0:
                    wf.setPixel(ledIndex + i, col)

    def draw(self):
        global gameTime, tick
        
        if not self.active or self.ringId == -1: return

        for i in range(16):
            n = 2+noise.pnoise2(pixels[self.firstLEDindex + i].x/80 + (time.time()-gameTime)*5, pixels[self.firstLEDindex + i].y/80 + (time.time()-gameTime)*2)*2
            self.currentColor = wf.hsv((n/15 + 0.87) % 1.0, 1, 0.9)
            wf.setPixel(self.firstLEDindex + i, self.currentColor)
        
def addLavaHex(hexX, hexY, relativePath = None, timePerTile = 1):
    if relativePath == None:
        lava.append(Lava(hexX, hexY, Lava.MODE_STATIC))
    else:
        newLava = Lava(hexX, hexY, Lava.MODE_PATH)
        newLava.path = relativePath
        newLava.pathStepTime = timePerTile
        newLava.pathStepTimer = timePerTile
        lava.append(newLava)


def addLava(ringId, relativePath = None, timePerTile = 1):
    hexX = rings[ringId].hexX
    hexY = rings[ringId].hexY
    addLavaHex(hexX, hexY, relativePath, timePerTile)
        
def removeAllLava():
    global lava
    for l in lava:
        l.active = False
        wf.rings[l.ringId].cell = None
        l = None
    lava = []

def isLava(ringId):
    for l in lava:
        if l.ringId == ringId and l.deadly:
            return True
    return False

def drawLava():
    for l in lava:
        l.drawWarn()
    for l in lava:
        l.draw()

def updateLava():
    for l in lava:
        l.update()

#endregion

#region ################################################### Liquid ###############################################

liquid = []
liquidType = 5
expandCooldown = 5 # seconds

class Liquid:
    hue = 0.0
    ownerId = -1
    charge = 10
    cellType = liquidType
    generatePerSec = 10
    lastExpand = 0

    def __init__(self, ringId, ownerId, hue = -1):
        self.ringId = ringId
        self.firstLEDindex = wf.rings[ringId].firstLEDindex
        rings[ringId].cell = self
        self.ownerId = ownerId
        self.hue = hue
        if hue == -1: 
            self.hue = self.ownerId / 3.0
        print "liquid owner:", self.ownerId, " hue:", self.hue

    def generateCharge(self):
        global dt
        self.charge += self.generatePerSec * dt

    def distributeCharge(self):
        if self.ownerId < 0 or self.charge < 6:
            return

        touchData = wf.getTouchData()
        # are we tapped? then don't send anything
        if touchData[self.ringId] > 0:
            pass
        else:
            #send charge to friendly neighbours with less charge
            neigh = rings[self.ringId].neighbours[:]
            random.shuffle(neigh)
            for n in neigh:
                if rings[n].cell != None and rings[n].cell.cellType == liquidType:
                    otherLiquidCell = rings[n].cell
                    if otherLiquidCell.ownerId == self.ownerId:
                        chargeDifference = self.charge - otherLiquidCell.charge
                        if chargeDifference > 6:
                            otherLiquidCell.receiveCharge(chargeDifference / 6, self.ownerId)

                        # if other cell is tapped, try to send more!
                        if touchData[n] > 0 and self.charge > otherLiquidCell.charge / 2:
                            toSend = self.charge / 2
                            # otherLiquidCell.receiveCharge(, self.ownerId)

    def changeOwner(self, newOwner):
        self.ownerId = newOwner
        self.hue = self.ownerId / 3.0


    def update(self):
        self.generateCharge()
        self.distributeCharge()


    def draw(self):
        global gameTime, tick
        brightness = 0.1
        noiseSpeed = 1.0
        if time.time() - self.lastExpand < expandCooldown:
            brightness = 0.01
            noiseSpeed = 0.1
        for i in range(16):
            n = 2+noise.pnoise2(pixels[self.firstLEDindex + i].x/50 + (time.time()-gameTime)*5*noiseSpeed, pixels[self.firstLEDindex + i].y/50 + (time.time()-gameTime)*2*noiseSpeed)*2
            self.currentColor = wf.hsv(n/10 + self.hue - 0.2, 1, brightness)
            wf.setPixel(self.firstLEDindex + i, self.currentColor)

    def receiveCharge(self, sentCharge, otherOwnerId):
        if otherOwnerId == self.ownerId:
            self.charge += sentCharge
            return

        result = self.charge - sentCharge 
        self.charge = abs(result)
        if result > 0:
            # we survive!
            pass
        else:
            print "takeover!"
            # takeOver!
            self.changeOwner(otherOwnerId)

    def expand(self):
        if time.time() - self.lastExpand < expandCooldown:
            print "Liquid at", self.ringId, "is still cooling down"
            return
        print "Liquid at", self.ringId, "is expanding"
        self.lastExpand = time.time()
        neigh = rings[self.ringId].neighbours
        sentCharge = (self.charge - 6) / len(neigh)
        for n in neigh:
            if rings[n].cell == None or rings[n].cell.cellType != liquidType:
                addLiquid(n, self.ownerId)
                rings[n].cell.lastExpand = time.time()
            otherLiquidCell = rings[n].cell
            otherLiquidCell.receiveCharge(sentCharge, self.ownerId)
            self.charge -= sentCharge



def liquidWobble(ringId):
    l = getLiquid(ringId)
    print "liquid wobble! liquid here:", l
    if l == None or l.ownerId == -1: 
        return
    if l.charge > 5:
        l.expand()

def getLiquid(ringId):
    for l in liquid:
        if l.ringId == ringId:
            return l
    return None

def getTeam(ringId):
    for l in liquid:
        if l.ringId == ringId:
            return l.ownerId
    return -1

def addLiquid(ringId, ownerId, hue = -1):
    liquid.append(Liquid(ringId, ownerId, hue))

def drawLiquid():
    for l in liquid:
        l.draw()

def updateLiquid():
    for l in liquid:
        l.update()

#endregion

#region ################################################### Handwalker ###############################################
handwalkers = []

class Handwalker:
    alive = True
    trail = []
    maxStepSpeed = 0.5
    currentLegs = []
    legsTouching = []
    maxLegs = 2
    stepCount = 0
    maxJumpStep = 10 # seconds that we can still step without another leg down

    def __init__(self, initialRingId):
        self.currentLegs.append(initialRingId)

        self.color = wf.hsv(1, 0.6, 0.05)
        self.trailcolor = wf.hsv(1, 0.6, 0.01)
        self.lastStepTime = -1

    def draw(self):
        global hw_state, HW_NEXTLEVEL
        touchData = wf.getTouchData()
        rings = wf.getRings()
        for leg in self.currentLegs:
            if touchData[leg] > 0:
                wf.addRing(leg, (55,55,55))
                if len(self.legsTouching) == 1:
                    for i, n in enumerate(rings[leg].neighbours):
                        if n >= 0:
                            light = 2 * (1 + math.sin(time.time() * 10 + i))
                            wf.addRing(n, (light, light, light))
                
                # wf.addRing(leg, wf.hsv(0, 0, 0.05 * (1 + math.sin(time.time() * 10 + leg))))
        if len(self.legsTouching) == 0 or hw_state == HW_NEXTLEVEL:
            # no touches, light up most recent leg
            light = 2 * (1 + math.sin(time.time() * 10 + leg))
            if len(self.currentLegs) == 1:
                wf.addRing(self.currentLegs[0], (light, light, light))

            elif len(self.currentLegs) == 2:
                if touchData[self.currentLegs[0]] < touchData[self.currentLegs[1]]:
                    wf.addRing(self.currentLegs[0], (light, light, light))
                else:
                    wf.addRing(self.currentLegs[0], (light, light, light))
            elif len(self.currentLegs) > 0: 
                wf.addRing(self.currentLegs[0], (light, light, light))
            else:
                print "no currentleg data!"                




    def doStep(self, ringId, legsTouching, mostRecentLeg, mostRecentLegTime):
        legs = legsTouching[:]
        if mostRecentLegTime < self.maxJumpStep:
            legs.append(mostRecentLeg)
        if len(legs) > 0:
            # needs to land near touched ring
            for l in legs:
                if ringId in rings[l].neighbours and not ringId in self.currentLegs:
                    print "Stepping to", ringId
                    legsTouching.append(ringId)
                    self.currentLegs = legsTouching
                    self.lastStepTime = time.time()
                    # s = pygame.mixer.Sound(file='audio/9/' + str(ringId) + '.wav')
                    wf.playSound('audio/oi16/' + str(ringId) + '.wav')
                    # s = pygame.mixer.Sound(file='audio/oi16/' + str(ringId) + '.wav')
                    # s.play()
                    self.stepCount += 1
                    # pygame.mixer.Sound(file=os.path.join('5','Break3.wav')) #' + str(ringId) + '
                    return True

        return False

    def die(self, deathRingId, playSound = True):
        global hw_state, HW_DEAD, hw_nextStateTimer, hw_lastTouchTime
        print "player dead!"
        rings = wf.getRings()
        self.alive = False
        hw_state = HW_DEAD
        hw_nextStateTimer = time.time() + 3
        hw_lastTouchTime = time.time() 
        # s = pygame.mixer.Sound(file='audio/loss.wav')
        # s.play()
        if playSound:
            wf.playSound('audio/loss.wav')

        addEffect(rings[deathRingId].x, rings[deathRingId].y, speed = 650, color = (255,255,255), maxRadius = 530, bandSize = 120)
        addEffect(rings[deathRingId].x, rings[deathRingId].y, speed = 550, color = wf.hsv(0, 1, 1), maxRadius = 530, bandSize = 120)

        for x in range(3):
            for i in range(16):
                addRunner(wf.rings[deathRingId].firstLEDindex + i, 40, wf.hsv(random.random()* 0.1, 1, 1), maxAge = time.time() + 0 + random.random() * 3)
            pass
        #spawn particles

    def hitEnemy(self):
        print "player hit enemy!"


    def update(self):
        global hw_state, HW_RUNNING, HW_NEARGOAL
        if not (hw_state == HW_RUNNING or hw_state == HW_NEARGOAL): return
        touchData = wf.getTouchData()
        previouslyTouching = len(self.legsTouching)
        self.legsTouching = []
        mostRecentLegTime = self.maxJumpStep
        mostRecentLeg = -1
        for cr in self.currentLegs:
            if touchData[cr] > 0:
                if previouslyTouching == 0:
                    #s = pygame.mixer.Sound(file='audio/9/' + str(cr) + '.wav')
                    # s = pygame.mixer.Sound(file='audio/oi16/' + str(cr) + '.wav')
                    # s.play()
                    wf.playSound('audio/oi16/' + str(cr) + '.wav')
                self.legsTouching.append(cr)
            else:
                if time.time() + touchData[cr] < mostRecentLegTime:
                    mostRecentLegTime = time.time() + touchData[cr]
                    mostRecentLeg = cr
                    #mostRecentLegTime = min(mostRecentLegTime, time.time() + touchData[cr])

        if len(self.legsTouching) < self.maxLegs:
            # room for some steppin'

            # check if any leg still recently touched
            if mostRecentLegTime < self.maxJumpStep:
                for index, tt in enumerate(touchData):
                    if tt > 0:
                        didStep = self.doStep(index, self.legsTouching, mostRecentLeg, mostRecentLegTime)
                        if didStep: 
                            break


def addWalker(ringId):
    handwalkers.append(Handwalker(ringId))

def drawHandWalkers():
    for hw in handwalkers:
        if hw.alive: hw.draw()

def updateHandWalkers():
    handWalkerManagerUpdate()
    if len(handwalkers) > 0:
        for hw in handwalkers:
            if hw.alive: hw.update()

def isPlayer(ringId):
    for hw in handwalkers:
        if hw.alive:
            for foot in hw.currentLegs:
                if foot == ringId:
                    return True

    return False

#endregion

#region ############################################## Handwalker Manager ###############################################

hw_currentLevel = 3
hw_nextLevelGatePos = 0
hw_previousLevelGatePos = 0
hw_initLevel = False
hw_caughtFriendCount = 0
HW_STARTING = 0
HW_RUNNING = 1
HW_NEARGOAL = 2
HW_NEXTLEVEL = 4
HW_DEAD = 3
hw_nextStateTimer = 0
hw_state = HW_RUNNING
hw_nextLevelEffect = 0
hw_lastTouchTime = 0

def handWalkerStartGame():
    global hw_currentLevel, hw_caughtFriendCount, hw_initLevel, hw_lastTouchTime
    hw_currentLevel = 0
    hw_caughtFriendCount = 0 
    hw_initLevel = False
    addWalker(1)
    hw_state = HW_RUNNING
    hw_lastTouchTime = time.time()

def handWalkerNextLevel():
    global hw_currentLevel, hw_initLevel
    hw_currentLevel += 1
    print "Starting level", hw_currentLevel
    hw_initLevel = False

def handWalkerWobble(index):
    rings = wf.getRings()
    addEffect(rings[index].x, rings[index].y, speed = 1350, color = wf.hsv(index/14.0 % 1.0, 1, 0.05), maxRadius = 30, bandSize = 120)

def clearLevel():
    global friends, handwalkers
    removeAllLava()

    friends = []        

def handWalkerManagerUpdate():
    global hw_initLevel, hw_currentLevel, hw_caughtFriendCount, hw_previousLevelGatePos, hw_state, HW_DEAD, HW_RUNNING, HW_STARTING, HW_STARTING, hw_nextStateTimer, hw_nextLevelGatePos, hw_nextLevelEffect,hw_lastTouchTime
    if hw_state == HW_DEAD or hw_state == HW_NEXTLEVEL:
        if time.time() > hw_nextStateTimer:
            #reset
            print "level reset timer done! resetting level!"
            if hw_state == HW_DEAD:
                hw_nextLevelGatePos = hw_previousLevelGatePos
            handwalkers[0].currentLegs = [hw_nextLevelGatePos]
            handwalkers[0].alive = True
            hw_initLevel = False
            clearLevel()
            if hw_state == HW_NEXTLEVEL:
                handWalkerNextLevel()

            hw_state = HW_RUNNING

    if hw_state == HW_NEXTLEVEL:
        # show victory animation
        if time.time() > hw_nextLevelEffect:
            hw_nextLevelEffect = time.time() + 0.1
            col = wf.hsv((time.time() + 0.2 * math.sin(time.time()*3)) % 1.0, 1, 0.5)
            addEffect(rings[hw_nextLevelGatePos].x, rings[hw_nextLevelGatePos].y, speed = 600, color = col, maxRadius = 350, bandSize = 140)
            # for x in range(1):
            #     for i in range(16):
            #         addRunner(wf.rings[hw_nextLevelGatePos].firstLEDindex + i, 70, col, maxAge = time.time() + 0 + random.random() * 3)

    if hw_state == HW_DEAD:
        for x in range(16*32):
            wf.setPixel(x, (5,0,0))

    if time.time() > hw_lastTouchTime + 15:
        handwalkers[0].die(handwalkers[0].currentLegs[0], False)
        handwalkers[0].stepCount = 0
        hw_nextLevelGatePos = 0
        hw_currentLevel = 0
        hw_initLevel = False

    if not hw_initLevel:
        hw_initLevel = True
        print "Starting HW Level", hw_currentLevel
        #initialize level
        hw_previousLevelGatePos = hw_nextLevelGatePos
        if hw_currentLevel == 0:
            pass
        if hw_currentLevel == 1:
            # hw_nextLevelGatePos = 6
            # addRandomFriend()
            hw_currentLevel = 2
        if hw_currentLevel == 2:
            addFriend(33)
            addFriend(26)
            addFriend(30)
            hw_nextLevelGatePos = 15
        if hw_currentLevel == 3:
            addLava(13)
            addLava(14)
            addLava(19)
            addLava(20)
            addLava(32)
            
            addFriend(6)
            addFriend(7)
            addFriend(9)
            hw_nextLevelGatePos = 11

        if hw_currentLevel == 4:
            # removeAllLava()
            # addLava(19, [0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5])
            addLava(21, [1, 1, 1, 2, 3, 3, 3, 4, 5, 5, 5, 0], 0.6)
            addLava(10, [1, 1, 2, 3, 3, 3, 4, 5, 5, 5, 0, 1], 0.6)
            # addLava(14, [0, 1, 2, 3, 4, 5])
            hw_nextLevelGatePos = 0 #32
            for x in xrange(1,5):
                addRandomFriend()


        if hw_currentLevel == 5:
            addLavaHex(7, -2,  [2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 1, 1, 1, 1], 0.8)
            addLavaHex(6, -2,  [2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 1, 1, 1, 1], 0.8)
            addLavaHex(5, -2,  [2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 1, 1, 1, 1], 0.8)
            addLavaHex(4, -2,  [2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 1, 1, 1, 1], 0.8)
            addLavaHex(3, -2,  [2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 1, 1, 1, 1], 0.8)
            addLavaHex(0, -2,  [2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 1, 1, 1, 1], 0.8)
            addLavaHex(-1, -2, [2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 1, 1, 1, 1], 0.8)
            addLavaHex(-2, -2, [2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 1, 1, 1, 1], 0.8)
            addLavaHex(-3, -2, [2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 1, 1, 1, 1], 0.8)
            # addLava(14, [0, 1, 2, 3, 4, 5])
            hw_nextLevelGatePos = 32
            for x in xrange(1,5):
                addRandomFriend()

        if hw_currentLevel == 6:
            pass


    else:

        if hw_state == HW_RUNNING:
            if (hw_currentLevel == 0 and handwalkers[0].stepCount > 4) or (hw_currentLevel > 0 and allFriendsCaught()):
                addNextLevelGate(hw_nextLevelGatePos)
                hw_state = HW_NEARGOAL

        if hw_state == HW_NEARGOAL and allFriendsCaught():
            hw_state = HW_NEXTLEVEL         
            hw_nextStateTimer = time.time() + 2
            print "Triggered next level"   
            # s = pygame.mixer.Sound(file='audio/win/' + str(random.randint(1, 6)) + '.wav')
            # s.play()
            wf.playSound('audio/win/' + str(random.randint(1, 6)) + '.wav')
            if hw_currentLevel == 7:
                switchGame(5)

#endregion

#region ################################################### Effects ###############################################
effects = []

class Effect:
    alive = True
    limitToWater = False

    def __init__(self, x, y, speed, color, maxRadius, bandSize = -1, startTime = -1):
        t = time.time()
        if startTime > 0: t = startTime
        self.x = x
        self.y = y
        self.startTime = t
        self.speed = speed
        self.color = color
        self.maxRadius = maxRadius
        self.bandSize = bandSize
        self.active = True
        # TODO: cleanup runners once in a while

    def draw(self):
        if not self.active: return
        currentR = (time.time() - self.startTime) * self.speed
        if currentR > self.maxRadius + self.bandSize:
            self.active = False
            return
        currentSqR = currentR * currentR
        maxR = self.maxRadius * self.maxRadius
        sqBand = self.bandSize * self.bandSize
        skipped = 0
        i = 0
        while i < len(pixels):
            p = pixels[i]
            sqDist = wf.getSqDist(self.x, self.y, p.x, p.y)
            if i % 16 == 0 and (sqDist > currentSqR + 1450*10 or sqDist < currentSqR - sqBand - 1450*2):
                #skip the entire ring
                skipped+=1
                i+=16
                continue
            if sqDist < min(maxR, currentSqR) and (self.bandSize == -1 or sqDist > currentSqR - sqBand) :
                #wf.addPixel(i, wf.dimColor(self.color, 1.1 - min(1, (currentSqR - sqDist)/20000)))
                wf.setPixel(i, wf.dimColor(self.color, 1.1 - min(1, (currentSqR - sqDist)/20000)))
            i+=1
        # print "skipped rings:", skipped


def addEffect(x, y, speed, color, maxRadius, bandSize = -1, startTime = -1):
    t = time.time()
    if startTime > 0: t = startTime

    for e in effects:
        if not e.active:
            e.x = x
            e.y = y
            e.speed = speed
            e.color = color
            e.maxRadius = maxRadius
            e.bandSize = bandSize
            e.startTime = t
            e.active = True
            return
    effects.append(Effect(x, y, speed, color, maxRadius, bandSize, t))

def drawEffects():
    for e in effects:
        e.draw()

#endregion

#region ################################################### Ball ###############################################

balls = []

class Ball:
    def __init__(self, x, y, dx, dy, radius, drag, color, index):
        self.x = x
        self.y = y
        self.dx = dx
        self.dy = dy
        self.radius = radius
        self.drag = drag
        self.color = color
        self.index = index

    def hitWalls(self, newX, newY):
        lines = wf.lines
        for index, l in enumerate(lines):
            d1 = l.dist(self.x, self.y)
            d2 = l.dist(newX, newY)
            if (d1 < 0) != (d2 < 0): # opposite signs?
                self.dx, self.dy = l.reflectVector(self.dx, self.dy)
                if index == 0 or index == 4 or index == 5:
                    self.goalHit(index)
                return True
        return False

    def goalHit(self, line):
        print "hit line", line
        if line == 0:
            col = 0.3
            if (time.time() - hw_lastTouchTime < 15):
                wf.playSound('audio/oiii16/22.wav')
                
        if line == 4:
            col = 0.6
            if (time.time() - hw_lastTouchTime < 15):
                wf.playSound('audio/oiii16/19.wav')
        if line == 5:
            col = 1
            if (time.time() - hw_lastTouchTime < 15):
                wf.playSound('audio/oiii16/35.wav')

        addEffect(self.x, self.y, speed = 350, color = wf.hsv(col, 1, 0.4), maxRadius = 100, bandSize = 170)



    def update(self):
        global dt, gameTime, balls
        newX = self.x + self.dx * dt
        newY = self.y + self.dy * dt
        if not self.hitWalls(newX, newY):
            # don't move if hit walls so we can check for more lines next tick
            self.x = newX
            self.y = newY

        # add and remove balls
        ballTimer = int((time.time() - gameTime) / 30) % 6
        #print "ball timer:", ballTimer
        if ballTimer == len(balls) and len(balls) < 3:
            addBall(50, 50, 100, 100, radius = 25, drag = 0, color = random.random())
        elif 7 - ballTimer == len(balls):
            balls = balls[1:]

            

    def addSpringForce(self, springIndex, amount):
        dist = math.sqrt(wf.getSqDist(self.x, self.y, rings[springIndex].x, rings[springIndex].y))
        if (dist < 100):
            if dist < 1: dist = 1
            self.dx += (self.x - rings[springIndex].x)/(dist * dist / 50) * 100
            self.dy += (self.y - rings[springIndex].y)/(dist * dist / 50) * 100
            # cap speed
            speed = math.sqrt(self.dx * self.dx + self.dy * self.dy)
            if speed > 200:
                self.dx = 200 * self.dx / speed
                self.dy = 200 * self.dy / speed

    def checkCollision(self, otherBall):
        if self == otherBall: return
        dist = math.sqrt(wf.getSqDist(self.x, self.y, otherBall.x, otherBall.y))
        if dist <= self.radius + otherBall.radius:
            # print "Balls are colliding! speed before: ", self.dx, self.dy, dist
            collisionX = self.x - otherBall.x
            collisionY = self.y - otherBall.y
            collXNormalized = collisionX / dist
            collYNormalized = collisionY / dist
            # print "collision normalized: ", collXNormalized, collYNormalized

            myCollisionSpeed = self.dx * collXNormalized + self.dy * collYNormalized
            otherCollisionSpeed = otherBall.dx * collXNormalized + otherBall.dy * collYNormalized

            # print "collision speeds: ", myCollisionSpeed, otherCollisionSpeed

            self.x = otherBall.x + collXNormalized * (self.radius + otherBall.radius)
            self.y = otherBall.y + collYNormalized * (self.radius + otherBall.radius)

            self.dx += (otherCollisionSpeed - myCollisionSpeed) * collXNormalized
            self.dy += (otherCollisionSpeed - myCollisionSpeed) * collYNormalized

            otherBall.dx += (myCollisionSpeed - otherCollisionSpeed) * collXNormalized
            otherBall.dy += (myCollisionSpeed - otherCollisionSpeed) * collYNormalized

            #wf.clackSound.play()
            # print "Speed after: ", self.dx, self.dy


    def draw(self):
        wf.drawDisk(self.x, self.y, self.radius + 5, self.color)

def addBall(x, y, dx, dy, radius, drag, color):
    balls.append(Ball(x, y, dx, dy, radius, drag, color, len(balls)))

def drawGoals():

    # 25 34 35
    wf.drawLEDsOnRing(25, 170, 50, wf.hsv(1, 1, 0.3))
    wf.drawLEDsOnRing(35, 170, 50, wf.hsv(1, 1, 0.3))
    wf.drawLEDsOnRing(34, 170, 60, wf.hsv(1, 1, 0.3))

    # 4 3 8
    wf.drawLEDsOnRing(4, 50, 50, wf.hsv(0.3, 1, 0.3))
    wf.drawLEDsOnRing(3, 50, 50, wf.hsv(0.3, 1, 0.3))
    wf.drawLEDsOnRing(8, 50, 60, wf.hsv(0.3, 1, 0.3))

    # 23 20 13
    wf.drawLEDsOnRing(23, 290, 50, wf.hsv(0.6, 1, 0.3))
    wf.drawLEDsOnRing(22, 290, 50, wf.hsv(0.6, 1, 0.3))
    wf.drawLEDsOnRing(13, 290, 60, wf.hsv(0.6, 1, 0.3))
    pass

def drawBalls():
    for b in balls:
        b.draw()

    if len(balls) > 0:
        drawGoals()

def updateBalls():
    for i, b in enumerate(balls):
        for j in range(i + 1, len(balls)):
            b.checkCollision(balls[j])

        b.update()
#endregion

#region ################################################ Freestyle ###############################################
def freestyleStart():
    pass

fs_lastUserInteraction = 0
fs_nextEvent = 0
def freestyleUpdate():
    global fs_nextEvent, balls
    if time.time() > fs_nextEvent and time.time() - fs_lastUserInteraction > 10:
        fs_nextEvent = time.time() + 1 + random.random()
        event = random.randint(0, 1)
        print "random event:", event
        if event == 0:
            rp = random.randint(0, 16 * 32)
            rc = wf.hsv(random.random(), 1, 1)
            for i in range(20):
                addRunner(rp, 30 + 30 * random.random(), rc, maxAge = time.time() + 2 * random.random())
        if event == 1:
            rr = random.randint(0, 35)
            addEffect(rings[rr].x, rings[rr].y, speed = 450, color = wf.hsv(rr/14.0 % 1.0, 1, 0.4), maxRadius = 450, bandSize = 120)
        if event == 2:
            if len(balls) > 0:
                balls = []
            else:
                addBall(50, 50, 100, 100, radius = 25, drag = 0, color = 0.7)
        if event == 3:
            pass
        if event == 4:
            pass


def freestyleTouch(index):
    global fs_lastUserInteraction
    rc = wf.hsv(index/14.0 % 1.0, 1, 0.6)
    runnerCount = 10 - activeRunnerCount/10
    if runnerCount < 3: 
        runnerCount = 3

    for i in range(runnerCount):
        addRunner(wf.rings[index].firstLEDindex, 30 + 30 * random.random(), rc, maxAge = time.time()+ 3 * random.random(), jumpChance = 1)
    fs_lastUserInteraction = time.time()
    # s = pygame.mixer.Sound(file='audio/oiii16/' + str(index) + '.wav')
    print "playing", index
    # s.play()
    wf.playSound('audio/oiii16/' + str(index) + '.wav')

def freestyleWobble(index, t):
    global fs_lastUserInteraction
    fs_lastUserInteraction = time.time()
    addEffect(rings[index].x, rings[index].y, speed = 450, color = wf.hsv((index/14.0 + 0.5) % 1.0, 1, 0.5), maxRadius = 250, bandSize = 100, startTime = t)
    
#endregion

#region ################################################# InteractiveThing ###################################

class InteractiveThing:
    def __init__(self):
        pass

    def wobbleStart(self):
        pass

    def wobbleUpdate(self, dt):
        pass

    def wobbleDraw(self):
        pass

    def wobbleShutdown(self):
        pass

def registerInteractiveThing(thing):
    interactiveThings.append(thing)

#endregion

#region ################################################### Twister ###############################################
class TwisterRing:
        id = 0
        owner = None
        mode = 0
        myTouchValue = 0
        def __init__(self, id, owner = None):
            print "05"
            self.id = id
            self.owner = owner
            self.mode = 0
            self.myTouchValue = 0

        def draw(self, gameStarted):
            if self.owner != None:
                if self.owner.alive:
                    if self.myTouchValue > 0:
                        #wf.lightUpRing(self.id, wf.hsv(self.owner.hue, 1, 0.5))
                        wf.lightUpRing(self.id, wf.hsv(self.owner.hue, 1, math.sin((time.time() * 10 + self.owner.myId))/ 4.0 + 0.75 ))

                    else:
                        if gameStarted:
                            wf.lightUpRing(self.id, wf.hsv(self.owner.hue, 1, 0.5 * (int(time.time() * 10) % 2) ))
                        else:
                            wf.lightUpRing(self.id, wf.hsv(self.owner.hue, 1, math.sin((time.time() * 10 + self.owner.myId))/ 2.0 + 0.5 ))

                else:
                    if time.time() - self.owner.timeOfDeath < 2: # seconds
                        deathDecay =  (time.time() - self.owner.timeOfDeath) / 2.0 # goes from 0 to 1
                        wf.setPixelsOnRing(self.id, [wf.hsv((self.owner.hue + random.uniform(-deathDecay, deathDecay)) % 1.0, 1, 1 - deathDecay * deathDecay) for x in range(16)])
                    else:
                        self.owner = None

class Twister(InteractiveThing):

    startingPositions = [9, 29, 17, 22, 3, 34]
    maxPlayers = 6
    currentPlayers = maxPlayers
    twisterPlayers = []
    twisterRings = []
    winningPlayer = None
    victoryTime = 0
    gameStarted = False
    gameTime = 0
    gameStage = 0
    ringsAdded = 0
    noThreatenBonus = 0

    

    def setRingOwner(self, ringId, newPlayer):
        r = [r for r in self.twisterRings if r.id == ringId]
        if len(r) == 0:
            print self
            tr = self.TwisterRing(ringId, newPlayer)
            self.twisterRings.append(tr)
        else:
            r[0].owner = newPlayer

    def getOrMakeRingAtId(self, ringId):
        r = [r for r in self.twisterRings if r.id == ringId]
        returnRing = None
        if len(r) == 0:
            returnRing = self.TwisterRing(ringId)
            self.twisterRings.append(returnRing)
        else:
            returnRing = r[0]
        return returnRing

    class TwisterPlayer:
        myId = 0
        color = None
        twister = None
        hue = 0
        alive = True
        touched = False
        timeOfDeath = 0
        threatened = False
        won = False
        maxLife = 5
        life = 5
        def __init__(self, startId, myId, twister, hue = -1):
            self.twister = twister
            self.myId = myId
            self.addRing(startId)
            if hue == -1:
                self.hue = myId / 6.0 
            else:
                self.hue = hue
            
            self.color = wf.hsv(self.hue, 1, 1)
            self.life = self.maxLife
            print "initialized twister player", myId, "with hue", self.hue

        def getRings(self):
            return [r for r in self.twister.twisterRings if r.owner == self]    

        def addRing(self, ringId):
            self.twister.setRingOwner(ringId, self)

        def checkTouches(self, touches, dt):
            allTouched = True
            for r in self.getRings():
                if r.myTouchValue != touches[r.id]:
                    r.myTouchValue = touches[r.id]
                    if r.myTouchValue > 0:
                        # s = pygame.mixer.Sound(file='audio/oi16/' + str(r.id) + '.wav')
                        # s.play()
                        wf.playSound('audio/oi16/' + str(r.id) + '.wav')
                        

                if touches[r.id] <= 0:
                    allTouched = False
                else:
                    self.touched = True

            if not allTouched and self.twister.gameStarted:
                self.life -= dt
            elif self.life < self.maxLife:
                self.life += dt * 2   # recover faster

            if self.life < 0:
                self.die()

            self.threatened = not allTouched

            return self.life

        def die(self):
            print "player", self.myId, "died"
            self.alive = False
            self.timeOfDeath = time.time()
            # s = pygame.mixer.Sound(file='audio/loss.wav')
            # s.play()
            wf.playSound('audio/loss.wav')
            
            for r in self.getRings():
                for i in range(16):
                    addRunner(wf.rings[r.id].firstLEDindex + i, 70, wf.hsv(self.hue, 1, 0.4), maxAge = time.time() + 0.5 * random.random())


    def __init__(self):
        InteractiveThing.__init__(self)

    def findEmptyRingId(self):
        occupiedRings = [r.id for r in self.twisterRings if r.owner != None]
        emptyRings = [r for r in range(36) if not r in occupiedRings]
        return random.choice(emptyRings)
        
    def findRingNotFromPlayer(self, twisterPlayer):
        playerRings = twisterPlayer.getRings()
        playerRingIds = [r.id for r in playerRings]
        notPlayerRings = [i for i in range(36) if not i in playerRingIds]
        return self.getOrMakeRingAtId(random.choice(notPlayerRings))


    def addMoreRings(self):
        # add ring for each alive player, if possible

        # check if enough space
        occupiedRings = [r for r in self.twisterRings if r.owner != None]
        if len(occupiedRings) + self.currentPlayers > 36:
            print "board is full!"
            return
        
        self.ringsAdded += 1
        for p in self.twisterPlayers:
            if p.alive:
                r = self.findEmptyRingId()
                pRings = p.getRings()
                # ensure good distance, at least to first own ring
                dist = wf.distance((wf.rings[pRings[0].id].x, wf.rings[pRings[0].id].y), (wf.rings[r].x, wf.rings[r].y))
                attempt = 0
                while (dist < 100 or dist > 200) and attempt < 50:
                    r = self.findEmptyRingId()
                    dist = wf.distance((wf.rings[pRings[0].id].x, wf.rings[pRings[0].id].y), (wf.rings[r].x, wf.rings[r].y))
                    attempt += 1

                
                p.addRing(r)


    def moveRings(self):
        for p in self.twisterPlayers:
            if p.alive:
                randomOwnRing = random.choice(p.getRings())
                randomOtherRing = self.findRingNotFromPlayer(p)
                #swap
                self.setRingOwner(randomOwnRing.id, randomOtherRing.owner)
                self.setRingOwner(randomOtherRing.id, p)


    def wobbleStart(self):
        print "Starting Twister"
        self.winningPlayer = None
        self.currentPlayers = self.maxPlayers
        self.gameStarted = False
        self.gameTime = 0
        self.gameStage = 0
        self.ringsAdded = 0
        self.twisterRings = [TwisterRing(i, None) for i in range(36)]
        #for i in range(36):
        #   self.twisterRings.append(self.TwisterRing(i, None))
        self.twisterPlayers = [self.TwisterPlayer(self.startingPositions[i],i,self) for i in range(6)]
        self.noThreatenBonus = 0
        print "Finished starting Twister"

    def wobbleUpdate(self, dt):
        global tick
        if self.winningPlayer != None:
            if time.time() - self.victoryTime > 10:
                #restart
                self.wobbleShutdown()
                switchGame(9)
                return
            if time.time() - self.victoryTime > 6:
                return

            if tick % 15 == 0:
                winrings = self.winningPlayer.getRings()
                if len(winrings) == 0:
                    return
                
                rr = random.choice(winrings).id
                addEffect(rings[rr].x, rings[rr].y, speed = 450, color = wf.hsv(self.winningPlayer.hue, 1, 0.4), maxRadius = 450, bandSize = 120)
                #rp = random.randint(0, 16 * 32)
                #rc = wf.hsv(self.winningPlayer.hue, 1, 1)
                #addRunner(rp, 30 + 30 * random.random(), rc, maxAge = time.time() + 0.5 * random.random())
            return


        # check if alive players are touching
        touches = wf.getTouchData()
        alivePlayers = 0
        touchedPlayers = 0
        anAlivePlayer = None
        threatenedPlayers = 0

        for p in self.twisterPlayers:
            if not p.alive:
                continue
            anAlivePlayer = p
            l = p.checkTouches(touches, dt)
            alivePlayers += 1
            if p.touched:
                touchedPlayers += 1
            if p.threatened:
                threatenedPlayers += 1
        
        self.currentPlayer = alivePlayers
        
        if not self.gameStarted and touchedPlayers > 1:
            print "game started!"
            self.gameStarted = True
            self.gameTime = time.time()
        
        if alivePlayers == 1:
            self.winningPlayer = anAlivePlayer
            print "Player", anAlivePlayer.myId, "won!"
            self.victoryTime = time.time()
            # s = pygame.mixer.Sound(file='audio/win/' + str(random.randint(1, 6)) + '.wav')
            # s.play()
            wf.playSound('audio/win/' + str(random.randint(1, 6)) + '.wav')
            return

        if self.gameStarted:
            if threatenedPlayers == 0:
                self.noThreatenBonus += dt
            timePassed = time.time() - self.gameTime + self.noThreatenBonus
            newGameStage = int(timePassed / 8)
            if newGameStage != self.gameStage:
                self.gameStage = newGameStage
                if newGameStage < 6:
                    if newGameStage % 2 == 1:
                        self.addMoreRings()
                    else:
                        self.moveRings()
                else:
                    if self.ringsAdded >= 8:
                        self.moveRings()
                    else:
                        if random.randint(0, 100) > 60:
                            self.moveRings()
                        else:
                            self.addMoreRings()



    def wobbleDraw(self):
        if self.winningPlayer != None and time.time() - self.victoryTime > 6:
            return

        for r in self.twisterRings:
            r.draw(self.gameStarted)

    def wobbleShutdown(self):
        self.twisterPlayers = []
        self.twisterRings = []

#endregion


#region ################################################### Main ###############################################


gameTime = 0

pixels = wf.getPixels()
rings = wf.getRings()
tick = 0
oldTouchData = []
lastSecretTouch = 0

def drawBackground(dt, factor = 1.0):
    fadeAmount = (1 - factor * dt)
    if fadeAmount < 0.5: fadeAmount = 0.5
    wf.fadeAllLeds(fadeAmount)

def getCellType(i):
    if rings[i] != None and rings[i].cell != None:
        return rings[i].cell.cellType
    return None

def drawTouches():
    if currentGame == 9: 
        return
    touchData = wf.getTouchData()
    for index, t in enumerate(touchData):
        if t > 0:
            if len(handwalkers) == 0:
                wf.addRing(index, wf.hsv(index/14.0 % 1.0, 1, 0.1))
                # addEffect(rings[index].x, rings[index].y, speed = 350, color = wf.hsv(index/14.0 % 1.0, 1, 0.5), maxRadius = 25, bandSize = 170)

def wobbleSpring(index, wobbleTime, amount = 5):
    global lastSpringWobble
    if lastSpringWobble[index] == wobbleTime:
        print "wobble ignored"
        return
    #print "executing wobble at", index, "t:", wobbleTime, "previous wobbleT:", lastSpringWobble[index]
    lastSpringWobble[index] = wobbleTime
    if currentGame == 5:
        pass
    elif currentGame == 8:
        pass
    elif currentGame == 7:
        for b in balls:
            b.addSpringForce(index, amount)
    elif currentGame == 9:
        pass
    else:
        addEffect(rings[index].x, rings[index].y, speed = 350, color = wf.hsv(index/14.0 % 1.0, 1, 0.3), maxRadius = 350, bandSize = 120, startTime = wobbleTime)
        # s = pygame.mixer.Sound(file='audio/8/' + str(index) + '.wav')
        # s.play()
        wf.playSound('audio/8/' + str(index) + '.wav')
    print index
    liquidWobble(index)
    handWalkerWobble(index)
    if currentGame == 8:
        freestyleWobble(index, wobbleTime)

    for r in runners:
        r.targetX = rings[index].x
        r.targetY = rings[index].y
        r.movementNoise = 1
        r.repelledByTarget = True

    if getCellType(index) == waterType:
        for f in fish:
            f.targetX = rings[index].x
            f.targetY = rings[index].y
            f.movementNoise = 0.5 + random.random()*0.5
            f.repelledByTarget = True


def handleInput():
    global currentGame, lastSecretTouch, hw_lastTouchTime, lastSpringWobble
    touchData = wf.getTouchData()
    springData = wf.getSpringData()
    wobbleData = wf.getWobbleData()
    wobbleTouchTimeOut = 0.2
    minTimeBetweenWobblesOnSameSpring = 0.4

    if wobbleData is not None and time.time() - wobbleData[0] < wobbleTouchTimeOut:
        for index in range(36):
            #if index == 2:
            #   print "spring 2 touchdata:", (time.time() + touchData[index])
            if time.time() + touchData[index] < wobbleTouchTimeOut + 0.1 and lastSpringWobble[index] + minTimeBetweenWobblesOnSameSpring < wobbleData[0]:
                wobbleSpring(index, wobbleData[0])


    # THIS IS OLD WOBBLE DATA FOR INDIVIDUAL SENSOR BOARDS
    for index, amount in enumerate(springData):
        if amount > 0 and (time.time() + touchData[index] < 1 or (wf.state == wf.SIMULATEDRUNNING)):
            #wf.drawDisk(rings[index].x, rings[index].y, amount + 24)
            #addEffect(rings[index].x, rings[index].y, speed = 350, color = wf.hsv(index/14.0 % 1.0, 1, min(0.1, amount / 10)), maxRadius = 100, bandSize = 20)
            # addEffect(rings[index].x, rings[index].y, speed = 350, color = wf.hsv(index/14.0 % 1.0, 1, 0.1), maxRadius = 50, bandSize = 170)
            
            wobbleSpring(index, time.time(), amount)


    for index, t in enumerate(touchData):
        if t != oldTouchData[index]:
            #value changed!

            if t > 0:
                #just touched
                #if len(handwalkers) == 0:
                if currentGame != 5 and currentGame != 9:
                    addEffect(rings[index].x, rings[index].y, speed = 1000, color = wf.hsv(index/14.0 % 1.0, 1, 0.01), maxRadius = 50, bandSize = 40)
                    if currentGame != 8:
                        # s = pygame.mixer.Sound(file='audio/11/' + str(index) + '.wav')
                        # s.play()
                        wf.playSound('audio/11/' + str(index) + '.wav')

                hw_lastTouchTime = time.time()
                wf.addRing(index, (0,5,5))

                if currentGame == 8:
                    freestyleTouch(index)

                if currentGame == 7:
                    for b in balls:
                        b.addSpringForce(index, 1)

                for r in runners:
                    r.targetX = rings[index].x
                    r.targetY = rings[index].y
                    r.movementNoise = 1
                    r.repelledByTarget = False

                if getCellType(index) == waterType:
                    for f in fish:
                        f.targetX = rings[index].x
                        f.targetY = rings[index].y
                        f.movementNoise = 0.5 + random.random()*0.5
                        f.repelledByTarget = False


            else:
                #just released, check if we just got a wobble
                #print "spring released at", index, " time since last wob:", (time.time() - wobbleData[0])
                if wobbleData is not None and time.time() - wobbleData[0] < wobbleTouchTimeOut and time.time() - lastSpringWobble[index] > minTimeBetweenWobblesOnSameSpring:
                    #print "timeout:", (time.time() - wobbleData[0])e
                    wobbleSpring(index, wobbleData[0])

            oldTouchData[index] = t


    # check secret touch
    touches = [1, 20, 32, 13, 23]
    secretCode = True
    for index, x in enumerate(touchData):
        if (index in touches and x <= 0) or (not index in touches and x > 0):
            secretCode = False
            break

    if secretCode and time.time() > lastSecretTouch:
        switchGame() 
        lastSecretTouch = time.time() + 1

        


def keyboardHandler(events):
    global currentGame, nextGame
    for event in events:
        if event.type == KEYUP:
            if event.key == K_e:
                addEffect(100, 100, 350, (255, 0, 0), 450, 170)
            if event.key == K_n:
                switchGame()
            if event.key == K_0:
                # finger walk
                switchGame(5) 
            if event.key == K_1:
                # twister
                switchGame(9)
            if event.key == K_2:
                # koi
                switchGame(1)
            if event.key == K_3:
                # freestyle
                switchGame(8)
            if event.key == K_4:
                # undef - ball
                switchGame(7)
            if event.key == K_5:
                # undef
                switchGame(2)

    if buttons[0].is_pressed and currentGame != 5: switchGame(5)
    if buttons[1].is_pressed and currentGame != 9: switchGame(9)
    if buttons[2].is_pressed and currentGame != 1: switchGame(1)
    if buttons[3].is_pressed and currentGame != 8: switchGame(8)
    if buttons[4].is_pressed and currentGame != 7: switchGame(7)
    if buttons[5].is_pressed and currentGame != 2: switchGame(2)
        


def loop():
    global dtPrevious, dt, tick, currentGame
    dt = time.time() - dtPrevious
    dtPrevious = time.time()
    tick += 1
    if currentGame == 8:
        # slower fadeout in freestyle
        drawBackground(dt, 3.0)
    elif currentGame == 7:
        # faster fadeout in Ballgame
        drawBackground(dt, 16.0)
    elif currentGame == 1 or currentGame == 2:
        # no background clearing necessaryd
        pass
    else:
        drawBackground(dt, 5.0)
    handleInput()
    updateRunners()
    updateFish()
    updateBalls()
    if currentGame == 8:
        freestyleUpdate()
    if currentGame == 5:
        updateFriends()
        updateHandWalkers()
        updateLava()
        updateEnemies()
    updateLiquid()
    drawWater()
    drawFish()
    drawTrees()
    drawEffects()
    drawRunners()
    drawBalls()
    if currentGame == 5:
        drawEnemies()
        drawHandWalkers()
        drawFriends()
        drawLava()

    if currentGame == 9:
        interactiveThings[0].wobbleUpdate(dt)
        interactiveThings[0].wobbleDraw()

    drawLiquid()
    drawTouches()
    # drawDisk(math.sin((time.time() - gameTime) * 5) * 50 + 30, math.cos((time.time() - gameTime)* 5) * 50 + 100, math.sin((time.time() - gameTime) *4)*40 + 50)

gameIndex = -1
def switchGame(newGame = -1):
    global gameTime, dtPrevious, oldTouchData, currentGame, runners, friends, fish, enemies, trees, water, lava, handwalkers, effects, balls, gameIndex, liquid
    runners = []
    friends = []
    fish = []
    enemies = []
    trees = []
    water = []
    for r in wf.rings:
        r.cell = None
    lava = []
    handwalkers = []
    effects = []
    balls = []
    liquid = []
    if newGame == -1: 
        # includedGames = [1,2,3,5,6,7,8]
        includedGames = [1,5,8]
        gameIndex += 1
        if gameIndex >= len(includedGames):
            gameIndex = 0
        newGame = includedGames[gameIndex]
    print "switching game to", newGame
    currentGame = newGame
    if time.time() > gameTime + 10:
        wf.resetAudio()
    startGame()
    print "finished switch"


def startGame():
    global gameTime, dtPrevious, oldTouchData, currentGame, interactiveThings
    gameTime = time.time()    
    version = currentGame

    if version == 1:
        for x in range(10):
            #addRunner(100, 30 + random.random() * 10, wf.hsv(random.random(),1,1)) 
            pass

        for x in range(36):
            #addTree(x)
            neighborCount = sum([i >= 0 for i in rings[x].neighbours])
            if neighborCount == 6 or True:
                addWater(x)
                if (x % 3 == 0): addFish(x * 16, 2 + random.random() * 3)
            else:
                addTree(x)  
                #addRunner(x * 16, 30 + random.random() * 10, wf.hsv(random.random(),1,1)) 

    elif version == 2:
        for x in range(10):
            #addRunner(100, 30 + random.random() * 10, wf.hsv(random.random(),1,1)) 
            pass

        for x in range(36):
            #addTree(x)
            neighborCount = sum([i >= 0 for i in rings[x].neighbours])
            if neighborCount == 6:
                addWater(x)
                if (x % 2 == 0): addFish(x * 16, 2 + random.random() * 3)
            else:
                addTree(x)
                addRunner(x * 16, 30 + random.random() * 10, wf.hsv(random.random(),1,1)) 
    elif version == 3:
        for x in range(10):
            addRunner(100, 30 + random.random() * 10, wf.hsv(random.random(),1,0.1)) 
            pass

    elif version == 4:
        for x in range(10):
            #addRunner(100, 30 + random.random() * 10, wf.hsv(random.random(),1,1)) 
            pass

        for x in range(16):
            #addTree(x)
            neighborCount = sum([i >= 0 for i in rings[x].neighbours])
            if neighborCount == 6:
                addWater(x)
                if (x % 4 == 0): addFish(x * 16, 2 + random.random() * 3)
            else:
                addTree(x)
                addRunner(x * 16, 30 + random.random() * 10, wf.hsv(random.random(),1,1)) 

    elif version == 5:
        handWalkerStartGame()
        # addEnemy(1, 10)
        # for x in range(36):
        #     neighborCount = sum([i >= 0 for i in rings[x].neighbours])
        #     if neighborCount < 4:
        #         addLava(x)

    elif version == 6:
        addLiquid(3, 0)
        addLiquid(4, 0)
        addLiquid(8, 0)
        addLiquid(13, 1)
        addLiquid(22, 1)
        addLiquid(23, 1)
        addLiquid(25, 2)
        addLiquid(35, 2)
        addLiquid(34, 2)

    elif version == 7:
        addBall(50, 50, 100, 100, radius = 25, drag = 0, color = 0.7)
        #addBall(150, 150, 100, -140, radius = 25, drag = 0, color = 0.1)
        #addBall(10, 150, -100, 40, radius = 25, drag = 0, color = 0.4)

    elif version == 8:
        freestyleStart()

    elif version == 9:
        interactiveThings[0].wobbleStart()


    dtPrevious = time.time()
    oldTouchData = [0 for i in range(0, 36)]


def init():
    global buttons


    registerInteractiveThing(Twister())
    wf.initialize(startGame, loop, keyboardHandler)

    wf.run()

init()
# help(noise)
#endregion

