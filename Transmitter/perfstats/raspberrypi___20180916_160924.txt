Sun Sep 16 16:09:24 2018    stats

         454285 function calls in 9.967 seconds

   Ordered by: internal time
   List reduced from 84 to 17 due to restriction <0.2>

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
      838    2.960    0.004    2.960    0.004 {select.select}
     1676    2.012    0.001    2.012    0.001 {map}
      838    1.034    0.001    1.034    0.001 wobbleforestunified.py:676(fadeAllLeds)
    37499    0.607    0.000    0.719    0.000 wobbleforestunified.py:633(addPixel)
    64522    0.468    0.000    0.468    0.000 wobbleforestunified.py:646(setPixel)
       56    0.363    0.006    0.658    0.012 wobbleforestunified.py:223(drawCoordinates)
    69328    0.307    0.000    0.319    0.000 springwalk.py:58(update)
    69440    0.236    0.000    0.865    0.000 springwalk.py:126(draw)
      838    0.184    0.000    0.503    0.001 springwalk.py:268(updateRunners)
    32312    0.174    0.000    0.174    0.000 {method 'fill' of 'pygame.Surface' objects}
      838    0.172    0.000    0.172    0.000 {method 'join' of 'str' objects}
      838    0.153    0.000    1.019    0.001 springwalk.py:293(drawRunners)
      838    0.142    0.000    0.625    0.001 springwalk.py:884(handWalkerManagerUpdate)
    37499    0.112    0.000    0.112    0.000 wobbleforestunified.py:630(scale)
       56    0.110    0.002    0.110    0.002 {pygame.display.update}
     6075    0.095    0.000    0.365    0.000 wobbleforestunified.py:620(addRingFloatPixel)
     1240    0.077    0.000    0.151    0.000 springwalk.py:1017(draw)


   Ordered by: internal time
   List reduced from 84 to 17 due to restriction <0.2>

Function                                       was called by...
                                                   ncalls  tottime  cumtime
{select.select}                                <-     838    2.960    2.960  serialposix.py:514(write)
{map}                                          <-    1676    2.012    2.012  wobbleforestunified.py:124(sendData)
wobbleforestunified.py:676(fadeAllLeds)        <-     838    1.034    1.034  springwalk.py:1559(drawBackground)
wobbleforestunified.py:633(addPixel)           <-   12150    0.194    0.229  springwalk.py:126(draw)
                                                     1823    0.029    0.034  springwalk.py:1017(draw)
                                                    11376    0.185    0.221  wobbleforestunified.py:441(addRing)
                                                    12150    0.199    0.235  wobbleforestunified.py:620(addRingFloatPixel)
wobbleforestunified.py:646(setPixel)           <-   64512    0.468    0.468  springwalk.py:884(handWalkerManagerUpdate)
wobbleforestunified.py:223(drawCoordinates)    <-      56    0.363    0.658  wobbleforestunified.py:124(sendData)
springwalk.py:58(update)                       <-   69328    0.307    0.319  springwalk.py:268(updateRunners)
springwalk.py:126(draw)                        <-   69440    0.236    0.865  springwalk.py:293(drawRunners)
springwalk.py:268(updateRunners)               <-     838    0.184    0.503  springwalk.py:1707(loop)
{method 'fill' of 'pygame.Surface' objects}    <-   32312    0.174    0.174  wobbleforestunified.py:223(drawCoordinates)
{method 'join' of 'str' objects}               <-     838    0.172    0.172  wobbleforestunified.py:124(sendData)
springwalk.py:293(drawRunners)                 <-     838    0.153    1.019  springwalk.py:1707(loop)
springwalk.py:884(handWalkerManagerUpdate)     <-     838    0.142    0.625  springwalk.py:825(updateHandWalkers)
wobbleforestunified.py:630(scale)              <-   37499    0.112    0.112  wobbleforestunified.py:633(addPixel)
{pygame.display.update}                        <-      56    0.110    0.110  wobbleforestunified.py:223(drawCoordinates)
wobbleforestunified.py:620(addRingFloatPixel)  <-    6075    0.095    0.365  springwalk.py:126(draw)
springwalk.py:1017(draw)                       <-    1240    0.077    0.151  springwalk.py:1056(drawEffects)


