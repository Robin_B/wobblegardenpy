Thu Aug 30 08:41:48 2018    stats

         6123066 function calls in 8.861 seconds

   Ordered by: internal time
   List reduced from 55 to 11 due to restriction <0.2>

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
    26136    1.491    0.000    4.474    0.000 springwalk.py:326(draw)
      726    1.253    0.002    2.409    0.003 wobbleforest.py:203(drawCoordinates)
      726    1.097    0.002    1.097    0.002 {pygame.time.wait}
   418176    1.000    0.000    1.867    0.000 wobbleforest.py:620(hsv)
   418185    0.748    0.000    0.748    0.000 wobbleforest.py:606(setPixel)
   418902    0.545    0.000    0.545    0.000 {method 'fill' of 'pygame.Surface' objects}
   418176    0.543    0.000    0.543    0.000 colorsys.py:135(hsv_to_rgb)
  1672704    0.324    0.000    0.324    0.000 wobbleforest.py:621(<genexpr>)
    60984    0.231    0.000    0.374    0.000 wobbleforest.py:597(addPixel)
      726    0.221    0.000    0.221    0.000 {pygame.display.update}
   418176    0.199    0.000    0.199    0.000 {noise._perlin.noise2}


   Ordered by: internal time
   List reduced from 55 to 11 due to restriction <0.2>

Function                                     was called by...
                                                 ncalls  tottime  cumtime
springwalk.py:326(draw)                      <-   26136    1.491    4.474  springwalk.py:357(drawWater)
wobbleforest.py:203(drawCoordinates)         <-
{pygame.time.wait}                           <-
wobbleforest.py:620(hsv)                     <-  418176    1.000    1.867  springwalk.py:326(draw)
wobbleforest.py:606(setPixel)                <-  418176    0.748    0.748  springwalk.py:326(draw)
{method 'fill' of 'pygame.Surface' objects}  <-  418902    0.545    0.545  wobbleforest.py:203(drawCoordinates)
colorsys.py:135(hsv_to_rgb)                  <-  418176    0.543    0.543  wobbleforest.py:620(hsv)
wobbleforest.py:621(<genexpr>)               <- 1672704    0.324    0.324  wobbleforest.py:620(hsv)
wobbleforest.py:597(addPixel)                <-   43560    0.160    0.259  springwalk.py:125(draw)
                                                  17424    0.071    0.115  wobbleforest.py:584(addRingFloatPixel)
{pygame.display.update}                      <-     726    0.221    0.221  wobbleforest.py:203(drawCoordinates)
{noise._perlin.noise2}                       <-  418176    0.199    0.199  springwalk.py:326(draw)


