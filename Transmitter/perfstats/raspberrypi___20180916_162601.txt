Sun Sep 16 16:26:01 2018    stats

         1224699 function calls in 10.014 seconds

   Ordered by: internal time
   List reduced from 64 to 13 due to restriction <0.2>

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
     9612    2.436    0.000    6.214    0.001 springwalk.py:327(draw)
   153801    1.434    0.000    1.434    0.000 wobbleforestunified.py:648(setPixel)
    77163    0.967    0.000    1.977    0.000 wobbleforestunified.py:662(hsv)
      267    0.936    0.004    0.936    0.004 {select.select}
    77163    0.658    0.000    0.658    0.000 colorsys.py:135(hsv_to_rgb)
    42186    0.654    0.000    0.772    0.000 wobbleforestunified.py:635(addPixel)
      267    0.360    0.001    0.360    0.001 wobbleforestunified.py:678(fadeAllLeds)
   308652    0.352    0.000    0.352    0.000 wobbleforestunified.py:663(<genexpr>)
      267    0.319    0.001    0.319    0.001 {map}
     7476    0.282    0.000    0.300    0.000 springwalk.py:58(update)
     7476    0.257    0.000    1.173    0.000 springwalk.py:126(draw)
   153792    0.225    0.000    0.225    0.000 {noise._perlin.noise2}
      267    0.172    0.001    1.703    0.006 wobbleforestunified.py:125(sendData)


   Ordered by: internal time
   List reduced from 64 to 13 due to restriction <0.2>

Function                                 was called by...
                                             ncalls  tottime  cumtime
springwalk.py:327(draw)                  <-    4806    0.979    1.818  springwalk.py:354(drawTrees)
                                               4806    1.457    4.396  springwalk.py:358(drawWater)
wobbleforestunified.py:648(setPixel)     <-  153792    1.434    1.434  springwalk.py:327(draw)
wobbleforestunified.py:662(hsv)          <-   76896    0.961    1.966  springwalk.py:327(draw)
                                                267    0.006    0.011  springwalk.py:1567(drawTouches)
{select.select}                          <-     267    0.936    0.936  serialposix.py:514(write)
colorsys.py:135(hsv_to_rgb)              <-   77163    0.658    0.658  wobbleforestunified.py:662(hsv)
wobbleforestunified.py:635(addPixel)     <-   22962    0.358    0.421  springwalk.py:126(draw)
                                               4272    0.066    0.078  wobbleforestunified.py:443(addRing)
                                              14952    0.230    0.272  wobbleforestunified.py:622(addRingFloatPixel)
wobbleforestunified.py:678(fadeAllLeds)  <-     267    0.360    0.360  springwalk.py:1559(drawBackground)
wobbleforestunified.py:663(<genexpr>)    <-  308652    0.352    0.352  wobbleforestunified.py:662(hsv)
{map}                                    <-     267    0.319    0.319  wobbleforestunified.py:125(sendData)
springwalk.py:58(update)                 <-    4806    0.201    0.218  springwalk.py:268(updateRunners)
                                               2670    0.081    0.083  springwalk.py:277(updateFish)
springwalk.py:126(draw)                  <-    4806    0.140    0.615  springwalk.py:293(drawRunners)
                                               2670    0.117    0.559  springwalk.py:297(drawFish)
{noise._perlin.noise2}                   <-  153792    0.225    0.225  springwalk.py:327(draw)
wobbleforestunified.py:125(sendData)     <-


