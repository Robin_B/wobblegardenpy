Thu Aug 30 18:22:25 2018    stats

         3757689 function calls in 37.925 seconds

   Ordered by: internal time
   List reduced from 80 to 16 due to restriction <0.2>

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
     2523   26.338    0.010   29.912    0.012 wobbleforest.py:207(drawCoordinates)
     2523    3.830    0.002    3.830    0.002 {pygame.time.wait}
    82638    2.115    0.000    2.824    0.000 wobbleforest.py:601(addPixel)
  1455771    1.640    0.000    1.640    0.000 {method 'fill' of 'pygame.Surface' objects}
     2523    0.731    0.000    0.731    0.000 {pygame.display.update}
   612255    0.645    0.000    0.645    0.000 {min}
    90828    0.605    0.000    0.605    0.000 {method 'blit' of 'pygame.Surface' objects}
   342737    0.292    0.000    0.292    0.000 {max}
    90828    0.290    0.000    0.290    0.000 {method 'render' of 'pygame.font.Font' objects}
   106022    0.187    0.000    0.187    0.000 wobbleforest.py:610(setPixel)
     2523    0.158    0.000    0.158    0.000 wobbleforest.py:643(fadeAllLeds)
   179984    0.136    0.000    1.589    0.000 springwalk.py:125(draw)
   179872    0.128    0.000    0.133    0.000 springwalk.py:57(update)
     2523    0.107    0.000    0.240    0.000 springwalk.py:267(updateRunners)
     2523    0.090    0.000    1.679    0.001 springwalk.py:292(drawRunners)
     2523    0.066    0.000    4.135    0.002 springwalk.py:1706(loop)


   Ordered by: internal time
   List reduced from 80 to 16 due to restriction <0.2>

Function                                         was called by...
                                                     ncalls  tottime  cumtime
wobbleforest.py:207(drawCoordinates)             <-
{pygame.time.wait}                               <-
wobbleforest.py:601(addPixel)                    <-   20666    0.514    0.689  springwalk.py:125(draw)
                                                       4282    0.105    0.140  springwalk.py:1016(draw)
                                                      37024    0.976    1.301  wobbleforest.py:415(addRing)
                                                      20666    0.521    0.694  wobbleforest.py:588(addRingFloatPixel)
{method 'fill' of 'pygame.Surface' objects}      <- 1455771    1.640    1.640  wobbleforest.py:207(drawCoordinates)
{pygame.display.update}                          <-    2523    0.731    0.731  wobbleforest.py:207(drawCoordinates)
{min}                                            <-   17226    0.004    0.004  springwalk.py:1016(draw)
                                                     264477    0.286    0.286  wobbleforest.py:207(drawCoordinates)
                                                     330552    0.355    0.355  wobbleforest.py:601(addPixel)
{method 'blit' of 'pygame.Surface' objects}      <-   90828    0.605    0.605  wobbleforest.py:207(drawCoordinates)
{max}                                            <-    3995    0.001    0.001  springwalk.py:57(update)
                                                     247914    0.268    0.268  wobbleforest.py:601(addPixel)
{method 'render' of 'pygame.font.Font' objects}  <-   90828    0.290    0.290  wobbleforest.py:207(drawCoordinates)
wobbleforest.py:610(setPixel)                    <-  105984    0.187    0.187  springwalk.py:883(handWalkerManagerUpdate)
wobbleforest.py:643(fadeAllLeds)                 <-    2523    0.158    0.158  springwalk.py:1558(drawBackground)
springwalk.py:125(draw)                          <-  179984    0.136    1.589  springwalk.py:292(drawRunners)
springwalk.py:57(update)                         <-  179872    0.128    0.133  springwalk.py:267(updateRunners)
springwalk.py:267(updateRunners)                 <-    2523    0.107    0.240  springwalk.py:1706(loop)
springwalk.py:292(drawRunners)                   <-    2523    0.090    1.679  springwalk.py:1706(loop)
springwalk.py:1706(loop)                         <-


