Sun Sep 16 15:53:44 2018    stats

         381154 function calls in 9.972 seconds

   Ordered by: internal time
   List reduced from 84 to 17 due to restriction <0.2>

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
      803    2.827    0.004    2.827    0.004 {select.select}
     4818    2.701    0.001    2.701    0.001 {map}
      803    0.951    0.001    0.951    0.001 wobbleforestunified.py:670(fadeAllLeds)
    36090    0.571    0.000    0.676    0.000 wobbleforestunified.py:627(addPixel)
    62986    0.448    0.000    0.448    0.000 wobbleforestunified.py:640(setPixel)
       53    0.336    0.006    0.610    0.012 wobbleforestunified.py:217(drawCoordinates)
    35728    0.250    0.000    0.261    0.000 springwalk.py:58(update)
    35840    0.194    0.000    0.785    0.000 springwalk.py:126(draw)
    30581    0.163    0.000    0.163    0.000 {method 'fill' of 'pygame.Surface' objects}
     1606    0.162    0.000    0.162    0.000 {method 'join' of 'str' objects}
      803    0.127    0.000    0.589    0.001 springwalk.py:884(handWalkerManagerUpdate)
    36090    0.104    0.000    0.104    0.000 wobbleforestunified.py:624(scale)
       53    0.101    0.002    0.101    0.002 {pygame.display.update}
      803    0.091    0.000    0.352    0.000 springwalk.py:268(updateRunners)
     5849    0.090    0.000    0.345    0.000 wobbleforestunified.py:614(addRingFloatPixel)
      803    0.081    0.000    6.495    0.008 wobbleforestunified.py:124(sendData)
      803    0.081    0.000    0.866    0.001 springwalk.py:293(drawRunners)


   Ordered by: internal time
   List reduced from 84 to 17 due to restriction <0.2>

Function                                       was called by...
                                                   ncalls  tottime  cumtime
{select.select}                                <-     803    2.827    2.827  serialposix.py:514(write)
{map}                                          <-    4818    2.701    2.701  wobbleforestunified.py:124(sendData)
wobbleforestunified.py:670(fadeAllLeds)        <-     803    0.951    0.951  springwalk.py:1559(drawBackground)
wobbleforestunified.py:627(addPixel)           <-   11698    0.183    0.215  springwalk.py:126(draw)
                                                     1830    0.027    0.032  springwalk.py:1017(draw)
                                                    10864    0.174    0.207  wobbleforestunified.py:435(addRing)
                                                    11698    0.188    0.221  wobbleforestunified.py:614(addRingFloatPixel)
wobbleforestunified.py:640(setPixel)           <-   62976    0.448    0.448  springwalk.py:884(handWalkerManagerUpdate)
wobbleforestunified.py:217(drawCoordinates)    <-      53    0.336    0.610  wobbleforestunified.py:124(sendData)
springwalk.py:58(update)                       <-   35728    0.250    0.261  springwalk.py:268(updateRunners)
springwalk.py:126(draw)                        <-   35840    0.194    0.785  springwalk.py:293(drawRunners)
{method 'fill' of 'pygame.Surface' objects}    <-   30581    0.163    0.163  wobbleforestunified.py:217(drawCoordinates)
{method 'join' of 'str' objects}               <-    1606    0.162    0.162  wobbleforestunified.py:124(sendData)
springwalk.py:884(handWalkerManagerUpdate)     <-     803    0.127    0.589  springwalk.py:825(updateHandWalkers)
wobbleforestunified.py:624(scale)              <-   36090    0.104    0.104  wobbleforestunified.py:627(addPixel)
{pygame.display.update}                        <-      53    0.101    0.101  wobbleforestunified.py:217(drawCoordinates)
springwalk.py:268(updateRunners)               <-     803    0.091    0.352  springwalk.py:1707(loop)
wobbleforestunified.py:614(addRingFloatPixel)  <-    5849    0.090    0.345  springwalk.py:126(draw)
wobbleforestunified.py:124(sendData)           <-
springwalk.py:293(drawRunners)                 <-     803    0.081    0.866  springwalk.py:1707(loop)


