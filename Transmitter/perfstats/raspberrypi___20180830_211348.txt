Thu Aug 30 21:13:10 2018    stats

         5341583716 function calls in 45101.991 seconds

   Ordered by: internal time
   List reduced from 76 to 15 due to restriction <0.2>

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
  2140984 17027.909    0.008 30200.646    0.014 wobbleforest.py:203(drawCoordinates)
1235347768 6315.200    0.000 6315.200    0.000 {method 'fill' of 'pygame.Surface' objects}
  2140984 3389.214    0.002 3389.214    0.002 {pygame.display.update}
  2140984 2846.286    0.001 2846.286    0.001 wobbleforest.py:636(fadeAllLeds)
1935815526 2465.503    0.000 2465.503    0.000 {min}
  2140984 2291.812    0.001 2291.812    0.001 {pygame.time.wait}
111096830 2020.611    0.000 3488.960    0.000 wobbleforest.py:597(addPixel)
 14987096 1166.883    0.000 2778.188    0.000 springwalk.py:1016(draw)
308293995 1042.058    0.000 1079.990    0.000 springwalk.py:57(update)
 77075424  861.857    0.000  861.857    0.000 {method 'render' of 'pygame.font.Font' objects}
308293995  820.402    0.000 3854.670    0.000 springwalk.py:125(draw)
 77075424  800.603    0.000  800.603    0.000 {method 'blit' of 'pygame.Surface' objects}
  2140984  726.692    0.000 1806.682    0.001 springwalk.py:267(updateRunners)
  2140984  591.814    0.000 4446.483    0.002 springwalk.py:292(drawRunners)
416350140  456.773    0.000  456.773    0.000 {max}


   Ordered by: internal time
   List reduced from 76 to 15 due to restriction <0.2>

Function                                         was called by...
                                                     ncalls  tottime  cumtime
wobbleforest.py:203(drawCoordinates)             <-
{method 'fill' of 'pygame.Surface' objects}      <- 1235347768 6315.200 6315.200  wobbleforest.py:203(drawCoordinates)
{pygame.display.update}                          <- 2140984 3389.214 3389.214  wobbleforest.py:203(drawCoordinates)
wobbleforest.py:636(fadeAllLeds)                 <- 2140984 2846.286 2846.286  springwalk.py:1558(drawBackground)
{min}                                            <- 120575450  158.987  158.987  springwalk.py:1016(draw)
                                                  1370852756 1732.907 1732.907  wobbleforest.py:203(drawCoordinates)
                                                  444387320  573.608  573.608  wobbleforest.py:597(addPixel)
{pygame.time.wait}                               <-
wobbleforest.py:597(addPixel)                    <- 39645618  717.067 1236.845  springwalk.py:125(draw)
                                                   31805050  575.354  990.531  springwalk.py:1016(draw)
                                                        544    0.011    0.018  wobbleforest.py:411(addRing)
                                                   39645618  728.179 1261.566  wobbleforest.py:584(addRingFloatPixel)
springwalk.py:1016(draw)                         <- 14987096 1166.883 2778.188  springwalk.py:1055(drawEffects)
springwalk.py:57(update)                         <- 308293995 1042.058 1079.990  springwalk.py:267(updateRunners)
{method 'render' of 'pygame.font.Font' objects}  <- 77075424  861.857  861.857  wobbleforest.py:203(drawCoordinates)
springwalk.py:125(draw)                          <- 308293995  820.402 3854.670  springwalk.py:292(drawRunners)
{method 'blit' of 'pygame.Surface' objects}      <- 77075424  800.603  800.603  wobbleforest.py:203(drawCoordinates)
springwalk.py:267(updateRunners)                 <- 2140984  726.692 1806.682  springwalk.py:1706(loop)
springwalk.py:292(drawRunners)                   <- 2140984  591.814 4446.483  springwalk.py:1706(loop)
{max}                                            <- 5984226   11.611   11.611  springwalk.py:57(update)
                                                  333290490  373.293  373.293  wobbleforest.py:597(addPixel)


