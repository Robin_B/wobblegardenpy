############################################ FROG ###############################################

frogs = []

class Frog:
    active = True
    distOnRing = 0
    limitToWater = False
    tailPos = []
    tailLength = 5
    previousPos = 0

    def __init__(self, i, speed, color, targetX = -100000, targetY = -1, jumpChance = 0.4, movementNoise = 0.1, active = True):
        self.i = i
        self.speed = speed
        self.color = color
        self.targetX = targetX
        self.targetY = targetY
        self.jumpChance = jumpChance
        self.movementNoise = movementNoise
        self.active = active

    def update(self):
        global dt
        if not self.active: return

        if self.limitToWater:
            advance = (self.speed * (1 + self.movementNoise*5))*dt
        else:
            advance = (self.speed * (1 + self.movementNoise))*dt
        myRing = pixels[int(self.i)].ringId
        myAngle = rings[myRing].angle + (int(self.i) - rings[myRing].firstLEDindex) * 22.5
        myNewAngle = rings[myRing].angle + (int(self.i + advance) - rings[myRing].firstLEDindex) * 22.5
        self.i += advance
        # print "updating runner, myring:",myRing,"myAngle", myAngle,"mypos", self.i
        while self.i - rings[myRing].firstLEDindex >= 16: self.i -=16
        while self.i - rings[myRing].firstLEDindex < 0: self.i +=16
        self.movementNoise *= (1 - dt/5)
        if (self.movementNoise < 0.1):
            self.targetX = -100000
        #print "movement noise:", self.movementNoise
        self.distOnRing += abs(advance)
        
        if (int(myAngle - 30)/60 != int(myNewAngle - 30)/60 and self.distOnRing > 3):
            #jump?
            changeAngle = (max(int(myAngle - 30)/60, int(myNewAngle - 30)/60) * 60 + 30) % 360

            jumpChance = self.jumpChance
            if self.targetX != -100000:
                targetAngle = (90 + math.degrees(math.atan2(self.targetY - pixels[int(self.i)].y, self.targetX - pixels[int(self.i)].x))) % 360
                # print "Runner wants to jump, his angles are", myAngle, myNewAngle
                # print "switchangle:", changeAngle, "target:", targetAngle
                if abs(targetAngle - changeAngle) < 60 and self.movementNoise < random.random():
                    jumpChance *= 3
                else:
                    jumpChance *= 0.1

            if random.random() < jumpChance:
                # neighborId = max(int(myAngle - 30)/60, int(myNewAngle - 30)/60) % 6
                neighborId = (changeAngle/60) % 6
                # print "Runner jumps, his angles are", myAngle, myNewAngle, neighborId
                # print "neighbours are", rings[myRing].neighbours
                newRing = rings[myRing].neighbours[neighborId]
                if newRing == -1 or (self.limitToWater and (rings[newRing].cell == None or rings[newRing].cell.cellType != waterType)):
                    return
                self.distOnRing = 0
                newAngle = (myNewAngle + 180) % 360
                newAngleExact = (neighborId * 60 + 30 + 180) % 360
                realNewAngle = 2*newAngleExact - newAngle # reversing direction
                self.speed *= -1
                newRingAngle = rings[newRing].angle
                newIndex = (realNewAngle - newRingAngle) / 22.5
                #newIndex = (newAngleExact - newRingAngle) / 22.5
                self.i = rings[newRing].firstLEDindex + newIndex
                while self.i - rings[newRing].firstLEDindex >= 16: self.i -=16
                while self.i - rings[newRing].firstLEDindex < 0: self.i +=16        

    def draw(self):
        if not self.active: return
        #wf.addPixel(int(self.i), self.color)
        if int(self.i) != self.previousPos:
            self.tailPos = [self.previousPos] + self.tailPos[0:(self.tailLength - 1)]
            #print "new tail:", self.tailPos
            self.previousPos = int(self.i)

        wf.addRingFloatPixel(self.i, self.color)

        #draw tail
        for i, p in enumerate(self.tailPos):
            wf.addPixel(p, wf.dimColor(self.color, 0.2 - i * (0.2/self.tailLength)))


def addRunner(i, speed, color, targetX = -100000, targetY = -1, jumpChance = 0.6):
    runners.append(Runner(i, speed, color, targetX, targetY, jumpChance))


def addFish(i, speed):
    f = Runner(i, speed, (255, 55, 20 + random.random() * 5))
    f.limitToWater = True
    f.tailLength = 5
    print "adding fish:", f, "limitToWater:", f.limitToWater
    fish.append(f)

def updateRunners():
    for r in runners: 
        r.update()

def updateFish():
    for f in fish: 
        f.update()

def drawRunners():
    for r in runners:
        r.draw()

def drawFish():
    for f in fish:
        f.draw()
