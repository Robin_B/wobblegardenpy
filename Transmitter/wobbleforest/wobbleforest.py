﻿import pygame, sys
from pygame.locals import *
import warnings
import serial
import serial.tools.list_ports
import colorsys
import time
import timeit
import math
import random
import numpy as np

# all for stats:
import datetime
import pstats
import platform
import cProfile
USE_PROFILER = True
currentlyProfiling = False
profilingTimeout = -1
profilingFrameCount = 0

WAITFORCONNECT = 0
CONNECTED = 1
SIMULATED = 2
SIMULATEDRUNNING = 3
state = WAITFORCONNECT
myfont = None
myfont2 = None
retryCounter = 0
leds = [0 for i in range(0, 576*3)]
ledsNP = np.array(leds)
springData = []
touchData = []
updatePyGameVisuals = True

ringXDist = 48.0 # mm
ringXOffset = 24.0 # from one row to the next
ringYDist = 41.575 
ringRadius = 19.0 # mm
ringHexSize = ringYDist * 2.0 / 3.0



#    x to the right
#    y to the bottom right
    
#       / \     / \
#     /     \ /     \
#    | 0 0   |  1  0 |
#    |       |       |
#   / \     / \     / \
# /     \ /     \ /     \
#| -1,1  |  0,1  |  1,1  |
#|       |       |       |
# \     / \     / \     /
#   \ /     \ /     \ /
#    | -1,2  |  0,2  |
#    |       |       |
#     \     / \     /
#       \ /     \ /

ringRight = (ringXDist, 0)
ringLeft = (-ringXDist, 0)
ringTopLeft = (-ringXOffset, -ringYDist)
ringTopRight = (+ringXOffset, -ringYDist)
ringBottomLeft = (-ringXOffset, ringYDist)
ringBottomRight = (+ringXOffset, ringYDist)
neighbours = [ringTopRight, ringRight, ringBottomRight, ringBottomLeft, ringLeft, ringTopLeft]

neighboursHexX = [1,  1,  0, -1, -1,  0]
neighboursHexY = [-1, 0,  1,  1,  0, -1]

debugViewRefreshInterval = 3

def initSounds():
    pygame.mixer.pre_init(44100, -16, 2, 1024)
    pygame.init()
    pygame.mixer.set_num_channels(64)
    # pass


def resetAudio():
    playSound('audio/loss.wav')
    pygame.mixer.stop()
    pygame.mixer.quit()
    #pygame.time.wait(10)
    pygame.mixer.pre_init(44100, -16, 1, 1024)
    pygame.mixer.init()
    pygame.mixer.set_num_channels(64)

def playSound(sound):
    # clackSound = pygame.mixer.Sound(file='C:\coding\arduino\Springfield\_python\Transmitter\Break3.wav')
    pygame.mixer.Sound(file=sound).play()
    # winsound.PlaySound(sound, winsound.SND_ASYNC | winsound.SND_NOSTOP | winsound.SND_FILENAME)
    # clackSound.play()
    pass

def initEnvironment():
    global myfont, screen, myfont2
    initSounds()
    BLACK = (0,0,0)
    WIDTH = 640
    HEIGHT = 480
    pygame.font.init()
    screen = pygame.display.set_mode((WIDTH, HEIGHT), 0, 32)
    myfont = pygame.font.SysFont('monofur', 30)
    myfont2 = pygame.font.SysFont('monofur',15)
    # screen.fill((55,55,55))
    screen.fill((0,0,0))
    textsurface = myfont.render('Some Text', False, (255, 0, 0))
    screen.blit(textsurface,(0,0))
    pygame.display.update() 
    # pygame.mixer.Sound(file='Break3.wav').play()
    # clackSound = pygame.mixer.Sound(file='C:\coding\arduino\Springfield\_python\Transmitter\Break3.wav')
    # playSound('Break3.wav')

def shutdown():
    print "goodbye!"
    pygame.quit()
    sys.exit()

def drawText(text):
    textsurface = myfont.render(text, False, (255, 0, 0))
    screen.blit(textsurface,(0,0))
    pygame.display.update() 

def sendData(counter, identification = False):
    global ser, state, leds, debugViewRefreshInterval
    if not ser.is_open:
        print "Error: Serial not open"
        state = WAITFORCONNECT
        return -1
    tstart = timeit.default_timer()
    headerValue = 1
    if counter == 0:
        headerValue = 2
    if identification:
        headerValue = 3
    s = [headerValue if i == 0 else 1 for i in range(0, 6)]
    # print s
    # for i in xrange(0,2):
    #     if (i < 2): 
    #         s[3 * i] = 1 if counter > 0 else 2
    #         s[3 * i+1] = 1
    #         s[3 * i+2] = 1
        # else:
        #     # colorsys.rgb_to_hsv(0.2, 0.4, 0.4)
        #     #col = colorsys.hsv_to_rgb((i+counter) / 192.0, 1, 0.5)
        #     col = (0, 0.05, 0)
        #     if (abs((counter) % 576 - i) < 16): col = (1, 0, 0)
        #     s[3 * i] = col[0] * 255
        #     s[3 * i+1] = col[1] * 255
        #     s[3 * i+2] = col[2] * 255

    #print "sending data: ", map(str,  map(int, s))
    try:
        bytesWritten = ser.write(b"".join(map(str, map(chr, map(int, s)))) + b"".join(map(str, map(chr, map(int, leds)))))
    except:
        print "Connection lost, trying to reconnect ...."
        state = WAITFORCONNECT
    if (counter % debugViewRefreshInterval == 0 and counter > 0):
        drawCoordinates()

    #print "sent", bytesWritten, "bytes, which took ", (timeit.default_timer() - tstart)*1000, "ms, throughput:", (1/(timeit.default_timer() - tstart))*bytesWritten/1024, "kb/s"
    #print "in waiting:", ser.in_waiting
    #if ser.in_waiting > 0: print ser.read(ser.in_waiting)
    #print ser.readline()

class Ring:
    cell = None
    def __init__(self, x, y, angle, firstLEDindex, neighbours):
        self.x = x
        self.y = y
        self.hexX = 0 # initialized at finding neighbours routine
        self.hexY = 0
        self.angle = angle
        self.firstLEDindex = firstLEDindex
        self.neighbours = neighbours

    def convertRealCoordsIntoHex(self):
        reducedX = self.x / ringXDist
        reducedY = self.y / ringYDist
        q = (self.x * math.sqrt(3.0)/3.0 - self.y / 3.0) / ringHexSize
        r = self.y * 2.0/3.0 / ringHexSize
        print "Real coords:", self.x, self.y, "converted:", q,r, "rounded", round(q), round(r)
        self.hexX = int(round(q))
        self.hexY = int(round(r))


class Pixel:
    distances = []
    def __init__(self, x, y, ringId):
        self.x = x
        self.y = y
        self.ringId = ringId

pixels = []
rings = []

def hexToReal(hexX, hexY):
    x = hexX * ringXDist + hexY * ringXOffset
    y = hexY * ringYDist
    return (x, y)

def drawLEDsOnRing(ringId, centerAngle, angleRange, col):
    ringAngle = rings[ringId].angle
    for x in range(16):
        angle = (ringAngle + x * 22.5) % 360
        if abs(angle - centerAngle) < angleRange:
            setPixel(rings[ringId].firstLEDindex + x, col)

def getRingDist(r1, r2, offsetTupel):
    return math.sqrt((rings[r1].x - (rings[r2].x + offsetTupel[0]))*(rings[r1].x - (rings[r2].x + offsetTupel[0])) + (rings[r1].y - (rings[r2].y + offsetTupel[1]))*(rings[r1].y - (rings[r2].y + offsetTupel[1])))

def drawCoordinates():
    global screen, pixels, rings, springData
    xoffset = 200
    yoffset = 100
    screen.fill((0,0,0))
    for i, p in enumerate(pixels):
        # pygame.draw.circle(screen, (255,255,255), (int(p.x) + xoffset, int(p.y) + yoffset), 2)
        # pygame.draw.circle(screen, (leds[i * 3], leds[i * 3 + 1], leds[i * 3 + 2]), (int(p.x) + xoffset, int(p.y) + yoffset), 2)
        r = leds[i * 3]
        g = leds[i * 3 + 1]
        b = leds[i * 3 + 2]
        if r > 1: r = min(255, 3*r + 100)
        if g > 1: g = min(255, 3*g + 100)
        if b > 1: b = min(255, 3*b + 100)
        screen.fill((r,g,b), (int(p.x) + xoffset, int(p.y) + yoffset, 3, 3))

    if len(springData) == len(rings):
        for i, r in enumerate(rings):
            d = springData[i]
            if d > 0:
                pygame.draw.circle(screen, (255,255,255), (int(r.x) + xoffset, int(r.y) + yoffset), d + 2)

            # coordinates

            # textsurface = myfont2.render('' + str(rings[i].hexX) + ' ' + str(rings[i].hexY), False, (255, 255, 255))
            textsurface = myfont2.render('' + str(i), False, (255, 255, 255))
            screen.blit(textsurface,(int(r.x) + xoffset - textsurface.get_width() / 2, int(r.y) + yoffset - textsurface.get_height() / 2))

    pygame.display.update()

def distance(p0, p1):
    return math.sqrt((p0[0] - p1[0])**2 + (p0[1] - p1[1])**2)

def getClosestSpring(coords):
    xoffset = 200
    yoffset = 100
    smallestDist = 10000;
    smallestDistRing = -1;
    for i, r in enumerate(rings):
        dist = distance(coords, (r.x + xoffset, r.y + yoffset))
        if dist < smallestDist:
            smallestDist = dist
            smallestDistRing = i

    return smallestDistRing

def getClosestSpringHex(hx, hy):
    for i, r in enumerate(rings):
        if r.hexX == hx and r.hexY == hy:
            return i

    return -1


# storing distances of pixels:
# worst: calculate it each time
# 
# other ways: cache distance calcs
#   for each pixel, save sorted distances to all (or nearest n) pixels
#   for one ring, for each of the 16 pixels, save sorted distances to all other pixels. then translate for actual ring

def populateDistanceGrid():
    for i, p in enumerate(pixels):
        m = [(i2, map(distance, ((p.x, p.y), (p2.x, p2.y)))) for i2, p2 in enumerate(pixels)]


def initSimulation():
    global state, springData
    print "Initializing Simulation!"
    receiveCoordinates(False, True)
    state = SIMULATEDRUNNING
    springData = [0 for x in range(len(rings))]
    touchData = [0 for x in range(len(rings))]
    startFunct()

def receiveCoordinates(saveToFile = True, readFromFile = False):
    global ser, pixels, rings, touchData

    file = None
    if saveToFile:
        file = open("springcoordinates.txt", "w")
    
    if readFromFile:
        file = open("springcoordinates.txt", "r")
        dataAvailable = True
    else:
        dataAvailable = ser.in_waiting > 0


    while dataAvailable:
        if readFromFile:
            line = file.readline()
            dataAvailable = line != ""
        else:
            line = ser.readline()

        if saveToFile:
            file.write(line)
        #print "coords received:", line
        spl = line.split()
        try:
            if len(spl) == 3:
                pixels.append(Pixel(float(spl[0]), float(spl[1]), int(spl[2])))
            elif len(spl) == 4:
                rings.append(Ring(float(spl[0]), float(spl[1]), int(spl[2]), int(spl[3]), [-1 for x in range(0, 6)]))
        except: 
            print "RcvCoordinate error at line", line
            pixels = []
            rings = []
            if not readFromFile:
                ser.reset_input_buffer()
            return

        if not readFromFile:
            dataAvailable = ser.in_waiting > 0  


    print "received", len(pixels), "coordinates and", len(rings), "rings."
    if len(pixels) == 0:
        print("Not enough :(")
        shutdown()

    touchData = [0 for x in range(len(rings))]

    if saveToFile:
        print "Saved coordinates to", file
        file.close()

    drawCoordinates()
    # populate ring neighbours
    for i1, ring in enumerate(rings):

        rings[i1].convertRealCoordsIntoHex()

        for offsetIndex, offset in enumerate(neighbours):
            for i2, ring2 in enumerate(rings):
                if i1 != i2 and getRingDist(i2, i1, offset) < 1:
                    rings[i1].neighbours[offsetIndex] = i2
                    break

    # create automatic border lines
    addAutomaticBorders()

def setOrientedPixelsOnRing(ringId, colorArray):
    if ringId == -1: return
    offset = 0
    ringAngle = rings[ringId].angle  
    # 0 60 120 180 240 300
    # 0  3   5   8  11  13

    ledOffset = ringAngle * 16 / 6
    for i, c in enumerate(colorArray):
        ledIndex = rings[ringId].firstLEDindex + (i + ledOffset) % 16
        setPixel(ledIndex, c)


def lerpPixelsOnRing(hexX, hexY, hex2X, hex2Y, lerp, colorArray):
    if lerp == 0:
        setOrientedPixelsOnRing(getClosestSpringHex(hexX, hexY), colorArray)
        return

    if lerp == 1:
        setOrientedPixelsOnRing(getClosestSpringHex(hex2X, hex2Y), colorArray)
        return        

    (r1x, r1y) = wf.hexToReal(hexX, hexY)
    (r2x, r2y) = wf.hexToReal(hex2X, hex2Y)
    # calculate center point
    lerpCenterX = lerp * r2x + (1 - lerp) * rx
    lerpCenterY = lerp * r2y + (1 - lerp) * ry

    r1 = getClosestSpringHex(hexX, hexY)
    if r1 != -1:
        for i in range(16):
            ledAngle = (rings[r1].angle + i * 22.5) % 360
            lx = math.sin(math.radians(ledAngle)) * ringRadius
            ly = math.cos(math.radians(ledAngle)) * ringRadius
            dist = math.sqrt((lerpCenterX - lx) ** 2 + (lerpCenterY - ly) ** 2)
            if dist < ringRadius:
                angle = math.degrees(math.atan2(ly - lerpCenterY, lx - lerpCenterX))
                
                # assume colorArray starts at 0 degrees (up)
                ledIndex = int(round(angle / 22.5)) % 16
                setPixel(rings[r1].firstLEDindex + i, colorArray[angle])
                


def lerpPixelsOnRing(hexX, hexY, neighbourId, lerp, colorArray):
    hex2X = hexX + wf.neighboursHexX[neighbourId]
    hex2Y = hexY + wf.neighboursHexY[neighbourId]
    lerpPixelsOnRing(hexX, hexY, hex2X, hex2Y, lerp, colorArray)

# lerp move ring of colours from ringId to neighbourId
def lerpPixelsOnRing(ringId, neighbourId, lerp, colorArray):
    if ringId == -1: return
    lerpPixelsOnRing(rings[ringId].hexX, rings[ringId].hexY, neighbourId, lerp, colorArray)


def setPixelsOnRing(ringId, colorArray):
    if ringId == -1: return
    for i, c in enumerate(colorArray):
        setPixel(rings[ringId].firstLEDindex + i, c)

def lightUpRing(ringId, color):
    if ringId == -1: return
    for x in xrange(0,16):
        setPixel(rings[ringId].firstLEDindex + x, color)


def addRing(ringId, color):
    if ringId == -1: return
    for x in xrange(0,16):
        addPixel(rings[ringId].firstLEDindex + x, color)

def getRandomLEDonRing(ringId):
    return rings[ringId].firstLEDindex + random.randint(0, 15)

def simulateTouch(coords):
    xoffset = 200
    yoffset = 100
    touchMaxDist = 35
    for i, r in enumerate(rings):
        dist = distance(coords, (r.x + xoffset, r.y + yoffset))
        if dist < touchMaxDist:
            if touchData[i] <= 0:
                # now touched
                touchData[i] = time.time();
        else:
            if touchData[i] > 0:
                touchData[i] = -time.time();


def receiveTouches():
    global serTouch, touchData
    if not serTouch.is_open:
        print "Error: Serial Touch not open"
        state = WAITFORCONNECT
        return

    if serTouch.in_waiting > 0:
        line = serTouch.readline().strip()
        if len(line) < 1: return
        if not line[0].isdigit():
            print line
            serTouch.reset_input_buffer()
            return

        spl = line.split()
        for spring in spl:
            springNo = int(spring[:-1])
            if spring[-1:] == "t":
                print "Touched", springNo
                # print touchData
                touchData[springNo] = time.time()
            if spring[-1:] == "r":
                print "Released", springNo
                touchData[springNo] = -time.time()


        serTouch.reset_input_buffer()
    try:
        serTouch.write("0")
    except:
        print "Connection lost, trying to reconnect ...."


def receiveSprings():
    global ser, springData
    if ser.in_waiting > 0:
        line = ser.read(36)
        ser.reset_input_buffer()
        springData = map(ord, line)

        # print "springs received:", springData

        #spl = line.split()
        #pixels.append((float(spl[0]), float(spl[1]), int(spl[2])))



def waitForConnection():
    global retryCounter, ser, serTouch, state, pixels
    arduino_ports = [
        p.device
        for p in serial.tools.list_ports.comports()
        if 'Teensy' in p.description
    ]

    for p in serial.tools.list_ports.comports():
        print p

    if not arduino_ports:
        print "No Arduino found"
        #screen.fill((55,55,10))
        s = "Searching." + '.'.join("" for w in range(1, retryCounter))

        drawText(s)
        pygame.time.wait(50)
        retryCounter+=1

        return
    if len(arduino_ports) > 1:
        #warnings.warn('Multiple Arduinos found')
        print "Found", len(arduino_ports), "Arduinos."

    serT1 = None
    try:
        serT1 = serial.Serial(arduino_ports[len(arduino_ports) - 1], 4000)
        pygame.time.wait(50)
        serT1.close()
        pygame.time.wait(100)
        serT1 = serial.Serial(arduino_ports[len(arduino_ports) - 1], 4000)

    except:
        print "Port found, but unavailable ..."
        pygame.time.wait(100)
        retryCounter+=1
        return

    serT2 = None
    if len(arduino_ports) >= 2:
        try:
            serT2 = serial.Serial(arduino_ports[len(arduino_ports) - 2], 4000)
            pygame.time.wait(50)
            serT2.close()
            pygame.time.wait(100)
            serT2 = serial.Serial(arduino_ports[len(arduino_ports) - 2], 4000)

        except:
            print "Port found, but unavailable ..."
            pygame.time.wait(100)
            retryCounter+=1
            return
    else:
        print "Still waiting for second arduino ..."
        pygame.time.wait(500)
        return

    print "Connection established!, baudrate: ", serT1.baudrate 
    state = CONNECTED
    # ask for identification
    ser = serT1
    while serT1.in_waiting == 0:
        sendData(0, True)
        pygame.time.wait(100)
    

    line = serT1.readline().strip()
    print "received response from first teensy:", line
    if line == "Display":
        print "!"
        # t1 is the display
        ser = serT1
        if serT2 != None: serTouch = serT2
    else:
        ser = serT2
        serTouch = serT1

    pygame.time.wait(100)

    if len(pixels) == 0: 
        pygame.time.wait(100)
        while ser.in_waiting == 0:
            print "Asking for pixel coordinates ..."
            sendData(0)
            pygame.time.wait(100)

        receiveCoordinates()
        pygame.time.wait(50)
        startFunct()
    return ser

def getSqDist(x1, y1, x2, y2):
    return (x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2)

def dimColor(color, factor):
    return (color[0] * factor, color[1] * factor, color[2] * factor)

#
    # int fract = (pos - round(pos)) * 255;
    # addPixel((int) pos, color % (255 - fract));
    # addPixel((int) pos + 1, color % fract);
def addRingFloatPixel(i, col):
    global leds
    fract = i - int(i)
    addPixel(int(i), dimColor(col, 1 - fract))
    # print "fatpixel:", i, i/16, int(i-15), int(i / 16),int((i+1) / 16), fract
    if int((i + 1) / 16) != int(i / 16):
        addPixel(int(i - 15), dimColor(col, fract))
    else:
        addPixel(int(i + 1), dimColor(col, fract))

def scale(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

def addPixel(i, col):
    global leds
    maxBrightness = 200
    csum = col[0] + col[1] + col[2]
    brightness = 255 if csum > 255 else csum
    f = scale(brightness, 0, 155, 1, 0.1)
    cr = col[0] + leds[3*i] * f
    cg = col[1] + leds[3*i + 1] * f
    cb = col[2] + leds[3*i + 2] * f
    leds[3*i] = 0 if cr < 0 else maxBrightness if cr > maxBrightness else cr
    leds[3*i + 1] = 0 if cg < 0 else maxBrightness if cg > maxBrightness else cg
    leds[3*i + 2] = 0 if cb < 0 else maxBrightness if cb > maxBrightness else int(cb)

def setPixel(i, col):
    global leds
    maxBrightness = 200
    leds[3*i] = 0 if col[0] < 0 else maxBrightness if col[0] > maxBrightness else col[0]
    leds[3*i + 1] = 0 if col[1] < 0 else maxBrightness if col[1] > maxBrightness else col[1]
    leds[3*i + 2] = 0 if col[2] < 0 else maxBrightness if col[2] > maxBrightness else int(col[2])

def setPixel2(i, col):
    global leds
    maxBrightness = 200
    leds[3*i] = min(maxBrightness, max(0, col[0]))
    leds[3*i + 1] = min(maxBrightness, max(0, col[1]))
    leds[3*i + 2] = min(maxBrightness, max(0, int(col[2])))


hsvCache = []
def hsv256buildCache():
    global hsvCache
    for h in range(256):
        vs = []
        for v in range(256):
            result = hsv256(h,255,v, True)
            vs.append(result)
        hsvCache.append(vs)


def hsv256cached(h, v):
    return hsvCache[h][v]

# from https://github.com/ManiacalLabs/BiblioPixel/wiki/HSV-Colors
def hsv256(h, s, v, buildingCache = False):
    """Generates RGB values from HSV that have an even visual
    distribution.  Be careful as this method is only have as fast as
    hsv2rgb_spectrum."""
    global hsvCache

    def nscale8x3_video(r, g, b, scale):
        nonzeroscale = 0
        if scale != 0:
            nonzeroscale = 1
        if r != 0:
            r = ((r * scale) >> 8) + nonzeroscale
        if g != 0:
            g = ((g * scale) >> 8) + nonzeroscale
        if b != 0:
            b = ((b * scale) >> 8) + nonzeroscale
        return (r, g, b)

    def scale8_video_LEAVING_R1_DIRTY(i, scale):
        nonzeroscale = 0
        if scale != 0:
            nonzeroscale = 1
        if i != 0:
            i = ((i * scale) >> 8) + nonzeroscale
        return i

    if s >= 255 and not buildingCache:
        return hsvCache[h][v]
    offset = h & 0x1F  # 0..31
    offset8 = offset * 8
    third = (offset8 * (256 // 3)) >> 8
    r, g, b = (0, 0, 0)

    if not (h & 0x80):
        if not (h & 0x40):
            if not (h & 0x20):
                r = 255 - third
                g = third
                b = 0
            else:
                r = 171
                g = 85 + third
                b = 0x00
        else:
            if not (h & 0x20):
                twothirds = (third << 1)
                r = 171 - twothirds
                g = 171 + third
                b = 0
            else:
                r = 0
                g = 255 - third
                b = third
    else:
        if not (h & 0x40):
            if not (h & 0x20):
                r = 0x00
                twothirds = (third << 1)
                g = 171 - twothirds
                b = 85 + twothirds
            else:
                r = third
                g = 0
                b = 255 - third
        else:
            if not (h & 0x20):
                r = 85 + third
                g = 0
                b = 171 - third
            else:
                r = 171 + third
                g = 0x00
                b = 85 - third

    if s != 255:
        r, g, b = nscale8x3_video(r, g, b, s)
        desat = 255 - s
        desat = (desat * desat) >> 8
        brightness_floor = desat
        r = r + brightness_floor
        g = g + brightness_floor
        b = b + brightness_floor

    if v != 255:
        v = scale8_video_LEAVING_R1_DIRTY(v, v)
        r, g, b = nscale8x3_video(r, g, b, v)

    return (r, g, b)

def hsv(h,s,v):
    if s > 0.99:
        return hsvCache[int(h * 255)][int(v * 255)]
    return hsv256(int(h * 255), int(s * 255), int(v * 255)) 

#def hsv(h, s, v):
#    return tuple(x * 255 for x in colorsys.hsv_to_rgb(h, s, v))

# Hue Saturation Value are 0.0 - 1.0
def setHSV(i, h, s, v):
    setPixel(i, hsv(h, s, v))



# def hsv(h, s, v):
#     return tuple(x * 255 for x in colorsys.hsv_to_rgb(h, s, v))

# # Hue Saturation Value are 0.0 - 1.0
# def setHSV(i, h, s, v):
#     setPixel(i, hsv(h, s, v))

def drawDisk(x, y, r, h = 0):
    global pixels
    sqMaxDist = r * r
    for i, p in enumerate(pixels):
        sqDist = getSqDist(x, y, p.x, p.y)
        if sqDist < sqMaxDist:
            setPixel(i, tuple(i * 255 for i in colorsys.hsv_to_rgb((h + (sqMaxDist - sqDist) / 5000) % 1., 1, 0.5)))
            # print colorsys.hsv_to_rgb(p[2] / 36.0, 1, 0.1) * 255

def fadeAllLeds(amount):
    leds[:] = [x * amount for x in leds]

def fadeAllLedsNP(amount):
    global leds
    leds *= amount  # with numpy

def startGame():
    pass

def keyboardHandler(events):
    pass

def initialize(externalStartGame, externalLoop, externalKBHandler):
    global startFunct, runFunct, keyboardFunct
    startFunct = externalStartGame
    runFunct = externalLoop
    keyboardFunct = externalKBHandler
    initEnvironment()


def getSpringData():
    return springData

def getTouchData():
    return touchData

def getWobbleData():
    return None

def getAccelData():
    return None

def getRings():
    return rings

def getPixels():
    return pixels

def runGame():
    pass

runFunct= runGame
startFunct = runGame
keyboardFunct = keyboardHandler

def hesseNormal(x1, y1, x2, y2):
    nx = y1 - y2
    ny = x2 - x1
    if nx * x1 + ny * y1 < 0: #to satisfy (x1 y1) * n > 0
        nx = y2 - y1
        ny = x1 - x2

    nlength = math.sqrt(nx * nx + ny * ny)
    if nlength == 0:
        print "hesse points too close together to form a line", x1, y1, x2, y2
        return
    n0x = nx / nlength
    n0y = ny / nlength

    d = x1 * n0x + y1 * n0y

    if d == 0 and (n0x + n0y) < 0:
        n0x *= -1
        n0y *= -1
    # print "pre-hesse:", nx, ny, nlength
    # print "hesse line: ", n0x, n0y, d
    return [n0x, n0y, d]

lines = []

class Line:
    def __init__(self, x1, y1, x2, y2, isGoal = False):
        self.n0x, self.n0y, self.d = hesseNormal(x1, y1, x2, y2)
        print "added line: ", self.n0x, self.n0y, self.d
        self.isGoal = isGoal

    def dist(self, x, y):
        return self.n0x * x + self.n0y * y - self.d

    def reflect(self, x, y):
        d = dist(x, y)
        return x - 2* self.n0x * d, y - 2 * self.n0y * d, d

    def reflectVector(self, vx, vy):
        d0 = self.n0x * vx + self.n0y * vy
        return vx - 2* self.n0x * d0, vy - 2 * self.n0y * d0

    def isSimilar(self, n0x, n0y, d, eps = 0.1):
        return abs(self.n0x - n0x) < eps and abs(self.n0y - n0y) < eps and abs(self.d - d) < eps 

def addLine(x1, y1, x2, y2):
    lines.append(Line(x1, y1, x2, y2))

def addLineIfNew(x1, y1, x2, y2):
    nx, ny, d = hesseNormal(x1, y1, x2, y2)
    for l in lines:
        if l.isSimilar(nx, ny, d): return
    lines.append(Line(x1, y1, x2, y2))

def addAutomaticBorders():
    # add lines around the outside of the hex
    for r in rings:
        neighborCount = sum([i >=0 for i in r.neighbours])
        if neighborCount < 6:
            for n in r.neighbours:
                if n == -1: continue
                r2 = rings[n]
                neighborCount2 = sum([i >= 0 for i in r2.neighbours])
                if neighborCount2 < 6:
                    addLineIfNew(r.x, r.y, r2.x, r2.y)
        

def testGame():
    global screen
    line = Line(100, 100, 200, 300)
    pygame.draw.line(screen, (255,255,255), (100,100),(200,300),2)
    pygame.draw.circle(screen, (255,255,0), (0,0), 10)
    print "test"
    pygame.draw.circle(screen, (0,255,0), (200,200), 44)
    print "dist f line:", line.dist(200, 200)
    pygame.draw.circle(screen, (0,255,0), (50,300), 10)
    print "dist f line:", line.dist(50, 300)

def saveStats(pr):
    print "Finished profiling."
    profilingTimeout = -1
    pr.disable()
    pr.dump_stats('stats')

    with open('perfstats/'+platform.node()+'___' + '{date:%Y%m%d_%H%M%S}'.format( date=datetime.datetime.now() ) + '.txt', 'w+') as stream:
        p = pstats.Stats('stats', stream=stream)
        p.strip_dirs().sort_stats('time').print_stats(.2)

        p.strip_dirs().sort_stats('time').print_callers(.2)

def bench():
    global ledsNP, leds
    start = time.time()
    leds[:] = [100 for x in leds]
    ledsNP[:] = [100 for x in ledsNP]
    print ledsNP

    for x in xrange(1,10000):
        #ledsNP *= 1.1
        rv = random.random() * 2
        leds[:] = [x * rv for x in leds]
        #for i,x in enumerate(leds):
        #    leds[i] = x * rv
        
    print ledsNP
    print "Execution time:", (time.time() - start)


def run():
    global debugViewRefreshInterval, springData, updatePyGameVisuals, currentlyProfiling, profilingTimeout, profilingFrameCount
    if USE_PROFILER and currentlyProfiling:
        pr = cProfile.Profile()
        pr.enable()
    counter = 1
    fps = 0
    hsv256buildCache()
    start_time = time.time()
    while True:
        if state == SIMULATEDRUNNING:
            #reset simulated wobble data
            #springData = [0 for x in range(len(rings))]
            springData = [max(0, x - 1) for x in springData]


        events = pygame.event.get()
        keyboardFunct(events)
        for event in events:
            if event.type == KEYUP:
                if event.key == K_b:
                    bench()
                if event.key == K_p:
                    if USE_PROFILER:
                        if currentlyProfiling:
                            pr.disable()
                            pr.dump_stats('stats')
                            # pr.print_stats(sort='time')
                            print "Stopped Profiling."
                        else:
                            print "Started Profiling"
                            pr = cProfile.Profile()
                            profilingFrameCount = 0
                            pr.enable()
                            profilingTimeout = -1
                        currentlyProfiling = not currentlyProfiling
                            
                if event.key == K_o:
                    if USE_PROFILER:
                        if currentlyProfiling:
                            saveStats(pr)
                            # pr.print_stats(sort='time')
                            print "Stopped Profiling."
                        else:
                            print "Started 10s Profiling"
                            pr = cProfile.Profile()
                            pr.enable()
                            profilingFrameCount = 0
                            profilingTimeout = time.time() + 10
                        currentlyProfiling = not currentlyProfiling
                            
                if event.key == K_v:
                    updatePyGameVisuals =  not updatePyGameVisuals
                    
            if event.type == QUIT:
                if USE_PROFILER and currentlyProfiling:
                    pr.disable()
                    pr.dump_stats('stats')
                    print "Stopped Profiling."
                    # pr.print_stats(sort='time')
                shutdown()
            # when simulating, mouse clicks are spring wobbles
            if state == SIMULATEDRUNNING:
                ring = getClosestSpring(pygame.mouse.get_pos())
                if event.type == MOUSEBUTTONDOWN:
                    print "mouse pressed ", pygame.mouse.get_pos()
                    if ring != -1:
                        springData[ring] = 10
                        print "nearest ring: ", ring

                #mouse pos for touch
                simulateTouch(pygame.mouse.get_pos())

        
        if pygame.mouse.get_focused():
            debugViewRefreshInterval = 3
        else:
            debugViewRefreshInterval = 15



        if state == WAITFORCONNECT:
            waitForConnection()
            #testGame()
        elif state == CONNECTED:
            sendData(counter)
            receiveSprings()
            receiveTouches()
            # runGame()
            runFunct()
            counter+=1
            fps+=1
        elif state == SIMULATED:
            initSimulation()
        elif state == SIMULATEDRUNNING:
            if updatePyGameVisuals:
                drawCoordinates()
            runFunct()
            if updatePyGameVisuals:
                pygame.time.wait(1)
            counter+=1
            fps+=1

            profilingFrameCount+=1

        if (time.time() - start_time) > 1 :
            print "FPS:", fps
            if fps < 145:
                setPixel(200, (10, 5, 0))
            else:
                setPixel(200, (0, 10, 0))
            fps = 0
            start_time = time.time()

        if currentlyProfiling and profilingTimeout > 0 and time.time() > profilingTimeout:
            saveStats(pr)
            print "total frames:", profilingFrameCount

            currentlyProfiling = False
