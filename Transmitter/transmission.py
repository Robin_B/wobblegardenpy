from wobbleforest import wobbleforest as wf
import time
import random
import colorsys
import math
import pygame
from pygame.locals import *

#################################################### Area Control ##########################################

areas = []
areaOwners = 3

class Area:
    currentColor = [0,0,0]

    def __init__(self, ringId, areaOwner = -1):
        self.ringId = ringId
        self.areaOwner = areaOwner
        self.influence = [0 for a in range(areaOwners)]
        self.firstLEDindex = wf.rings[ringId].firstLEDindex

        self.lastWobble = 0
        rings[ringId].area = self

    def update(self):
    	global dt
        #recalculate owner
        maximum = max(self.influence)
        if maximum > 1:
        	bestOwner = self.influence.index(maximum)  # this only finds the first index with that value
        	if bestOwner != self.areaOwner:
        		print "Owner of", self.ringId, "changed from", self.areaOwner, "to", bestOwner
        		self.areaOwner = bestOwner

        if maximum < 0.01:
        	self.areaOwner = -1

        #decay influence
        decay = (1 - dt)
        self.influence[:] = [i * decay for i in self.influence]   # in-place replacement 

    def draw(self):
        global gameTime, tick
        if self.areaOwner == -1:
        	return
        for i in range(16):
            # wf.setPixel(self.firstLEDindex + i, self.currentColor)
            wf.setHSV(self.firstLEDindex + i, self.areaOwner / float(areaOwners), 1, 0.2)

def addInfluence(ringId, ownerId, amount):
	rings[ringId].area.influence[ownerId] += amount

def hitArea(ringId, amount):
	a = rings[ringId].area
	if a != None and a.areaOwner > -1:
		a.influence[a.areaOwner] += 6
		for n in rings[ringId].neighbours:
			addInfluence(n, a.areaOwner, 1)

def addArea(ringId):
	areas.append(Area(ringId))

def updateAreas():
	for a in areas:
		a.update()

def drawAreas():
	for a in areas:
		a.draw()

#################################################### Effects ###############################################
effects = []

class Effect:
    active = True
    limitToWater = False

    def __init__(self, x, y, speed, color, maxRadius, bandSize = -1):
        self.x = x
        self.y = y
        self.startTime = time.time()
        self.speed = speed
        self.color = color
        self.maxRadius = maxRadius
        self.bandSize = bandSize
        self.active = True
        # TODO: cleanup runners once in a while

    def draw(self):
        if not self.active: return
        currentR = (time.time() - self.startTime) * self.speed
        if currentR > self.maxRadius + self.bandSize:
            self.active = False
            return
        currentSqR = currentR * currentR
        maxR = self.maxRadius * self.maxRadius
        sqBand = self.bandSize * self.bandSize
        skipped = 0
        i = 0
        while i < len(pixels):
            p = pixels[i]
            sqDist = wf.getSqDist(self.x, self.y, p.x, p.y)
            if i % 16 == 0 and (sqDist > currentSqR + 1450*10 or sqDist < currentSqR - sqBand - 1450*2):
                #skip the entire ring
                skipped+=1
                i+=16
                continue
            if sqDist < min(maxR, currentSqR) and (self.bandSize == -1 or sqDist > currentSqR - sqBand) :
                wf.addPixel(i, wf.dimColor(self.color, 1.1 - min(1, (currentSqR - sqDist)/20000)))
            i+=1
        # print "skipped rings:", skipped


def addEffect(x, y, speed, color, maxRadius, bandSize = -1):
    for e in effects:
        if not e.active:
            e.x = x
            e.y = y
            e.speed = speed
            e.color = color
            e.maxRadius = maxRadius
            e.bandSize = bandSize
            e.startTime = time.time()
            e.active = True
            return
    effects.append(Effect(x, y, speed, color, maxRadius, bandSize))

def drawEffects():
    for e in effects:
        e.draw()


#################################################### Main ###############################################


gameTime = 0

pixels = wf.getPixels()
rings = wf.getRings()
tick = 0

def drawBackground():
    wf.fadeAllLeds(0.9)

def getAreaOwner(i):
    if rings[i] != None and rings[i].area != None:
        return rings[i].areaOwner
    return None

def handleInput():
    springData = wf.getSpringData()
    for index, amount in enumerate(springData):
        if amount > 0:
            #addEffect(rings[index].x, rings[index].y, speed = 350, color = wf.hsv(index/14.0 % 1.0, 1, min(0.1, amount / 10)), maxRadius = 100, bandSize = 20)
            addEffect(rings[index].x, rings[index].y, speed = 550, color = wf.hsv(index/14.0 % 1.0, 1, 0.1), maxRadius = 20, bandSize = 170)
            print index, amount
            hitArea(index, amount)

def keyboardHandler(events):
    for event in events:
        if event.type == KEYUP:
            if event.key == K_e:
                addEffect(100, 100, 350, (255, 0, 0), 450, 170)

def loop():
    global dtPrevious, dt, tick
    dt = time.time() - dtPrevious
    dtPrevious = time.time()
    tick += 1

    updateAreas()

    drawBackground()
    handleInput()
    drawAreas()
    drawEffects()

def startGame():
    global gameTime, dtPrevious
    gameTime = time.time()    
    for i, r in enumerate(rings):
    	addArea(i)
    addInfluence(0, 0, 100)
    addInfluence(10, 1, 100)
    addInfluence(20, 2, 100)

    dtPrevious = time.time()




def init():
    wf.initialize(startGame, loop, keyboardHandler)
    wf.run()
init()

