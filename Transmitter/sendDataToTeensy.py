import warnings
import serial
import time
import serial.tools.list_ports

def waitForConnection():
	while True:
		arduino_ports = [
		    p.device
		    for p in serial.tools.list_ports.comports()
		    if 'Teensy' in p.description
		]

		for p in serial.tools.list_ports.comports():
			print p

		if not arduino_ports:
			print "No Arduino found"
			time.sleep(1)
			continue
		if len(arduino_ports) > 1:
		    warnings.warn('Multiple Arduinos found - using the first')

		ser = serial.Serial(arduino_ports[0], 4000000)
		print "Connection established!" 
		return ser;
		break;




waitForConnection()