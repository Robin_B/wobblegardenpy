from wobbleforest import wobbleforest as wf
import time
import random
import colorsys
import math
import pygame
from pygame.locals import *

#################################################### Runner ###############################################
automata = []
automataTick = 0
automataLastTick = 0
tickRate = 0.5 # per second

class Automata:
    active = True

    def __init__(self, color, ringId, direction, active = True):
        self.i = len(automata)
        self.color = color
        self.ringId = ringId
        self.direction = direction
        self.x = rings[ringId].x
        self.y = rings[ringId].y
        self.dx = math.cos(self.direction * math.pi / 3) * wf.ringXDist
        self.dy = math.sin(self.direction * math.pi / 3) * wf.ringXDist
        self.active = active
        self.new = True

    def update(self):
        global dt
        if not self.active: return

    def eventTick(self, wallsHit):
        if wallsHit == 0:
            pass
        elif wallsHit == 1:
            pass
        elif wallsHit == 2: # corner
            pass

        if wallsHit > 0:
            addEffect(rings[self.ringId].x, rings[self.ringId].y, speed = 550, color = self.color, maxRadius = 50, bandSize = 170)


    def eventCollision(self, other):
        addEffect(rings[self.ringId].x, rings[self.ringId].y, speed = 550, color = wf.hsv(self.ringId/14.0 % 1.0, 1, 0.1), maxRadius = 200, bandSize = 170)
        

    def eventSpawn(self):
        pass

    def checkCollision(self):
        for index in xrange(self.i + 1, len(automata)):
            if self.ringId == automata[index].ringId:
                self.eventCollision(automata[index])

    def updateTick(self):
        global automataTick
        if not self.active: return
        if self.new:
            self.eventSpawn()
            self.new = False
    
        r = rings[self.ringId]
        if r.neighbours[self.direction] == -1:
            # hit wall!
            if r.neighbours[(self.direction + 1) % 6] == -1 and r.neighbours[(self.direction - 1) % 6] == -1:
                self.direction = (self.direction + 3) % 6
                self.eventTick(2)
                if r.neighbours[self.direction] == -1:
                    print "error: reflection failed"
            elif r.neighbours[(self.direction + 1) % 6] == -1:
                self.direction = (self.direction - 2) % 6
                self.eventTick(1)
                if r.neighbours[self.direction] == -1:
                    print "error: reflection failed"
            elif r.neighbours[(self.direction - 1) % 6] == -1:
                self.direction = (self.direction + 2) % 6
                self.eventTick(1)
                if r.neighbours[self.direction] == -1:
                    print "error: reflection failed"
        else:
            self.eventTick(0)
        self.ringId = r.neighbours[self.direction]

    def draw(self):
        if not self.active: 
            return
        for i in range(16):
            wf.setPixel(rings[self.ringId].firstLEDindex + i, self.color)

def addAutomat(color, ringId, direction):
    # check if there's one here already
    for a in automata:
        if a.ringId == ringId:
            return
    automata.append(Automata(color, ringId, direction))

def updateAutomata():
    global automataLastTick, tickRate
    if time.time() - automataLastTick > tickRate:
        automataLastTick = time.time()
        updateAutomataTicks()

def updateAutomataTicks():
    global automataTick
    automataTick += 1
    for a in automata:
        a.updateTick()

    #check collisions
    for a in automata:
        a.checkCollision()

def drawAutomata():
    for a in automata:
        a.draw()


#################################################### Effects ###############################################
effects = []

class Effect:
    active = True
    limitToWater = False

    def __init__(self, x, y, speed, color, maxRadius, bandSize = -1):
        self.x = x
        self.y = y
        self.startTime = time.time()
        self.speed = speed
        self.color = color
        self.maxRadius = maxRadius
        self.bandSize = bandSize
        self.active = True
        # TODO: cleanup runners once in a while

    def draw(self):
        if not self.active: return
        currentR = (time.time() - self.startTime) * self.speed
        if currentR > self.maxRadius + self.bandSize:
            self.active = False
            return
        currentSqR = currentR * currentR
        maxR = self.maxRadius * self.maxRadius
        sqBand = self.bandSize * self.bandSize
        skipped = 0
        i = 0
        while i < len(pixels):
            p = pixels[i]
            sqDist = wf.getSqDist(self.x, self.y, p.x, p.y)
            if i % 16 == 0 and (sqDist > currentSqR + 1450*10 or sqDist < currentSqR - sqBand - 1450*2):
                #skip the entire ring
                skipped+=1
                i+=16
                continue
            if sqDist < min(maxR, currentSqR) and (self.bandSize == -1 or sqDist > currentSqR - sqBand) :
                wf.addPixel(i, wf.dimColor(self.color, 1.1 - min(1, (currentSqR - sqDist)/20000)))
            i+=1
        # print "skipped rings:", skipped


def addEffect(x, y, speed, color, maxRadius, bandSize = -1):
    for e in effects:
        if not e.active:
            e.x = x
            e.y = y
            e.speed = speed
            e.color = color
            e.maxRadius = maxRadius
            e.bandSize = bandSize
            e.startTime = time.time()
            e.active = True
            return
    effects.append(Effect(x, y, speed, color, maxRadius, bandSize))

def drawEffects():
    for e in effects:
        e.draw()


#################################################### Main ###############################################


gameTime = 0

pixels = wf.getPixels()
rings = wf.getRings()
tick = 0

def drawBackground():
    wf.fadeAllLeds(0.95)

def getAreaOwner(i):
    if rings[i] != None and rings[i].area != None:
        return rings[i].areaOwner
    return None

def handleInput():
    springData = wf.getSpringData()
    for index, amount in enumerate(springData):
        if amount > 0:
            #addEffect(rings[index].x, rings[index].y, speed = 350, color = wf.hsv(index/14.0 % 1.0, 1, min(0.1, amount / 10)), maxRadius = 100, bandSize = 20)
            #addEffect(rings[index].x, rings[index].y, speed = 550, color = wf.hsv(index/14.0 % 1.0, 1, 0.1), maxRadius = 20, bandSize = 170)
            addAutomat(wf.hsv(random.random(),1,1), index, random.randint(0, 5))
            
            print index, amount
            #hitArea(index, amount)

def keyboardHandler(events):
    global tickRate
    for event in events:
        if event.type == KEYUP:
            if event.key == K_e:
                addEffect(100, 100, 350, (255, 0, 0), 450, 170)
            if event.key == K_a:
                addAutomat(wf.hsv(random.random(),1,1), random.randint(0, 35), 1)

            if event.key == K_1:
                tickRate = 0.1
            if event.key == K_2:
                tickRate = 0.2
            if event.key == K_3:
                tickRate = 0.3
            if event.key == K_4:
                tickRate = 0.4
            if event.key == K_5:
                tickRate = 0.5
            if event.key == K_6:
                tickRate = 0.6
            if event.key == K_7:
                tickRate = 0.7
            if event.key == K_8:
                tickRate = 0.8
            if event.key == K_9:
                tickRate = 0.9
            if event.key == K_0:
                tickRate = 1.0
            if event.key == K_MINUS:
                tickRate = tickRate * 0.9
            if event.key == K_PLUS:
                tickRate = tickRate / 0.9

def loop():
    global dtPrevious, dt, tick
    dt = time.time() - dtPrevious
    dtPrevious = time.time()
    tick += 1

    updateAutomata()
    drawBackground()
    handleInput()
    drawAutomata()
    drawEffects()

def startGame():
    global gameTime, dtPrevious
    gameTime = time.time()    

    addAutomat(wf.hsv(random.random(),1,1), 10, 1)

    dtPrevious = time.time()




def init():
    wf.initialize(startGame, loop, keyboardHandler)
    wf.run()
init()

