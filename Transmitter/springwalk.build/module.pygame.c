/* Generated code for Python source for module 'pygame'
 * created by Nuitka version 0.5.25
 *
 * This code is in part copyright 2017 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The _module_pygame is a Python object pointer of module type. */

/* Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pygame;
PyDictObject *moduledict_pygame;

/* The module constants used, if any. */
extern PyObject *const_str_digest_0652bb03f90c2cca6cd14fe6735986b2;
static PyObject *const_str_plain_extension_ext;
extern PyObject *const_str_digest_311cff636bc9fe2337ef6b561e3a7fa9;
static PyObject *const_str_plain_SDL_VIDEO_X11_WMCLASS;
extern PyObject *const_str_plain_Rect;
extern PyObject *const_str_plain_type;
extern PyObject *const_tuple_str_chr_42_tuple;
extern PyObject *const_str_plain_event;
static PyObject *const_str_digest_2b423aeb9d85cb7f5281708f4d65ec91;
static PyObject *const_str_plain_RuntimeWarning;
extern PyObject *const_str_plain_draw;
extern PyObject *const_dict_empty;
extern PyObject *const_str_plain___getattr__;
static PyObject *const_str_plain_pickle;
static PyObject *const_str_digest_4d931959d4904cd58987bae6e017827c;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain___nonzero__;
static PyObject *const_str_plain_level;
static PyObject *const_str_plain_message;
static PyObject *const_str_digest_5eca16d15671924950b779eaf97354e8;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_warn;
static PyObject *const_str_plain_encode_string;
extern PyObject *const_str_plain_key;
static PyObject *const_str_plain_exc;
static PyObject *const_str_plain_var;
extern PyObject *const_str_digest_ec33b54dbd13f5c2292ba451b0640cb1;
static PyObject *const_str_digest_af33ac6f76b57fc1d268b00073a5299d;
static PyObject *const_str_digest_0f35c35a0eb6061c558e2354c7ddd13c;
extern PyObject *const_str_digest_80672e58a7263f3bf64f5274ab7b87ec;
static PyObject *const_str_digest_986039c828ebfac6d897c7144e2675a1;
extern PyObject *const_str_plain_numpy;
extern PyObject *const_str_plain_threads;
static PyObject *const_str_plain___rect_constructor;
extern PyObject *const_str_plain_transform;
extern PyObject *const_str_plain_math;
static PyObject *const_str_digest_efb40b5afdc4a3fe12b5b2abd84c3b2c;
static PyObject *const_str_plain___color_reduce;
static PyObject *const_str_digest_5fcbec45bc3da16c13f80a1884e17c80;
extern PyObject *const_str_digest_1ef86d6d9cfb907791755ff131616348;
static PyObject *const_str_digest_7b0fa3f5e123d1c086be16c591086ba7;
extern PyObject *const_str_plain___path__;
static PyObject *const_str_plain_ext_to_remove;
extern PyObject *const_str_plain_image;
static PyObject *const_str_digest_7bd43877639e0f19be26ff1685fb6ed8;
extern PyObject *const_str_plain_warnings;
static PyObject *const_str_plain_reason;
static PyObject *const_str_plain_fastevent;
static PyObject *const_str_digest_0e39c7d44292b1ea20c2359e567f1f73;
extern PyObject *const_str_plain_mouse;
static PyObject *const_str_plain_dx_version_string;
extern PyObject *const_str_plain_encode_file_path;
extern PyObject *const_str_digest_86c7bb6b17330427d0fab757eb020984;
extern PyObject *const_str_digest_b2e0d613f6b2aca091d775f4312aede2;
extern PyObject *const_str_plain_basename;
static PyObject *const_tuple_acb1e81c4b8a91a31fcad6506f654583_tuple;
static PyObject *const_str_plain_warn_unwanted_files;
static PyObject *const_str_plain_cdrom;
extern PyObject *const_str_plain_sndarray;
static PyObject *const_str_plain_base;
static PyObject *const_str_digest_8baa0629094b3ae0d209f2bab26a380b;
static PyObject *const_str_plain_packager_imports;
extern PyObject *const_str_digest_886429de8fdd53f8d5c9561510a627a3;
static PyObject *const_str_plain_py_to_remove;
extern PyObject *const_int_neg_1;
static PyObject *const_tuple_str_plain_x_str_plain_y_str_plain_w_str_plain_h_tuple;
extern PyObject *const_str_digest_f8b7823bdfb14e2c4cbd75b5ca5fd166;
static PyObject *const_tuple_str_plain_encode_string_str_plain_encode_file_path_tuple;
static PyObject *const_str_plain_copyreg;
static PyObject *const_str_plain_install_path;
static PyObject *const_str_digest_a1ceada0fc56fb3222d9e4c601c90860;
extern PyObject *const_str_plain_color;
extern PyObject *const_str_plain_environ;
extern PyObject *const_tuple_str_dot_tuple;
static PyObject *const_str_digest_802d97a8584e66528f40ec5b87c9e84a;
static PyObject *const_str_plain_imageext;
extern PyObject *const_str_plain_split;
extern PyObject *const_str_plain_sysfont;
static PyObject *const_str_plain_SDL_VIDEODRIVER;
static PyObject *const_str_plain_camera;
static PyObject *const_str_plain_Overlay;
extern PyObject *const_str_plain_os;
static PyObject *const_str_digest_f07bf5e644656e87f62eac4d3bcf8f50;
static PyObject *const_tuple_8a6c6c24e7b08c8211c31aeebc3a2ad2_tuple;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_plain_font;
static PyObject *const_str_digest_faf15e32c97b89cce21d7cae648a2203;
static PyObject *const_str_plain_PixelArray;
extern PyObject *const_str_plain_time;
static PyObject *const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple;
extern PyObject *const_str_plain_HKEY_LOCAL_MACHINE;
static PyObject *const_str_digest_43e023631edb4fe3adb723d1a9be5d8b;
static PyObject *const_tuple_str_plain_c_tuple;
static PyObject *const_str_plain_Missing_Function;
extern PyObject *const_str_plain___file__;
extern PyObject *const_tuple_str_plain_r_tuple;
extern PyObject *const_str_digest_436e7eb61b35d39994d896416c082efa;
extern PyObject *const_str_plain_modules;
extern PyObject *const_int_pos_4;
extern PyObject *const_int_pos_5;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_digest_df4bac290232ff9f319b12d3d498971c;
extern PyObject *const_int_pos_1;
static PyObject *const_str_digest_696ad36776747b6f48fc3f1aa9b773d0;
extern PyObject *const_str_plain___module__;
static PyObject *const_str_plain_extension_files;
extern PyObject *const_str_plain_w;
extern PyObject *const_str_digest_d174e39ef12748481d2d13702313aa6c;
static PyObject *const_str_plain_atexit;
static PyObject *const_str_digest_34991cbebc9c3c29f49037d50b5646cb;
static PyObject *const_list_str_digest_1c56da59a7e2c66861e4dcf491f01ca3_list;
static PyObject *const_str_plain_OpenGL;
extern PyObject *const_str_plain___metaclass__;
extern PyObject *const_str_plain_argv;
extern PyObject *const_str_plain_exists;
extern PyObject *const_str_plain_SysFont;
static PyObject *const_str_plain_MissingModule;
extern PyObject *const_str_plain_r;
extern PyObject *const_str_plain_mixer;
extern PyObject *const_str_plain_nt;
static PyObject *const_tuple_str_plain_geterror_str_plain_PY_MAJOR_VERSION_tuple;
static PyObject *const_str_plain___color_constructor;
static PyObject *const_str_digest_cce077e082a4881040c7eb5aaee71576;
extern PyObject *const_str_plain_surfarray;
static PyObject *const_str_plain_use;
static PyObject *const_tuple_9ccc413142d94e0a6ed4cf37ede89573_tuple;
static PyObject *const_str_plain_py_ext;
static PyObject *const_str_plain_Version;
extern PyObject *const_str_dot;
static PyObject *const_str_digest_c8f3d166477347e14e4757a9d9042089;
extern PyObject *const_str_digest_a9c9058b24a02dd5e800ad5af29701ac;
extern PyObject *const_str_plain_get_fonts;
static PyObject *const_str_digest_1fa66e4f234ef5e087341795686ac2cd;
static PyObject *const_str_plain_MissingPygameModule;
static PyObject *const_str_digest_1f66733679db725cd71591b56a11db4d;
static PyObject *const_str_digest_ef6d38a8ea87f7841d4186a2ae1508bd;
static PyObject *const_str_plain_joystick;
extern PyObject *const_str_digest_397a17452f1c53e6ae5af26f0cec775b;
extern PyObject *const_str_digest_b646956a0fd89f2f0ee5168bcdb2f2b8;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_info;
extern PyObject *const_str_digest_09071ce7627c997b2ddfbec9a930c08b;
extern PyObject *const_str_plain_path;
static PyObject *const_str_digest_d76462298f66c95b4d3ee22e9427a916;
static PyObject *const_str_plain_cursors;
extern PyObject *const_str_plain_f;
static PyObject *const_str_digest_e15e890042ab4a6b7ec8dfe0abc28e83;
static PyObject *const_list_str_plain_color_list;
extern PyObject *const_str_plain_name;
static PyObject *const_str_digest_e3755b9b9606dea316504ecae16b4f2b;
static PyObject *const_str_plain_minor_dx_version;
static PyObject *const_str_digest_d72ace371c6cc7fe9399808f16741547;
extern PyObject *const_str_digest_749448536aec7c722f6b23d71bbbb4b1;
extern PyObject *const_str_plain_ver;
static PyObject *const_str_plain_BufferProxy;
static PyObject *const_tuple_str_empty_int_0_tuple;
static PyObject *const_str_plain__NOT_IMPLEMENTED_;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_PY_MAJOR_VERSION;
static PyObject *const_str_digest_d369391a103bc2293238ec66417a6f4a;
extern PyObject *const_str_plain_h;
static PyObject *const_str_digest_6c123f9166cb99007134f43a7cb75ca9;
extern PyObject *const_str_plain__winreg;
extern PyObject *const_str_digest_18565fc6928515f154561cd2cc076c49;
extern PyObject *const_str_plain_g;
extern PyObject *const_str_plain_a;
extern PyObject *const_str_plain_b;
extern PyObject *const_str_plain_c;
static PyObject *const_str_plain_QueryValueEx;
extern PyObject *const_str_plain_x;
extern PyObject *const_str_plain_y;
static PyObject *const_tuple_str_plain_self_str_plain_var_str_plain_MissingPygameModule_tuple;
static PyObject *const_str_digest_fffc3d5116a376d1dabb110d3a2e29ab;
extern PyObject *const_str_plain_OpenKey;
static PyObject *const_str_plain_files;
static PyObject *const_str_plain_PYGAME_FREETYPE;
extern PyObject *const_tuple_str_plain_Rect_tuple;
static PyObject *const_str_plain_import;
static PyObject *const_str_plain_pixelcopy;
extern PyObject *const_str_plain_Color;
static PyObject *const_str_plain_DISPLAY;
static PyObject *const_str_plain_scrap;
extern PyObject *const_str_plain_ftfont;
static PyObject *const_str_plain_bufferproxy;
static PyObject *const_str_plain_py_files;
extern PyObject *const_str_plain_match_font;
extern PyObject *const_str_plain_splitext;
extern PyObject *const_str_plain_geterror;
extern PyObject *const_str_plain_pygame;
extern PyObject *const_str_chr_42;
static PyObject *const_str_plain_copy_reg;
static PyObject *const_str_digest_169fd7f487ca29a49c432443e40ddebf;
static PyObject *const_list_str_plain_camera_list;
static PyObject *const_str_plain_urgent;
static PyObject *const_str_digest_d1ba13dd50db3e68acb1eb6b0ed3b7bd;
extern PyObject *const_str_digest_11bb5f146c38d7c06053353ff65da84a;
static PyObject *const_str_digest_d13a293f5ac9c8a4c435ce57df702b77;
static PyObject *const_str_digest_b0cc034d9177fde48a3b44ac122139d9;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_sys;
extern PyObject *const_int_0;
static PyObject *const_str_plain_surflock;
static PyObject *const_str_digest_fb7343c4cb62afaab89b717acddf9b6f;
static PyObject *const_str_digest_1c56da59a7e2c66861e4dcf491f01ca3;
static PyObject *const_str_plain_ask_remove;
extern PyObject *const_str_plain_Close;
extern PyObject *const_str_plain___version__;
static PyObject *const_str_plain_unwanted_files;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_empty;
static PyObject *const_str_plain___rect_reduce;
static PyObject *const_str_digest_60aed3f244f0c03471c1ce952e69d963;
static PyObject *const_tuple_str_plain_Mask_tuple;
extern PyObject *const_str_plain_Surface;
extern PyObject *const_str_digest_e420c3319159a1e6d3a583b2fbaf6bd0;
extern PyObject *const_str_plain_exc_info;
extern PyObject *const_str_plain_self;
static PyObject *const_str_plain_movie;
static PyObject *const_str_digest_887953ae3c9b867d7ff6903985ec2d3c;
static PyObject *const_str_plain_directx;
static PyObject *const_str_plain_e32;
extern PyObject *const_str_plain_display;
static PyObject *const_str_digest_c5a6147046c8b4eaac4d39ca37af165a;
static PyObject *const_tuple_73b025f8ca1696071bff8cf0ee4b7ec2_tuple;
extern PyObject *const_str_plain_getwindowsversion;
extern PyObject *const_str_plain_sprite;
static PyObject *const_str_plain_Mask;
static PyObject *const_tuple_str_plain_r_str_plain_g_str_plain_b_str_plain_a_tuple;
extern PyObject *const_str_angle_lambda;
static PyObject *module_filename_obj;

static bool constants_created = false;

static void createModuleConstants( void )
{
    const_str_plain_extension_ext = UNSTREAM_STRING( &constant_bin[ 5391 ], 13, 1 );
    const_str_plain_SDL_VIDEO_X11_WMCLASS = UNSTREAM_STRING( &constant_bin[ 5404 ], 21, 1 );
    const_str_digest_2b423aeb9d85cb7f5281708f4d65ec91 = UNSTREAM_STRING( &constant_bin[ 5425 ], 51, 0 );
    const_str_plain_RuntimeWarning = UNSTREAM_STRING( &constant_bin[ 5476 ], 14, 1 );
    const_str_plain_pickle = UNSTREAM_STRING( &constant_bin[ 5490 ], 6, 1 );
    const_str_digest_4d931959d4904cd58987bae6e017827c = UNSTREAM_STRING( &constant_bin[ 5496 ], 5, 0 );
    const_str_plain_level = UNSTREAM_STRING( &constant_bin[ 1695 ], 5, 1 );
    const_str_plain_message = UNSTREAM_STRING( &constant_bin[ 5501 ], 7, 1 );
    const_str_digest_5eca16d15671924950b779eaf97354e8 = UNSTREAM_STRING( &constant_bin[ 5508 ], 29, 0 );
    const_str_plain_encode_string = UNSTREAM_STRING( &constant_bin[ 5537 ], 13, 1 );
    const_str_plain_exc = UNSTREAM_STRING( &constant_bin[ 387 ], 3, 1 );
    const_str_plain_var = UNSTREAM_STRING( &constant_bin[ 377 ], 3, 1 );
    const_str_digest_af33ac6f76b57fc1d268b00073a5299d = UNSTREAM_STRING( &constant_bin[ 5550 ], 15, 0 );
    const_str_digest_0f35c35a0eb6061c558e2354c7ddd13c = UNSTREAM_STRING( &constant_bin[ 5565 ], 50, 0 );
    const_str_digest_986039c828ebfac6d897c7144e2675a1 = UNSTREAM_STRING( &constant_bin[ 5615 ], 14, 0 );
    const_str_plain___rect_constructor = UNSTREAM_STRING( &constant_bin[ 5629 ], 18, 1 );
    const_str_digest_efb40b5afdc4a3fe12b5b2abd84c3b2c = UNSTREAM_STRING( &constant_bin[ 5647 ], 9, 0 );
    const_str_plain___color_reduce = UNSTREAM_STRING( &constant_bin[ 5656 ], 14, 1 );
    const_str_digest_5fcbec45bc3da16c13f80a1884e17c80 = UNSTREAM_STRING( &constant_bin[ 5670 ], 15, 0 );
    const_str_digest_7b0fa3f5e123d1c086be16c591086ba7 = UNSTREAM_STRING( &constant_bin[ 5685 ], 11, 0 );
    const_str_plain_ext_to_remove = UNSTREAM_STRING( &constant_bin[ 5696 ], 13, 1 );
    const_str_digest_7bd43877639e0f19be26ff1685fb6ed8 = UNSTREAM_STRING( &constant_bin[ 5709 ], 4, 0 );
    const_str_plain_reason = UNSTREAM_STRING( &constant_bin[ 5713 ], 6, 1 );
    const_str_plain_fastevent = UNSTREAM_STRING( &constant_bin[ 5719 ], 9, 1 );
    const_str_digest_0e39c7d44292b1ea20c2359e567f1f73 = UNSTREAM_STRING( &constant_bin[ 5728 ], 12, 0 );
    const_str_plain_dx_version_string = UNSTREAM_STRING( &constant_bin[ 31 ], 17, 1 );
    const_tuple_acb1e81c4b8a91a31fcad6506f654583_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_acb1e81c4b8a91a31fcad6506f654583_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_acb1e81c4b8a91a31fcad6506f654583_tuple, 1, const_str_plain_type ); Py_INCREF( const_str_plain_type );
    PyTuple_SET_ITEM( const_tuple_acb1e81c4b8a91a31fcad6506f654583_tuple, 2, const_str_plain_message ); Py_INCREF( const_str_plain_message );
    PyTuple_SET_ITEM( const_tuple_acb1e81c4b8a91a31fcad6506f654583_tuple, 3, const_str_plain_warnings ); Py_INCREF( const_str_plain_warnings );
    PyTuple_SET_ITEM( const_tuple_acb1e81c4b8a91a31fcad6506f654583_tuple, 4, const_str_plain_level ); Py_INCREF( const_str_plain_level );
    const_str_plain_warn_unwanted_files = UNSTREAM_STRING( &constant_bin[ 5740 ], 19, 1 );
    const_str_plain_cdrom = UNSTREAM_STRING( &constant_bin[ 5759 ], 5, 1 );
    const_str_plain_base = UNSTREAM_STRING( &constant_bin[ 3740 ], 4, 1 );
    const_str_digest_8baa0629094b3ae0d209f2bab26a380b = UNSTREAM_STRING( &constant_bin[ 5764 ], 9, 0 );
    const_str_plain_packager_imports = UNSTREAM_STRING( &constant_bin[ 5773 ], 16, 1 );
    const_str_plain_py_to_remove = UNSTREAM_STRING( &constant_bin[ 5789 ], 12, 1 );
    const_tuple_str_plain_x_str_plain_y_str_plain_w_str_plain_h_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_x_str_plain_y_str_plain_w_str_plain_h_tuple, 0, const_str_plain_x ); Py_INCREF( const_str_plain_x );
    PyTuple_SET_ITEM( const_tuple_str_plain_x_str_plain_y_str_plain_w_str_plain_h_tuple, 1, const_str_plain_y ); Py_INCREF( const_str_plain_y );
    PyTuple_SET_ITEM( const_tuple_str_plain_x_str_plain_y_str_plain_w_str_plain_h_tuple, 2, const_str_plain_w ); Py_INCREF( const_str_plain_w );
    PyTuple_SET_ITEM( const_tuple_str_plain_x_str_plain_y_str_plain_w_str_plain_h_tuple, 3, const_str_plain_h ); Py_INCREF( const_str_plain_h );
    const_tuple_str_plain_encode_string_str_plain_encode_file_path_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_encode_string_str_plain_encode_file_path_tuple, 0, const_str_plain_encode_string ); Py_INCREF( const_str_plain_encode_string );
    PyTuple_SET_ITEM( const_tuple_str_plain_encode_string_str_plain_encode_file_path_tuple, 1, const_str_plain_encode_file_path ); Py_INCREF( const_str_plain_encode_file_path );
    const_str_plain_copyreg = UNSTREAM_STRING( &constant_bin[ 5801 ], 7, 1 );
    const_str_plain_install_path = UNSTREAM_STRING( &constant_bin[ 5808 ], 12, 1 );
    const_str_digest_a1ceada0fc56fb3222d9e4c601c90860 = UNSTREAM_STRING( &constant_bin[ 5820 ], 12, 0 );
    const_str_digest_802d97a8584e66528f40ec5b87c9e84a = UNSTREAM_STRING( &constant_bin[ 3163 ], 3, 0 );
    const_str_plain_imageext = UNSTREAM_STRING( &constant_bin[ 5832 ], 8, 1 );
    const_str_plain_SDL_VIDEODRIVER = UNSTREAM_STRING( &constant_bin[ 5840 ], 15, 1 );
    const_str_plain_camera = UNSTREAM_STRING( &constant_bin[ 5855 ], 6, 1 );
    const_str_plain_Overlay = UNSTREAM_STRING( &constant_bin[ 5861 ], 7, 1 );
    const_str_digest_f07bf5e644656e87f62eac4d3bcf8f50 = UNSTREAM_STRING( &constant_bin[ 5868 ], 315, 0 );
    const_tuple_8a6c6c24e7b08c8211c31aeebc3a2ad2_tuple = PyTuple_New( 4 );
    const_str_plain_atexit = UNSTREAM_STRING( &constant_bin[ 6183 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_8a6c6c24e7b08c8211c31aeebc3a2ad2_tuple, 0, const_str_plain_atexit ); Py_INCREF( const_str_plain_atexit );
    PyTuple_SET_ITEM( const_tuple_8a6c6c24e7b08c8211c31aeebc3a2ad2_tuple, 1, const_str_plain_numpy ); Py_INCREF( const_str_plain_numpy );
    const_str_plain_OpenGL = UNSTREAM_STRING( &constant_bin[ 5764 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_8a6c6c24e7b08c8211c31aeebc3a2ad2_tuple, 2, const_str_plain_OpenGL ); Py_INCREF( const_str_plain_OpenGL );
    PyTuple_SET_ITEM( const_tuple_8a6c6c24e7b08c8211c31aeebc3a2ad2_tuple, 3, const_str_plain_pygame ); Py_INCREF( const_str_plain_pygame );
    const_str_digest_faf15e32c97b89cce21d7cae648a2203 = UNSTREAM_STRING( &constant_bin[ 6189 ], 17, 0 );
    const_str_plain_PixelArray = UNSTREAM_STRING( &constant_bin[ 6206 ], 10, 1 );
    const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple = PyTuple_New( 15 );
    PyTuple_SET_ITEM( const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 0, const_str_plain_install_path ); Py_INCREF( const_str_plain_install_path );
    PyTuple_SET_ITEM( const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 1, const_str_plain_extension_ext ); Py_INCREF( const_str_plain_extension_ext );
    PyTuple_SET_ITEM( const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 2, const_str_plain_ext_to_remove ); Py_INCREF( const_str_plain_ext_to_remove );
    PyTuple_SET_ITEM( const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 3, const_str_plain_py_to_remove ); Py_INCREF( const_str_plain_py_to_remove );
    PyTuple_SET_ITEM( const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 4, const_str_plain_x ); Py_INCREF( const_str_plain_x );
    const_str_plain_extension_files = UNSTREAM_STRING( &constant_bin[ 6216 ], 15, 1 );
    PyTuple_SET_ITEM( const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 5, const_str_plain_extension_files ); Py_INCREF( const_str_plain_extension_files );
    const_str_plain_py_ext = UNSTREAM_STRING( &constant_bin[ 6231 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 6, const_str_plain_py_ext ); Py_INCREF( const_str_plain_py_ext );
    const_str_plain_py_files = UNSTREAM_STRING( &constant_bin[ 6237 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 7, const_str_plain_py_files ); Py_INCREF( const_str_plain_py_files );
    const_str_plain_files = UNSTREAM_STRING( &constant_bin[ 5532 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 8, const_str_plain_files ); Py_INCREF( const_str_plain_files );
    const_str_plain_unwanted_files = UNSTREAM_STRING( &constant_bin[ 5745 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 9, const_str_plain_unwanted_files ); Py_INCREF( const_str_plain_unwanted_files );
    PyTuple_SET_ITEM( const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 10, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    const_str_plain_ask_remove = UNSTREAM_STRING( &constant_bin[ 6245 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 11, const_str_plain_ask_remove ); Py_INCREF( const_str_plain_ask_remove );
    PyTuple_SET_ITEM( const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 12, const_str_plain_message ); Py_INCREF( const_str_plain_message );
    PyTuple_SET_ITEM( const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 13, const_str_plain_warnings ); Py_INCREF( const_str_plain_warnings );
    PyTuple_SET_ITEM( const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 14, const_str_plain_level ); Py_INCREF( const_str_plain_level );
    const_str_digest_43e023631edb4fe3adb723d1a9be5d8b = UNSTREAM_STRING( &constant_bin[ 6255 ], 23, 0 );
    const_tuple_str_plain_c_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_c_tuple, 0, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    const_str_plain_Missing_Function = UNSTREAM_STRING( &constant_bin[ 6278 ], 16, 1 );
    const_str_digest_696ad36776747b6f48fc3f1aa9b773d0 = UNSTREAM_STRING( &constant_bin[ 6294 ], 15, 0 );
    const_str_digest_34991cbebc9c3c29f49037d50b5646cb = UNSTREAM_STRING( &constant_bin[ 6309 ], 14, 0 );
    const_list_str_digest_1c56da59a7e2c66861e4dcf491f01ca3_list = PyList_New( 1 );
    const_str_digest_1c56da59a7e2c66861e4dcf491f01ca3 = UNSTREAM_STRING( &constant_bin[ 5425 ], 39, 0 );
    PyList_SET_ITEM( const_list_str_digest_1c56da59a7e2c66861e4dcf491f01ca3_list, 0, const_str_digest_1c56da59a7e2c66861e4dcf491f01ca3 ); Py_INCREF( const_str_digest_1c56da59a7e2c66861e4dcf491f01ca3 );
    const_str_plain_MissingModule = UNSTREAM_STRING( &constant_bin[ 244 ], 13, 1 );
    const_tuple_str_plain_geterror_str_plain_PY_MAJOR_VERSION_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_geterror_str_plain_PY_MAJOR_VERSION_tuple, 0, const_str_plain_geterror ); Py_INCREF( const_str_plain_geterror );
    PyTuple_SET_ITEM( const_tuple_str_plain_geterror_str_plain_PY_MAJOR_VERSION_tuple, 1, const_str_plain_PY_MAJOR_VERSION ); Py_INCREF( const_str_plain_PY_MAJOR_VERSION );
    const_str_plain___color_constructor = UNSTREAM_STRING( &constant_bin[ 6323 ], 19, 1 );
    const_str_digest_cce077e082a4881040c7eb5aaee71576 = UNSTREAM_STRING( &constant_bin[ 6342 ], 16, 0 );
    const_str_plain_use = UNSTREAM_STRING( &constant_bin[ 6358 ], 3, 1 );
    const_tuple_9ccc413142d94e0a6ed4cf37ede89573_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_9ccc413142d94e0a6ed4cf37ede89573_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_9ccc413142d94e0a6ed4cf37ede89573_tuple, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_9ccc413142d94e0a6ed4cf37ede89573_tuple, 2, const_str_plain_info ); Py_INCREF( const_str_plain_info );
    const_str_plain_urgent = UNSTREAM_STRING( &constant_bin[ 6361 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_9ccc413142d94e0a6ed4cf37ede89573_tuple, 3, const_str_plain_urgent ); Py_INCREF( const_str_plain_urgent );
    PyTuple_SET_ITEM( const_tuple_9ccc413142d94e0a6ed4cf37ede89573_tuple, 4, const_str_plain_exc ); Py_INCREF( const_str_plain_exc );
    const_str_plain_Version = UNSTREAM_STRING( &constant_bin[ 6367 ], 7, 1 );
    const_str_digest_c8f3d166477347e14e4757a9d9042089 = UNSTREAM_STRING( &constant_bin[ 6374 ], 52, 0 );
    const_str_digest_1fa66e4f234ef5e087341795686ac2cd = UNSTREAM_STRING( &constant_bin[ 6426 ], 18, 0 );
    const_str_plain_MissingPygameModule = UNSTREAM_STRING( &constant_bin[ 6444 ], 19, 1 );
    const_str_digest_1f66733679db725cd71591b56a11db4d = UNSTREAM_STRING( &constant_bin[ 6463 ], 12, 0 );
    const_str_digest_ef6d38a8ea87f7841d4186a2ae1508bd = UNSTREAM_STRING( &constant_bin[ 6475 ], 12, 0 );
    const_str_plain_joystick = UNSTREAM_STRING( &constant_bin[ 6301 ], 8, 1 );
    const_str_digest_d76462298f66c95b4d3ee22e9427a916 = UNSTREAM_STRING( &constant_bin[ 6487 ], 4, 0 );
    const_str_plain_cursors = UNSTREAM_STRING( &constant_bin[ 6491 ], 7, 1 );
    const_str_digest_e15e890042ab4a6b7ec8dfe0abc28e83 = UNSTREAM_STRING( &constant_bin[ 6498 ], 11, 0 );
    const_list_str_plain_color_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_plain_color_list, 0, const_str_plain_color ); Py_INCREF( const_str_plain_color );
    const_str_digest_e3755b9b9606dea316504ecae16b4f2b = UNSTREAM_STRING( &constant_bin[ 6509 ], 11, 0 );
    const_str_plain_minor_dx_version = UNSTREAM_STRING( &constant_bin[ 70 ], 16, 1 );
    const_str_digest_d72ace371c6cc7fe9399808f16741547 = UNSTREAM_STRING( &constant_bin[ 6520 ], 10, 0 );
    const_str_plain_BufferProxy = UNSTREAM_STRING( &constant_bin[ 6530 ], 11, 1 );
    const_tuple_str_empty_int_0_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_empty_int_0_tuple, 0, const_str_empty ); Py_INCREF( const_str_empty );
    PyTuple_SET_ITEM( const_tuple_str_empty_int_0_tuple, 1, const_int_0 ); Py_INCREF( const_int_0 );
    const_str_plain__NOT_IMPLEMENTED_ = UNSTREAM_STRING( &constant_bin[ 6541 ], 17, 1 );
    const_str_digest_d369391a103bc2293238ec66417a6f4a = UNSTREAM_STRING( &constant_bin[ 6558 ], 12, 0 );
    const_str_digest_6c123f9166cb99007134f43a7cb75ca9 = UNSTREAM_STRING( &constant_bin[ 5647 ], 3, 0 );
    const_str_plain_QueryValueEx = UNSTREAM_STRING( &constant_bin[ 6570 ], 12, 1 );
    const_tuple_str_plain_self_str_plain_var_str_plain_MissingPygameModule_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_var_str_plain_MissingPygameModule_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_var_str_plain_MissingPygameModule_tuple, 1, const_str_plain_var ); Py_INCREF( const_str_plain_var );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_var_str_plain_MissingPygameModule_tuple, 2, const_str_plain_MissingPygameModule ); Py_INCREF( const_str_plain_MissingPygameModule );
    const_str_digest_fffc3d5116a376d1dabb110d3a2e29ab = UNSTREAM_STRING( &constant_bin[ 6582 ], 12, 0 );
    const_str_plain_PYGAME_FREETYPE = UNSTREAM_STRING( &constant_bin[ 6594 ], 15, 1 );
    const_str_plain_import = UNSTREAM_STRING( &constant_bin[ 5782 ], 6, 1 );
    const_str_plain_pixelcopy = UNSTREAM_STRING( &constant_bin[ 6609 ], 9, 1 );
    const_str_plain_DISPLAY = UNSTREAM_STRING( &constant_bin[ 6618 ], 7, 1 );
    const_str_plain_scrap = UNSTREAM_STRING( &constant_bin[ 6625 ], 5, 1 );
    const_str_plain_bufferproxy = UNSTREAM_STRING( &constant_bin[ 6630 ], 11, 1 );
    const_str_plain_copy_reg = UNSTREAM_STRING( &constant_bin[ 279 ], 8, 1 );
    const_str_digest_169fd7f487ca29a49c432443e40ddebf = UNSTREAM_STRING( &constant_bin[ 6641 ], 26, 0 );
    const_list_str_plain_camera_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_plain_camera_list, 0, const_str_plain_camera ); Py_INCREF( const_str_plain_camera );
    const_str_digest_d1ba13dd50db3e68acb1eb6b0ed3b7bd = UNSTREAM_STRING( &constant_bin[ 6667 ], 12, 0 );
    const_str_digest_d13a293f5ac9c8a4c435ce57df702b77 = UNSTREAM_STRING( &constant_bin[ 6679 ], 4, 0 );
    const_str_digest_b0cc034d9177fde48a3b44ac122139d9 = UNSTREAM_STRING( &constant_bin[ 5650 ], 6, 0 );
    const_str_plain_surflock = UNSTREAM_STRING( &constant_bin[ 214 ], 8, 1 );
    const_str_digest_fb7343c4cb62afaab89b717acddf9b6f = UNSTREAM_STRING( &constant_bin[ 6683 ], 59, 0 );
    const_str_plain___rect_reduce = UNSTREAM_STRING( &constant_bin[ 6742 ], 13, 1 );
    const_str_digest_60aed3f244f0c03471c1ce952e69d963 = UNSTREAM_STRING( &constant_bin[ 6755 ], 15, 0 );
    const_tuple_str_plain_Mask_tuple = PyTuple_New( 1 );
    const_str_plain_Mask = UNSTREAM_STRING( &constant_bin[ 6770 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Mask_tuple, 0, const_str_plain_Mask ); Py_INCREF( const_str_plain_Mask );
    const_str_plain_movie = UNSTREAM_STRING( &constant_bin[ 5735 ], 5, 1 );
    const_str_digest_887953ae3c9b867d7ff6903985ec2d3c = UNSTREAM_STRING( &constant_bin[ 6774 ], 18, 0 );
    const_str_plain_directx = UNSTREAM_STRING( &constant_bin[ 6792 ], 7, 1 );
    const_str_plain_e32 = UNSTREAM_STRING( &constant_bin[ 6799 ], 3, 1 );
    const_str_digest_c5a6147046c8b4eaac4d39ca37af165a = UNSTREAM_STRING( &constant_bin[ 6802 ], 14, 0 );
    const_tuple_73b025f8ca1696071bff8cf0ee4b7ec2_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_73b025f8ca1696071bff8cf0ee4b7ec2_tuple, 0, const_str_digest_802d97a8584e66528f40ec5b87c9e84a ); Py_INCREF( const_str_digest_802d97a8584e66528f40ec5b87c9e84a );
    PyTuple_SET_ITEM( const_tuple_73b025f8ca1696071bff8cf0ee4b7ec2_tuple, 1, const_str_digest_7bd43877639e0f19be26ff1685fb6ed8 ); Py_INCREF( const_str_digest_7bd43877639e0f19be26ff1685fb6ed8 );
    PyTuple_SET_ITEM( const_tuple_73b025f8ca1696071bff8cf0ee4b7ec2_tuple, 2, const_str_digest_d13a293f5ac9c8a4c435ce57df702b77 ); Py_INCREF( const_str_digest_d13a293f5ac9c8a4c435ce57df702b77 );
    const_tuple_str_plain_r_str_plain_g_str_plain_b_str_plain_a_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_r_str_plain_g_str_plain_b_str_plain_a_tuple, 0, const_str_plain_r ); Py_INCREF( const_str_plain_r );
    PyTuple_SET_ITEM( const_tuple_str_plain_r_str_plain_g_str_plain_b_str_plain_a_tuple, 1, const_str_plain_g ); Py_INCREF( const_str_plain_g );
    PyTuple_SET_ITEM( const_tuple_str_plain_r_str_plain_g_str_plain_b_str_plain_a_tuple, 2, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyTuple_SET_ITEM( const_tuple_str_plain_r_str_plain_g_str_plain_b_str_plain_a_tuple, 3, const_str_plain_a ); Py_INCREF( const_str_plain_a );

    constants_created = true;
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pygame( void )
{
    // The module may not have been used at all.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_8be58a0ea832634d7416aca5115f8ecc;
static PyCodeObject *codeobj_888f561eb46bb0575657e15bb4d113d5;
static PyCodeObject *codeobj_5d8f0b46bf818938aa1ad9fc8273f52c;
static PyCodeObject *codeobj_68ad5cffb1ffd264c465be50849008ed;
static PyCodeObject *codeobj_f1509337cc982b665a24d58434f03494;
static PyCodeObject *codeobj_0eb6abfa5b89cc7690bf6ea1efdcbaa1;
static PyCodeObject *codeobj_fefabc1607c3488e9c496958c63958f5;
static PyCodeObject *codeobj_7500321594c6bfc0890afb341857d467;
static PyCodeObject *codeobj_61b2705797fcf52d0d0432331b24a84b;
static PyCodeObject *codeobj_0d5b6e5d8fb8c07220d89866742ba372;
static PyCodeObject *codeobj_9488e2d0f026bd566833afd6d12d6f30;
static PyCodeObject *codeobj_d5ea83a9c0c823d105f3b8cd2facff47;
static PyCodeObject *codeobj_d441b2a24565a20b1747a415ce202150;
static PyCodeObject *codeobj_8fcae91c12da0870d2b84d1df9e1f101;
static PyCodeObject *codeobj_d6e9e297db4118c3f15c126c4012d5d1;

static void createModuleCodeObjects(void)
{
    module_filename_obj = const_str_digest_2b423aeb9d85cb7f5281708f4d65ec91;
    codeobj_8be58a0ea832634d7416aca5115f8ecc = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 273, const_tuple_empty, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_888f561eb46bb0575657e15bb4d113d5 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 280, const_tuple_empty, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5d8f0b46bf818938aa1ad9fc8273f52c = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 285, const_tuple_empty, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_68ad5cffb1ffd264c465be50849008ed = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 290, const_tuple_empty, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f1509337cc982b665a24d58434f03494 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___color_constructor, 398, const_tuple_str_plain_r_str_plain_g_str_plain_b_str_plain_a_tuple, 4, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0eb6abfa5b89cc7690bf6ea1efdcbaa1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___color_reduce, 402, const_tuple_str_plain_c_tuple, 1, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_fefabc1607c3488e9c496958c63958f5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___getattr__, 98, const_tuple_str_plain_self_str_plain_var_str_plain_MissingPygameModule_tuple, 2, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7500321594c6bfc0890afb341857d467 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 83, const_tuple_9ccc413142d94e0a6ed4cf37ede89573_tuple, 4, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_61b2705797fcf52d0d0432331b24a84b = MAKE_CODEOBJ( module_filename_obj, const_str_plain___nonzero__, 107, const_tuple_str_plain_self_tuple, 1, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0d5b6e5d8fb8c07220d89866742ba372 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___rect_constructor, 387, const_tuple_str_plain_x_str_plain_y_str_plain_w_str_plain_h_tuple, 4, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9488e2d0f026bd566833afd6d12d6f30 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___rect_reduce, 391, const_tuple_str_plain_r_tuple, 1, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d5ea83a9c0c823d105f3b8cd2facff47 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_packager_imports, 370, const_tuple_8a6c6c24e7b08c8211c31aeebc3a2ad2_tuple, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d441b2a24565a20b1747a415ce202150 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_pygame, 1, const_tuple_empty, 0, CO_NOFREE );
    codeobj_8fcae91c12da0870d2b84d1df9e1f101 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_warn, 110, const_tuple_acb1e81c4b8a91a31fcad6506f654583_tuple, 1, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d6e9e297db4118c3f15c126c4012d5d1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_warn_unwanted_files, 216, const_tuple_b0f81c3dd59b5c80a95789c4e95abcfc_tuple, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
NUITKA_LOCAL_MODULE PyObject *impl_pygame$$$class_1_MissingModule( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_pygame$$$class_1_MissingModule$$$function_1___init__( PyObject *defaults );


static PyObject *MAKE_FUNCTION_pygame$$$class_1_MissingModule$$$function_2___getattr__(  );


static PyObject *MAKE_FUNCTION_pygame$$$class_1_MissingModule$$$function_3___nonzero__(  );


static PyObject *MAKE_FUNCTION_pygame$$$class_1_MissingModule$$$function_4_warn(  );


static PyObject *MAKE_FUNCTION_pygame$$$function_10___color_reduce(  );


static PyObject *MAKE_FUNCTION_pygame$$$function_1_warn_unwanted_files(  );


static PyObject *MAKE_FUNCTION_pygame$$$function_2_lambda(  );


static PyObject *MAKE_FUNCTION_pygame$$$function_3_lambda(  );


static PyObject *MAKE_FUNCTION_pygame$$$function_4_lambda(  );


static PyObject *MAKE_FUNCTION_pygame$$$function_5_lambda(  );


static PyObject *MAKE_FUNCTION_pygame$$$function_6_packager_imports(  );


static PyObject *MAKE_FUNCTION_pygame$$$function_7___rect_constructor(  );


static PyObject *MAKE_FUNCTION_pygame$$$function_8___rect_reduce(  );


static PyObject *MAKE_FUNCTION_pygame$$$function_9___color_constructor(  );


// The module function definitions.
NUITKA_LOCAL_MODULE PyObject *impl_pygame$$$class_1_MissingModule( PyObject **python_pars )
{
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
    assert(!had_error); // Do not enter inlined functions with error set.
#endif

    // Local variable declarations.
    PyObject *var___module__ = NULL;
    PyObject *var__NOT_IMPLEMENTED_ = NULL;
    PyObject *var___init__ = NULL;
    PyObject *var___getattr__ = NULL;
    PyObject *var___nonzero__ = NULL;
    PyObject *var_warn = NULL;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_defaults_1;
    PyObject *tmp_return_value;
    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = const_str_plain_pygame;
    assert( var___module__ == NULL );
    Py_INCREF( tmp_assign_source_1 );
    var___module__ = tmp_assign_source_1;

    tmp_assign_source_2 = Py_True;
    assert( var__NOT_IMPLEMENTED_ == NULL );
    Py_INCREF( tmp_assign_source_2 );
    var__NOT_IMPLEMENTED_ = tmp_assign_source_2;

    tmp_defaults_1 = const_tuple_str_empty_int_0_tuple;
    tmp_assign_source_3 = MAKE_FUNCTION_pygame$$$class_1_MissingModule$$$function_1___init__( INCREASE_REFCOUNT( tmp_defaults_1 ) );
    assert( var___init__ == NULL );
    var___init__ = tmp_assign_source_3;

    tmp_assign_source_4 = MAKE_FUNCTION_pygame$$$class_1_MissingModule$$$function_2___getattr__(  );
    assert( var___getattr__ == NULL );
    var___getattr__ = tmp_assign_source_4;

    tmp_assign_source_5 = MAKE_FUNCTION_pygame$$$class_1_MissingModule$$$function_3___nonzero__(  );
    assert( var___nonzero__ == NULL );
    var___nonzero__ = tmp_assign_source_5;

    tmp_assign_source_6 = MAKE_FUNCTION_pygame$$$class_1_MissingModule$$$function_4_warn(  );
    assert( var_warn == NULL );
    var_warn = tmp_assign_source_6;

    // Tried code:
    tmp_return_value = PyDict_New();
    if ( var___module__ )
    {
        int res = PyDict_SetItem(
            tmp_return_value,
            const_str_plain___module__,
            var___module__
        );

        assert( res == 0 );
    }

    if ( var__NOT_IMPLEMENTED_ )
    {
        int res = PyDict_SetItem(
            tmp_return_value,
            const_str_plain__NOT_IMPLEMENTED_,
            var__NOT_IMPLEMENTED_
        );

        assert( res == 0 );
    }

    if ( var___init__ )
    {
        int res = PyDict_SetItem(
            tmp_return_value,
            const_str_plain___init__,
            var___init__
        );

        assert( res == 0 );
    }

    if ( var___getattr__ )
    {
        int res = PyDict_SetItem(
            tmp_return_value,
            const_str_plain___getattr__,
            var___getattr__
        );

        assert( res == 0 );
    }

    if ( var___nonzero__ )
    {
        int res = PyDict_SetItem(
            tmp_return_value,
            const_str_plain___nonzero__,
            var___nonzero__
        );

        assert( res == 0 );
    }

    if ( var_warn )
    {
        int res = PyDict_SetItem(
            tmp_return_value,
            const_str_plain_warn,
            var_warn
        );

        assert( res == 0 );
    }

    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$$$class_1_MissingModule );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var___module__ );
    Py_DECREF( var___module__ );
    var___module__ = NULL;

    CHECK_OBJECT( (PyObject *)var__NOT_IMPLEMENTED_ );
    Py_DECREF( var__NOT_IMPLEMENTED_ );
    var__NOT_IMPLEMENTED_ = NULL;

    CHECK_OBJECT( (PyObject *)var___init__ );
    Py_DECREF( var___init__ );
    var___init__ = NULL;

    CHECK_OBJECT( (PyObject *)var___getattr__ );
    Py_DECREF( var___getattr__ );
    var___getattr__ = NULL;

    CHECK_OBJECT( (PyObject *)var___nonzero__ );
    Py_DECREF( var___nonzero__ );
    var___nonzero__ = NULL;

    CHECK_OBJECT( (PyObject *)var_warn );
    Py_DECREF( var_warn );
    var_warn = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$class_1_MissingModule );
    return NULL;

    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_pygame$$$class_1_MissingModule$$$function_1___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_name = python_pars[ 1 ];
    PyObject *par_info = python_pars[ 2 ];
    PyObject *par_urgent = python_pars[ 3 ];
    PyObject *var_exc = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_assattr_name_1;
    PyObject *tmp_assattr_name_2;
    PyObject *tmp_assattr_name_3;
    PyObject *tmp_assattr_name_4;
    PyObject *tmp_assattr_name_5;
    PyObject *tmp_assattr_target_1;
    PyObject *tmp_assattr_target_2;
    PyObject *tmp_assattr_target_3;
    PyObject *tmp_assattr_target_4;
    PyObject *tmp_assattr_target_5;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_instance_2;
    int tmp_cmp_NotEq_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_right_1;
    int tmp_cond_truth_1;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_frame_locals;
    PyObject *tmp_left_name_1;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_source_name_1;
    PyObject *tmp_str_arg_1;
    PyObject *tmp_str_arg_2;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscribed_name_2;
    PyObject *tmp_subscribed_name_3;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_subscript_name_2;
    PyObject *tmp_subscript_name_3;
    PyObject *tmp_tuple_element_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_7500321594c6bfc0890afb341857d467, module_pygame );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_assattr_name_1 = par_name;

    tmp_assattr_target_1 = par_self;

    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_name, tmp_assattr_name_1 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 84;
        goto frame_exception_exit_1;
    }
    tmp_str_arg_1 = par_info;

    tmp_assattr_name_2 = PyObject_Str( tmp_str_arg_1 );
    if ( tmp_assattr_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 85;
        goto frame_exception_exit_1;
    }
    tmp_assattr_target_2 = par_self;

    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_info, tmp_assattr_name_2 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assattr_name_2 );

        exception_lineno = 85;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_assattr_name_2 );
    // Tried code:
    tmp_called_instance_1 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_called_instance_1 == NULL ))
    {
        tmp_called_instance_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_called_instance_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "sys" );
        exception_tb = NULL;

        exception_lineno = 87;
        goto try_except_handler_2;
    }

    frame_function->f_lineno = 87;
    tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_exc_info );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 87;
        goto try_except_handler_2;
    }
    assert( var_exc == NULL );
    var_exc = tmp_assign_source_1;

    tmp_subscribed_name_1 = var_exc;

    tmp_subscript_name_1 = const_int_0;
    tmp_compare_left_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
    if ( tmp_compare_left_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 88;
        goto try_except_handler_2;
    }
    tmp_compare_right_1 = Py_None;
    tmp_cmp_NotEq_1 = RICH_COMPARE_BOOL_NE( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_cmp_NotEq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_1 );

        exception_lineno = 88;
        goto try_except_handler_2;
    }
    Py_DECREF( tmp_compare_left_1 );
    if ( tmp_cmp_NotEq_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_left_name_1 = const_str_digest_b0cc034d9177fde48a3b44ac122139d9;
    tmp_right_name_1 = PyTuple_New( 2 );
    tmp_subscribed_name_2 = var_exc;

    tmp_subscript_name_2 = const_int_0;
    tmp_source_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
    if ( tmp_source_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_right_name_1 );

        exception_lineno = 89;
        goto try_except_handler_2;
    }
    tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___name__ );
    Py_DECREF( tmp_source_name_1 );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_right_name_1 );

        exception_lineno = 89;
        goto try_except_handler_2;
    }
    PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
    tmp_subscribed_name_3 = var_exc;

    tmp_subscript_name_3 = const_int_pos_1;
    tmp_str_arg_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
    if ( tmp_str_arg_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_right_name_1 );

        exception_lineno = 89;
        goto try_except_handler_2;
    }
    tmp_tuple_element_1 = PyObject_Str( tmp_str_arg_2 );
    Py_DECREF( tmp_str_arg_2 );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_right_name_1 );

        exception_lineno = 89;
        goto try_except_handler_2;
    }
    PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
    tmp_assattr_name_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_right_name_1 );
    if ( tmp_assattr_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 89;
        goto try_except_handler_2;
    }
    tmp_assattr_target_3 = par_self;

    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_reason, tmp_assattr_name_3 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assattr_name_3 );

        exception_lineno = 89;
        goto try_except_handler_2;
    }
    Py_DECREF( tmp_assattr_name_3 );
    goto branch_end_1;
    branch_no_1:;
    tmp_assattr_name_4 = const_str_empty;
    tmp_assattr_target_4 = par_self;

    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_reason, tmp_assattr_name_4 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 91;
        goto try_except_handler_2;
    }
    branch_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    tmp_result = var_exc != NULL;
    if ( tmp_result == true )
    {
        Py_DECREF( var_exc );
        var_exc = NULL;
    }

    if ( tmp_result == false )
    {

        Py_DECREF( exception_keeper_type_1 );
        Py_XDECREF( exception_keeper_value_1 );
        Py_XDECREF( exception_keeper_tb_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 371 ], 49, 0 );
        exception_tb = NULL;

        exception_lineno = 93;
        goto frame_exception_exit_1;
    }

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( var_exc );
    Py_DECREF( var_exc );
    var_exc = NULL;

    tmp_assattr_name_5 = par_urgent;

    tmp_assattr_target_5 = par_self;

    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain_urgent, tmp_assattr_name_5 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 94;
        goto frame_exception_exit_1;
    }
    tmp_cond_value_1 = par_urgent;

    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 95;
        goto frame_exception_exit_1;
    }
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_called_instance_2 = par_self;

    frame_function->f_lineno = 96;
    tmp_unused = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_warn );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 96;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    branch_no_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();
            if ( par_self )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_self,
                    par_self
                );

                assert( res == 0 );
            }

            if ( par_name )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_name,
                    par_name
                );

                assert( res == 0 );
            }

            if ( par_info )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_info,
                    par_info
                );

                assert( res == 0 );
            }

            if ( par_urgent )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_urgent,
                    par_urgent
                );

                assert( res == 0 );
            }

            if ( var_exc )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_exc,
                    var_exc
                );

                assert( res == 0 );
            }



            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$$$class_1_MissingModule$$$function_1___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_info );
    Py_DECREF( par_info );
    par_info = NULL;

    CHECK_OBJECT( (PyObject *)par_urgent );
    Py_DECREF( par_urgent );
    par_urgent = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_info );
    Py_DECREF( par_info );
    par_info = NULL;

    CHECK_OBJECT( (PyObject *)par_urgent );
    Py_DECREF( par_urgent );
    par_urgent = NULL;

    Py_XDECREF( var_exc );
    var_exc = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$class_1_MissingModule$$$function_1___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_pygame$$$class_1_MissingModule$$$function_2___getattr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_var = python_pars[ 1 ];
    PyObject *var_MissingPygameModule = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_assattr_name_1;
    PyObject *tmp_assattr_target_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_called_instance_1;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_frame_locals;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_left_name_3;
    PyObject *tmp_make_exception_arg_1;
    PyObject *tmp_raise_type_1;
    bool tmp_result;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_right_name_3;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;


    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_fefabc1607c3488e9c496958c63958f5, module_pygame );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_source_name_1 = par_self;

    tmp_cond_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_urgent );
    if ( tmp_cond_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 99;
        goto frame_exception_exit_1;
    }
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        exception_lineno = 99;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_no_1;
    }
    else
    {
        goto branch_yes_1;
    }
    branch_yes_1:;
    tmp_called_instance_1 = par_self;

    frame_function->f_lineno = 100;
    tmp_unused = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_warn );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 100;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_assattr_name_1 = const_int_pos_1;
    tmp_assattr_target_1 = par_self;

    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_urgent, tmp_assattr_name_1 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 101;
        goto frame_exception_exit_1;
    }
    branch_no_1:;
    tmp_left_name_1 = const_str_digest_43e023631edb4fe3adb723d1a9be5d8b;
    tmp_source_name_2 = par_self;

    tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_name );
    if ( tmp_right_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 102;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_right_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 102;
        goto frame_exception_exit_1;
    }
    assert( var_MissingPygameModule == NULL );
    var_MissingPygameModule = tmp_assign_source_1;

    tmp_source_name_3 = par_self;

    tmp_cond_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_reason );
    if ( tmp_cond_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 103;
        goto frame_exception_exit_1;
    }
    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_2 );

        exception_lineno = 103;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_left_name_2 = var_MissingPygameModule;

    tmp_left_name_3 = const_str_digest_4d931959d4904cd58987bae6e017827c;
    tmp_source_name_4 = par_self;

    tmp_right_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_reason );
    if ( tmp_right_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 104;
        goto frame_exception_exit_1;
    }
    tmp_right_name_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
    Py_DECREF( tmp_right_name_3 );
    if ( tmp_right_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 104;
        goto frame_exception_exit_1;
    }
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_2, tmp_right_name_2 );
    tmp_assign_source_2 = tmp_left_name_2;
    Py_DECREF( tmp_right_name_2 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 104;
        goto frame_exception_exit_1;
    }
    var_MissingPygameModule = tmp_assign_source_2;

    branch_no_2:;
    tmp_make_exception_arg_1 = var_MissingPygameModule;

    frame_function->f_lineno = 105;
    {
        PyObject *call_args[] = { tmp_make_exception_arg_1 };
        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_NotImplementedError, call_args );
    }

    assert( tmp_raise_type_1 != NULL );
    exception_type = tmp_raise_type_1;
    exception_lineno = 105;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    goto frame_exception_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();
            if ( par_self )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_self,
                    par_self
                );

                assert( res == 0 );
            }

            if ( par_var )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_var,
                    par_var
                );

                assert( res == 0 );
            }

            if ( var_MissingPygameModule )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_MissingPygameModule,
                    var_MissingPygameModule
                );

                assert( res == 0 );
            }



            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$$$class_1_MissingModule$$$function_2___getattr__ );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_var );
    Py_DECREF( par_var );
    par_var = NULL;

    Py_XDECREF( var_MissingPygameModule );
    var_MissingPygameModule = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$class_1_MissingModule$$$function_2___getattr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

}


static PyObject *impl_pygame$$$class_1_MissingModule$$$function_3___nonzero__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    tmp_return_value = const_int_0;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$$$class_1_MissingModule$$$function_3___nonzero__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$class_1_MissingModule$$$function_3___nonzero__ );
    return NULL;

    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_pygame$$$class_1_MissingModule$$$function_4_warn( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_type = NULL;
    PyObject *var_message = NULL;
    PyObject *var_warnings = NULL;
    PyObject *var_level = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_called_name_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_right_1;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    int tmp_cond_truth_3;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_cond_value_3;
    int tmp_exc_match_exception_match_1;
    PyObject *tmp_frame_locals;
    PyObject *tmp_import_globals_1;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_left_name_3;
    PyObject *tmp_print_value;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_right_name_3;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_source_name_6;
    PyObject *tmp_source_name_7;
    PyObject *tmp_tuple_element_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_8fcae91c12da0870d2b84d1df9e1f101, module_pygame );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_source_name_1 = par_self;

    tmp_cond_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_urgent );
    if ( tmp_cond_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 111;
        goto frame_exception_exit_1;
    }
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        exception_lineno = 111;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_assign_source_1 = const_str_plain_import;
    assert( var_type == NULL );
    Py_INCREF( tmp_assign_source_1 );
    var_type = tmp_assign_source_1;

    goto branch_end_1;
    branch_no_1:;
    tmp_assign_source_2 = const_str_plain_use;
    assert( var_type == NULL );
    Py_INCREF( tmp_assign_source_2 );
    var_type = tmp_assign_source_2;

    branch_end_1:;
    tmp_left_name_1 = const_str_digest_efb40b5afdc4a3fe12b5b2abd84c3b2c;
    tmp_right_name_1 = PyTuple_New( 3 );
    tmp_tuple_element_1 = var_type;

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
    tmp_source_name_2 = par_self;

    tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_name );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_right_name_1 );

        exception_lineno = 115;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
    tmp_source_name_3 = par_self;

    tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_info );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_right_name_1 );

        exception_lineno = 115;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_right_name_1, 2, tmp_tuple_element_1 );
    tmp_assign_source_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_right_name_1 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 115;
        goto frame_exception_exit_1;
    }
    assert( var_message == NULL );
    var_message = tmp_assign_source_3;

    tmp_source_name_4 = par_self;

    tmp_cond_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_reason );
    if ( tmp_cond_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 116;
        goto frame_exception_exit_1;
    }
    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_2 );

        exception_lineno = 116;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_left_name_2 = var_message;

    tmp_left_name_3 = const_str_digest_4d931959d4904cd58987bae6e017827c;
    tmp_source_name_5 = par_self;

    tmp_right_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_reason );
    if ( tmp_right_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 117;
        goto frame_exception_exit_1;
    }
    tmp_right_name_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
    Py_DECREF( tmp_right_name_3 );
    if ( tmp_right_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 117;
        goto frame_exception_exit_1;
    }
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_2, tmp_right_name_2 );
    tmp_assign_source_4 = tmp_left_name_2;
    Py_DECREF( tmp_right_name_2 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 117;
        goto frame_exception_exit_1;
    }
    var_message = tmp_assign_source_4;

    branch_no_2:;
    // Tried code:
    tmp_import_globals_1 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_function->f_lineno = 119;
    tmp_assign_source_5 = IMPORT_MODULE( const_str_plain_warnings, tmp_import_globals_1, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 119;
        goto try_except_handler_2;
    }
    assert( var_warnings == NULL );
    var_warnings = tmp_assign_source_5;

    tmp_source_name_6 = par_self;

    tmp_cond_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_urgent );
    if ( tmp_cond_value_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 120;
        goto try_except_handler_2;
    }
    tmp_cond_truth_3 = CHECK_IF_TRUE( tmp_cond_value_3 );
    if ( tmp_cond_truth_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_3 );

        exception_lineno = 120;
        goto try_except_handler_2;
    }
    Py_DECREF( tmp_cond_value_3 );
    if ( tmp_cond_truth_3 == 1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_assign_source_6 = const_int_pos_4;
    assert( var_level == NULL );
    Py_INCREF( tmp_assign_source_6 );
    var_level = tmp_assign_source_6;

    goto branch_end_3;
    branch_no_3:;
    tmp_assign_source_7 = const_int_pos_3;
    assert( var_level == NULL );
    Py_INCREF( tmp_assign_source_7 );
    var_level = tmp_assign_source_7;

    branch_end_3:;
    tmp_source_name_7 = var_warnings;

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_warn );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 124;
        goto try_except_handler_2;
    }
    tmp_args_element_name_1 = var_message;

    tmp_args_element_name_2 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_RuntimeWarning );

    if (unlikely( tmp_args_element_name_2 == NULL ))
    {
        tmp_args_element_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RuntimeWarning );
    }

    if ( tmp_args_element_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "RuntimeWarning" );
        exception_tb = NULL;

        exception_lineno = 124;
        goto try_except_handler_2;
    }

    tmp_args_element_name_3 = var_level;

    frame_function->f_lineno = 124;
    {
        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_called_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 124;
        goto try_except_handler_2;
    }
    Py_DECREF( tmp_unused );
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_function );
    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_function, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != -1 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_function, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    tmp_compare_left_1 = PyThreadState_GET()->exc_type;
    tmp_compare_right_1 = PyExc_ImportError;
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 125;
        goto frame_exception_exit_1;
    }
    if ( tmp_exc_match_exception_match_1 == 1 )
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_print_value = var_message;

    if ( PRINT_ITEM( tmp_print_value ) == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 126;
        goto frame_exception_exit_1;
    }
    if ( PRINT_NEW_LINE() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 126;
        goto frame_exception_exit_1;
    }
    goto branch_end_4;
    branch_no_4:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_function) frame_function->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_4:;
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$$$class_1_MissingModule$$$function_4_warn );
    return NULL;
    // End of try:
    try_end_1:;

#if 1
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 1
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();
            if ( par_self )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_self,
                    par_self
                );

                assert( res == 0 );
            }

            if ( var_type )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_type,
                    var_type
                );

                assert( res == 0 );
            }

            if ( var_message )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_message,
                    var_message
                );

                assert( res == 0 );
            }

            if ( var_warnings )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_warnings,
                    var_warnings
                );

                assert( res == 0 );
            }

            if ( var_level )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_level,
                    var_level
                );

                assert( res == 0 );
            }



            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$$$class_1_MissingModule$$$function_4_warn );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_type );
    Py_DECREF( var_type );
    var_type = NULL;

    CHECK_OBJECT( (PyObject *)var_message );
    Py_DECREF( var_message );
    var_message = NULL;

    Py_XDECREF( var_warnings );
    var_warnings = NULL;

    Py_XDECREF( var_level );
    var_level = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_type );
    var_type = NULL;

    Py_XDECREF( var_message );
    var_message = NULL;

    Py_XDECREF( var_warnings );
    var_warnings = NULL;

    Py_XDECREF( var_level );
    var_level = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$class_1_MissingModule$$$function_4_warn );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_pygame$$$function_1_warn_unwanted_files( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_install_path = NULL;
    PyObject *var_extension_ext = NULL;
    PyObject *var_ext_to_remove = NULL;
    PyObject *var_py_to_remove = NULL;
    PyObject *var_x = NULL;
    PyObject *var_extension_files = NULL;
    PyObject *var_py_ext = NULL;
    PyObject *var_py_files = NULL;
    PyObject *var_files = NULL;
    PyObject *var_unwanted_files = NULL;
    PyObject *var_f = NULL;
    PyObject *var_ask_remove = NULL;
    PyObject *var_message = NULL;
    PyObject *var_warnings = NULL;
    PyObject *tmp_list_contraction_1__$0 = NULL;
    PyObject *tmp_list_contraction_1__contraction_result = NULL;
    PyObject *tmp_list_contraction_1__iter_value_0 = NULL;
    PyObject *tmp_list_contraction_2__$0 = NULL;
    PyObject *tmp_list_contraction_2__contraction_result = NULL;
    PyObject *tmp_list_contraction_2__iter_value_0 = NULL;
    PyObject *tmp_list_contraction_2__contraction_iter_0 = NULL;
    PyObject *tmp_list_contraction_2__iter_value_1 = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_for_loop_3__for_iterator = NULL;
    PyObject *tmp_for_loop_3__iter_value = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *tmp_append_list_1;
    PyObject *tmp_append_list_2;
    PyObject *tmp_append_value_1;
    PyObject *tmp_append_value_2;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_element_name_6;
    PyObject *tmp_args_element_name_7;
    PyObject *tmp_args_element_name_8;
    PyObject *tmp_args_element_name_9;
    PyObject *tmp_args_element_name_10;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    PyObject *tmp_assign_source_28;
    PyObject *tmp_assign_source_29;
    PyObject *tmp_assign_source_30;
    PyObject *tmp_assign_source_31;
    PyObject *tmp_assign_source_32;
    PyObject *tmp_assign_source_33;
    PyObject *tmp_assign_source_34;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_instance_2;
    PyObject *tmp_called_instance_3;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    int tmp_cmp_Eq_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    int tmp_exc_match_exception_match_1;
    PyObject *tmp_frame_locals;
    PyObject *tmp_import_globals_1;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iter_arg_2;
    PyObject *tmp_iter_arg_3;
    PyObject *tmp_iter_arg_4;
    PyObject *tmp_iter_arg_5;
    PyObject *tmp_iter_arg_6;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_left_name_3;
    PyObject *tmp_left_name_4;
    PyObject *tmp_left_name_5;
    PyObject *tmp_left_name_6;
    PyObject *tmp_next_source_1;
    PyObject *tmp_next_source_2;
    PyObject *tmp_next_source_3;
    PyObject *tmp_next_source_4;
    PyObject *tmp_next_source_5;
    PyObject *tmp_next_source_6;
    PyObject *tmp_outline_return_value_1;
    PyObject *tmp_outline_return_value_2;
    PyObject *tmp_print_value;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_right_name_3;
    PyObject *tmp_right_name_4;
    PyObject *tmp_right_name_5;
    PyObject *tmp_right_name_6;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_source_name_6;
    PyObject *tmp_source_name_7;
    PyObject *tmp_source_name_8;
    PyObject *tmp_source_name_9;
    PyObject *tmp_source_name_10;
    PyObject *tmp_source_name_11;
    PyObject *tmp_source_name_12;
    PyObject *tmp_source_name_13;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscribed_name_2;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_subscript_name_2;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;
    tmp_outline_return_value_1 = NULL;
    tmp_outline_return_value_2 = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_d6e9e297db4118c3f15c126c4012d5d1, module_pygame );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "os" );
        exception_tb = NULL;

        exception_lineno = 220;
        goto frame_exception_exit_1;
    }

    tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_path );
    if ( tmp_source_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 220;
        goto frame_exception_exit_1;
    }
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_split );
    Py_DECREF( tmp_source_name_1 );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 220;
        goto frame_exception_exit_1;
    }
    tmp_source_name_4 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame );

    if (unlikely( tmp_source_name_4 == NULL ))
    {
        tmp_source_name_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pygame );
    }

    if ( tmp_source_name_4 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "pygame" );
        exception_tb = NULL;

        exception_lineno = 220;
        goto frame_exception_exit_1;
    }

    tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_base );
    if ( tmp_source_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );

        exception_lineno = 220;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain___file__ );
    Py_DECREF( tmp_source_name_3 );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );

        exception_lineno = 220;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 220;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_subscribed_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_element_name_1 );
    if ( tmp_subscribed_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 220;
        goto frame_exception_exit_1;
    }
    tmp_subscript_name_1 = const_int_0;
    tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
    Py_DECREF( tmp_subscribed_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 220;
        goto frame_exception_exit_1;
    }
    assert( var_install_path == NULL );
    var_install_path = tmp_assign_source_1;

    tmp_source_name_6 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_6 == NULL ))
    {
        tmp_source_name_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_6 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "os" );
        exception_tb = NULL;

        exception_lineno = 221;
        goto frame_exception_exit_1;
    }

    tmp_source_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_path );
    if ( tmp_source_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 221;
        goto frame_exception_exit_1;
    }
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_splitext );
    Py_DECREF( tmp_source_name_5 );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 221;
        goto frame_exception_exit_1;
    }
    tmp_source_name_8 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame );

    if (unlikely( tmp_source_name_8 == NULL ))
    {
        tmp_source_name_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pygame );
    }

    if ( tmp_source_name_8 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "pygame" );
        exception_tb = NULL;

        exception_lineno = 221;
        goto frame_exception_exit_1;
    }

    tmp_source_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_base );
    if ( tmp_source_name_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_2 );

        exception_lineno = 221;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain___file__ );
    Py_DECREF( tmp_source_name_7 );
    if ( tmp_args_element_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_2 );

        exception_lineno = 221;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 221;
    {
        PyObject *call_args[] = { tmp_args_element_name_2 };
        tmp_subscribed_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
    }

    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_args_element_name_2 );
    if ( tmp_subscribed_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 221;
        goto frame_exception_exit_1;
    }
    tmp_subscript_name_2 = const_int_pos_1;
    tmp_assign_source_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
    Py_DECREF( tmp_subscribed_name_2 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 221;
        goto frame_exception_exit_1;
    }
    assert( var_extension_ext == NULL );
    var_extension_ext = tmp_assign_source_2;

    tmp_assign_source_3 = LIST_COPY( const_list_str_plain_camera_list );
    assert( var_ext_to_remove == NULL );
    var_ext_to_remove = tmp_assign_source_3;

    tmp_assign_source_4 = LIST_COPY( const_list_str_plain_color_list );
    assert( var_py_to_remove == NULL );
    var_py_to_remove = tmp_assign_source_4;

    tmp_source_name_9 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_9 == NULL ))
    {
        tmp_source_name_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_9 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "os" );
        exception_tb = NULL;

        exception_lineno = 230;
        goto frame_exception_exit_1;
    }

    tmp_compare_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_name );
    if ( tmp_compare_left_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 230;
        goto frame_exception_exit_1;
    }
    tmp_compare_right_1 = const_str_plain_e32;
    tmp_cmp_Eq_1 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_cmp_Eq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_1 );

        exception_lineno = 230;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_left_1 );
    if ( tmp_cmp_Eq_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_assign_source_5 = PyList_New( 0 );
    {
        PyObject *old = var_py_to_remove;
        assert( old != NULL );
        var_py_to_remove = tmp_assign_source_5;
        Py_DECREF( old );
    }

    branch_no_1:;
    // Tried code:
    tmp_iter_arg_1 = var_ext_to_remove;

    tmp_assign_source_7 = MAKE_ITERATOR( tmp_iter_arg_1 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 234;
        goto try_except_handler_2;
    }
    assert( tmp_list_contraction_1__$0 == NULL );
    tmp_list_contraction_1__$0 = tmp_assign_source_7;

    tmp_assign_source_8 = PyList_New( 0 );
    assert( tmp_list_contraction_1__contraction_result == NULL );
    tmp_list_contraction_1__contraction_result = tmp_assign_source_8;

    loop_start_1:;
    tmp_next_source_1 = tmp_list_contraction_1__$0;

    tmp_assign_source_9 = ITERATOR_NEXT( tmp_next_source_1 );
    if ( tmp_assign_source_9 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_1;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            PyThreadState_GET()->frame->f_lineno = 234;
            goto try_except_handler_2;
        }
    }

    {
        PyObject *old = tmp_list_contraction_1__iter_value_0;
        tmp_list_contraction_1__iter_value_0 = tmp_assign_source_9;
        Py_XDECREF( old );
    }

    tmp_assign_source_10 = tmp_list_contraction_1__iter_value_0;

    {
        PyObject *old = var_x;
        var_x = tmp_assign_source_10;
        Py_INCREF( var_x );
        Py_XDECREF( old );
    }

    tmp_append_list_1 = tmp_list_contraction_1__contraction_result;

    tmp_left_name_1 = const_str_digest_d76462298f66c95b4d3ee22e9427a916;
    tmp_right_name_1 = PyTuple_New( 2 );
    tmp_tuple_element_1 = var_x;

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = var_extension_ext;

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
    tmp_append_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_right_name_1 );
    if ( tmp_append_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 234;
        goto try_except_handler_2;
    }
    assert( PyList_Check( tmp_append_list_1 ) );
    tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
    Py_DECREF( tmp_append_value_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 234;
        goto try_except_handler_2;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 234;
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    tmp_outline_return_value_1 = tmp_list_contraction_1__contraction_result;

    Py_INCREF( tmp_outline_return_value_1 );
    goto try_return_handler_2;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$$$function_1_warn_unwanted_files );
    return NULL;
    // Return handler code:
    try_return_handler_2:;
    CHECK_OBJECT( (PyObject *)tmp_list_contraction_1__$0 );
    Py_DECREF( tmp_list_contraction_1__$0 );
    tmp_list_contraction_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_list_contraction_1__contraction_result );
    Py_DECREF( tmp_list_contraction_1__contraction_result );
    tmp_list_contraction_1__contraction_result = NULL;

    Py_XDECREF( tmp_list_contraction_1__iter_value_0 );
    tmp_list_contraction_1__iter_value_0 = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( tmp_list_contraction_1__$0 );
    tmp_list_contraction_1__$0 = NULL;

    Py_XDECREF( tmp_list_contraction_1__contraction_result );
    tmp_list_contraction_1__contraction_result = NULL;

    Py_XDECREF( tmp_list_contraction_1__iter_value_0 );
    tmp_list_contraction_1__iter_value_0 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$function_1_warn_unwanted_files );
    return NULL;
    outline_result_1:;
    tmp_assign_source_6 = tmp_outline_return_value_1;
    assert( var_extension_files == NULL );
    var_extension_files = tmp_assign_source_6;

    tmp_iter_arg_2 = const_tuple_73b025f8ca1696071bff8cf0ee4b7ec2_tuple;
    tmp_assign_source_12 = MAKE_ITERATOR( tmp_iter_arg_2 );
    assert( tmp_assign_source_12 != NULL );
    assert( tmp_list_contraction_2__$0 == NULL );
    tmp_list_contraction_2__$0 = tmp_assign_source_12;

    tmp_assign_source_13 = PyList_New( 0 );
    assert( tmp_list_contraction_2__contraction_result == NULL );
    tmp_list_contraction_2__contraction_result = tmp_assign_source_13;

    // Tried code:
    loop_start_2:;
    tmp_next_source_2 = tmp_list_contraction_2__$0;

    tmp_assign_source_14 = ITERATOR_NEXT( tmp_next_source_2 );
    if ( tmp_assign_source_14 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_2;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            PyThreadState_GET()->frame->f_lineno = 236;
            goto try_except_handler_3;
        }
    }

    {
        PyObject *old = tmp_list_contraction_2__iter_value_1;
        tmp_list_contraction_2__iter_value_1 = tmp_assign_source_14;
        Py_XDECREF( old );
    }

    tmp_assign_source_15 = tmp_list_contraction_2__iter_value_1;

    {
        PyObject *old = var_py_ext;
        var_py_ext = tmp_assign_source_15;
        Py_INCREF( var_py_ext );
        Py_XDECREF( old );
    }

    tmp_iter_arg_3 = var_py_to_remove;

    tmp_assign_source_16 = MAKE_ITERATOR( tmp_iter_arg_3 );
    if ( tmp_assign_source_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 236;
        goto try_except_handler_3;
    }
    {
        PyObject *old = tmp_list_contraction_2__contraction_iter_0;
        tmp_list_contraction_2__contraction_iter_0 = tmp_assign_source_16;
        Py_XDECREF( old );
    }

    loop_start_3:;
    tmp_next_source_3 = tmp_list_contraction_2__contraction_iter_0;

    tmp_assign_source_17 = ITERATOR_NEXT( tmp_next_source_3 );
    if ( tmp_assign_source_17 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_3;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            PyThreadState_GET()->frame->f_lineno = 236;
            goto try_except_handler_3;
        }
    }

    {
        PyObject *old = tmp_list_contraction_2__iter_value_0;
        tmp_list_contraction_2__iter_value_0 = tmp_assign_source_17;
        Py_XDECREF( old );
    }

    tmp_assign_source_18 = tmp_list_contraction_2__iter_value_0;

    {
        PyObject *old = var_x;
        var_x = tmp_assign_source_18;
        Py_INCREF( var_x );
        Py_XDECREF( old );
    }

    tmp_append_list_2 = tmp_list_contraction_2__contraction_result;

    tmp_left_name_2 = const_str_digest_d76462298f66c95b4d3ee22e9427a916;
    tmp_right_name_2 = PyTuple_New( 2 );
    tmp_tuple_element_2 = var_x;

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_2 );
    tmp_tuple_element_2 = var_py_ext;

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_2 );
    tmp_append_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
    Py_DECREF( tmp_right_name_2 );
    if ( tmp_append_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 236;
        goto try_except_handler_3;
    }
    assert( PyList_Check( tmp_append_list_2 ) );
    tmp_res = PyList_Append( tmp_append_list_2, tmp_append_value_2 );
    Py_DECREF( tmp_append_value_2 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 236;
        goto try_except_handler_3;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 236;
        goto try_except_handler_3;
    }
    goto loop_start_3;
    loop_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_list_contraction_2__contraction_iter_0 );
    Py_DECREF( tmp_list_contraction_2__contraction_iter_0 );
    tmp_list_contraction_2__contraction_iter_0 = NULL;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 236;
        goto try_except_handler_3;
    }
    goto loop_start_2;
    loop_end_2:;
    tmp_outline_return_value_2 = tmp_list_contraction_2__contraction_result;

    Py_INCREF( tmp_outline_return_value_2 );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$$$function_1_warn_unwanted_files );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    CHECK_OBJECT( (PyObject *)tmp_list_contraction_2__$0 );
    Py_DECREF( tmp_list_contraction_2__$0 );
    tmp_list_contraction_2__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_list_contraction_2__contraction_result );
    Py_DECREF( tmp_list_contraction_2__contraction_result );
    tmp_list_contraction_2__contraction_result = NULL;

    Py_XDECREF( tmp_list_contraction_2__iter_value_0 );
    tmp_list_contraction_2__iter_value_0 = NULL;

    Py_XDECREF( tmp_list_contraction_2__contraction_iter_0 );
    tmp_list_contraction_2__contraction_iter_0 = NULL;

    Py_XDECREF( tmp_list_contraction_2__iter_value_1 );
    tmp_list_contraction_2__iter_value_1 = NULL;

    goto outline_result_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    CHECK_OBJECT( (PyObject *)tmp_list_contraction_2__$0 );
    Py_DECREF( tmp_list_contraction_2__$0 );
    tmp_list_contraction_2__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_list_contraction_2__contraction_result );
    Py_DECREF( tmp_list_contraction_2__contraction_result );
    tmp_list_contraction_2__contraction_result = NULL;

    Py_XDECREF( tmp_list_contraction_2__iter_value_0 );
    tmp_list_contraction_2__iter_value_0 = NULL;

    Py_XDECREF( tmp_list_contraction_2__contraction_iter_0 );
    tmp_list_contraction_2__contraction_iter_0 = NULL;

    Py_XDECREF( tmp_list_contraction_2__iter_value_1 );
    tmp_list_contraction_2__iter_value_1 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$function_1_warn_unwanted_files );
    return NULL;
    outline_result_2:;
    tmp_assign_source_11 = tmp_outline_return_value_2;
    assert( var_py_files == NULL );
    var_py_files = tmp_assign_source_11;

    tmp_left_name_3 = var_py_files;

    tmp_right_name_3 = var_extension_files;

    tmp_assign_source_19 = BINARY_OPERATION_ADD( tmp_left_name_3, tmp_right_name_3 );
    if ( tmp_assign_source_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 240;
        goto frame_exception_exit_1;
    }
    assert( var_files == NULL );
    var_files = tmp_assign_source_19;

    tmp_assign_source_20 = PyList_New( 0 );
    assert( var_unwanted_files == NULL );
    var_unwanted_files = tmp_assign_source_20;

    tmp_iter_arg_4 = var_files;

    tmp_assign_source_21 = MAKE_ITERATOR( tmp_iter_arg_4 );
    if ( tmp_assign_source_21 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 243;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_21;

    // Tried code:
    loop_start_4:;
    tmp_next_source_4 = tmp_for_loop_1__for_iterator;

    tmp_assign_source_22 = ITERATOR_NEXT( tmp_next_source_4 );
    if ( tmp_assign_source_22 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_4;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 243;
            goto try_except_handler_4;
        }
    }

    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_22;
        Py_XDECREF( old );
    }

    tmp_assign_source_23 = tmp_for_loop_1__iter_value;

    {
        PyObject *old = var_f;
        var_f = tmp_assign_source_23;
        Py_INCREF( var_f );
        Py_XDECREF( old );
    }

    tmp_source_name_10 = var_unwanted_files;

    tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_append );
    if ( tmp_called_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 244;
        goto try_except_handler_4;
    }
    tmp_source_name_11 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_11 == NULL ))
    {
        tmp_source_name_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_11 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "os" );
        exception_tb = NULL;

        exception_lineno = 244;
        goto try_except_handler_4;
    }

    tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_path );
    if ( tmp_called_instance_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );

        exception_lineno = 244;
        goto try_except_handler_4;
    }
    tmp_args_element_name_4 = var_install_path;

    tmp_args_element_name_5 = var_f;

    frame_function->f_lineno = 244;
    {
        PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
        tmp_args_element_name_3 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_join, call_args );
    }

    Py_DECREF( tmp_called_instance_1 );
    if ( tmp_args_element_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );

        exception_lineno = 244;
        goto try_except_handler_4;
    }
    frame_function->f_lineno = 244;
    {
        PyObject *call_args[] = { tmp_args_element_name_3 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
    }

    Py_DECREF( tmp_called_name_3 );
    Py_DECREF( tmp_args_element_name_3 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 244;
        goto try_except_handler_4;
    }
    Py_DECREF( tmp_unused );
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 243;
        goto try_except_handler_4;
    }
    goto loop_start_4;
    loop_end_4:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_assign_source_24 = PyList_New( 0 );
    assert( var_ask_remove == NULL );
    var_ask_remove = tmp_assign_source_24;

    tmp_iter_arg_5 = var_unwanted_files;

    tmp_assign_source_25 = MAKE_ITERATOR( tmp_iter_arg_5 );
    if ( tmp_assign_source_25 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 247;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_2__for_iterator == NULL );
    tmp_for_loop_2__for_iterator = tmp_assign_source_25;

    // Tried code:
    loop_start_5:;
    tmp_next_source_5 = tmp_for_loop_2__for_iterator;

    tmp_assign_source_26 = ITERATOR_NEXT( tmp_next_source_5 );
    if ( tmp_assign_source_26 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_5;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 247;
            goto try_except_handler_5;
        }
    }

    {
        PyObject *old = tmp_for_loop_2__iter_value;
        tmp_for_loop_2__iter_value = tmp_assign_source_26;
        Py_XDECREF( old );
    }

    tmp_assign_source_27 = tmp_for_loop_2__iter_value;

    {
        PyObject *old = var_f;
        var_f = tmp_assign_source_27;
        Py_INCREF( var_f );
        Py_XDECREF( old );
    }

    tmp_source_name_12 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_12 == NULL ))
    {
        tmp_source_name_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_12 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "os" );
        exception_tb = NULL;

        exception_lineno = 248;
        goto try_except_handler_5;
    }

    tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_path );
    if ( tmp_called_instance_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 248;
        goto try_except_handler_5;
    }
    tmp_args_element_name_6 = var_f;

    frame_function->f_lineno = 248;
    {
        PyObject *call_args[] = { tmp_args_element_name_6 };
        tmp_cond_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_exists, call_args );
    }

    Py_DECREF( tmp_called_instance_2 );
    if ( tmp_cond_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 248;
        goto try_except_handler_5;
    }
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        exception_lineno = 248;
        goto try_except_handler_5;
    }
    Py_DECREF( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_called_instance_3 = var_ask_remove;

    tmp_args_element_name_7 = var_f;

    frame_function->f_lineno = 249;
    {
        PyObject *call_args[] = { tmp_args_element_name_7 };
        tmp_unused = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_append, call_args );
    }

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 249;
        goto try_except_handler_5;
    }
    Py_DECREF( tmp_unused );
    branch_no_2:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 247;
        goto try_except_handler_5;
    }
    goto loop_start_5;
    loop_end_5:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    tmp_cond_value_2 = var_ask_remove;

    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 251;
        goto frame_exception_exit_1;
    }
    if ( tmp_cond_truth_2 == 1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_assign_source_28 = const_str_digest_c8f3d166477347e14e4757a9d9042089;
    assert( var_message == NULL );
    Py_INCREF( tmp_assign_source_28 );
    var_message = tmp_assign_source_28;

    tmp_iter_arg_6 = var_ask_remove;

    tmp_assign_source_29 = MAKE_ITERATOR( tmp_iter_arg_6 );
    if ( tmp_assign_source_29 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 254;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_3__for_iterator == NULL );
    tmp_for_loop_3__for_iterator = tmp_assign_source_29;

    // Tried code:
    loop_start_6:;
    tmp_next_source_6 = tmp_for_loop_3__for_iterator;

    tmp_assign_source_30 = ITERATOR_NEXT( tmp_next_source_6 );
    if ( tmp_assign_source_30 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_6;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 254;
            goto try_except_handler_6;
        }
    }

    {
        PyObject *old = tmp_for_loop_3__iter_value;
        tmp_for_loop_3__iter_value = tmp_assign_source_30;
        Py_XDECREF( old );
    }

    tmp_assign_source_31 = tmp_for_loop_3__iter_value;

    {
        PyObject *old = var_f;
        var_f = tmp_assign_source_31;
        Py_INCREF( var_f );
        Py_XDECREF( old );
    }

    tmp_left_name_4 = var_message;

    if ( tmp_left_name_4 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "local variable '%s' referenced before assignment", "message" );
        exception_tb = NULL;

        exception_lineno = 255;
        goto try_except_handler_6;
    }

    tmp_left_name_5 = const_str_digest_6c123f9166cb99007134f43a7cb75ca9;
    tmp_right_name_5 = var_f;

    tmp_right_name_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_5, tmp_right_name_5 );
    if ( tmp_right_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 255;
        goto try_except_handler_6;
    }
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_4, tmp_right_name_4 );
    tmp_assign_source_32 = tmp_left_name_4;
    Py_DECREF( tmp_right_name_4 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 255;
        goto try_except_handler_6;
    }
    var_message = tmp_assign_source_32;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 254;
        goto try_except_handler_6;
    }
    goto loop_start_6;
    loop_end_6:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( tmp_for_loop_3__iter_value );
    tmp_for_loop_3__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
    Py_DECREF( tmp_for_loop_3__for_iterator );
    tmp_for_loop_3__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_3__iter_value );
    tmp_for_loop_3__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
    Py_DECREF( tmp_for_loop_3__for_iterator );
    tmp_for_loop_3__for_iterator = NULL;

    tmp_left_name_6 = var_message;

    if ( tmp_left_name_6 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "local variable '%s' referenced before assignment", "message" );
        exception_tb = NULL;

        exception_lineno = 256;
        goto frame_exception_exit_1;
    }

    tmp_right_name_6 = const_str_digest_0f35c35a0eb6061c558e2354c7ddd13c;
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_6, tmp_right_name_6 );
    tmp_assign_source_33 = tmp_left_name_6;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 256;
        goto frame_exception_exit_1;
    }
    var_message = tmp_assign_source_33;

    // Tried code:
    tmp_import_globals_1 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_function->f_lineno = 259;
    tmp_assign_source_34 = IMPORT_MODULE( const_str_plain_warnings, tmp_import_globals_1, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_34 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 259;
        goto try_except_handler_7;
    }
    assert( var_warnings == NULL );
    var_warnings = tmp_assign_source_34;

    tmp_source_name_13 = var_warnings;

    tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_warn );
    if ( tmp_called_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 261;
        goto try_except_handler_7;
    }
    tmp_args_element_name_8 = var_message;

    tmp_args_element_name_9 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_RuntimeWarning );

    if (unlikely( tmp_args_element_name_9 == NULL ))
    {
        tmp_args_element_name_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RuntimeWarning );
    }

    if ( tmp_args_element_name_9 == NULL )
    {
        Py_DECREF( tmp_called_name_4 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "RuntimeWarning" );
        exception_tb = NULL;

        exception_lineno = 261;
        goto try_except_handler_7;
    }

    tmp_args_element_name_10 = const_int_pos_4;
    frame_function->f_lineno = 261;
    {
        PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9, tmp_args_element_name_10 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
    }

    Py_DECREF( tmp_called_name_4 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 261;
        goto try_except_handler_7;
    }
    Py_DECREF( tmp_unused );
    goto try_end_4;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_function );
    if ( exception_keeper_tb_6 == NULL )
    {
        exception_keeper_tb_6 = MAKE_TRACEBACK( frame_function, exception_keeper_lineno_6 );
    }
    else if ( exception_keeper_lineno_6 != -1 )
    {
        exception_keeper_tb_6 = ADD_TRACEBACK( exception_keeper_tb_6, frame_function, exception_keeper_lineno_6 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_6, &exception_keeper_value_6, &exception_keeper_tb_6 );
    PUBLISH_EXCEPTION( &exception_keeper_type_6, &exception_keeper_value_6, &exception_keeper_tb_6 );
    tmp_compare_left_2 = PyThreadState_GET()->exc_type;
    tmp_compare_right_2 = PyExc_ImportError;
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_2, tmp_compare_right_2 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 262;
        goto frame_exception_exit_1;
    }
    if ( tmp_exc_match_exception_match_1 == 1 )
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_print_value = var_message;

    if ( PRINT_ITEM( tmp_print_value ) == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 263;
        goto frame_exception_exit_1;
    }
    if ( PRINT_NEW_LINE() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 263;
        goto frame_exception_exit_1;
    }
    goto branch_end_4;
    branch_no_4:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_function) frame_function->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_4:;
    goto try_end_4;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$$$function_1_warn_unwanted_files );
    return NULL;
    // End of try:
    try_end_4:;
    branch_no_3:;

#if 1
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 1
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();
            if ( var_install_path )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_install_path,
                    var_install_path
                );

                assert( res == 0 );
            }

            if ( var_extension_ext )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_extension_ext,
                    var_extension_ext
                );

                assert( res == 0 );
            }

            if ( var_ext_to_remove )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_ext_to_remove,
                    var_ext_to_remove
                );

                assert( res == 0 );
            }

            if ( var_py_to_remove )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_py_to_remove,
                    var_py_to_remove
                );

                assert( res == 0 );
            }

            if ( var_x )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_x,
                    var_x
                );

                assert( res == 0 );
            }

            if ( var_extension_files )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_extension_files,
                    var_extension_files
                );

                assert( res == 0 );
            }

            if ( var_py_ext )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_py_ext,
                    var_py_ext
                );

                assert( res == 0 );
            }

            if ( var_py_files )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_py_files,
                    var_py_files
                );

                assert( res == 0 );
            }

            if ( var_files )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_files,
                    var_files
                );

                assert( res == 0 );
            }

            if ( var_unwanted_files )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_unwanted_files,
                    var_unwanted_files
                );

                assert( res == 0 );
            }

            if ( var_f )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_f,
                    var_f
                );

                assert( res == 0 );
            }

            if ( var_ask_remove )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_ask_remove,
                    var_ask_remove
                );

                assert( res == 0 );
            }

            if ( var_message )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_message,
                    var_message
                );

                assert( res == 0 );
            }

            if ( var_warnings )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_warnings,
                    var_warnings
                );

                assert( res == 0 );
            }



            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$$$function_1_warn_unwanted_files );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_install_path );
    Py_DECREF( var_install_path );
    var_install_path = NULL;

    CHECK_OBJECT( (PyObject *)var_extension_ext );
    Py_DECREF( var_extension_ext );
    var_extension_ext = NULL;

    CHECK_OBJECT( (PyObject *)var_ext_to_remove );
    Py_DECREF( var_ext_to_remove );
    var_ext_to_remove = NULL;

    CHECK_OBJECT( (PyObject *)var_py_to_remove );
    Py_DECREF( var_py_to_remove );
    var_py_to_remove = NULL;

    Py_XDECREF( var_x );
    var_x = NULL;

    CHECK_OBJECT( (PyObject *)var_extension_files );
    Py_DECREF( var_extension_files );
    var_extension_files = NULL;

    Py_XDECREF( var_py_ext );
    var_py_ext = NULL;

    CHECK_OBJECT( (PyObject *)var_py_files );
    Py_DECREF( var_py_files );
    var_py_files = NULL;

    CHECK_OBJECT( (PyObject *)var_files );
    Py_DECREF( var_files );
    var_files = NULL;

    CHECK_OBJECT( (PyObject *)var_unwanted_files );
    Py_DECREF( var_unwanted_files );
    var_unwanted_files = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    CHECK_OBJECT( (PyObject *)var_ask_remove );
    Py_DECREF( var_ask_remove );
    var_ask_remove = NULL;

    Py_XDECREF( var_message );
    var_message = NULL;

    Py_XDECREF( var_warnings );
    var_warnings = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( var_install_path );
    var_install_path = NULL;

    Py_XDECREF( var_extension_ext );
    var_extension_ext = NULL;

    Py_XDECREF( var_ext_to_remove );
    var_ext_to_remove = NULL;

    Py_XDECREF( var_py_to_remove );
    var_py_to_remove = NULL;

    Py_XDECREF( var_x );
    var_x = NULL;

    Py_XDECREF( var_extension_files );
    var_extension_files = NULL;

    Py_XDECREF( var_py_ext );
    var_py_ext = NULL;

    Py_XDECREF( var_py_files );
    var_py_files = NULL;

    Py_XDECREF( var_files );
    var_files = NULL;

    Py_XDECREF( var_unwanted_files );
    var_unwanted_files = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_ask_remove );
    var_ask_remove = NULL;

    Py_XDECREF( var_message );
    var_message = NULL;

    Py_XDECREF( var_warnings );
    var_warnings = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$function_1_warn_unwanted_files );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_pygame$$$function_2_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *tmp_frame_locals;
    PyObject *tmp_return_value;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_8be58a0ea832634d7416aca5115f8ecc, module_pygame );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_return_value = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Missing_Function );

    if (unlikely( tmp_return_value == NULL ))
    {
        tmp_return_value = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Missing_Function );
    }

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "Missing_Function" );
        exception_tb = NULL;

        exception_lineno = 273;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto function_return_exit;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();


            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$function_2_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_pygame$$$function_3_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *tmp_frame_locals;
    PyObject *tmp_return_value;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_888f561eb46bb0575657e15bb4d113d5, module_pygame );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_return_value = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Missing_Function );

    if (unlikely( tmp_return_value == NULL ))
    {
        tmp_return_value = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Missing_Function );
    }

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "Missing_Function" );
        exception_tb = NULL;

        exception_lineno = 280;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto function_return_exit;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();


            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$function_3_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_pygame$$$function_4_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *tmp_frame_locals;
    PyObject *tmp_return_value;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_5d8f0b46bf818938aa1ad9fc8273f52c, module_pygame );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_return_value = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Missing_Function );

    if (unlikely( tmp_return_value == NULL ))
    {
        tmp_return_value = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Missing_Function );
    }

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "Missing_Function" );
        exception_tb = NULL;

        exception_lineno = 285;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto function_return_exit;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();


            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$function_4_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_pygame$$$function_5_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *tmp_frame_locals;
    PyObject *tmp_return_value;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_68ad5cffb1ffd264c465be50849008ed, module_pygame );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_return_value = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Missing_Function );

    if (unlikely( tmp_return_value == NULL ))
    {
        tmp_return_value = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Missing_Function );
    }

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "Missing_Function" );
        exception_tb = NULL;

        exception_lineno = 290;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto function_return_exit;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();


            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$function_5_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_pygame$$$function_6_packager_imports( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_atexit = NULL;
    PyObject *var_numpy = NULL;
    PyObject *var_OpenGL = NULL;
    PyObject *var_pygame = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_frame_locals;
    PyObject *tmp_import_globals_1;
    PyObject *tmp_import_globals_2;
    PyObject *tmp_import_globals_3;
    PyObject *tmp_import_globals_4;
    PyObject *tmp_import_globals_5;
    PyObject *tmp_import_globals_6;
    PyObject *tmp_import_globals_7;
    PyObject *tmp_return_value;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_d5ea83a9c0c823d105f3b8cd2facff47, module_pygame );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_import_globals_1 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_function->f_lineno = 372;
    tmp_assign_source_1 = IMPORT_MODULE( const_str_plain_atexit, tmp_import_globals_1, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 372;
        goto frame_exception_exit_1;
    }
    assert( var_atexit == NULL );
    var_atexit = tmp_assign_source_1;

    tmp_import_globals_2 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_function->f_lineno = 373;
    tmp_assign_source_2 = IMPORT_MODULE( const_str_plain_numpy, tmp_import_globals_2, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 373;
        goto frame_exception_exit_1;
    }
    assert( var_numpy == NULL );
    var_numpy = tmp_assign_source_2;

    tmp_import_globals_3 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_function->f_lineno = 374;
    tmp_assign_source_3 = IMPORT_MODULE( const_str_digest_8baa0629094b3ae0d209f2bab26a380b, tmp_import_globals_3, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 374;
        goto frame_exception_exit_1;
    }
    assert( var_OpenGL == NULL );
    var_OpenGL = tmp_assign_source_3;

    tmp_import_globals_4 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_function->f_lineno = 375;
    tmp_assign_source_4 = IMPORT_MODULE( const_str_digest_b646956a0fd89f2f0ee5168bcdb2f2b8, tmp_import_globals_4, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 375;
        goto frame_exception_exit_1;
    }
    assert( var_pygame == NULL );
    var_pygame = tmp_assign_source_4;

    tmp_import_globals_5 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_function->f_lineno = 376;
    tmp_assign_source_5 = IMPORT_MODULE( const_str_digest_887953ae3c9b867d7ff6903985ec2d3c, tmp_import_globals_5, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 376;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_pygame;
        assert( old != NULL );
        var_pygame = tmp_assign_source_5;
        Py_DECREF( old );
    }

    tmp_import_globals_6 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_function->f_lineno = 377;
    tmp_assign_source_6 = IMPORT_MODULE( const_str_digest_df4bac290232ff9f319b12d3d498971c, tmp_import_globals_6, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 377;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_pygame;
        assert( old != NULL );
        var_pygame = tmp_assign_source_6;
        Py_DECREF( old );
    }

    tmp_import_globals_7 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_function->f_lineno = 378;
    tmp_assign_source_7 = IMPORT_MODULE( const_str_digest_ef6d38a8ea87f7841d4186a2ae1508bd, tmp_import_globals_7, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 378;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_pygame;
        assert( old != NULL );
        var_pygame = tmp_assign_source_7;
        Py_DECREF( old );
    }


#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();
            if ( var_atexit )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_atexit,
                    var_atexit
                );

                assert( res == 0 );
            }

            if ( var_numpy )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_numpy,
                    var_numpy
                );

                assert( res == 0 );
            }

            if ( var_OpenGL )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_OpenGL,
                    var_OpenGL
                );

                assert( res == 0 );
            }

            if ( var_pygame )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_pygame,
                    var_pygame
                );

                assert( res == 0 );
            }



            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$$$function_6_packager_imports );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_atexit );
    Py_DECREF( var_atexit );
    var_atexit = NULL;

    CHECK_OBJECT( (PyObject *)var_numpy );
    Py_DECREF( var_numpy );
    var_numpy = NULL;

    CHECK_OBJECT( (PyObject *)var_OpenGL );
    Py_DECREF( var_OpenGL );
    var_OpenGL = NULL;

    CHECK_OBJECT( (PyObject *)var_pygame );
    Py_DECREF( var_pygame );
    var_pygame = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( var_atexit );
    var_atexit = NULL;

    Py_XDECREF( var_numpy );
    var_numpy = NULL;

    Py_XDECREF( var_OpenGL );
    var_OpenGL = NULL;

    Py_XDECREF( var_pygame );
    var_pygame = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$function_6_packager_imports );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_pygame$$$function_7___rect_constructor( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    PyObject *par_y = python_pars[ 1 ];
    PyObject *par_w = python_pars[ 2 ];
    PyObject *par_h = python_pars[ 3 ];
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_called_name_1;
    PyObject *tmp_frame_locals;
    PyObject *tmp_return_value;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_0d5b6e5d8fb8c07220d89866742ba372, module_pygame );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Rect );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Rect );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "Rect" );
        exception_tb = NULL;

        exception_lineno = 388;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_1 = par_x;

    tmp_args_element_name_2 = par_y;

    tmp_args_element_name_3 = par_w;

    tmp_args_element_name_4 = par_h;

    frame_function->f_lineno = 388;
    {
        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
        tmp_return_value = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
    }

    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 388;
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto try_return_handler_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();
            if ( par_x )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_x,
                    par_x
                );

                assert( res == 0 );
            }

            if ( par_y )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_y,
                    par_y
                );

                assert( res == 0 );
            }

            if ( par_w )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_w,
                    par_w
                );

                assert( res == 0 );
            }

            if ( par_h )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_h,
                    par_h
                );

                assert( res == 0 );
            }



            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$$$function_7___rect_constructor );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    CHECK_OBJECT( (PyObject *)par_y );
    Py_DECREF( par_y );
    par_y = NULL;

    CHECK_OBJECT( (PyObject *)par_w );
    Py_DECREF( par_w );
    par_w = NULL;

    CHECK_OBJECT( (PyObject *)par_h );
    Py_DECREF( par_h );
    par_h = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    CHECK_OBJECT( (PyObject *)par_y );
    Py_DECREF( par_y );
    par_y = NULL;

    CHECK_OBJECT( (PyObject *)par_w );
    Py_DECREF( par_w );
    par_w = NULL;

    CHECK_OBJECT( (PyObject *)par_h );
    Py_DECREF( par_h );
    par_h = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$function_7___rect_constructor );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_pygame$$$function_8___rect_reduce( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_r = python_pars[ 0 ];
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    int tmp_cmp_Eq_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_frame_locals;
    PyObject *tmp_raise_type_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    PyObject *tmp_type_arg_1;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_9488e2d0f026bd566833afd6d12d6f30, module_pygame );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_type_arg_1 = par_r;

    tmp_compare_left_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
    assert( tmp_compare_left_1 != NULL );
    tmp_compare_right_1 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Rect );

    if (unlikely( tmp_compare_right_1 == NULL ))
    {
        tmp_compare_right_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Rect );
    }

    if ( tmp_compare_right_1 == NULL )
    {
        Py_DECREF( tmp_compare_left_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "Rect" );
        exception_tb = NULL;

        exception_lineno = 392;
        goto frame_exception_exit_1;
    }

    tmp_cmp_Eq_1 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_cmp_Eq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_1 );

        exception_lineno = 392;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_left_1 );
    if ( tmp_cmp_Eq_1 == 1 )
    {
        goto branch_no_1;
    }
    else
    {
        goto branch_yes_1;
    }
    branch_yes_1:;
    tmp_raise_type_1 = PyExc_AssertionError;
    exception_type = tmp_raise_type_1;
    Py_INCREF( tmp_raise_type_1 );
    exception_lineno = 392;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    goto frame_exception_exit_1;
    branch_no_1:;
    tmp_return_value = PyTuple_New( 2 );
    tmp_tuple_element_1 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain___rect_constructor );

    if (unlikely( tmp_tuple_element_1 == NULL ))
    {
        tmp_tuple_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___rect_constructor );
    }

    if ( tmp_tuple_element_1 == NULL )
    {
        Py_DECREF( tmp_return_value );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "__rect_constructor" );
        exception_tb = NULL;

        exception_lineno = 393;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = PyTuple_New( 4 );
    tmp_source_name_1 = par_r;

    tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_x );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );
        Py_DECREF( tmp_tuple_element_1 );

        exception_lineno = 393;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_tuple_element_1, 0, tmp_tuple_element_2 );
    tmp_source_name_2 = par_r;

    tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_y );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );
        Py_DECREF( tmp_tuple_element_1 );

        exception_lineno = 393;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_tuple_element_1, 1, tmp_tuple_element_2 );
    tmp_source_name_3 = par_r;

    tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_w );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );
        Py_DECREF( tmp_tuple_element_1 );

        exception_lineno = 393;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_tuple_element_1, 2, tmp_tuple_element_2 );
    tmp_source_name_4 = par_r;

    tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_h );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );
        Py_DECREF( tmp_tuple_element_1 );

        exception_lineno = 393;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_tuple_element_1, 3, tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto try_return_handler_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();
            if ( par_r )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_r,
                    par_r
                );

                assert( res == 0 );
            }



            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$$$function_8___rect_reduce );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_r );
    Py_DECREF( par_r );
    par_r = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    CHECK_OBJECT( (PyObject *)par_r );
    Py_DECREF( par_r );
    par_r = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$function_8___rect_reduce );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_pygame$$$function_9___color_constructor( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_r = python_pars[ 0 ];
    PyObject *par_g = python_pars[ 1 ];
    PyObject *par_b = python_pars[ 2 ];
    PyObject *par_a = python_pars[ 3 ];
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_called_name_1;
    PyObject *tmp_frame_locals;
    PyObject *tmp_return_value;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_f1509337cc982b665a24d58434f03494, module_pygame );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Color );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Color );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "Color" );
        exception_tb = NULL;

        exception_lineno = 399;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_1 = par_r;

    tmp_args_element_name_2 = par_g;

    tmp_args_element_name_3 = par_b;

    tmp_args_element_name_4 = par_a;

    frame_function->f_lineno = 399;
    {
        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
        tmp_return_value = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
    }

    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 399;
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto try_return_handler_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();
            if ( par_r )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_r,
                    par_r
                );

                assert( res == 0 );
            }

            if ( par_g )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_g,
                    par_g
                );

                assert( res == 0 );
            }

            if ( par_b )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_b,
                    par_b
                );

                assert( res == 0 );
            }

            if ( par_a )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_a,
                    par_a
                );

                assert( res == 0 );
            }



            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$$$function_9___color_constructor );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_r );
    Py_DECREF( par_r );
    par_r = NULL;

    CHECK_OBJECT( (PyObject *)par_g );
    Py_DECREF( par_g );
    par_g = NULL;

    CHECK_OBJECT( (PyObject *)par_b );
    Py_DECREF( par_b );
    par_b = NULL;

    CHECK_OBJECT( (PyObject *)par_a );
    Py_DECREF( par_a );
    par_a = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    CHECK_OBJECT( (PyObject *)par_r );
    Py_DECREF( par_r );
    par_r = NULL;

    CHECK_OBJECT( (PyObject *)par_g );
    Py_DECREF( par_g );
    par_g = NULL;

    CHECK_OBJECT( (PyObject *)par_b );
    Py_DECREF( par_b );
    par_b = NULL;

    CHECK_OBJECT( (PyObject *)par_a );
    Py_DECREF( par_a );
    par_a = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$function_9___color_constructor );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_pygame$$$function_10___color_reduce( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_c = python_pars[ 0 ];
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    int tmp_cmp_Eq_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_frame_locals;
    PyObject *tmp_raise_type_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    PyObject *tmp_type_arg_1;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_0eb6abfa5b89cc7690bf6ea1efdcbaa1, module_pygame );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_type_arg_1 = par_c;

    tmp_compare_left_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
    assert( tmp_compare_left_1 != NULL );
    tmp_compare_right_1 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Color );

    if (unlikely( tmp_compare_right_1 == NULL ))
    {
        tmp_compare_right_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Color );
    }

    if ( tmp_compare_right_1 == NULL )
    {
        Py_DECREF( tmp_compare_left_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "Color" );
        exception_tb = NULL;

        exception_lineno = 403;
        goto frame_exception_exit_1;
    }

    tmp_cmp_Eq_1 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_cmp_Eq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_1 );

        exception_lineno = 403;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_left_1 );
    if ( tmp_cmp_Eq_1 == 1 )
    {
        goto branch_no_1;
    }
    else
    {
        goto branch_yes_1;
    }
    branch_yes_1:;
    tmp_raise_type_1 = PyExc_AssertionError;
    exception_type = tmp_raise_type_1;
    Py_INCREF( tmp_raise_type_1 );
    exception_lineno = 403;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    goto frame_exception_exit_1;
    branch_no_1:;
    tmp_return_value = PyTuple_New( 2 );
    tmp_tuple_element_1 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain___color_constructor );

    if (unlikely( tmp_tuple_element_1 == NULL ))
    {
        tmp_tuple_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___color_constructor );
    }

    if ( tmp_tuple_element_1 == NULL )
    {
        Py_DECREF( tmp_return_value );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "global name '%s' is not defined", "__color_constructor" );
        exception_tb = NULL;

        exception_lineno = 404;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = PyTuple_New( 4 );
    tmp_source_name_1 = par_c;

    tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_r );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );
        Py_DECREF( tmp_tuple_element_1 );

        exception_lineno = 404;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_tuple_element_1, 0, tmp_tuple_element_2 );
    tmp_source_name_2 = par_c;

    tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_g );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );
        Py_DECREF( tmp_tuple_element_1 );

        exception_lineno = 404;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_tuple_element_1, 1, tmp_tuple_element_2 );
    tmp_source_name_3 = par_c;

    tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_b );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );
        Py_DECREF( tmp_tuple_element_1 );

        exception_lineno = 404;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_tuple_element_1, 2, tmp_tuple_element_2 );
    tmp_source_name_4 = par_c;

    tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_a );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );
        Py_DECREF( tmp_tuple_element_1 );

        exception_lineno = 404;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_tuple_element_1, 3, tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto try_return_handler_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();
            if ( par_c )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_c,
                    par_c
                );

                assert( res == 0 );
            }



            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$$$function_10___color_reduce );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_c );
    Py_DECREF( par_c );
    par_c = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    CHECK_OBJECT( (PyObject *)par_c );
    Py_DECREF( par_c );
    par_c = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$$$function_10___color_reduce );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}



static PyObject *MAKE_FUNCTION_pygame$$$class_1_MissingModule$$$function_1___init__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$$$class_1_MissingModule$$$function_1___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_7500321594c6bfc0890afb341857d467,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygame$$$class_1_MissingModule$$$function_2___getattr__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$$$class_1_MissingModule$$$function_2___getattr__,
        const_str_plain___getattr__,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_fefabc1607c3488e9c496958c63958f5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygame$$$class_1_MissingModule$$$function_3___nonzero__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$$$class_1_MissingModule$$$function_3___nonzero__,
        const_str_plain___nonzero__,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_61b2705797fcf52d0d0432331b24a84b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygame$$$class_1_MissingModule$$$function_4_warn(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$$$class_1_MissingModule$$$function_4_warn,
        const_str_plain_warn,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_8fcae91c12da0870d2b84d1df9e1f101,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygame$$$function_10___color_reduce(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$$$function_10___color_reduce,
        const_str_plain___color_reduce,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_0eb6abfa5b89cc7690bf6ea1efdcbaa1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygame$$$function_1_warn_unwanted_files(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$$$function_1_warn_unwanted_files,
        const_str_plain_warn_unwanted_files,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_d6e9e297db4118c3f15c126c4012d5d1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame,
        const_str_digest_5eca16d15671924950b779eaf97354e8,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygame$$$function_2_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$$$function_2_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_8be58a0ea832634d7416aca5115f8ecc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygame$$$function_3_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$$$function_3_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_888f561eb46bb0575657e15bb4d113d5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygame$$$function_4_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$$$function_4_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_5d8f0b46bf818938aa1ad9fc8273f52c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygame$$$function_5_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$$$function_5_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_68ad5cffb1ffd264c465be50849008ed,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygame$$$function_6_packager_imports(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$$$function_6_packager_imports,
        const_str_plain_packager_imports,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_d5ea83a9c0c823d105f3b8cd2facff47,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame,
        const_str_digest_fb7343c4cb62afaab89b717acddf9b6f,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygame$$$function_7___rect_constructor(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$$$function_7___rect_constructor,
        const_str_plain___rect_constructor,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_0d5b6e5d8fb8c07220d89866742ba372,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygame$$$function_8___rect_reduce(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$$$function_8___rect_reduce,
        const_str_plain___rect_reduce,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_9488e2d0f026bd566833afd6d12d6f30,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygame$$$function_9___color_constructor(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$$$function_9___color_constructor,
        const_str_plain___color_constructor,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_f1509337cc982b665a24d58434f03494,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame,
        Py_None,
        0
    );


    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pygame =
{
    PyModuleDef_HEAD_INIT,
    "pygame",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

#if PYTHON_VERSION >= 300
extern PyObject *metapath_based_loader;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineType();
extern void _initCompiledCoroutineWrapperType();
#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( pygame )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pygame );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();

#if PYTHON_VERSION >= 350
    _initCompiledCoroutineType();
    _initCompiledCoroutineWrapperType();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pygame: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pygame: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpygame" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pygame = Py_InitModule4(
        "pygame",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No __doc__ is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else
    module_pygame = PyModule_Create( &mdef_pygame );
#endif

    moduledict_pygame = (PyDictObject *)((PyModuleObject *)module_pygame)->md_dict;

    CHECK_OBJECT( module_pygame );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_plain_pygame, module_pygame );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    PyObject *module_dict = PyModule_GetDict( module_pygame );

    if ( PyDict_GetItem( module_dict, const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

#ifndef __NUITKA_NO_ASSERT__
        int res =
#endif
            PyDict_SetItem( module_dict, const_str_plain___builtins__, value );

        assert( res == 0 );
    }

#if PYTHON_VERSION >= 330
    PyDict_SetItem( module_dict, const_str_plain___loader__, metapath_based_loader );
#endif

    // Temp variables if any
    PyObject *tmp_class_creation_1__class_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__class = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;
    PyObject *exception_keeper_type_20;
    PyObject *exception_keeper_value_20;
    PyTracebackObject *exception_keeper_tb_20;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_20;
    PyObject *exception_keeper_type_21;
    PyObject *exception_keeper_value_21;
    PyTracebackObject *exception_keeper_tb_21;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_21;
    PyObject *exception_keeper_type_22;
    PyObject *exception_keeper_value_22;
    PyTracebackObject *exception_keeper_tb_22;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_22;
    PyObject *exception_keeper_type_23;
    PyObject *exception_keeper_value_23;
    PyTracebackObject *exception_keeper_tb_23;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_23;
    PyObject *exception_keeper_type_24;
    PyObject *exception_keeper_value_24;
    PyTracebackObject *exception_keeper_tb_24;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_24;
    PyObject *exception_keeper_type_25;
    PyObject *exception_keeper_value_25;
    PyTracebackObject *exception_keeper_tb_25;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_25;
    PyObject *exception_keeper_type_26;
    PyObject *exception_keeper_value_26;
    PyTracebackObject *exception_keeper_tb_26;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_26;
    PyObject *exception_keeper_type_27;
    PyObject *exception_keeper_value_27;
    PyTracebackObject *exception_keeper_tb_27;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_27;
    PyObject *exception_keeper_type_28;
    PyObject *exception_keeper_value_28;
    PyTracebackObject *exception_keeper_tb_28;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_28;
    PyObject *exception_keeper_type_29;
    PyObject *exception_keeper_value_29;
    PyTracebackObject *exception_keeper_tb_29;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_29;
    PyObject *exception_keeper_type_30;
    PyObject *exception_keeper_value_30;
    PyTracebackObject *exception_keeper_tb_30;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_30;
    PyObject *exception_keeper_type_31;
    PyObject *exception_keeper_value_31;
    PyTracebackObject *exception_keeper_tb_31;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_31;
    PyObject *exception_keeper_type_32;
    PyObject *exception_keeper_value_32;
    PyTracebackObject *exception_keeper_tb_32;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_32;
    PyObject *exception_keeper_type_33;
    PyObject *exception_keeper_value_33;
    PyTracebackObject *exception_keeper_tb_33;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_33;
    int tmp_and_left_truth_1;
    PyObject *tmp_and_left_value_1;
    PyObject *tmp_and_right_value_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_element_name_6;
    PyObject *tmp_args_element_name_7;
    PyObject *tmp_args_element_name_8;
    PyObject *tmp_args_element_name_9;
    PyObject *tmp_args_element_name_10;
    PyObject *tmp_args_element_name_11;
    PyObject *tmp_args_element_name_12;
    PyObject *tmp_args_element_name_13;
    PyObject *tmp_args_element_name_14;
    PyObject *tmp_args_element_name_15;
    PyObject *tmp_args_element_name_16;
    PyObject *tmp_args_element_name_17;
    PyObject *tmp_args_element_name_18;
    PyObject *tmp_args_element_name_19;
    PyObject *tmp_args_element_name_20;
    PyObject *tmp_args_element_name_21;
    PyObject *tmp_args_element_name_22;
    PyObject *tmp_args_element_name_23;
    PyObject *tmp_args_element_name_24;
    PyObject *tmp_args_element_name_25;
    PyObject *tmp_args_element_name_26;
    PyObject *tmp_args_element_name_27;
    PyObject *tmp_args_element_name_28;
    PyObject *tmp_args_element_name_29;
    PyObject *tmp_args_element_name_30;
    PyObject *tmp_args_element_name_31;
    PyObject *tmp_args_element_name_32;
    PyObject *tmp_args_element_name_33;
    PyObject *tmp_args_element_name_34;
    PyObject *tmp_args_element_name_35;
    PyObject *tmp_args_element_name_36;
    PyObject *tmp_args_element_name_37;
    PyObject *tmp_args_element_name_38;
    PyObject *tmp_args_element_name_39;
    PyObject *tmp_args_element_name_40;
    PyObject *tmp_args_element_name_41;
    PyObject *tmp_args_element_name_42;
    PyObject *tmp_args_element_name_43;
    PyObject *tmp_args_element_name_44;
    PyObject *tmp_args_element_name_45;
    PyObject *tmp_args_element_name_46;
    PyObject *tmp_args_element_name_47;
    PyObject *tmp_args_element_name_48;
    PyObject *tmp_args_element_name_49;
    PyObject *tmp_args_element_name_50;
    PyObject *tmp_args_element_name_51;
    PyObject *tmp_args_element_name_52;
    PyObject *tmp_args_element_name_53;
    PyObject *tmp_args_element_name_54;
    PyObject *tmp_args_element_name_55;
    PyObject *tmp_args_element_name_56;
    PyObject *tmp_args_element_name_57;
    PyObject *tmp_args_element_name_58;
    PyObject *tmp_args_element_name_59;
    PyObject *tmp_args_element_name_60;
    PyObject *tmp_args_element_name_61;
    PyObject *tmp_args_element_name_62;
    PyObject *tmp_args_element_name_63;
    PyObject *tmp_args_element_name_64;
    PyObject *tmp_args_element_name_65;
    PyObject *tmp_args_element_name_66;
    PyObject *tmp_args_element_name_67;
    PyObject *tmp_args_element_name_68;
    PyObject *tmp_args_element_name_69;
    PyObject *tmp_args_element_name_70;
    PyObject *tmp_args_element_name_71;
    PyObject *tmp_args_element_name_72;
    PyObject *tmp_args_element_name_73;
    PyObject *tmp_args_element_name_74;
    PyObject *tmp_args_element_name_75;
    PyObject *tmp_args_element_name_76;
    PyObject *tmp_args_element_name_77;
    PyObject *tmp_args_element_name_78;
    PyObject *tmp_args_element_name_79;
    PyObject *tmp_args_element_name_80;
    PyObject *tmp_ass_subscribed_1;
    PyObject *tmp_ass_subscribed_2;
    PyObject *tmp_ass_subscribed_3;
    PyObject *tmp_ass_subscript_1;
    PyObject *tmp_ass_subscript_2;
    PyObject *tmp_ass_subscript_3;
    PyObject *tmp_ass_subvalue_1;
    PyObject *tmp_ass_subvalue_2;
    PyObject *tmp_ass_subvalue_3;
    PyObject *tmp_assattr_name_1;
    PyObject *tmp_assattr_name_2;
    PyObject *tmp_assattr_name_3;
    PyObject *tmp_assattr_target_1;
    PyObject *tmp_assattr_target_2;
    PyObject *tmp_assattr_target_3;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    PyObject *tmp_assign_source_28;
    PyObject *tmp_assign_source_29;
    PyObject *tmp_assign_source_30;
    PyObject *tmp_assign_source_31;
    PyObject *tmp_assign_source_32;
    PyObject *tmp_assign_source_33;
    PyObject *tmp_assign_source_34;
    PyObject *tmp_assign_source_35;
    PyObject *tmp_assign_source_36;
    PyObject *tmp_assign_source_37;
    PyObject *tmp_assign_source_38;
    PyObject *tmp_assign_source_39;
    PyObject *tmp_assign_source_40;
    PyObject *tmp_assign_source_41;
    PyObject *tmp_assign_source_42;
    PyObject *tmp_assign_source_43;
    PyObject *tmp_assign_source_44;
    PyObject *tmp_assign_source_45;
    PyObject *tmp_assign_source_46;
    PyObject *tmp_assign_source_47;
    PyObject *tmp_assign_source_48;
    PyObject *tmp_assign_source_49;
    PyObject *tmp_assign_source_50;
    PyObject *tmp_assign_source_51;
    PyObject *tmp_assign_source_52;
    PyObject *tmp_assign_source_53;
    PyObject *tmp_assign_source_54;
    PyObject *tmp_assign_source_55;
    PyObject *tmp_assign_source_56;
    PyObject *tmp_assign_source_57;
    PyObject *tmp_assign_source_58;
    PyObject *tmp_assign_source_59;
    PyObject *tmp_assign_source_60;
    PyObject *tmp_assign_source_61;
    PyObject *tmp_assign_source_62;
    PyObject *tmp_assign_source_63;
    PyObject *tmp_assign_source_64;
    PyObject *tmp_assign_source_65;
    PyObject *tmp_assign_source_66;
    PyObject *tmp_assign_source_67;
    PyObject *tmp_assign_source_68;
    PyObject *tmp_assign_source_69;
    PyObject *tmp_assign_source_70;
    PyObject *tmp_assign_source_71;
    PyObject *tmp_assign_source_72;
    PyObject *tmp_assign_source_73;
    PyObject *tmp_assign_source_74;
    PyObject *tmp_assign_source_75;
    PyObject *tmp_assign_source_76;
    PyObject *tmp_assign_source_77;
    PyObject *tmp_assign_source_78;
    PyObject *tmp_assign_source_79;
    PyObject *tmp_assign_source_80;
    PyObject *tmp_assign_source_81;
    PyObject *tmp_assign_source_82;
    PyObject *tmp_assign_source_83;
    PyObject *tmp_assign_source_84;
    PyObject *tmp_assign_source_85;
    PyObject *tmp_assign_source_86;
    PyObject *tmp_assign_source_87;
    PyObject *tmp_assign_source_88;
    PyObject *tmp_attrdel_target_1;
    PyObject *tmp_bases_name_1;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_instance_2;
    PyObject *tmp_called_instance_3;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_called_name_5;
    PyObject *tmp_called_name_6;
    PyObject *tmp_called_name_7;
    PyObject *tmp_called_name_8;
    PyObject *tmp_called_name_9;
    PyObject *tmp_called_name_10;
    PyObject *tmp_called_name_11;
    PyObject *tmp_called_name_12;
    PyObject *tmp_called_name_13;
    PyObject *tmp_called_name_14;
    PyObject *tmp_called_name_15;
    PyObject *tmp_called_name_16;
    PyObject *tmp_called_name_17;
    PyObject *tmp_called_name_18;
    PyObject *tmp_called_name_19;
    PyObject *tmp_called_name_20;
    PyObject *tmp_called_name_21;
    PyObject *tmp_called_name_22;
    PyObject *tmp_called_name_23;
    PyObject *tmp_called_name_24;
    PyObject *tmp_called_name_25;
    PyObject *tmp_called_name_26;
    PyObject *tmp_called_name_27;
    PyObject *tmp_called_name_28;
    PyObject *tmp_called_name_29;
    PyObject *tmp_called_name_30;
    PyObject *tmp_called_name_31;
    PyObject *tmp_called_name_32;
    PyObject *tmp_called_name_33;
    PyObject *tmp_called_name_34;
    PyObject *tmp_called_name_35;
    PyObject *tmp_called_name_36;
    PyObject *tmp_called_name_37;
    PyObject *tmp_called_name_38;
    PyObject *tmp_called_name_39;
    PyObject *tmp_called_name_40;
    PyObject *tmp_called_name_41;
    PyObject *tmp_called_name_42;
    PyObject *tmp_called_name_43;
    PyObject *tmp_called_name_44;
    PyObject *tmp_called_name_45;
    PyObject *tmp_called_name_46;
    PyObject *tmp_called_name_47;
    PyObject *tmp_called_name_48;
    PyObject *tmp_called_name_49;
    PyObject *tmp_called_name_50;
    int tmp_cmp_Eq_1;
    int tmp_cmp_Eq_2;
    int tmp_cmp_GtE_1;
    int tmp_cmp_GtE_2;
    int tmp_cmp_In_1;
    int tmp_cmp_In_2;
    int tmp_cmp_NotIn_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_left_4;
    PyObject *tmp_compare_left_5;
    PyObject *tmp_compare_left_6;
    PyObject *tmp_compare_left_7;
    PyObject *tmp_compare_left_8;
    PyObject *tmp_compare_left_9;
    PyObject *tmp_compare_left_10;
    PyObject *tmp_compare_left_11;
    PyObject *tmp_compare_left_12;
    PyObject *tmp_compare_left_13;
    PyObject *tmp_compare_left_14;
    PyObject *tmp_compare_left_15;
    PyObject *tmp_compare_left_16;
    PyObject *tmp_compare_left_17;
    PyObject *tmp_compare_left_18;
    PyObject *tmp_compare_left_19;
    PyObject *tmp_compare_left_20;
    PyObject *tmp_compare_left_21;
    PyObject *tmp_compare_left_22;
    PyObject *tmp_compare_left_23;
    PyObject *tmp_compare_left_24;
    PyObject *tmp_compare_left_25;
    PyObject *tmp_compare_left_26;
    PyObject *tmp_compare_left_27;
    PyObject *tmp_compare_left_28;
    PyObject *tmp_compare_left_29;
    PyObject *tmp_compare_left_30;
    PyObject *tmp_compare_left_31;
    PyObject *tmp_compare_left_32;
    PyObject *tmp_compare_left_33;
    PyObject *tmp_compare_left_34;
    PyObject *tmp_compare_left_35;
    PyObject *tmp_compare_left_36;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    PyObject *tmp_compare_right_4;
    PyObject *tmp_compare_right_5;
    PyObject *tmp_compare_right_6;
    PyObject *tmp_compare_right_7;
    PyObject *tmp_compare_right_8;
    PyObject *tmp_compare_right_9;
    PyObject *tmp_compare_right_10;
    PyObject *tmp_compare_right_11;
    PyObject *tmp_compare_right_12;
    PyObject *tmp_compare_right_13;
    PyObject *tmp_compare_right_14;
    PyObject *tmp_compare_right_15;
    PyObject *tmp_compare_right_16;
    PyObject *tmp_compare_right_17;
    PyObject *tmp_compare_right_18;
    PyObject *tmp_compare_right_19;
    PyObject *tmp_compare_right_20;
    PyObject *tmp_compare_right_21;
    PyObject *tmp_compare_right_22;
    PyObject *tmp_compare_right_23;
    PyObject *tmp_compare_right_24;
    PyObject *tmp_compare_right_25;
    PyObject *tmp_compare_right_26;
    PyObject *tmp_compare_right_27;
    PyObject *tmp_compare_right_28;
    PyObject *tmp_compare_right_29;
    PyObject *tmp_compare_right_30;
    PyObject *tmp_compare_right_31;
    PyObject *tmp_compare_right_32;
    PyObject *tmp_compare_right_33;
    PyObject *tmp_compare_right_34;
    PyObject *tmp_compare_right_35;
    PyObject *tmp_compare_right_36;
    PyObject *tmp_compexpr_left_1;
    PyObject *tmp_compexpr_left_2;
    PyObject *tmp_compexpr_right_1;
    PyObject *tmp_compexpr_right_2;
    int tmp_cond_truth_1;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_dict_name_1;
    int tmp_exc_match_exception_match_1;
    int tmp_exc_match_exception_match_2;
    int tmp_exc_match_exception_match_3;
    int tmp_exc_match_exception_match_4;
    int tmp_exc_match_exception_match_5;
    int tmp_exc_match_exception_match_6;
    int tmp_exc_match_exception_match_7;
    int tmp_exc_match_exception_match_8;
    int tmp_exc_match_exception_match_9;
    int tmp_exc_match_exception_match_10;
    int tmp_exc_match_exception_match_11;
    int tmp_exc_match_exception_match_12;
    int tmp_exc_match_exception_match_13;
    int tmp_exc_match_exception_match_14;
    int tmp_exc_match_exception_match_15;
    int tmp_exc_match_exception_match_16;
    int tmp_exc_match_exception_match_17;
    int tmp_exc_match_exception_match_18;
    int tmp_exc_match_exception_match_19;
    int tmp_exc_match_exception_match_20;
    int tmp_exc_match_exception_match_21;
    int tmp_exc_match_exception_match_22;
    int tmp_exc_match_exception_match_23;
    int tmp_exc_match_exception_match_24;
    int tmp_exc_match_exception_match_25;
    int tmp_exc_match_exception_match_26;
    int tmp_exc_match_exception_match_27;
    int tmp_exc_match_exception_match_28;
    int tmp_exc_match_exception_match_29;
    PyObject *tmp_import_globals_1;
    PyObject *tmp_import_globals_2;
    PyObject *tmp_import_globals_3;
    PyObject *tmp_import_globals_4;
    PyObject *tmp_import_globals_5;
    PyObject *tmp_import_globals_6;
    PyObject *tmp_import_globals_7;
    PyObject *tmp_import_globals_8;
    PyObject *tmp_import_globals_9;
    PyObject *tmp_import_globals_10;
    PyObject *tmp_import_globals_11;
    PyObject *tmp_import_globals_12;
    PyObject *tmp_import_globals_13;
    PyObject *tmp_import_globals_14;
    PyObject *tmp_import_globals_15;
    PyObject *tmp_import_globals_16;
    PyObject *tmp_import_globals_17;
    PyObject *tmp_import_globals_18;
    PyObject *tmp_import_globals_19;
    PyObject *tmp_import_globals_20;
    PyObject *tmp_import_globals_21;
    PyObject *tmp_import_globals_22;
    PyObject *tmp_import_globals_23;
    PyObject *tmp_import_globals_24;
    PyObject *tmp_import_globals_25;
    PyObject *tmp_import_globals_26;
    PyObject *tmp_import_globals_27;
    PyObject *tmp_import_globals_28;
    PyObject *tmp_import_globals_29;
    PyObject *tmp_import_globals_30;
    PyObject *tmp_import_globals_31;
    PyObject *tmp_import_globals_32;
    PyObject *tmp_import_globals_33;
    PyObject *tmp_import_globals_34;
    PyObject *tmp_import_globals_35;
    PyObject *tmp_import_globals_36;
    PyObject *tmp_import_globals_37;
    PyObject *tmp_import_globals_38;
    PyObject *tmp_import_globals_39;
    PyObject *tmp_import_globals_40;
    PyObject *tmp_import_globals_41;
    PyObject *tmp_import_globals_42;
    PyObject *tmp_import_globals_43;
    PyObject *tmp_import_globals_44;
    PyObject *tmp_import_globals_45;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_import_name_from_2;
    PyObject *tmp_import_name_from_3;
    PyObject *tmp_import_name_from_4;
    PyObject *tmp_import_name_from_5;
    PyObject *tmp_import_name_from_6;
    PyObject *tmp_import_name_from_7;
    PyObject *tmp_int_arg_1;
    PyObject *tmp_key_name_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_source_name_6;
    PyObject *tmp_source_name_7;
    PyObject *tmp_source_name_8;
    PyObject *tmp_source_name_9;
    PyObject *tmp_source_name_10;
    PyObject *tmp_source_name_11;
    PyObject *tmp_source_name_12;
    PyObject *tmp_source_name_13;
    PyObject *tmp_source_name_14;
    PyObject *tmp_source_name_15;
    PyObject *tmp_source_name_16;
    PyObject *tmp_source_name_17;
    PyObject *tmp_source_name_18;
    PyObject *tmp_source_name_19;
    PyObject *tmp_source_name_20;
    PyObject *tmp_source_name_21;
    PyObject *tmp_source_name_22;
    PyObject *tmp_source_name_23;
    PyObject *tmp_source_name_24;
    PyObject *tmp_source_name_25;
    PyObject *tmp_source_name_26;
    PyObject *tmp_source_name_27;
    PyObject *tmp_star_imported_1;
    PyObject *tmp_star_imported_2;
    PyObject *tmp_star_imported_3;
    PyObject *tmp_star_imported_4;
    PyObject *tmp_star_imported_5;
    PyObject *tmp_star_imported_6;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscribed_name_2;
    PyObject *tmp_subscribed_name_3;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_subscript_name_2;
    PyObject *tmp_subscript_name_3;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    PyObject *tmp_tuple_element_3;
    PyObject *tmp_tuple_element_4;
    PyObject *tmp_tuple_element_5;
    PyObject *tmp_tuple_element_6;
    PyObject *tmp_tuple_element_7;
    PyObject *tmp_tuple_element_8;
    PyObject *tmp_tuple_element_9;
    PyObject *tmp_tuple_element_10;
    PyObject *tmp_tuple_element_11;
    PyObject *tmp_tuple_element_12;
    PyObject *tmp_tuple_element_13;
    PyObject *tmp_tuple_element_14;
    PyObject *tmp_tuple_element_15;
    PyObject *tmp_tuple_element_16;
    PyObject *tmp_tuple_element_17;
    PyObject *tmp_tuple_element_18;
    PyObject *tmp_tuple_element_19;
    PyObject *tmp_tuple_element_20;
    PyObject *tmp_tuple_element_21;
    PyObject *tmp_tuple_element_22;
    PyObject *tmp_tuple_element_23;
    PyObject *tmp_tuple_element_24;
    PyObject *tmp_tuple_element_25;
    PyObject *tmp_tuple_element_26;
    PyObject *tmp_tuple_element_27;
    PyObject *tmp_tuple_element_28;
    PyObject *tmp_tuple_element_29;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    PyFrameObject *frame_module;


    // Module code.
    tmp_assign_source_1 = const_str_digest_f07bf5e644656e87f62eac4d3bcf8f50;
    UPDATE_STRING_DICT0( moduledict_pygame, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = const_str_digest_2b423aeb9d85cb7f5281708f4d65ec91;
    UPDATE_STRING_DICT0( moduledict_pygame, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = LIST_COPY( const_list_str_digest_1c56da59a7e2c66861e4dcf491f01ca3_list );
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain___path__, tmp_assign_source_3 );
    // Frame without reuse.
    frame_module = MAKE_MODULE_FRAME( codeobj_d441b2a24565a20b1747a415ce202150, module_pygame );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_module );
    assert( Py_REFCNT( frame_module ) == 1 );

#if PYTHON_VERSION >= 340
    frame_module->f_executing += 1;
#endif

    // Framed code:
    tmp_import_globals_1 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 27;
    tmp_assign_source_4 = IMPORT_MODULE( const_str_plain_sys, tmp_import_globals_1, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 27;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_4 );
    tmp_import_globals_2 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 28;
    tmp_assign_source_5 = IMPORT_MODULE( const_str_plain_os, tmp_import_globals_2, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 28;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_5 );
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;

        exception_lineno = 31;
        goto frame_exception_exit_1;
    }

    tmp_compare_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_name );
    if ( tmp_compare_left_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 31;
        goto frame_exception_exit_1;
    }
    tmp_compare_right_1 = const_str_plain_nt;
    tmp_cmp_Eq_1 = RICH_COMPARE_BOOL_EQ_NORECURSE( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_cmp_Eq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_1 );

        exception_lineno = 31;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_left_1 );
    if ( tmp_cmp_Eq_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_compare_left_2 = const_str_plain_SDL_VIDEODRIVER;
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;

        exception_lineno = 34;
        goto frame_exception_exit_1;
    }

    tmp_compare_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_environ );
    if ( tmp_compare_right_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 34;
        goto frame_exception_exit_1;
    }
    tmp_cmp_NotIn_1 = PySequence_Contains( tmp_compare_right_2, tmp_compare_left_2 );
    assert( !(tmp_cmp_NotIn_1 == -1) );
    Py_DECREF( tmp_compare_right_2 );
    if ( tmp_cmp_NotIn_1 == 0 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_called_instance_1 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_called_instance_1 == NULL ))
    {
        tmp_called_instance_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_called_instance_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "sys" );
        exception_tb = NULL;

        exception_lineno = 45;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 45;
    tmp_subscribed_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_getwindowsversion );
    if ( tmp_subscribed_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 45;
        goto frame_exception_exit_1;
    }
    tmp_subscript_name_1 = const_int_0;
    tmp_compare_left_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
    Py_DECREF( tmp_subscribed_name_1 );
    if ( tmp_compare_left_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 45;
        goto frame_exception_exit_1;
    }
    tmp_compare_right_3 = const_int_pos_1;
    tmp_cmp_Eq_2 = RICH_COMPARE_BOOL_EQ_NORECURSE( tmp_compare_left_3, tmp_compare_right_3 );
    if ( tmp_cmp_Eq_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_3 );

        exception_lineno = 45;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_left_3 );
    if ( tmp_cmp_Eq_2 == 1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_import_globals_3 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 47;
    tmp_assign_source_6 = IMPORT_MODULE( const_str_plain__winreg, tmp_import_globals_3, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 47;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain__winreg, tmp_assign_source_6 );
    // Tried code:
    tmp_source_name_3 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain__winreg );

    if (unlikely( tmp_source_name_3 == NULL ))
    {
        tmp_source_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__winreg );
    }

    if ( tmp_source_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "_winreg" );
        exception_tb = NULL;

        exception_lineno = 52;
        goto try_except_handler_1;
    }

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_OpenKey );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 52;
        goto try_except_handler_1;
    }
    tmp_source_name_4 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain__winreg );

    if (unlikely( tmp_source_name_4 == NULL ))
    {
        tmp_source_name_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__winreg );
    }

    if ( tmp_source_name_4 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "_winreg" );
        exception_tb = NULL;

        exception_lineno = 52;
        goto try_except_handler_1;
    }

    tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_HKEY_LOCAL_MACHINE );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );

        exception_lineno = 52;
        goto try_except_handler_1;
    }
    tmp_args_element_name_2 = const_str_digest_169fd7f487ca29a49c432443e40ddebf;
    frame_module->f_lineno = 52;
    {
        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
        tmp_assign_source_7 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_element_name_1 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 52;
        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_key, tmp_assign_source_7 );
    tmp_source_name_5 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain__winreg );

    if (unlikely( tmp_source_name_5 == NULL ))
    {
        tmp_source_name_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__winreg );
    }

    if ( tmp_source_name_5 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "_winreg" );
        exception_tb = NULL;

        exception_lineno = 54;
        goto try_except_handler_1;
    }

    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_QueryValueEx );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 54;
        goto try_except_handler_1;
    }
    tmp_args_element_name_3 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_key );

    if (unlikely( tmp_args_element_name_3 == NULL ))
    {
        tmp_args_element_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_key );
    }

    if ( tmp_args_element_name_3 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "key" );
        exception_tb = NULL;

        exception_lineno = 54;
        goto try_except_handler_1;
    }

    tmp_args_element_name_4 = const_str_plain_Version;
    frame_module->f_lineno = 54;
    {
        PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
        tmp_assign_source_8 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
    }

    Py_DECREF( tmp_called_name_2 );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 54;
        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_dx_version_string, tmp_assign_source_8 );
    tmp_called_instance_2 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_key );

    if (unlikely( tmp_called_instance_2 == NULL ))
    {
        tmp_called_instance_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_key );
    }

    if ( tmp_called_instance_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "key" );
        exception_tb = NULL;

        exception_lineno = 55;
        goto try_except_handler_1;
    }

    frame_module->f_lineno = 55;
    tmp_unused = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_Close );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 55;
        goto try_except_handler_1;
    }
    Py_DECREF( tmp_unused );
    tmp_called_instance_3 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_dx_version_string );

    if (unlikely( tmp_called_instance_3 == NULL ))
    {
        tmp_called_instance_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dx_version_string );
    }

    if ( tmp_called_instance_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "dx_version_string" );
        exception_tb = NULL;

        exception_lineno = 61;
        goto try_except_handler_1;
    }

    frame_module->f_lineno = 61;
    tmp_subscribed_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_dot_tuple, 0 ) );

    if ( tmp_subscribed_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 61;
        goto try_except_handler_1;
    }
    tmp_subscript_name_2 = const_int_pos_1;
    tmp_int_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
    Py_DECREF( tmp_subscribed_name_2 );
    if ( tmp_int_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 61;
        goto try_except_handler_1;
    }
    tmp_assign_source_9 = PyNumber_Int( tmp_int_arg_1 );
    Py_DECREF( tmp_int_arg_1 );
    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 61;
        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_minor_dx_version, tmp_assign_source_9 );
    tmp_compare_left_4 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_minor_dx_version );

    if (unlikely( tmp_compare_left_4 == NULL ))
    {
        tmp_compare_left_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_minor_dx_version );
    }

    if ( tmp_compare_left_4 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "minor_dx_version" );
        exception_tb = NULL;

        exception_lineno = 62;
        goto try_except_handler_1;
    }

    tmp_compare_right_4 = const_int_pos_5;
    tmp_cmp_GtE_1 = RICH_COMPARE_BOOL_GE( tmp_compare_left_4, tmp_compare_right_4 );
    if ( tmp_cmp_GtE_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 62;
        goto try_except_handler_1;
    }
    if ( tmp_cmp_GtE_1 == 1 )
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_ass_subvalue_1 = const_str_plain_directx;
    tmp_source_name_6 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_6 == NULL ))
    {
        tmp_source_name_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_6 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;

        exception_lineno = 63;
        goto try_except_handler_1;
    }

    tmp_ass_subscribed_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_environ );
    if ( tmp_ass_subscribed_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 63;
        goto try_except_handler_1;
    }
    tmp_ass_subscript_1 = const_str_plain_SDL_VIDEODRIVER;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
    Py_DECREF( tmp_ass_subscribed_1 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 63;
        goto try_except_handler_1;
    }
    branch_no_4:;
    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pygame, const_str_plain_key );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 0 ], 25, 0 );
        exception_tb = NULL;

        exception_lineno = 66;
        goto try_except_handler_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pygame, const_str_plain_dx_version_string );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 25 ], 39, 0 );
        exception_tb = NULL;

        exception_lineno = 66;
        goto try_except_handler_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pygame, const_str_plain_minor_dx_version );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 64 ], 38, 0 );
        exception_tb = NULL;

        exception_lineno = 66;
        goto try_except_handler_1;
    }

    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != -1 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_module, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_1:;
    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pygame, const_str_plain__winreg );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 102 ], 29, 0 );
        exception_tb = NULL;

        exception_lineno = 72;
        goto frame_exception_exit_1;
    }

    branch_no_3:;
    branch_no_2:;
    goto branch_end_1;
    branch_no_1:;
    tmp_compexpr_left_1 = const_str_plain_DISPLAY;
    tmp_source_name_7 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_7 == NULL ))
    {
        tmp_source_name_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_7 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;

        exception_lineno = 76;
        goto frame_exception_exit_1;
    }

    tmp_compexpr_right_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_environ );
    if ( tmp_compexpr_right_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 76;
        goto frame_exception_exit_1;
    }
    tmp_and_left_value_1 = SEQUENCE_CONTAINS( tmp_compexpr_left_1, tmp_compexpr_right_1 );
    Py_DECREF( tmp_compexpr_right_1 );
    if ( tmp_and_left_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 76;
        goto frame_exception_exit_1;
    }
    tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
    assert( !(tmp_and_left_truth_1 == -1) );
    if ( tmp_and_left_truth_1 == 1 )
    {
        goto and_right_1;
    }
    else
    {
        goto and_left_1;
    }
    and_right_1:;
    tmp_compexpr_left_2 = const_str_plain_SDL_VIDEO_X11_WMCLASS;
    tmp_source_name_8 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_8 == NULL ))
    {
        tmp_source_name_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_8 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;

        exception_lineno = 76;
        goto frame_exception_exit_1;
    }

    tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_environ );
    if ( tmp_compexpr_right_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 76;
        goto frame_exception_exit_1;
    }
    tmp_and_right_value_1 = SEQUENCE_CONTAINS_NOT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
    Py_DECREF( tmp_compexpr_right_2 );
    if ( tmp_and_right_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 76;
        goto frame_exception_exit_1;
    }
    tmp_cond_value_1 = tmp_and_right_value_1;
    goto and_end_1;
    and_left_1:;
    tmp_cond_value_1 = tmp_and_left_value_1;
    and_end_1:;
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    assert( !(tmp_cond_truth_1 == -1) );
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_5;
    }
    else
    {
        goto branch_no_5;
    }
    branch_yes_5:;
    tmp_source_name_10 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_10 == NULL ))
    {
        tmp_source_name_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_10 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;

        exception_lineno = 77;
        goto frame_exception_exit_1;
    }

    tmp_source_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_path );
    if ( tmp_source_name_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 77;
        goto frame_exception_exit_1;
    }
    tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_basename );
    Py_DECREF( tmp_source_name_9 );
    if ( tmp_called_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 77;
        goto frame_exception_exit_1;
    }
    tmp_source_name_11 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_11 == NULL ))
    {
        tmp_source_name_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_11 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "sys" );
        exception_tb = NULL;

        exception_lineno = 77;
        goto frame_exception_exit_1;
    }

    tmp_subscribed_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_argv );
    if ( tmp_subscribed_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );

        exception_lineno = 77;
        goto frame_exception_exit_1;
    }
    tmp_subscript_name_3 = const_int_0;
    tmp_args_element_name_5 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
    Py_DECREF( tmp_subscribed_name_3 );
    if ( tmp_args_element_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );

        exception_lineno = 77;
        goto frame_exception_exit_1;
    }
    frame_module->f_lineno = 77;
    {
        PyObject *call_args[] = { tmp_args_element_name_5 };
        tmp_ass_subvalue_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
    }

    Py_DECREF( tmp_called_name_3 );
    Py_DECREF( tmp_args_element_name_5 );
    if ( tmp_ass_subvalue_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 77;
        goto frame_exception_exit_1;
    }
    tmp_source_name_12 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_12 == NULL ))
    {
        tmp_source_name_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_12 == NULL )
    {
        Py_DECREF( tmp_ass_subvalue_2 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;

        exception_lineno = 77;
        goto frame_exception_exit_1;
    }

    tmp_ass_subscribed_2 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_environ );
    if ( tmp_ass_subscribed_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_ass_subvalue_2 );

        exception_lineno = 77;
        goto frame_exception_exit_1;
    }
    tmp_ass_subscript_2 = const_str_plain_SDL_VIDEO_X11_WMCLASS;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
    Py_DECREF( tmp_ass_subscribed_2 );
    Py_DECREF( tmp_ass_subvalue_2 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 77;
        goto frame_exception_exit_1;
    }
    branch_no_5:;
    branch_end_1:;
    tmp_assign_source_10 = impl_pygame$$$class_1_MissingModule( NULL );
    assert( tmp_assign_source_10 != NULL );
    assert( tmp_class_creation_1__class_dict == NULL );
    tmp_class_creation_1__class_dict = tmp_assign_source_10;

    // Tried code:
    tmp_compare_left_5 = const_str_plain___metaclass__;
    tmp_compare_right_5 = tmp_class_creation_1__class_dict;

    tmp_cmp_In_1 = PySequence_Contains( tmp_compare_right_5, tmp_compare_left_5 );
    assert( !(tmp_cmp_In_1 == -1) );
    if ( tmp_cmp_In_1 == 1 )
    {
        goto condexpr_true_1;
    }
    else
    {
        goto condexpr_false_1;
    }
    condexpr_true_1:;
    tmp_dict_name_1 = tmp_class_creation_1__class_dict;

    tmp_key_name_1 = const_str_plain___metaclass__;
    tmp_assign_source_11 = DICT_GET_ITEM( tmp_dict_name_1, tmp_key_name_1 );
    if ( tmp_assign_source_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 80;
        goto try_except_handler_2;
    }
    goto condexpr_end_1;
    condexpr_false_1:;
    tmp_bases_name_1 = const_tuple_empty;
    tmp_assign_source_11 = SELECT_METACLASS( tmp_bases_name_1, GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain___metaclass__ ) );
    condexpr_end_1:;
    assert( tmp_class_creation_1__metaclass == NULL );
    tmp_class_creation_1__metaclass = tmp_assign_source_11;

    tmp_called_name_4 = tmp_class_creation_1__metaclass;

    tmp_args_element_name_6 = const_str_plain_MissingModule;
    tmp_args_element_name_7 = const_tuple_empty;
    tmp_args_element_name_8 = tmp_class_creation_1__class_dict;

    frame_module->f_lineno = 80;
    {
        PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8 };
        tmp_assign_source_12 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
    }

    if ( tmp_assign_source_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 80;
        goto try_except_handler_2;
    }
    assert( tmp_class_creation_1__class == NULL );
    tmp_class_creation_1__class = tmp_assign_source_12;

    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_dict );
    Py_DECREF( tmp_class_creation_1__class_dict );
    tmp_class_creation_1__class_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    tmp_assign_source_13 = tmp_class_creation_1__class;

    UPDATE_STRING_DICT0( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule, tmp_assign_source_13 );
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class );
    Py_DECREF( tmp_class_creation_1__class );
    tmp_class_creation_1__class = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_dict );
    Py_DECREF( tmp_class_creation_1__class_dict );
    tmp_class_creation_1__class_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    tmp_import_globals_4 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 133;
    tmp_star_imported_1 = IMPORT_MODULE( const_str_digest_e3755b9b9606dea316504ecae16b4f2b, tmp_import_globals_4, Py_None, const_tuple_str_chr_42_tuple, const_int_neg_1 );
    if ( tmp_star_imported_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 133;
        goto frame_exception_exit_1;
    }
    tmp_result = IMPORT_MODULE_STAR( module_pygame, true, tmp_star_imported_1 );
    Py_DECREF( tmp_star_imported_1 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 133;
        goto frame_exception_exit_1;
    }
    tmp_import_globals_5 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 134;
    tmp_star_imported_2 = IMPORT_MODULE( const_str_digest_18565fc6928515f154561cd2cc076c49, tmp_import_globals_5, Py_None, const_tuple_str_chr_42_tuple, const_int_neg_1 );
    if ( tmp_star_imported_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 134;
        goto frame_exception_exit_1;
    }
    tmp_result = IMPORT_MODULE_STAR( module_pygame, true, tmp_star_imported_2 );
    Py_DECREF( tmp_star_imported_2 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 134;
        goto frame_exception_exit_1;
    }
    tmp_import_globals_6 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 135;
    tmp_star_imported_3 = IMPORT_MODULE( const_str_digest_749448536aec7c722f6b23d71bbbb4b1, tmp_import_globals_6, Py_None, const_tuple_str_chr_42_tuple, const_int_neg_1 );
    if ( tmp_star_imported_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 135;
        goto frame_exception_exit_1;
    }
    tmp_result = IMPORT_MODULE_STAR( module_pygame, true, tmp_star_imported_3 );
    Py_DECREF( tmp_star_imported_3 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 135;
        goto frame_exception_exit_1;
    }
    tmp_import_globals_7 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 136;
    tmp_import_name_from_1 = IMPORT_MODULE( const_str_digest_80672e58a7263f3bf64f5274ab7b87ec, tmp_import_globals_7, Py_None, const_tuple_str_plain_Rect_tuple, const_int_neg_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 136;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Rect );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_14 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 136;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Rect, tmp_assign_source_14 );
    tmp_import_globals_8 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 137;
    tmp_assign_source_15 = IMPORT_MODULE( const_str_digest_311cff636bc9fe2337ef6b561e3a7fa9, tmp_import_globals_8, Py_None, const_tuple_str_plain_geterror_str_plain_PY_MAJOR_VERSION_tuple, const_int_neg_1 );
    if ( tmp_assign_source_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 137;
        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_1__module == NULL );
    tmp_import_from_1__module = tmp_assign_source_15;

    // Tried code:
    tmp_import_name_from_2 = tmp_import_from_1__module;

    tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_geterror );
    if ( tmp_assign_source_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 137;
        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror, tmp_assign_source_16 );
    tmp_import_name_from_3 = tmp_import_from_1__module;

    tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_PY_MAJOR_VERSION );
    if ( tmp_assign_source_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 137;
        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_PY_MAJOR_VERSION, tmp_assign_source_17 );
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    tmp_import_globals_9 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 138;
    tmp_assign_source_18 = IMPORT_MODULE( const_str_digest_5fcbec45bc3da16c13f80a1884e17c80, tmp_import_globals_9, Py_None, const_tuple_str_plain_encode_string_str_plain_encode_file_path_tuple, const_int_neg_1 );
    if ( tmp_assign_source_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 138;
        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_2__module == NULL );
    tmp_import_from_2__module = tmp_assign_source_18;

    // Tried code:
    tmp_import_name_from_4 = tmp_import_from_2__module;

    tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_encode_string );
    if ( tmp_assign_source_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 138;
        goto try_except_handler_4;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_encode_string, tmp_assign_source_19 );
    tmp_import_name_from_5 = tmp_import_from_2__module;

    tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_encode_file_path );
    if ( tmp_assign_source_20 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 138;
        goto try_except_handler_4;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_encode_file_path, tmp_assign_source_20 );
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    tmp_import_globals_10 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 139;
    tmp_assign_source_21 = IMPORT_MODULE( const_str_digest_af33ac6f76b57fc1d268b00073a5299d, tmp_import_globals_10, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_21 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 139;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_21 );
    tmp_import_globals_11 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 140;
    tmp_assign_source_22 = IMPORT_MODULE( const_str_digest_a9c9058b24a02dd5e800ad5af29701ac, tmp_import_globals_11, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_22 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 140;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_22 );
    tmp_source_name_13 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_color );

    if (unlikely( tmp_source_name_13 == NULL ))
    {
        tmp_source_name_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_color );
    }

    if ( tmp_source_name_13 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "color" );
        exception_tb = NULL;

        exception_lineno = 141;
        goto frame_exception_exit_1;
    }

    tmp_assign_source_23 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_Color );
    if ( tmp_assign_source_23 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 141;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Color, tmp_assign_source_23 );
    tmp_import_globals_12 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 142;
    tmp_assign_source_24 = IMPORT_MODULE( const_str_digest_887953ae3c9b867d7ff6903985ec2d3c, tmp_import_globals_12, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_24 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 142;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_24 );
    tmp_source_name_14 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_bufferproxy );

    if (unlikely( tmp_source_name_14 == NULL ))
    {
        tmp_source_name_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_bufferproxy );
    }

    if ( tmp_source_name_14 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "bufferproxy" );
        exception_tb = NULL;

        exception_lineno = 143;
        goto frame_exception_exit_1;
    }

    tmp_assign_source_25 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_BufferProxy );
    if ( tmp_assign_source_25 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 143;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_BufferProxy, tmp_assign_source_25 );
    tmp_assign_source_26 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_ver );

    if (unlikely( tmp_assign_source_26 == NULL ))
    {
        tmp_assign_source_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ver );
    }

    if ( tmp_assign_source_26 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "ver" );
        exception_tb = NULL;

        exception_lineno = 145;
        goto frame_exception_exit_1;
    }

    UPDATE_STRING_DICT0( moduledict_pygame, (Nuitka_StringObject *)const_str_plain___version__, tmp_assign_source_26 );
    // Tried code:
    tmp_import_globals_13 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 150;
    tmp_assign_source_27 = IMPORT_MODULE( const_str_digest_e15e890042ab4a6b7ec8dfe0abc28e83, tmp_import_globals_13, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_27 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 150;
        goto try_except_handler_5;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_27 );
    goto try_end_5;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_5 == NULL )
    {
        exception_keeper_tb_5 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_5 );
    }
    else if ( exception_keeper_lineno_5 != -1 )
    {
        exception_keeper_tb_5 = ADD_TRACEBACK( exception_keeper_tb_5, frame_module, exception_keeper_lineno_5 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
    PUBLISH_EXCEPTION( &exception_keeper_type_5, &exception_keeper_value_5, &exception_keeper_tb_5 );
    tmp_compare_left_6 = PyThreadState_GET()->exc_type;
    tmp_compare_right_6 = PyTuple_New( 2 );
    tmp_tuple_element_1 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_compare_right_6, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_compare_right_6, 1, tmp_tuple_element_1 );
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_6, tmp_compare_right_6 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_6 );

        exception_lineno = 151;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_6 );
    if ( tmp_exc_match_exception_match_1 == 1 )
    {
        goto branch_yes_6;
    }
    else
    {
        goto branch_no_6;
    }
    branch_yes_6:;
    tmp_called_name_5 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_5 == NULL ))
    {
        tmp_called_name_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_5 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 152;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_9 = const_str_plain_math;
    tmp_called_name_6 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_6 == NULL ))
    {
        tmp_called_name_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_6 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 152;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 152;
    tmp_args_element_name_10 = CALL_FUNCTION_NO_ARGS( tmp_called_name_6 );
    if ( tmp_args_element_name_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 152;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_11 = const_int_pos_1;
    frame_module->f_lineno = 152;
    {
        PyObject *call_args[] = { tmp_args_element_name_9, tmp_args_element_name_10, tmp_args_element_name_11 };
        tmp_assign_source_28 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, call_args );
    }

    Py_DECREF( tmp_args_element_name_10 );
    if ( tmp_assign_source_28 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 152;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_math, tmp_assign_source_28 );
    goto branch_end_6;
    branch_no_6:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_6:;
    goto try_end_5;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_5:;
    // Tried code:
    tmp_import_globals_14 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 155;
    tmp_assign_source_29 = IMPORT_MODULE( const_str_digest_fffc3d5116a376d1dabb110d3a2e29ab, tmp_import_globals_14, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_29 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 155;
        goto try_except_handler_6;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_29 );
    goto try_end_6;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_6 == NULL )
    {
        exception_keeper_tb_6 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_6 );
    }
    else if ( exception_keeper_lineno_6 != -1 )
    {
        exception_keeper_tb_6 = ADD_TRACEBACK( exception_keeper_tb_6, frame_module, exception_keeper_lineno_6 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_6, &exception_keeper_value_6, &exception_keeper_tb_6 );
    PUBLISH_EXCEPTION( &exception_keeper_type_6, &exception_keeper_value_6, &exception_keeper_tb_6 );
    tmp_compare_left_7 = PyThreadState_GET()->exc_type;
    tmp_compare_right_7 = PyTuple_New( 2 );
    tmp_tuple_element_2 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_compare_right_7, 0, tmp_tuple_element_2 );
    tmp_tuple_element_2 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_compare_right_7, 1, tmp_tuple_element_2 );
    tmp_exc_match_exception_match_2 = EXCEPTION_MATCH_BOOL( tmp_compare_left_7, tmp_compare_right_7 );
    if ( tmp_exc_match_exception_match_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_7 );

        exception_lineno = 156;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_7 );
    if ( tmp_exc_match_exception_match_2 == 1 )
    {
        goto branch_yes_7;
    }
    else
    {
        goto branch_no_7;
    }
    branch_yes_7:;
    tmp_called_name_7 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_7 == NULL ))
    {
        tmp_called_name_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_7 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 157;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_12 = const_str_plain_cdrom;
    tmp_called_name_8 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_8 == NULL ))
    {
        tmp_called_name_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_8 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 157;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 157;
    tmp_args_element_name_13 = CALL_FUNCTION_NO_ARGS( tmp_called_name_8 );
    if ( tmp_args_element_name_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 157;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_14 = const_int_pos_1;
    frame_module->f_lineno = 157;
    {
        PyObject *call_args[] = { tmp_args_element_name_12, tmp_args_element_name_13, tmp_args_element_name_14 };
        tmp_assign_source_30 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_7, call_args );
    }

    Py_DECREF( tmp_args_element_name_13 );
    if ( tmp_assign_source_30 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 157;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_cdrom, tmp_assign_source_30 );
    goto branch_end_7;
    branch_no_7:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_7:;
    goto try_end_6;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_6:;
    // Tried code:
    tmp_import_globals_15 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 160;
    tmp_assign_source_31 = IMPORT_MODULE( const_str_digest_436e7eb61b35d39994d896416c082efa, tmp_import_globals_15, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_31 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 160;
        goto try_except_handler_7;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_31 );
    goto try_end_7;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_7 == NULL )
    {
        exception_keeper_tb_7 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_7 );
    }
    else if ( exception_keeper_lineno_7 != -1 )
    {
        exception_keeper_tb_7 = ADD_TRACEBACK( exception_keeper_tb_7, frame_module, exception_keeper_lineno_7 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    PUBLISH_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    tmp_compare_left_8 = PyThreadState_GET()->exc_type;
    tmp_compare_right_8 = PyTuple_New( 2 );
    tmp_tuple_element_3 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_3 );
    PyTuple_SET_ITEM( tmp_compare_right_8, 0, tmp_tuple_element_3 );
    tmp_tuple_element_3 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_3 );
    PyTuple_SET_ITEM( tmp_compare_right_8, 1, tmp_tuple_element_3 );
    tmp_exc_match_exception_match_3 = EXCEPTION_MATCH_BOOL( tmp_compare_left_8, tmp_compare_right_8 );
    if ( tmp_exc_match_exception_match_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_8 );

        exception_lineno = 161;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_8 );
    if ( tmp_exc_match_exception_match_3 == 1 )
    {
        goto branch_yes_8;
    }
    else
    {
        goto branch_no_8;
    }
    branch_yes_8:;
    tmp_called_name_9 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_9 == NULL ))
    {
        tmp_called_name_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_9 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 162;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_15 = const_str_plain_cursors;
    tmp_called_name_10 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_10 == NULL ))
    {
        tmp_called_name_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_10 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 162;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 162;
    tmp_args_element_name_16 = CALL_FUNCTION_NO_ARGS( tmp_called_name_10 );
    if ( tmp_args_element_name_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 162;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_17 = const_int_pos_1;
    frame_module->f_lineno = 162;
    {
        PyObject *call_args[] = { tmp_args_element_name_15, tmp_args_element_name_16, tmp_args_element_name_17 };
        tmp_assign_source_32 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_9, call_args );
    }

    Py_DECREF( tmp_args_element_name_16 );
    if ( tmp_assign_source_32 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 162;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_cursors, tmp_assign_source_32 );
    goto branch_end_8;
    branch_no_8:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_8:;
    goto try_end_7;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_7:;
    // Tried code:
    tmp_import_globals_16 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 165;
    tmp_assign_source_33 = IMPORT_MODULE( const_str_digest_c5a6147046c8b4eaac4d39ca37af165a, tmp_import_globals_16, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_33 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 165;
        goto try_except_handler_8;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_33 );
    goto try_end_8;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_8 == NULL )
    {
        exception_keeper_tb_8 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_8 );
    }
    else if ( exception_keeper_lineno_8 != -1 )
    {
        exception_keeper_tb_8 = ADD_TRACEBACK( exception_keeper_tb_8, frame_module, exception_keeper_lineno_8 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_8, &exception_keeper_value_8, &exception_keeper_tb_8 );
    PUBLISH_EXCEPTION( &exception_keeper_type_8, &exception_keeper_value_8, &exception_keeper_tb_8 );
    tmp_compare_left_9 = PyThreadState_GET()->exc_type;
    tmp_compare_right_9 = PyTuple_New( 2 );
    tmp_tuple_element_4 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_4 );
    PyTuple_SET_ITEM( tmp_compare_right_9, 0, tmp_tuple_element_4 );
    tmp_tuple_element_4 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_4 );
    PyTuple_SET_ITEM( tmp_compare_right_9, 1, tmp_tuple_element_4 );
    tmp_exc_match_exception_match_4 = EXCEPTION_MATCH_BOOL( tmp_compare_left_9, tmp_compare_right_9 );
    if ( tmp_exc_match_exception_match_4 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_9 );

        exception_lineno = 166;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_9 );
    if ( tmp_exc_match_exception_match_4 == 1 )
    {
        goto branch_yes_9;
    }
    else
    {
        goto branch_no_9;
    }
    branch_yes_9:;
    tmp_called_name_11 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_11 == NULL ))
    {
        tmp_called_name_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_11 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 167;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_18 = const_str_plain_display;
    tmp_called_name_12 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_12 == NULL ))
    {
        tmp_called_name_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_12 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 167;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 167;
    tmp_args_element_name_19 = CALL_FUNCTION_NO_ARGS( tmp_called_name_12 );
    if ( tmp_args_element_name_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 167;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_20 = const_int_pos_1;
    frame_module->f_lineno = 167;
    {
        PyObject *call_args[] = { tmp_args_element_name_18, tmp_args_element_name_19, tmp_args_element_name_20 };
        tmp_assign_source_34 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_11, call_args );
    }

    Py_DECREF( tmp_args_element_name_19 );
    if ( tmp_assign_source_34 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 167;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_display, tmp_assign_source_34 );
    goto branch_end_9;
    branch_no_9:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_9:;
    goto try_end_8;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_8:;
    // Tried code:
    tmp_import_globals_17 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 170;
    tmp_assign_source_35 = IMPORT_MODULE( const_str_digest_7b0fa3f5e123d1c086be16c591086ba7, tmp_import_globals_17, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_35 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 170;
        goto try_except_handler_9;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_35 );
    goto try_end_9;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_9 == NULL )
    {
        exception_keeper_tb_9 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_9 );
    }
    else if ( exception_keeper_lineno_9 != -1 )
    {
        exception_keeper_tb_9 = ADD_TRACEBACK( exception_keeper_tb_9, frame_module, exception_keeper_lineno_9 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_9, &exception_keeper_value_9, &exception_keeper_tb_9 );
    PUBLISH_EXCEPTION( &exception_keeper_type_9, &exception_keeper_value_9, &exception_keeper_tb_9 );
    tmp_compare_left_10 = PyThreadState_GET()->exc_type;
    tmp_compare_right_10 = PyTuple_New( 2 );
    tmp_tuple_element_5 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_5 );
    PyTuple_SET_ITEM( tmp_compare_right_10, 0, tmp_tuple_element_5 );
    tmp_tuple_element_5 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_5 );
    PyTuple_SET_ITEM( tmp_compare_right_10, 1, tmp_tuple_element_5 );
    tmp_exc_match_exception_match_5 = EXCEPTION_MATCH_BOOL( tmp_compare_left_10, tmp_compare_right_10 );
    if ( tmp_exc_match_exception_match_5 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_10 );

        exception_lineno = 171;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_10 );
    if ( tmp_exc_match_exception_match_5 == 1 )
    {
        goto branch_yes_10;
    }
    else
    {
        goto branch_no_10;
    }
    branch_yes_10:;
    tmp_called_name_13 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_13 == NULL ))
    {
        tmp_called_name_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_13 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 172;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_21 = const_str_plain_draw;
    tmp_called_name_14 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_14 == NULL ))
    {
        tmp_called_name_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_14 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 172;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 172;
    tmp_args_element_name_22 = CALL_FUNCTION_NO_ARGS( tmp_called_name_14 );
    if ( tmp_args_element_name_22 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 172;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_23 = const_int_pos_1;
    frame_module->f_lineno = 172;
    {
        PyObject *call_args[] = { tmp_args_element_name_21, tmp_args_element_name_22, tmp_args_element_name_23 };
        tmp_assign_source_36 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_13, call_args );
    }

    Py_DECREF( tmp_args_element_name_22 );
    if ( tmp_assign_source_36 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 172;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_draw, tmp_assign_source_36 );
    goto branch_end_10;
    branch_no_10:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_10:;
    goto try_end_9;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_9:;
    // Tried code:
    tmp_import_globals_18 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 175;
    tmp_assign_source_37 = IMPORT_MODULE( const_str_digest_a1ceada0fc56fb3222d9e4c601c90860, tmp_import_globals_18, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_37 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 175;
        goto try_except_handler_10;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_37 );
    goto try_end_10;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_10 == NULL )
    {
        exception_keeper_tb_10 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_10 );
    }
    else if ( exception_keeper_lineno_10 != -1 )
    {
        exception_keeper_tb_10 = ADD_TRACEBACK( exception_keeper_tb_10, frame_module, exception_keeper_lineno_10 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_10, &exception_keeper_value_10, &exception_keeper_tb_10 );
    PUBLISH_EXCEPTION( &exception_keeper_type_10, &exception_keeper_value_10, &exception_keeper_tb_10 );
    tmp_compare_left_11 = PyThreadState_GET()->exc_type;
    tmp_compare_right_11 = PyTuple_New( 2 );
    tmp_tuple_element_6 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_6 );
    PyTuple_SET_ITEM( tmp_compare_right_11, 0, tmp_tuple_element_6 );
    tmp_tuple_element_6 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_6 );
    PyTuple_SET_ITEM( tmp_compare_right_11, 1, tmp_tuple_element_6 );
    tmp_exc_match_exception_match_6 = EXCEPTION_MATCH_BOOL( tmp_compare_left_11, tmp_compare_right_11 );
    if ( tmp_exc_match_exception_match_6 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_11 );

        exception_lineno = 176;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_11 );
    if ( tmp_exc_match_exception_match_6 == 1 )
    {
        goto branch_yes_11;
    }
    else
    {
        goto branch_no_11;
    }
    branch_yes_11:;
    tmp_called_name_15 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_15 == NULL ))
    {
        tmp_called_name_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_15 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 177;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_24 = const_str_plain_event;
    tmp_called_name_16 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_16 == NULL ))
    {
        tmp_called_name_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_16 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 177;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 177;
    tmp_args_element_name_25 = CALL_FUNCTION_NO_ARGS( tmp_called_name_16 );
    if ( tmp_args_element_name_25 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 177;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_26 = const_int_pos_1;
    frame_module->f_lineno = 177;
    {
        PyObject *call_args[] = { tmp_args_element_name_24, tmp_args_element_name_25, tmp_args_element_name_26 };
        tmp_assign_source_38 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_15, call_args );
    }

    Py_DECREF( tmp_args_element_name_25 );
    if ( tmp_assign_source_38 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 177;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_event, tmp_assign_source_38 );
    goto branch_end_11;
    branch_no_11:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_11:;
    goto try_end_10;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_10:;
    // Tried code:
    tmp_import_globals_19 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 180;
    tmp_assign_source_39 = IMPORT_MODULE( const_str_digest_1f66733679db725cd71591b56a11db4d, tmp_import_globals_19, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_39 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 180;
        goto try_except_handler_11;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_39 );
    goto try_end_11;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_11 == NULL )
    {
        exception_keeper_tb_11 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_11 );
    }
    else if ( exception_keeper_lineno_11 != -1 )
    {
        exception_keeper_tb_11 = ADD_TRACEBACK( exception_keeper_tb_11, frame_module, exception_keeper_lineno_11 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_11, &exception_keeper_value_11, &exception_keeper_tb_11 );
    PUBLISH_EXCEPTION( &exception_keeper_type_11, &exception_keeper_value_11, &exception_keeper_tb_11 );
    tmp_compare_left_12 = PyThreadState_GET()->exc_type;
    tmp_compare_right_12 = PyTuple_New( 2 );
    tmp_tuple_element_7 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_7 );
    PyTuple_SET_ITEM( tmp_compare_right_12, 0, tmp_tuple_element_7 );
    tmp_tuple_element_7 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_7 );
    PyTuple_SET_ITEM( tmp_compare_right_12, 1, tmp_tuple_element_7 );
    tmp_exc_match_exception_match_7 = EXCEPTION_MATCH_BOOL( tmp_compare_left_12, tmp_compare_right_12 );
    if ( tmp_exc_match_exception_match_7 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_12 );

        exception_lineno = 181;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_12 );
    if ( tmp_exc_match_exception_match_7 == 1 )
    {
        goto branch_yes_12;
    }
    else
    {
        goto branch_no_12;
    }
    branch_yes_12:;
    tmp_called_name_17 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_17 == NULL ))
    {
        tmp_called_name_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_17 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 182;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_27 = const_str_plain_image;
    tmp_called_name_18 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_18 == NULL ))
    {
        tmp_called_name_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_18 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 182;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 182;
    tmp_args_element_name_28 = CALL_FUNCTION_NO_ARGS( tmp_called_name_18 );
    if ( tmp_args_element_name_28 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 182;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_29 = const_int_pos_1;
    frame_module->f_lineno = 182;
    {
        PyObject *call_args[] = { tmp_args_element_name_27, tmp_args_element_name_28, tmp_args_element_name_29 };
        tmp_assign_source_40 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_17, call_args );
    }

    Py_DECREF( tmp_args_element_name_28 );
    if ( tmp_assign_source_40 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 182;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_image, tmp_assign_source_40 );
    goto branch_end_12;
    branch_no_12:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_12:;
    goto try_end_11;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_11:;
    // Tried code:
    tmp_import_globals_20 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 185;
    tmp_assign_source_41 = IMPORT_MODULE( const_str_digest_696ad36776747b6f48fc3f1aa9b773d0, tmp_import_globals_20, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_41 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 185;
        goto try_except_handler_12;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_41 );
    goto try_end_12;
    // Exception handler code:
    try_except_handler_12:;
    exception_keeper_type_12 = exception_type;
    exception_keeper_value_12 = exception_value;
    exception_keeper_tb_12 = exception_tb;
    exception_keeper_lineno_12 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_12 == NULL )
    {
        exception_keeper_tb_12 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_12 );
    }
    else if ( exception_keeper_lineno_12 != -1 )
    {
        exception_keeper_tb_12 = ADD_TRACEBACK( exception_keeper_tb_12, frame_module, exception_keeper_lineno_12 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_12, &exception_keeper_value_12, &exception_keeper_tb_12 );
    PUBLISH_EXCEPTION( &exception_keeper_type_12, &exception_keeper_value_12, &exception_keeper_tb_12 );
    tmp_compare_left_13 = PyThreadState_GET()->exc_type;
    tmp_compare_right_13 = PyTuple_New( 2 );
    tmp_tuple_element_8 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_8 );
    PyTuple_SET_ITEM( tmp_compare_right_13, 0, tmp_tuple_element_8 );
    tmp_tuple_element_8 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_8 );
    PyTuple_SET_ITEM( tmp_compare_right_13, 1, tmp_tuple_element_8 );
    tmp_exc_match_exception_match_8 = EXCEPTION_MATCH_BOOL( tmp_compare_left_13, tmp_compare_right_13 );
    if ( tmp_exc_match_exception_match_8 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_13 );

        exception_lineno = 186;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_13 );
    if ( tmp_exc_match_exception_match_8 == 1 )
    {
        goto branch_yes_13;
    }
    else
    {
        goto branch_no_13;
    }
    branch_yes_13:;
    tmp_called_name_19 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_19 == NULL ))
    {
        tmp_called_name_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_19 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 187;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_30 = const_str_plain_joystick;
    tmp_called_name_20 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_20 == NULL ))
    {
        tmp_called_name_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_20 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 187;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 187;
    tmp_args_element_name_31 = CALL_FUNCTION_NO_ARGS( tmp_called_name_20 );
    if ( tmp_args_element_name_31 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 187;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_32 = const_int_pos_1;
    frame_module->f_lineno = 187;
    {
        PyObject *call_args[] = { tmp_args_element_name_30, tmp_args_element_name_31, tmp_args_element_name_32 };
        tmp_assign_source_42 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_19, call_args );
    }

    Py_DECREF( tmp_args_element_name_31 );
    if ( tmp_assign_source_42 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 187;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_joystick, tmp_assign_source_42 );
    goto branch_end_13;
    branch_no_13:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_13:;
    goto try_end_12;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_12:;
    // Tried code:
    tmp_import_globals_21 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 190;
    tmp_assign_source_43 = IMPORT_MODULE( const_str_digest_d72ace371c6cc7fe9399808f16741547, tmp_import_globals_21, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_43 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 190;
        goto try_except_handler_13;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_43 );
    goto try_end_13;
    // Exception handler code:
    try_except_handler_13:;
    exception_keeper_type_13 = exception_type;
    exception_keeper_value_13 = exception_value;
    exception_keeper_tb_13 = exception_tb;
    exception_keeper_lineno_13 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_13 == NULL )
    {
        exception_keeper_tb_13 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_13 );
    }
    else if ( exception_keeper_lineno_13 != -1 )
    {
        exception_keeper_tb_13 = ADD_TRACEBACK( exception_keeper_tb_13, frame_module, exception_keeper_lineno_13 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_13, &exception_keeper_value_13, &exception_keeper_tb_13 );
    PUBLISH_EXCEPTION( &exception_keeper_type_13, &exception_keeper_value_13, &exception_keeper_tb_13 );
    tmp_compare_left_14 = PyThreadState_GET()->exc_type;
    tmp_compare_right_14 = PyTuple_New( 2 );
    tmp_tuple_element_9 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_9 );
    PyTuple_SET_ITEM( tmp_compare_right_14, 0, tmp_tuple_element_9 );
    tmp_tuple_element_9 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_9 );
    PyTuple_SET_ITEM( tmp_compare_right_14, 1, tmp_tuple_element_9 );
    tmp_exc_match_exception_match_9 = EXCEPTION_MATCH_BOOL( tmp_compare_left_14, tmp_compare_right_14 );
    if ( tmp_exc_match_exception_match_9 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_14 );

        exception_lineno = 191;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_14 );
    if ( tmp_exc_match_exception_match_9 == 1 )
    {
        goto branch_yes_14;
    }
    else
    {
        goto branch_no_14;
    }
    branch_yes_14:;
    tmp_called_name_21 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_21 == NULL ))
    {
        tmp_called_name_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_21 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 192;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_33 = const_str_plain_key;
    tmp_called_name_22 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_22 == NULL ))
    {
        tmp_called_name_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_22 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 192;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 192;
    tmp_args_element_name_34 = CALL_FUNCTION_NO_ARGS( tmp_called_name_22 );
    if ( tmp_args_element_name_34 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 192;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_35 = const_int_pos_1;
    frame_module->f_lineno = 192;
    {
        PyObject *call_args[] = { tmp_args_element_name_33, tmp_args_element_name_34, tmp_args_element_name_35 };
        tmp_assign_source_44 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_21, call_args );
    }

    Py_DECREF( tmp_args_element_name_34 );
    if ( tmp_assign_source_44 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 192;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_key, tmp_assign_source_44 );
    goto branch_end_14;
    branch_no_14:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_14:;
    goto try_end_13;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_13:;
    // Tried code:
    tmp_import_globals_22 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 195;
    tmp_assign_source_45 = IMPORT_MODULE( const_str_digest_d369391a103bc2293238ec66417a6f4a, tmp_import_globals_22, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_45 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 195;
        goto try_except_handler_14;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_45 );
    goto try_end_14;
    // Exception handler code:
    try_except_handler_14:;
    exception_keeper_type_14 = exception_type;
    exception_keeper_value_14 = exception_value;
    exception_keeper_tb_14 = exception_tb;
    exception_keeper_lineno_14 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_14 == NULL )
    {
        exception_keeper_tb_14 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_14 );
    }
    else if ( exception_keeper_lineno_14 != -1 )
    {
        exception_keeper_tb_14 = ADD_TRACEBACK( exception_keeper_tb_14, frame_module, exception_keeper_lineno_14 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_14, &exception_keeper_value_14, &exception_keeper_tb_14 );
    PUBLISH_EXCEPTION( &exception_keeper_type_14, &exception_keeper_value_14, &exception_keeper_tb_14 );
    tmp_compare_left_15 = PyThreadState_GET()->exc_type;
    tmp_compare_right_15 = PyTuple_New( 2 );
    tmp_tuple_element_10 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_10 );
    PyTuple_SET_ITEM( tmp_compare_right_15, 0, tmp_tuple_element_10 );
    tmp_tuple_element_10 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_10 );
    PyTuple_SET_ITEM( tmp_compare_right_15, 1, tmp_tuple_element_10 );
    tmp_exc_match_exception_match_10 = EXCEPTION_MATCH_BOOL( tmp_compare_left_15, tmp_compare_right_15 );
    if ( tmp_exc_match_exception_match_10 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_15 );

        exception_lineno = 196;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_15 );
    if ( tmp_exc_match_exception_match_10 == 1 )
    {
        goto branch_yes_15;
    }
    else
    {
        goto branch_no_15;
    }
    branch_yes_15:;
    tmp_called_name_23 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_23 == NULL ))
    {
        tmp_called_name_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_23 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 197;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_36 = const_str_plain_mouse;
    tmp_called_name_24 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_24 == NULL ))
    {
        tmp_called_name_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_24 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 197;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 197;
    tmp_args_element_name_37 = CALL_FUNCTION_NO_ARGS( tmp_called_name_24 );
    if ( tmp_args_element_name_37 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 197;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_38 = const_int_pos_1;
    frame_module->f_lineno = 197;
    {
        PyObject *call_args[] = { tmp_args_element_name_36, tmp_args_element_name_37, tmp_args_element_name_38 };
        tmp_assign_source_46 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_23, call_args );
    }

    Py_DECREF( tmp_args_element_name_37 );
    if ( tmp_assign_source_46 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 197;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_mouse, tmp_assign_source_46 );
    goto branch_end_15;
    branch_no_15:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_15:;
    goto try_end_14;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_14:;
    // Tried code:
    tmp_import_globals_23 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 200;
    tmp_assign_source_47 = IMPORT_MODULE( const_str_digest_1ef86d6d9cfb907791755ff131616348, tmp_import_globals_23, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_47 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 200;
        goto try_except_handler_15;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_47 );
    goto try_end_15;
    // Exception handler code:
    try_except_handler_15:;
    exception_keeper_type_15 = exception_type;
    exception_keeper_value_15 = exception_value;
    exception_keeper_tb_15 = exception_tb;
    exception_keeper_lineno_15 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_15 == NULL )
    {
        exception_keeper_tb_15 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_15 );
    }
    else if ( exception_keeper_lineno_15 != -1 )
    {
        exception_keeper_tb_15 = ADD_TRACEBACK( exception_keeper_tb_15, frame_module, exception_keeper_lineno_15 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_15, &exception_keeper_value_15, &exception_keeper_tb_15 );
    PUBLISH_EXCEPTION( &exception_keeper_type_15, &exception_keeper_value_15, &exception_keeper_tb_15 );
    tmp_compare_left_16 = PyThreadState_GET()->exc_type;
    tmp_compare_right_16 = PyTuple_New( 2 );
    tmp_tuple_element_11 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_11 );
    PyTuple_SET_ITEM( tmp_compare_right_16, 0, tmp_tuple_element_11 );
    tmp_tuple_element_11 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_11 );
    PyTuple_SET_ITEM( tmp_compare_right_16, 1, tmp_tuple_element_11 );
    tmp_exc_match_exception_match_11 = EXCEPTION_MATCH_BOOL( tmp_compare_left_16, tmp_compare_right_16 );
    if ( tmp_exc_match_exception_match_11 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_16 );

        exception_lineno = 201;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_16 );
    if ( tmp_exc_match_exception_match_11 == 1 )
    {
        goto branch_yes_16;
    }
    else
    {
        goto branch_no_16;
    }
    branch_yes_16:;
    tmp_called_name_25 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_25 == NULL ))
    {
        tmp_called_name_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_25 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 202;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_39 = const_str_plain_sprite;
    tmp_called_name_26 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_26 == NULL ))
    {
        tmp_called_name_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_26 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 202;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 202;
    tmp_args_element_name_40 = CALL_FUNCTION_NO_ARGS( tmp_called_name_26 );
    if ( tmp_args_element_name_40 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 202;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_41 = const_int_pos_1;
    frame_module->f_lineno = 202;
    {
        PyObject *call_args[] = { tmp_args_element_name_39, tmp_args_element_name_40, tmp_args_element_name_41 };
        tmp_assign_source_48 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_25, call_args );
    }

    Py_DECREF( tmp_args_element_name_40 );
    if ( tmp_assign_source_48 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 202;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_sprite, tmp_assign_source_48 );
    goto branch_end_16;
    branch_no_16:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_16:;
    goto try_end_15;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_15:;
    // Tried code:
    tmp_import_globals_24 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 206;
    tmp_assign_source_49 = IMPORT_MODULE( const_str_digest_b2e0d613f6b2aca091d775f4312aede2, tmp_import_globals_24, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_49 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 206;
        goto try_except_handler_16;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_49 );
    goto try_end_16;
    // Exception handler code:
    try_except_handler_16:;
    exception_keeper_type_16 = exception_type;
    exception_keeper_value_16 = exception_value;
    exception_keeper_tb_16 = exception_tb;
    exception_keeper_lineno_16 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_16 == NULL )
    {
        exception_keeper_tb_16 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_16 );
    }
    else if ( exception_keeper_lineno_16 != -1 )
    {
        exception_keeper_tb_16 = ADD_TRACEBACK( exception_keeper_tb_16, frame_module, exception_keeper_lineno_16 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_16, &exception_keeper_value_16, &exception_keeper_tb_16 );
    PUBLISH_EXCEPTION( &exception_keeper_type_16, &exception_keeper_value_16, &exception_keeper_tb_16 );
    tmp_compare_left_17 = PyThreadState_GET()->exc_type;
    tmp_compare_right_17 = PyTuple_New( 2 );
    tmp_tuple_element_12 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_12 );
    PyTuple_SET_ITEM( tmp_compare_right_17, 0, tmp_tuple_element_12 );
    tmp_tuple_element_12 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_12 );
    PyTuple_SET_ITEM( tmp_compare_right_17, 1, tmp_tuple_element_12 );
    tmp_exc_match_exception_match_12 = EXCEPTION_MATCH_BOOL( tmp_compare_left_17, tmp_compare_right_17 );
    if ( tmp_exc_match_exception_match_12 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_17 );

        exception_lineno = 207;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_17 );
    if ( tmp_exc_match_exception_match_12 == 1 )
    {
        goto branch_yes_17;
    }
    else
    {
        goto branch_no_17;
    }
    branch_yes_17:;
    tmp_called_name_27 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_27 == NULL ))
    {
        tmp_called_name_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_27 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 208;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_42 = const_str_plain_threads;
    tmp_called_name_28 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_28 == NULL ))
    {
        tmp_called_name_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_28 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 208;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 208;
    tmp_args_element_name_43 = CALL_FUNCTION_NO_ARGS( tmp_called_name_28 );
    if ( tmp_args_element_name_43 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 208;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_44 = const_int_pos_1;
    frame_module->f_lineno = 208;
    {
        PyObject *call_args[] = { tmp_args_element_name_42, tmp_args_element_name_43, tmp_args_element_name_44 };
        tmp_assign_source_50 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_27, call_args );
    }

    Py_DECREF( tmp_args_element_name_43 );
    if ( tmp_assign_source_50 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 208;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_threads, tmp_assign_source_50 );
    goto branch_end_17;
    branch_no_17:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_17:;
    goto try_end_16;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_16:;
    // Tried code:
    tmp_import_globals_25 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 211;
    tmp_assign_source_51 = IMPORT_MODULE( const_str_digest_e420c3319159a1e6d3a583b2fbaf6bd0, tmp_import_globals_25, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_51 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 211;
        goto try_except_handler_17;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_51 );
    goto try_end_17;
    // Exception handler code:
    try_except_handler_17:;
    exception_keeper_type_17 = exception_type;
    exception_keeper_value_17 = exception_value;
    exception_keeper_tb_17 = exception_tb;
    exception_keeper_lineno_17 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_17 == NULL )
    {
        exception_keeper_tb_17 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_17 );
    }
    else if ( exception_keeper_lineno_17 != -1 )
    {
        exception_keeper_tb_17 = ADD_TRACEBACK( exception_keeper_tb_17, frame_module, exception_keeper_lineno_17 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_17, &exception_keeper_value_17, &exception_keeper_tb_17 );
    PUBLISH_EXCEPTION( &exception_keeper_type_17, &exception_keeper_value_17, &exception_keeper_tb_17 );
    tmp_compare_left_18 = PyThreadState_GET()->exc_type;
    tmp_compare_right_18 = PyTuple_New( 2 );
    tmp_tuple_element_13 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_13 );
    PyTuple_SET_ITEM( tmp_compare_right_18, 0, tmp_tuple_element_13 );
    tmp_tuple_element_13 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_13 );
    PyTuple_SET_ITEM( tmp_compare_right_18, 1, tmp_tuple_element_13 );
    tmp_exc_match_exception_match_13 = EXCEPTION_MATCH_BOOL( tmp_compare_left_18, tmp_compare_right_18 );
    if ( tmp_exc_match_exception_match_13 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_18 );

        exception_lineno = 212;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_18 );
    if ( tmp_exc_match_exception_match_13 == 1 )
    {
        goto branch_yes_18;
    }
    else
    {
        goto branch_no_18;
    }
    branch_yes_18:;
    tmp_called_name_29 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_29 == NULL ))
    {
        tmp_called_name_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_29 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 213;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_45 = const_str_plain_pixelcopy;
    tmp_called_name_30 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_30 == NULL ))
    {
        tmp_called_name_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_30 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 213;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 213;
    tmp_args_element_name_46 = CALL_FUNCTION_NO_ARGS( tmp_called_name_30 );
    if ( tmp_args_element_name_46 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 213;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_47 = const_int_pos_1;
    frame_module->f_lineno = 213;
    {
        PyObject *call_args[] = { tmp_args_element_name_45, tmp_args_element_name_46, tmp_args_element_name_47 };
        tmp_assign_source_52 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_29, call_args );
    }

    Py_DECREF( tmp_args_element_name_46 );
    if ( tmp_assign_source_52 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 213;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pixelcopy, tmp_assign_source_52 );
    goto branch_end_18;
    branch_no_18:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_18:;
    goto try_end_17;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_17:;
    tmp_assign_source_53 = MAKE_FUNCTION_pygame$$$function_1_warn_unwanted_files(  );
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_warn_unwanted_files, tmp_assign_source_53 );
    // Tried code:
    tmp_import_globals_26 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 271;
    tmp_star_imported_4 = IMPORT_MODULE( const_str_digest_986039c828ebfac6d897c7144e2675a1, tmp_import_globals_26, Py_None, const_tuple_str_chr_42_tuple, const_int_neg_1 );
    if ( tmp_star_imported_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 271;
        goto try_except_handler_18;
    }
    tmp_result = IMPORT_MODULE_STAR( module_pygame, true, tmp_star_imported_4 );
    Py_DECREF( tmp_star_imported_4 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 271;
        goto try_except_handler_18;
    }
    goto try_end_18;
    // Exception handler code:
    try_except_handler_18:;
    exception_keeper_type_18 = exception_type;
    exception_keeper_value_18 = exception_value;
    exception_keeper_tb_18 = exception_tb;
    exception_keeper_lineno_18 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_18 == NULL )
    {
        exception_keeper_tb_18 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_18 );
    }
    else if ( exception_keeper_lineno_18 != -1 )
    {
        exception_keeper_tb_18 = ADD_TRACEBACK( exception_keeper_tb_18, frame_module, exception_keeper_lineno_18 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_18, &exception_keeper_value_18, &exception_keeper_tb_18 );
    PUBLISH_EXCEPTION( &exception_keeper_type_18, &exception_keeper_value_18, &exception_keeper_tb_18 );
    tmp_compare_left_19 = PyThreadState_GET()->exc_type;
    tmp_compare_right_19 = PyTuple_New( 2 );
    tmp_tuple_element_14 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_14 );
    PyTuple_SET_ITEM( tmp_compare_right_19, 0, tmp_tuple_element_14 );
    tmp_tuple_element_14 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_14 );
    PyTuple_SET_ITEM( tmp_compare_right_19, 1, tmp_tuple_element_14 );
    tmp_exc_match_exception_match_14 = EXCEPTION_MATCH_BOOL( tmp_compare_left_19, tmp_compare_right_19 );
    if ( tmp_exc_match_exception_match_14 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_19 );

        exception_lineno = 272;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_19 );
    if ( tmp_exc_match_exception_match_14 == 1 )
    {
        goto branch_yes_19;
    }
    else
    {
        goto branch_no_19;
    }
    branch_yes_19:;
    tmp_assign_source_54 = MAKE_FUNCTION_pygame$$$function_2_lambda(  );
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Surface, tmp_assign_source_54 );
    goto branch_end_19;
    branch_no_19:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_19:;
    goto try_end_18;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_18:;
    // Tried code:
    tmp_import_globals_27 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 277;
    tmp_assign_source_55 = IMPORT_MODULE( const_str_digest_86c7bb6b17330427d0fab757eb020984, tmp_import_globals_27, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_55 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 277;
        goto try_except_handler_19;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_55 );
    tmp_import_globals_28 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 278;
    tmp_import_name_from_6 = IMPORT_MODULE( const_str_digest_86c7bb6b17330427d0fab757eb020984, tmp_import_globals_28, Py_None, const_tuple_str_plain_Mask_tuple, const_int_neg_1 );
    if ( tmp_import_name_from_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 278;
        goto try_except_handler_19;
    }
    tmp_assign_source_56 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_Mask );
    Py_DECREF( tmp_import_name_from_6 );
    if ( tmp_assign_source_56 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 278;
        goto try_except_handler_19;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Mask, tmp_assign_source_56 );
    goto try_end_19;
    // Exception handler code:
    try_except_handler_19:;
    exception_keeper_type_19 = exception_type;
    exception_keeper_value_19 = exception_value;
    exception_keeper_tb_19 = exception_tb;
    exception_keeper_lineno_19 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_19 == NULL )
    {
        exception_keeper_tb_19 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_19 );
    }
    else if ( exception_keeper_lineno_19 != -1 )
    {
        exception_keeper_tb_19 = ADD_TRACEBACK( exception_keeper_tb_19, frame_module, exception_keeper_lineno_19 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_19, &exception_keeper_value_19, &exception_keeper_tb_19 );
    PUBLISH_EXCEPTION( &exception_keeper_type_19, &exception_keeper_value_19, &exception_keeper_tb_19 );
    tmp_compare_left_20 = PyThreadState_GET()->exc_type;
    tmp_compare_right_20 = PyTuple_New( 2 );
    tmp_tuple_element_15 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_15 );
    PyTuple_SET_ITEM( tmp_compare_right_20, 0, tmp_tuple_element_15 );
    tmp_tuple_element_15 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_15 );
    PyTuple_SET_ITEM( tmp_compare_right_20, 1, tmp_tuple_element_15 );
    tmp_exc_match_exception_match_15 = EXCEPTION_MATCH_BOOL( tmp_compare_left_20, tmp_compare_right_20 );
    if ( tmp_exc_match_exception_match_15 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_20 );

        exception_lineno = 279;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_20 );
    if ( tmp_exc_match_exception_match_15 == 1 )
    {
        goto branch_yes_20;
    }
    else
    {
        goto branch_no_20;
    }
    branch_yes_20:;
    tmp_assign_source_57 = MAKE_FUNCTION_pygame$$$function_3_lambda(  );
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Mask, tmp_assign_source_57 );
    goto branch_end_20;
    branch_no_20:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_20:;
    goto try_end_19;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_19:;
    // Tried code:
    tmp_import_globals_29 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 283;
    tmp_star_imported_5 = IMPORT_MODULE( const_str_digest_faf15e32c97b89cce21d7cae648a2203, tmp_import_globals_29, Py_None, const_tuple_str_chr_42_tuple, const_int_neg_1 );
    if ( tmp_star_imported_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 283;
        goto try_except_handler_20;
    }
    tmp_result = IMPORT_MODULE_STAR( module_pygame, true, tmp_star_imported_5 );
    Py_DECREF( tmp_star_imported_5 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 283;
        goto try_except_handler_20;
    }
    goto try_end_20;
    // Exception handler code:
    try_except_handler_20:;
    exception_keeper_type_20 = exception_type;
    exception_keeper_value_20 = exception_value;
    exception_keeper_tb_20 = exception_tb;
    exception_keeper_lineno_20 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_20 == NULL )
    {
        exception_keeper_tb_20 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_20 );
    }
    else if ( exception_keeper_lineno_20 != -1 )
    {
        exception_keeper_tb_20 = ADD_TRACEBACK( exception_keeper_tb_20, frame_module, exception_keeper_lineno_20 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_20, &exception_keeper_value_20, &exception_keeper_tb_20 );
    PUBLISH_EXCEPTION( &exception_keeper_type_20, &exception_keeper_value_20, &exception_keeper_tb_20 );
    tmp_compare_left_21 = PyThreadState_GET()->exc_type;
    tmp_compare_right_21 = PyTuple_New( 2 );
    tmp_tuple_element_16 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_16 );
    PyTuple_SET_ITEM( tmp_compare_right_21, 0, tmp_tuple_element_16 );
    tmp_tuple_element_16 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_16 );
    PyTuple_SET_ITEM( tmp_compare_right_21, 1, tmp_tuple_element_16 );
    tmp_exc_match_exception_match_16 = EXCEPTION_MATCH_BOOL( tmp_compare_left_21, tmp_compare_right_21 );
    if ( tmp_exc_match_exception_match_16 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_21 );

        exception_lineno = 284;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_21 );
    if ( tmp_exc_match_exception_match_16 == 1 )
    {
        goto branch_yes_21;
    }
    else
    {
        goto branch_no_21;
    }
    branch_yes_21:;
    tmp_assign_source_58 = MAKE_FUNCTION_pygame$$$function_4_lambda(  );
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_PixelArray, tmp_assign_source_58 );
    goto branch_end_21;
    branch_no_21:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_21:;
    goto try_end_20;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_20:;
    // Tried code:
    tmp_import_globals_30 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 288;
    tmp_star_imported_6 = IMPORT_MODULE( const_str_digest_34991cbebc9c3c29f49037d50b5646cb, tmp_import_globals_30, Py_None, const_tuple_str_chr_42_tuple, const_int_neg_1 );
    if ( tmp_star_imported_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 288;
        goto try_except_handler_21;
    }
    tmp_result = IMPORT_MODULE_STAR( module_pygame, true, tmp_star_imported_6 );
    Py_DECREF( tmp_star_imported_6 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 288;
        goto try_except_handler_21;
    }
    goto try_end_21;
    // Exception handler code:
    try_except_handler_21:;
    exception_keeper_type_21 = exception_type;
    exception_keeper_value_21 = exception_value;
    exception_keeper_tb_21 = exception_tb;
    exception_keeper_lineno_21 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_21 == NULL )
    {
        exception_keeper_tb_21 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_21 );
    }
    else if ( exception_keeper_lineno_21 != -1 )
    {
        exception_keeper_tb_21 = ADD_TRACEBACK( exception_keeper_tb_21, frame_module, exception_keeper_lineno_21 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_21, &exception_keeper_value_21, &exception_keeper_tb_21 );
    PUBLISH_EXCEPTION( &exception_keeper_type_21, &exception_keeper_value_21, &exception_keeper_tb_21 );
    tmp_compare_left_22 = PyThreadState_GET()->exc_type;
    tmp_compare_right_22 = PyTuple_New( 2 );
    tmp_tuple_element_17 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_17 );
    PyTuple_SET_ITEM( tmp_compare_right_22, 0, tmp_tuple_element_17 );
    tmp_tuple_element_17 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_17 );
    PyTuple_SET_ITEM( tmp_compare_right_22, 1, tmp_tuple_element_17 );
    tmp_exc_match_exception_match_17 = EXCEPTION_MATCH_BOOL( tmp_compare_left_22, tmp_compare_right_22 );
    if ( tmp_exc_match_exception_match_17 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_22 );

        exception_lineno = 289;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_22 );
    if ( tmp_exc_match_exception_match_17 == 1 )
    {
        goto branch_yes_22;
    }
    else
    {
        goto branch_no_22;
    }
    branch_yes_22:;
    tmp_assign_source_59 = MAKE_FUNCTION_pygame$$$function_5_lambda(  );
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Overlay, tmp_assign_source_59 );
    goto branch_end_22;
    branch_no_22:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_22:;
    goto try_end_21;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_21:;
    // Tried code:
    tmp_import_globals_31 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 293;
    tmp_assign_source_60 = IMPORT_MODULE( const_str_digest_0652bb03f90c2cca6cd14fe6735986b2, tmp_import_globals_31, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_60 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 293;
        goto try_except_handler_22;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_60 );
    goto try_end_22;
    // Exception handler code:
    try_except_handler_22:;
    exception_keeper_type_22 = exception_type;
    exception_keeper_value_22 = exception_value;
    exception_keeper_tb_22 = exception_tb;
    exception_keeper_lineno_22 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_22 == NULL )
    {
        exception_keeper_tb_22 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_22 );
    }
    else if ( exception_keeper_lineno_22 != -1 )
    {
        exception_keeper_tb_22 = ADD_TRACEBACK( exception_keeper_tb_22, frame_module, exception_keeper_lineno_22 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_22, &exception_keeper_value_22, &exception_keeper_tb_22 );
    PUBLISH_EXCEPTION( &exception_keeper_type_22, &exception_keeper_value_22, &exception_keeper_tb_22 );
    tmp_compare_left_23 = PyThreadState_GET()->exc_type;
    tmp_compare_right_23 = PyTuple_New( 2 );
    tmp_tuple_element_18 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_18 );
    PyTuple_SET_ITEM( tmp_compare_right_23, 0, tmp_tuple_element_18 );
    tmp_tuple_element_18 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_18 );
    PyTuple_SET_ITEM( tmp_compare_right_23, 1, tmp_tuple_element_18 );
    tmp_exc_match_exception_match_18 = EXCEPTION_MATCH_BOOL( tmp_compare_left_23, tmp_compare_right_23 );
    if ( tmp_exc_match_exception_match_18 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_23 );

        exception_lineno = 294;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_23 );
    if ( tmp_exc_match_exception_match_18 == 1 )
    {
        goto branch_yes_23;
    }
    else
    {
        goto branch_no_23;
    }
    branch_yes_23:;
    tmp_called_name_31 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_31 == NULL ))
    {
        tmp_called_name_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_31 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 295;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_48 = const_str_plain_time;
    tmp_called_name_32 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_32 == NULL ))
    {
        tmp_called_name_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_32 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 295;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 295;
    tmp_args_element_name_49 = CALL_FUNCTION_NO_ARGS( tmp_called_name_32 );
    if ( tmp_args_element_name_49 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 295;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_50 = const_int_pos_1;
    frame_module->f_lineno = 295;
    {
        PyObject *call_args[] = { tmp_args_element_name_48, tmp_args_element_name_49, tmp_args_element_name_50 };
        tmp_assign_source_61 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_31, call_args );
    }

    Py_DECREF( tmp_args_element_name_49 );
    if ( tmp_assign_source_61 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 295;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_time, tmp_assign_source_61 );
    goto branch_end_23;
    branch_no_23:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_23:;
    goto try_end_22;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_22:;
    // Tried code:
    tmp_import_globals_32 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 298;
    tmp_assign_source_62 = IMPORT_MODULE( const_str_digest_ec33b54dbd13f5c2292ba451b0640cb1, tmp_import_globals_32, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_62 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 298;
        goto try_except_handler_23;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_62 );
    goto try_end_23;
    // Exception handler code:
    try_except_handler_23:;
    exception_keeper_type_23 = exception_type;
    exception_keeper_value_23 = exception_value;
    exception_keeper_tb_23 = exception_tb;
    exception_keeper_lineno_23 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_23 == NULL )
    {
        exception_keeper_tb_23 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_23 );
    }
    else if ( exception_keeper_lineno_23 != -1 )
    {
        exception_keeper_tb_23 = ADD_TRACEBACK( exception_keeper_tb_23, frame_module, exception_keeper_lineno_23 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_23, &exception_keeper_value_23, &exception_keeper_tb_23 );
    PUBLISH_EXCEPTION( &exception_keeper_type_23, &exception_keeper_value_23, &exception_keeper_tb_23 );
    tmp_compare_left_24 = PyThreadState_GET()->exc_type;
    tmp_compare_right_24 = PyTuple_New( 2 );
    tmp_tuple_element_19 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_19 );
    PyTuple_SET_ITEM( tmp_compare_right_24, 0, tmp_tuple_element_19 );
    tmp_tuple_element_19 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_19 );
    PyTuple_SET_ITEM( tmp_compare_right_24, 1, tmp_tuple_element_19 );
    tmp_exc_match_exception_match_19 = EXCEPTION_MATCH_BOOL( tmp_compare_left_24, tmp_compare_right_24 );
    if ( tmp_exc_match_exception_match_19 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_24 );

        exception_lineno = 299;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_24 );
    if ( tmp_exc_match_exception_match_19 == 1 )
    {
        goto branch_yes_24;
    }
    else
    {
        goto branch_no_24;
    }
    branch_yes_24:;
    tmp_called_name_33 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_33 == NULL ))
    {
        tmp_called_name_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_33 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 300;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_51 = const_str_plain_transform;
    tmp_called_name_34 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_34 == NULL ))
    {
        tmp_called_name_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_34 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 300;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 300;
    tmp_args_element_name_52 = CALL_FUNCTION_NO_ARGS( tmp_called_name_34 );
    if ( tmp_args_element_name_52 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 300;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_53 = const_int_pos_1;
    frame_module->f_lineno = 300;
    {
        PyObject *call_args[] = { tmp_args_element_name_51, tmp_args_element_name_52, tmp_args_element_name_53 };
        tmp_assign_source_63 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_33, call_args );
    }

    Py_DECREF( tmp_args_element_name_52 );
    if ( tmp_assign_source_63 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 300;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_transform, tmp_assign_source_63 );
    goto branch_end_24;
    branch_no_24:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_24:;
    goto try_end_23;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_23:;
    tmp_compare_left_25 = const_str_plain_PYGAME_FREETYPE;
    tmp_source_name_15 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_15 == NULL ))
    {
        tmp_source_name_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_15 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;

        exception_lineno = 303;
        goto frame_exception_exit_1;
    }

    tmp_compare_right_25 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_environ );
    if ( tmp_compare_right_25 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 303;
        goto frame_exception_exit_1;
    }
    tmp_cmp_In_2 = PySequence_Contains( tmp_compare_right_25, tmp_compare_left_25 );
    assert( !(tmp_cmp_In_2 == -1) );
    Py_DECREF( tmp_compare_right_25 );
    if ( tmp_cmp_In_2 == 1 )
    {
        goto branch_yes_25;
    }
    else
    {
        goto branch_no_25;
    }
    branch_yes_25:;
    // Tried code:
    tmp_import_globals_33 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 305;
    tmp_import_name_from_7 = IMPORT_MODULE( const_str_digest_886429de8fdd53f8d5c9561510a627a3, tmp_import_globals_33, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_import_name_from_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 305;
        goto try_except_handler_24;
    }
    tmp_assign_source_64 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_ftfont );
    Py_DECREF( tmp_import_name_from_7 );
    if ( tmp_assign_source_64 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 305;
        goto try_except_handler_24;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_font, tmp_assign_source_64 );
    tmp_ass_subvalue_3 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_font );

    if (unlikely( tmp_ass_subvalue_3 == NULL ))
    {
        tmp_ass_subvalue_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_font );
    }

    if ( tmp_ass_subvalue_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "font" );
        exception_tb = NULL;

        exception_lineno = 306;
        goto try_except_handler_24;
    }

    tmp_source_name_16 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_16 == NULL ))
    {
        tmp_source_name_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_16 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "sys" );
        exception_tb = NULL;

        exception_lineno = 306;
        goto try_except_handler_24;
    }

    tmp_ass_subscribed_3 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_modules );
    if ( tmp_ass_subscribed_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 306;
        goto try_except_handler_24;
    }
    tmp_ass_subscript_3 = const_str_digest_11bb5f146c38d7c06053353ff65da84a;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_3, tmp_ass_subscript_3, tmp_ass_subvalue_3 );
    Py_DECREF( tmp_ass_subscribed_3 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 306;
        goto try_except_handler_24;
    }
    goto try_end_24;
    // Exception handler code:
    try_except_handler_24:;
    exception_keeper_type_24 = exception_type;
    exception_keeper_value_24 = exception_value;
    exception_keeper_tb_24 = exception_tb;
    exception_keeper_lineno_24 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_24 == NULL )
    {
        exception_keeper_tb_24 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_24 );
    }
    else if ( exception_keeper_lineno_24 != -1 )
    {
        exception_keeper_tb_24 = ADD_TRACEBACK( exception_keeper_tb_24, frame_module, exception_keeper_lineno_24 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_24, &exception_keeper_value_24, &exception_keeper_tb_24 );
    PUBLISH_EXCEPTION( &exception_keeper_type_24, &exception_keeper_value_24, &exception_keeper_tb_24 );
    tmp_compare_left_26 = PyThreadState_GET()->exc_type;
    tmp_compare_right_26 = PyTuple_New( 2 );
    tmp_tuple_element_20 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_20 );
    PyTuple_SET_ITEM( tmp_compare_right_26, 0, tmp_tuple_element_20 );
    tmp_tuple_element_20 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_20 );
    PyTuple_SET_ITEM( tmp_compare_right_26, 1, tmp_tuple_element_20 );
    tmp_exc_match_exception_match_20 = EXCEPTION_MATCH_BOOL( tmp_compare_left_26, tmp_compare_right_26 );
    if ( tmp_exc_match_exception_match_20 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_26 );

        exception_lineno = 307;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_26 );
    if ( tmp_exc_match_exception_match_20 == 1 )
    {
        goto branch_no_26;
    }
    else
    {
        goto branch_yes_26;
    }
    branch_yes_26:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_no_26:;
    goto try_end_24;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_24:;
    branch_no_25:;
    // Tried code:
    tmp_import_globals_34 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 310;
    tmp_assign_source_65 = IMPORT_MODULE( const_str_digest_11bb5f146c38d7c06053353ff65da84a, tmp_import_globals_34, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_65 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 310;
        goto try_except_handler_25;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_65 );
    tmp_import_globals_35 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 311;
    tmp_assign_source_66 = IMPORT_MODULE( const_str_digest_09071ce7627c997b2ddfbec9a930c08b, tmp_import_globals_35, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_66 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 311;
        goto try_except_handler_25;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_66 );
    tmp_source_name_18 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame );

    if (unlikely( tmp_source_name_18 == NULL ))
    {
        tmp_source_name_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pygame );
    }

    if ( tmp_source_name_18 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "pygame" );
        exception_tb = NULL;

        exception_lineno = 312;
        goto try_except_handler_25;
    }

    tmp_source_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_sysfont );
    if ( tmp_source_name_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 312;
        goto try_except_handler_25;
    }
    tmp_assattr_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_SysFont );
    Py_DECREF( tmp_source_name_17 );
    if ( tmp_assattr_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 312;
        goto try_except_handler_25;
    }
    tmp_source_name_19 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame );

    if (unlikely( tmp_source_name_19 == NULL ))
    {
        tmp_source_name_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pygame );
    }

    if ( tmp_source_name_19 == NULL )
    {
        Py_DECREF( tmp_assattr_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "pygame" );
        exception_tb = NULL;

        exception_lineno = 312;
        goto try_except_handler_25;
    }

    tmp_assattr_target_1 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_font );
    if ( tmp_assattr_target_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assattr_name_1 );

        exception_lineno = 312;
        goto try_except_handler_25;
    }
    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_SysFont, tmp_assattr_name_1 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_target_1 );

        exception_lineno = 312;
        goto try_except_handler_25;
    }
    Py_DECREF( tmp_assattr_name_1 );
    Py_DECREF( tmp_assattr_target_1 );
    tmp_source_name_21 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame );

    if (unlikely( tmp_source_name_21 == NULL ))
    {
        tmp_source_name_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pygame );
    }

    if ( tmp_source_name_21 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "pygame" );
        exception_tb = NULL;

        exception_lineno = 313;
        goto try_except_handler_25;
    }

    tmp_source_name_20 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_sysfont );
    if ( tmp_source_name_20 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 313;
        goto try_except_handler_25;
    }
    tmp_assattr_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_get_fonts );
    Py_DECREF( tmp_source_name_20 );
    if ( tmp_assattr_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 313;
        goto try_except_handler_25;
    }
    tmp_source_name_22 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame );

    if (unlikely( tmp_source_name_22 == NULL ))
    {
        tmp_source_name_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pygame );
    }

    if ( tmp_source_name_22 == NULL )
    {
        Py_DECREF( tmp_assattr_name_2 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "pygame" );
        exception_tb = NULL;

        exception_lineno = 313;
        goto try_except_handler_25;
    }

    tmp_assattr_target_2 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_font );
    if ( tmp_assattr_target_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assattr_name_2 );

        exception_lineno = 313;
        goto try_except_handler_25;
    }
    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_get_fonts, tmp_assattr_name_2 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_target_2 );

        exception_lineno = 313;
        goto try_except_handler_25;
    }
    Py_DECREF( tmp_assattr_name_2 );
    Py_DECREF( tmp_assattr_target_2 );
    tmp_source_name_24 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame );

    if (unlikely( tmp_source_name_24 == NULL ))
    {
        tmp_source_name_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pygame );
    }

    if ( tmp_source_name_24 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "pygame" );
        exception_tb = NULL;

        exception_lineno = 314;
        goto try_except_handler_25;
    }

    tmp_source_name_23 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_sysfont );
    if ( tmp_source_name_23 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 314;
        goto try_except_handler_25;
    }
    tmp_assattr_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_match_font );
    Py_DECREF( tmp_source_name_23 );
    if ( tmp_assattr_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 314;
        goto try_except_handler_25;
    }
    tmp_source_name_25 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame );

    if (unlikely( tmp_source_name_25 == NULL ))
    {
        tmp_source_name_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pygame );
    }

    if ( tmp_source_name_25 == NULL )
    {
        Py_DECREF( tmp_assattr_name_3 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "pygame" );
        exception_tb = NULL;

        exception_lineno = 314;
        goto try_except_handler_25;
    }

    tmp_assattr_target_3 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_font );
    if ( tmp_assattr_target_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assattr_name_3 );

        exception_lineno = 314;
        goto try_except_handler_25;
    }
    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_match_font, tmp_assattr_name_3 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_target_3 );

        exception_lineno = 314;
        goto try_except_handler_25;
    }
    Py_DECREF( tmp_assattr_name_3 );
    Py_DECREF( tmp_assattr_target_3 );
    goto try_end_25;
    // Exception handler code:
    try_except_handler_25:;
    exception_keeper_type_25 = exception_type;
    exception_keeper_value_25 = exception_value;
    exception_keeper_tb_25 = exception_tb;
    exception_keeper_lineno_25 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_25 == NULL )
    {
        exception_keeper_tb_25 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_25 );
    }
    else if ( exception_keeper_lineno_25 != -1 )
    {
        exception_keeper_tb_25 = ADD_TRACEBACK( exception_keeper_tb_25, frame_module, exception_keeper_lineno_25 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_25, &exception_keeper_value_25, &exception_keeper_tb_25 );
    PUBLISH_EXCEPTION( &exception_keeper_type_25, &exception_keeper_value_25, &exception_keeper_tb_25 );
    tmp_compare_left_27 = PyThreadState_GET()->exc_type;
    tmp_compare_right_27 = PyTuple_New( 2 );
    tmp_tuple_element_21 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_21 );
    PyTuple_SET_ITEM( tmp_compare_right_27, 0, tmp_tuple_element_21 );
    tmp_tuple_element_21 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_21 );
    PyTuple_SET_ITEM( tmp_compare_right_27, 1, tmp_tuple_element_21 );
    tmp_exc_match_exception_match_21 = EXCEPTION_MATCH_BOOL( tmp_compare_left_27, tmp_compare_right_27 );
    if ( tmp_exc_match_exception_match_21 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_27 );

        exception_lineno = 315;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_27 );
    if ( tmp_exc_match_exception_match_21 == 1 )
    {
        goto branch_yes_27;
    }
    else
    {
        goto branch_no_27;
    }
    branch_yes_27:;
    tmp_called_name_35 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_35 == NULL ))
    {
        tmp_called_name_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_35 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 316;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_54 = const_str_plain_font;
    tmp_called_name_36 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_36 == NULL ))
    {
        tmp_called_name_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_36 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 316;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 316;
    tmp_args_element_name_55 = CALL_FUNCTION_NO_ARGS( tmp_called_name_36 );
    if ( tmp_args_element_name_55 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 316;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_56 = const_int_0;
    frame_module->f_lineno = 316;
    {
        PyObject *call_args[] = { tmp_args_element_name_54, tmp_args_element_name_55, tmp_args_element_name_56 };
        tmp_assign_source_67 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_35, call_args );
    }

    Py_DECREF( tmp_args_element_name_55 );
    if ( tmp_assign_source_67 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 316;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_font, tmp_assign_source_67 );
    goto branch_end_27;
    branch_no_27:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_27:;
    goto try_end_25;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_25:;
    // Tried code:
    tmp_import_globals_36 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 320;
    tmp_assign_source_68 = IMPORT_MODULE( const_str_digest_1fa66e4f234ef5e087341795686ac2cd, tmp_import_globals_36, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_68 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 320;
        goto try_except_handler_26;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_68 );
    goto try_end_26;
    // Exception handler code:
    try_except_handler_26:;
    exception_keeper_type_26 = exception_type;
    exception_keeper_value_26 = exception_value;
    exception_keeper_tb_26 = exception_tb;
    exception_keeper_lineno_26 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_26 == NULL )
    {
        exception_keeper_tb_26 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_26 );
    }
    else if ( exception_keeper_lineno_26 != -1 )
    {
        exception_keeper_tb_26 = ADD_TRACEBACK( exception_keeper_tb_26, frame_module, exception_keeper_lineno_26 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_26, &exception_keeper_value_26, &exception_keeper_tb_26 );
    PUBLISH_EXCEPTION( &exception_keeper_type_26, &exception_keeper_value_26, &exception_keeper_tb_26 );
    tmp_compare_left_28 = PyThreadState_GET()->exc_type;
    tmp_compare_right_28 = PyTuple_New( 2 );
    tmp_tuple_element_22 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_22 );
    PyTuple_SET_ITEM( tmp_compare_right_28, 0, tmp_tuple_element_22 );
    tmp_tuple_element_22 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_22 );
    PyTuple_SET_ITEM( tmp_compare_right_28, 1, tmp_tuple_element_22 );
    tmp_exc_match_exception_match_22 = EXCEPTION_MATCH_BOOL( tmp_compare_left_28, tmp_compare_right_28 );
    if ( tmp_exc_match_exception_match_22 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_28 );

        exception_lineno = 323;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_28 );
    if ( tmp_exc_match_exception_match_22 == 1 )
    {
        goto branch_no_28;
    }
    else
    {
        goto branch_yes_28;
    }
    branch_yes_28:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_no_28:;
    goto try_end_26;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_26:;
    // Tried code:
    tmp_import_globals_37 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 327;
    tmp_assign_source_69 = IMPORT_MODULE( const_str_digest_f8b7823bdfb14e2c4cbd75b5ca5fd166, tmp_import_globals_37, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_69 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 327;
        goto try_except_handler_27;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_69 );
    goto try_end_27;
    // Exception handler code:
    try_except_handler_27:;
    exception_keeper_type_27 = exception_type;
    exception_keeper_value_27 = exception_value;
    exception_keeper_tb_27 = exception_tb;
    exception_keeper_lineno_27 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_27 == NULL )
    {
        exception_keeper_tb_27 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_27 );
    }
    else if ( exception_keeper_lineno_27 != -1 )
    {
        exception_keeper_tb_27 = ADD_TRACEBACK( exception_keeper_tb_27, frame_module, exception_keeper_lineno_27 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_27, &exception_keeper_value_27, &exception_keeper_tb_27 );
    PUBLISH_EXCEPTION( &exception_keeper_type_27, &exception_keeper_value_27, &exception_keeper_tb_27 );
    tmp_compare_left_29 = PyThreadState_GET()->exc_type;
    tmp_compare_right_29 = PyTuple_New( 2 );
    tmp_tuple_element_23 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_23 );
    PyTuple_SET_ITEM( tmp_compare_right_29, 0, tmp_tuple_element_23 );
    tmp_tuple_element_23 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_23 );
    PyTuple_SET_ITEM( tmp_compare_right_29, 1, tmp_tuple_element_23 );
    tmp_exc_match_exception_match_23 = EXCEPTION_MATCH_BOOL( tmp_compare_left_29, tmp_compare_right_29 );
    if ( tmp_exc_match_exception_match_23 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_29 );

        exception_lineno = 328;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_29 );
    if ( tmp_exc_match_exception_match_23 == 1 )
    {
        goto branch_yes_29;
    }
    else
    {
        goto branch_no_29;
    }
    branch_yes_29:;
    tmp_called_name_37 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_37 == NULL ))
    {
        tmp_called_name_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_37 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 329;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_57 = const_str_plain_mixer;
    tmp_called_name_38 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_38 == NULL ))
    {
        tmp_called_name_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_38 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 329;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 329;
    tmp_args_element_name_58 = CALL_FUNCTION_NO_ARGS( tmp_called_name_38 );
    if ( tmp_args_element_name_58 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 329;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_59 = const_int_0;
    frame_module->f_lineno = 329;
    {
        PyObject *call_args[] = { tmp_args_element_name_57, tmp_args_element_name_58, tmp_args_element_name_59 };
        tmp_assign_source_70 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_37, call_args );
    }

    Py_DECREF( tmp_args_element_name_58 );
    if ( tmp_assign_source_70 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 329;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_mixer, tmp_assign_source_70 );
    goto branch_end_29;
    branch_no_29:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_29:;
    goto try_end_27;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_27:;
    // Tried code:
    tmp_import_globals_38 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 332;
    tmp_assign_source_71 = IMPORT_MODULE( const_str_digest_0e39c7d44292b1ea20c2359e567f1f73, tmp_import_globals_38, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_71 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 332;
        goto try_except_handler_28;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_71 );
    goto try_end_28;
    // Exception handler code:
    try_except_handler_28:;
    exception_keeper_type_28 = exception_type;
    exception_keeper_value_28 = exception_value;
    exception_keeper_tb_28 = exception_tb;
    exception_keeper_lineno_28 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_28 == NULL )
    {
        exception_keeper_tb_28 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_28 );
    }
    else if ( exception_keeper_lineno_28 != -1 )
    {
        exception_keeper_tb_28 = ADD_TRACEBACK( exception_keeper_tb_28, frame_module, exception_keeper_lineno_28 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_28, &exception_keeper_value_28, &exception_keeper_tb_28 );
    PUBLISH_EXCEPTION( &exception_keeper_type_28, &exception_keeper_value_28, &exception_keeper_tb_28 );
    tmp_compare_left_30 = PyThreadState_GET()->exc_type;
    tmp_compare_right_30 = PyTuple_New( 2 );
    tmp_tuple_element_24 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_24 );
    PyTuple_SET_ITEM( tmp_compare_right_30, 0, tmp_tuple_element_24 );
    tmp_tuple_element_24 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_24 );
    PyTuple_SET_ITEM( tmp_compare_right_30, 1, tmp_tuple_element_24 );
    tmp_exc_match_exception_match_24 = EXCEPTION_MATCH_BOOL( tmp_compare_left_30, tmp_compare_right_30 );
    if ( tmp_exc_match_exception_match_24 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_30 );

        exception_lineno = 333;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_30 );
    if ( tmp_exc_match_exception_match_24 == 1 )
    {
        goto branch_yes_30;
    }
    else
    {
        goto branch_no_30;
    }
    branch_yes_30:;
    tmp_called_name_39 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_39 == NULL ))
    {
        tmp_called_name_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_39 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 334;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_60 = const_str_plain_movie;
    tmp_called_name_40 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_40 == NULL ))
    {
        tmp_called_name_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_40 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 334;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 334;
    tmp_args_element_name_61 = CALL_FUNCTION_NO_ARGS( tmp_called_name_40 );
    if ( tmp_args_element_name_61 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 334;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_62 = const_int_0;
    frame_module->f_lineno = 334;
    {
        PyObject *call_args[] = { tmp_args_element_name_60, tmp_args_element_name_61, tmp_args_element_name_62 };
        tmp_assign_source_72 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_39, call_args );
    }

    Py_DECREF( tmp_args_element_name_61 );
    if ( tmp_assign_source_72 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 334;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_movie, tmp_assign_source_72 );
    goto branch_end_30;
    branch_no_30:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_30:;
    goto try_end_28;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_28:;
    // Tried code:
    tmp_import_globals_39 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 341;
    tmp_assign_source_73 = IMPORT_MODULE( const_str_digest_d1ba13dd50db3e68acb1eb6b0ed3b7bd, tmp_import_globals_39, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_73 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 341;
        goto try_except_handler_29;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_73 );
    goto try_end_29;
    // Exception handler code:
    try_except_handler_29:;
    exception_keeper_type_29 = exception_type;
    exception_keeper_value_29 = exception_value;
    exception_keeper_tb_29 = exception_tb;
    exception_keeper_lineno_29 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_29 == NULL )
    {
        exception_keeper_tb_29 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_29 );
    }
    else if ( exception_keeper_lineno_29 != -1 )
    {
        exception_keeper_tb_29 = ADD_TRACEBACK( exception_keeper_tb_29, frame_module, exception_keeper_lineno_29 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_29, &exception_keeper_value_29, &exception_keeper_tb_29 );
    PUBLISH_EXCEPTION( &exception_keeper_type_29, &exception_keeper_value_29, &exception_keeper_tb_29 );
    tmp_compare_left_31 = PyThreadState_GET()->exc_type;
    tmp_compare_right_31 = PyTuple_New( 2 );
    tmp_tuple_element_25 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_25 );
    PyTuple_SET_ITEM( tmp_compare_right_31, 0, tmp_tuple_element_25 );
    tmp_tuple_element_25 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_25 );
    PyTuple_SET_ITEM( tmp_compare_right_31, 1, tmp_tuple_element_25 );
    tmp_exc_match_exception_match_25 = EXCEPTION_MATCH_BOOL( tmp_compare_left_31, tmp_compare_right_31 );
    if ( tmp_exc_match_exception_match_25 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_31 );

        exception_lineno = 342;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_31 );
    if ( tmp_exc_match_exception_match_25 == 1 )
    {
        goto branch_yes_31;
    }
    else
    {
        goto branch_no_31;
    }
    branch_yes_31:;
    tmp_called_name_41 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_41 == NULL ))
    {
        tmp_called_name_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_41 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 343;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_63 = const_str_plain_scrap;
    tmp_called_name_42 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_42 == NULL ))
    {
        tmp_called_name_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_42 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 343;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 343;
    tmp_args_element_name_64 = CALL_FUNCTION_NO_ARGS( tmp_called_name_42 );
    if ( tmp_args_element_name_64 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 343;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_65 = const_int_0;
    frame_module->f_lineno = 343;
    {
        PyObject *call_args[] = { tmp_args_element_name_63, tmp_args_element_name_64, tmp_args_element_name_65 };
        tmp_assign_source_74 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_41, call_args );
    }

    Py_DECREF( tmp_args_element_name_64 );
    if ( tmp_assign_source_74 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 343;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_scrap, tmp_assign_source_74 );
    goto branch_end_31;
    branch_no_31:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_31:;
    goto try_end_29;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_29:;
    // Tried code:
    tmp_import_globals_40 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 346;
    tmp_assign_source_75 = IMPORT_MODULE( const_str_digest_d174e39ef12748481d2d13702313aa6c, tmp_import_globals_40, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_75 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 346;
        goto try_except_handler_30;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_75 );
    goto try_end_30;
    // Exception handler code:
    try_except_handler_30:;
    exception_keeper_type_30 = exception_type;
    exception_keeper_value_30 = exception_value;
    exception_keeper_tb_30 = exception_tb;
    exception_keeper_lineno_30 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_30 == NULL )
    {
        exception_keeper_tb_30 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_30 );
    }
    else if ( exception_keeper_lineno_30 != -1 )
    {
        exception_keeper_tb_30 = ADD_TRACEBACK( exception_keeper_tb_30, frame_module, exception_keeper_lineno_30 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_30, &exception_keeper_value_30, &exception_keeper_tb_30 );
    PUBLISH_EXCEPTION( &exception_keeper_type_30, &exception_keeper_value_30, &exception_keeper_tb_30 );
    tmp_compare_left_32 = PyThreadState_GET()->exc_type;
    tmp_compare_right_32 = PyTuple_New( 2 );
    tmp_tuple_element_26 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_26 );
    PyTuple_SET_ITEM( tmp_compare_right_32, 0, tmp_tuple_element_26 );
    tmp_tuple_element_26 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_26 );
    PyTuple_SET_ITEM( tmp_compare_right_32, 1, tmp_tuple_element_26 );
    tmp_exc_match_exception_match_26 = EXCEPTION_MATCH_BOOL( tmp_compare_left_32, tmp_compare_right_32 );
    if ( tmp_exc_match_exception_match_26 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_32 );

        exception_lineno = 347;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_32 );
    if ( tmp_exc_match_exception_match_26 == 1 )
    {
        goto branch_yes_32;
    }
    else
    {
        goto branch_no_32;
    }
    branch_yes_32:;
    tmp_called_name_43 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_43 == NULL ))
    {
        tmp_called_name_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_43 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 348;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_66 = const_str_plain_surfarray;
    tmp_called_name_44 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_44 == NULL ))
    {
        tmp_called_name_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_44 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 348;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 348;
    tmp_args_element_name_67 = CALL_FUNCTION_NO_ARGS( tmp_called_name_44 );
    if ( tmp_args_element_name_67 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 348;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_68 = const_int_0;
    frame_module->f_lineno = 348;
    {
        PyObject *call_args[] = { tmp_args_element_name_66, tmp_args_element_name_67, tmp_args_element_name_68 };
        tmp_assign_source_76 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_43, call_args );
    }

    Py_DECREF( tmp_args_element_name_67 );
    if ( tmp_assign_source_76 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 348;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_surfarray, tmp_assign_source_76 );
    goto branch_end_32;
    branch_no_32:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_32:;
    goto try_end_30;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_30:;
    // Tried code:
    tmp_import_globals_41 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 351;
    tmp_assign_source_77 = IMPORT_MODULE( const_str_digest_397a17452f1c53e6ae5af26f0cec775b, tmp_import_globals_41, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_77 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 351;
        goto try_except_handler_31;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_77 );
    goto try_end_31;
    // Exception handler code:
    try_except_handler_31:;
    exception_keeper_type_31 = exception_type;
    exception_keeper_value_31 = exception_value;
    exception_keeper_tb_31 = exception_tb;
    exception_keeper_lineno_31 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_31 == NULL )
    {
        exception_keeper_tb_31 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_31 );
    }
    else if ( exception_keeper_lineno_31 != -1 )
    {
        exception_keeper_tb_31 = ADD_TRACEBACK( exception_keeper_tb_31, frame_module, exception_keeper_lineno_31 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_31, &exception_keeper_value_31, &exception_keeper_tb_31 );
    PUBLISH_EXCEPTION( &exception_keeper_type_31, &exception_keeper_value_31, &exception_keeper_tb_31 );
    tmp_compare_left_33 = PyThreadState_GET()->exc_type;
    tmp_compare_right_33 = PyTuple_New( 2 );
    tmp_tuple_element_27 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_27 );
    PyTuple_SET_ITEM( tmp_compare_right_33, 0, tmp_tuple_element_27 );
    tmp_tuple_element_27 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_27 );
    PyTuple_SET_ITEM( tmp_compare_right_33, 1, tmp_tuple_element_27 );
    tmp_exc_match_exception_match_27 = EXCEPTION_MATCH_BOOL( tmp_compare_left_33, tmp_compare_right_33 );
    if ( tmp_exc_match_exception_match_27 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_33 );

        exception_lineno = 352;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_33 );
    if ( tmp_exc_match_exception_match_27 == 1 )
    {
        goto branch_yes_33;
    }
    else
    {
        goto branch_no_33;
    }
    branch_yes_33:;
    tmp_called_name_45 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_45 == NULL ))
    {
        tmp_called_name_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_45 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 353;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_69 = const_str_plain_sndarray;
    tmp_called_name_46 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_46 == NULL ))
    {
        tmp_called_name_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_46 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 353;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 353;
    tmp_args_element_name_70 = CALL_FUNCTION_NO_ARGS( tmp_called_name_46 );
    if ( tmp_args_element_name_70 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 353;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_71 = const_int_0;
    frame_module->f_lineno = 353;
    {
        PyObject *call_args[] = { tmp_args_element_name_69, tmp_args_element_name_70, tmp_args_element_name_71 };
        tmp_assign_source_78 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_45, call_args );
    }

    Py_DECREF( tmp_args_element_name_70 );
    if ( tmp_assign_source_78 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 353;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_sndarray, tmp_assign_source_78 );
    goto branch_end_33;
    branch_no_33:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_33:;
    goto try_end_31;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_31:;
    // Tried code:
    tmp_import_globals_42 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 356;
    tmp_assign_source_79 = IMPORT_MODULE( const_str_digest_cce077e082a4881040c7eb5aaee71576, tmp_import_globals_42, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_79 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 356;
        goto try_except_handler_32;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_79 );
    goto try_end_32;
    // Exception handler code:
    try_except_handler_32:;
    exception_keeper_type_32 = exception_type;
    exception_keeper_value_32 = exception_value;
    exception_keeper_tb_32 = exception_tb;
    exception_keeper_lineno_32 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_32 == NULL )
    {
        exception_keeper_tb_32 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_32 );
    }
    else if ( exception_keeper_lineno_32 != -1 )
    {
        exception_keeper_tb_32 = ADD_TRACEBACK( exception_keeper_tb_32, frame_module, exception_keeper_lineno_32 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_32, &exception_keeper_value_32, &exception_keeper_tb_32 );
    PUBLISH_EXCEPTION( &exception_keeper_type_32, &exception_keeper_value_32, &exception_keeper_tb_32 );
    tmp_compare_left_34 = PyThreadState_GET()->exc_type;
    tmp_compare_right_34 = PyTuple_New( 2 );
    tmp_tuple_element_28 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_28 );
    PyTuple_SET_ITEM( tmp_compare_right_34, 0, tmp_tuple_element_28 );
    tmp_tuple_element_28 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_28 );
    PyTuple_SET_ITEM( tmp_compare_right_34, 1, tmp_tuple_element_28 );
    tmp_exc_match_exception_match_28 = EXCEPTION_MATCH_BOOL( tmp_compare_left_34, tmp_compare_right_34 );
    if ( tmp_exc_match_exception_match_28 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_34 );

        exception_lineno = 357;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_34 );
    if ( tmp_exc_match_exception_match_28 == 1 )
    {
        goto branch_yes_34;
    }
    else
    {
        goto branch_no_34;
    }
    branch_yes_34:;
    tmp_called_name_47 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_MissingModule );

    if (unlikely( tmp_called_name_47 == NULL ))
    {
        tmp_called_name_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MissingModule );
    }

    if ( tmp_called_name_47 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "MissingModule" );
        exception_tb = NULL;

        exception_lineno = 358;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_72 = const_str_plain_fastevent;
    tmp_called_name_48 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_geterror );

    if (unlikely( tmp_called_name_48 == NULL ))
    {
        tmp_called_name_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_geterror );
    }

    if ( tmp_called_name_48 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "geterror" );
        exception_tb = NULL;

        exception_lineno = 358;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 358;
    tmp_args_element_name_73 = CALL_FUNCTION_NO_ARGS( tmp_called_name_48 );
    if ( tmp_args_element_name_73 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 358;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_74 = const_int_0;
    frame_module->f_lineno = 358;
    {
        PyObject *call_args[] = { tmp_args_element_name_72, tmp_args_element_name_73, tmp_args_element_name_74 };
        tmp_assign_source_80 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_47, call_args );
    }

    Py_DECREF( tmp_args_element_name_73 );
    if ( tmp_assign_source_80 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 358;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_fastevent, tmp_assign_source_80 );
    goto branch_end_34;
    branch_no_34:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_34:;
    goto try_end_32;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_32:;
    // Tried code:
    tmp_import_globals_43 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 364;
    tmp_assign_source_81 = IMPORT_MODULE( const_str_digest_60aed3f244f0c03471c1ce952e69d963, tmp_import_globals_43, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_81 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 364;
        goto try_except_handler_33;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame, tmp_assign_source_81 );
    tmp_attrdel_target_1 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_pygame );

    if (unlikely( tmp_attrdel_target_1 == NULL ))
    {
        tmp_attrdel_target_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pygame );
    }

    if ( tmp_attrdel_target_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "pygame" );
        exception_tb = NULL;

        exception_lineno = 365;
        goto try_except_handler_33;
    }

    tmp_res = PyObject_DelAttr( tmp_attrdel_target_1, const_str_plain_imageext );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 365;
        goto try_except_handler_33;
    }
    goto try_end_33;
    // Exception handler code:
    try_except_handler_33:;
    exception_keeper_type_33 = exception_type;
    exception_keeper_value_33 = exception_value;
    exception_keeper_tb_33 = exception_tb;
    exception_keeper_lineno_33 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if ( exception_keeper_tb_33 == NULL )
    {
        exception_keeper_tb_33 = MAKE_TRACEBACK( frame_module, exception_keeper_lineno_33 );
    }
    else if ( exception_keeper_lineno_33 != -1 )
    {
        exception_keeper_tb_33 = ADD_TRACEBACK( exception_keeper_tb_33, frame_module, exception_keeper_lineno_33 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_33, &exception_keeper_value_33, &exception_keeper_tb_33 );
    PUBLISH_EXCEPTION( &exception_keeper_type_33, &exception_keeper_value_33, &exception_keeper_tb_33 );
    tmp_compare_left_35 = PyThreadState_GET()->exc_type;
    tmp_compare_right_35 = PyTuple_New( 2 );
    tmp_tuple_element_29 = PyExc_ImportError;
    Py_INCREF( tmp_tuple_element_29 );
    PyTuple_SET_ITEM( tmp_compare_right_35, 0, tmp_tuple_element_29 );
    tmp_tuple_element_29 = PyExc_IOError;
    Py_INCREF( tmp_tuple_element_29 );
    PyTuple_SET_ITEM( tmp_compare_right_35, 1, tmp_tuple_element_29 );
    tmp_exc_match_exception_match_29 = EXCEPTION_MATCH_BOOL( tmp_compare_left_35, tmp_compare_right_35 );
    if ( tmp_exc_match_exception_match_29 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_right_35 );

        exception_lineno = 366;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_right_35 );
    if ( tmp_exc_match_exception_match_29 == 1 )
    {
        goto branch_no_35;
    }
    else
    {
        goto branch_yes_35;
    }
    branch_yes_35:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (exception_tb && exception_tb->tb_frame == frame_module) frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_no_35:;
    goto try_end_33;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_33:;
    tmp_assign_source_82 = MAKE_FUNCTION_pygame$$$function_6_packager_imports(  );
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_packager_imports, tmp_assign_source_82 );
    tmp_compare_left_36 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_PY_MAJOR_VERSION );

    if (unlikely( tmp_compare_left_36 == NULL ))
    {
        tmp_compare_left_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PY_MAJOR_VERSION );
    }

    if ( tmp_compare_left_36 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "PY_MAJOR_VERSION" );
        exception_tb = NULL;

        exception_lineno = 381;
        goto frame_exception_exit_1;
    }

    tmp_compare_right_36 = const_int_pos_3;
    tmp_cmp_GtE_2 = RICH_COMPARE_BOOL_GE( tmp_compare_left_36, tmp_compare_right_36 );
    if ( tmp_cmp_GtE_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 381;
        goto frame_exception_exit_1;
    }
    if ( tmp_cmp_GtE_2 == 1 )
    {
        goto branch_yes_36;
    }
    else
    {
        goto branch_no_36;
    }
    branch_yes_36:;
    tmp_import_globals_44 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 382;
    tmp_assign_source_83 = IMPORT_MODULE( const_str_plain_copyreg, tmp_import_globals_44, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_83 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 382;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_copy_reg, tmp_assign_source_83 );
    goto branch_end_36;
    branch_no_36:;
    tmp_import_globals_45 = ((PyModuleObject *)module_pygame)->md_dict;
    frame_module->f_lineno = 384;
    tmp_assign_source_84 = IMPORT_MODULE( const_str_plain_copy_reg, tmp_import_globals_45, Py_None, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_84 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 384;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_copy_reg, tmp_assign_source_84 );
    branch_end_36:;
    tmp_assign_source_85 = MAKE_FUNCTION_pygame$$$function_7___rect_constructor(  );
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain___rect_constructor, tmp_assign_source_85 );
    tmp_assign_source_86 = MAKE_FUNCTION_pygame$$$function_8___rect_reduce(  );
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain___rect_reduce, tmp_assign_source_86 );
    tmp_source_name_26 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_copy_reg );

    if (unlikely( tmp_source_name_26 == NULL ))
    {
        tmp_source_name_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_copy_reg );
    }

    if ( tmp_source_name_26 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "copy_reg" );
        exception_tb = NULL;

        exception_lineno = 394;
        goto frame_exception_exit_1;
    }

    tmp_called_name_49 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_pickle );
    if ( tmp_called_name_49 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 394;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_75 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Rect );

    if (unlikely( tmp_args_element_name_75 == NULL ))
    {
        tmp_args_element_name_75 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Rect );
    }

    if ( tmp_args_element_name_75 == NULL )
    {
        Py_DECREF( tmp_called_name_49 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "Rect" );
        exception_tb = NULL;

        exception_lineno = 394;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_76 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain___rect_reduce );

    if (unlikely( tmp_args_element_name_76 == NULL ))
    {
        tmp_args_element_name_76 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___rect_reduce );
    }

    if ( tmp_args_element_name_76 == NULL )
    {
        Py_DECREF( tmp_called_name_49 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "__rect_reduce" );
        exception_tb = NULL;

        exception_lineno = 394;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_77 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain___rect_constructor );

    if (unlikely( tmp_args_element_name_77 == NULL ))
    {
        tmp_args_element_name_77 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___rect_constructor );
    }

    if ( tmp_args_element_name_77 == NULL )
    {
        Py_DECREF( tmp_called_name_49 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "__rect_constructor" );
        exception_tb = NULL;

        exception_lineno = 394;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 394;
    {
        PyObject *call_args[] = { tmp_args_element_name_75, tmp_args_element_name_76, tmp_args_element_name_77 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_49, call_args );
    }

    Py_DECREF( tmp_called_name_49 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 394;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_assign_source_87 = MAKE_FUNCTION_pygame$$$function_9___color_constructor(  );
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain___color_constructor, tmp_assign_source_87 );
    tmp_assign_source_88 = MAKE_FUNCTION_pygame$$$function_10___color_reduce(  );
    UPDATE_STRING_DICT1( moduledict_pygame, (Nuitka_StringObject *)const_str_plain___color_reduce, tmp_assign_source_88 );
    tmp_source_name_27 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_copy_reg );

    if (unlikely( tmp_source_name_27 == NULL ))
    {
        tmp_source_name_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_copy_reg );
    }

    if ( tmp_source_name_27 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "copy_reg" );
        exception_tb = NULL;

        exception_lineno = 405;
        goto frame_exception_exit_1;
    }

    tmp_called_name_50 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_pickle );
    if ( tmp_called_name_50 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 405;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_78 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain_Color );

    if (unlikely( tmp_args_element_name_78 == NULL ))
    {
        tmp_args_element_name_78 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Color );
    }

    if ( tmp_args_element_name_78 == NULL )
    {
        Py_DECREF( tmp_called_name_50 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "Color" );
        exception_tb = NULL;

        exception_lineno = 405;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_79 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain___color_reduce );

    if (unlikely( tmp_args_element_name_79 == NULL ))
    {
        tmp_args_element_name_79 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___color_reduce );
    }

    if ( tmp_args_element_name_79 == NULL )
    {
        Py_DECREF( tmp_called_name_50 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "__color_reduce" );
        exception_tb = NULL;

        exception_lineno = 405;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_80 = GET_STRING_DICT_VALUE( moduledict_pygame, (Nuitka_StringObject *)const_str_plain___color_constructor );

    if (unlikely( tmp_args_element_name_80 == NULL ))
    {
        tmp_args_element_name_80 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___color_constructor );
    }

    if ( tmp_args_element_name_80 == NULL )
    {
        Py_DECREF( tmp_called_name_50 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "name '%s' is not defined", "__color_constructor" );
        exception_tb = NULL;

        exception_lineno = 405;
        goto frame_exception_exit_1;
    }

    frame_module->f_lineno = 405;
    {
        PyObject *call_args[] = { tmp_args_element_name_78, tmp_args_element_name_79, tmp_args_element_name_80 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_50, call_args );
    }

    Py_DECREF( tmp_called_name_50 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 405;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pygame, const_str_plain_pygame );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 131 ], 28, 0 );
        exception_tb = NULL;

        exception_lineno = 409;
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pygame, const_str_plain_os );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 159 ], 24, 0 );
        exception_tb = NULL;

        exception_lineno = 409;
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pygame, const_str_plain_sys );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 183 ], 25, 0 );
        exception_tb = NULL;

        exception_lineno = 409;
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pygame, const_str_plain_surflock );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 208 ], 30, 0 );
        exception_tb = NULL;

        exception_lineno = 409;
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pygame, const_str_plain_MissingModule );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 238 ], 35, 0 );
        exception_tb = NULL;

        exception_lineno = 409;
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pygame, const_str_plain_copy_reg );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 273 ], 30, 0 );
        exception_tb = NULL;

        exception_lineno = 409;
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pygame, const_str_plain_geterror );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 303 ], 30, 0 );
        exception_tb = NULL;

        exception_lineno = 409;
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pygame, const_str_plain_PY_MAJOR_VERSION );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 333 ], 38, 0 );
        exception_tb = NULL;

        exception_lineno = 409;
        goto frame_exception_exit_1;
    }


    // Restore frame exception if necessary.
#if 1
    RESTORE_FRAME_EXCEPTION( frame_module );
#endif
    popFrameStack();

    assertFrameObject( frame_module );
    Py_DECREF( frame_module );

    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 1
    RESTORE_FRAME_EXCEPTION( frame_module );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_module, exception_lineno );
    }
    else if ( exception_tb->tb_frame != frame_module )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_module, exception_lineno );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }

    // Put the previous frame back on top.
    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_module->f_executing -= 1;
#endif
    Py_DECREF( frame_module );

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_pygame );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
