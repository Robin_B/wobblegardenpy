/* Generated code for Python source for module 'pygame.cursors'
 * created by Nuitka version 0.5.25
 *
 * This code is in part copyright 2017 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The _module_pygame$cursors is a Python object pointer of module type. */

/* Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pygame$cursors;
PyDictObject *moduledict_pygame$cursors;

/* The module constants used, if any. */
static PyObject *const_str_plain_filldata;
static PyObject *const_str_digest_8a244d4f06cfd47791362860096740cb;
static PyObject *const_tuple_str_chr_44_str_space_tuple;
static PyObject *const_str_digest_632e992f0216721e4402932436d128b8;
static PyObject *const_str_digest_b8b5aead6e09884da3f6dd805c61d13b;
static PyObject *const_tuple_str_digest_0f5748902608737e1fdb2ef20fee5302_tuple;
static PyObject *const_tuple_96db87ae5af861479adab30eb4bf579d_tuple;
static PyObject *const_str_digest_2a648bfbc67de22c9b95d257cdaf98d3;
static PyObject *const_str_digest_6375e56f5fcdd0a4a1c5333f06107399;
static PyObject *const_str_plain_tri_left;
static PyObject *const_str_digest_ee6982adf6363a5b5f480384c2e084d2;
static PyObject *const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple;
extern PyObject *const_str_plain_startswith;
static PyObject *const_str_plain_fillitem;
static PyObject *const_str_digest_ac9f1045f8ca0fed745ab4c3ae5c23f9;
extern PyObject *const_str_plain_width;
static PyObject *const_str_plain_step;
static PyObject *const_str_digest_4ab6627b8404f8e1cb5bb8163c347985;
static PyObject *const_str_plain_tri_right;
extern PyObject *const_str_plain_data;
extern PyObject *const_str_plain_size;
static PyObject *const_tuple_str_digest_c6ea53ac2684e039b757c7a331688c7d_str_empty_tuple;
static PyObject *const_str_digest_c347bb4ad121826969f6165741a4f107;
static PyObject *const_str_plain_arrow;
extern PyObject *const_str_plain_o;
extern PyObject *const_str_plain_val;
static PyObject *const_str_plain_diamond;
static PyObject *const_tuple_922ba2f432159266b6122aae41c64070_tuple;
static PyObject *const_str_plain_cursdata;
static PyObject *const_str_plain_sizer_xy_strings;
static PyObject *const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple;
static PyObject *const_tuple_str_plain_num_str_plain_val_str_plain_x_str_plain_b_tuple;
static PyObject *const_str_plain_textmarker_strings;
static PyObject *const_str_plain_curs;
static PyObject *const_str_plain_maskdata;
static PyObject *const_tuple_c502d6295ca78f7c883cc52991623ead_tuple;
static PyObject *const_str_digest_7336a9dd343cfb1bed1a3fce43bb0bc3;
static PyObject *const_str_digest_caa0eb53245ef3ef5d9e4e558083b2a6;
extern PyObject *const_int_neg_1;
static PyObject *const_str_digest_a5f3bdf7042a2c94726acc36d30aa47f;
static PyObject *const_str_digest_8d3cab125ca9976d324a4961419d4bea;
static PyObject *const_str_digest_f76a964d55ae5f4933f8f8b38accc642;
static PyObject *const_str_digest_2f54e52824c029c1e44de8b5a174c37a;
static PyObject *const_tuple_2c1d6d232dd3e0e3e167fc51ca591929_tuple;
extern PyObject *const_str_plain_split;
extern PyObject *const_int_pos_16;
extern PyObject *const_dict_empty;
static PyObject *const_str_digest_3b868d2a2ac3b9e7e7ad0de4b49b5250;
static PyObject *const_str_digest_fc21ef38dce58b5b585a7f48e4278526;
static PyObject *const_str_plain_broken_x;
static PyObject *const_str_digest_6b6df9ef410005bd837fc89125c22573;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_digest_436e7eb61b35d39994d896416c082efa;
static PyObject *const_str_digest_d303224accee291ce3fe897e8af818d3;
static PyObject *const_tuple_10848a3ccffd6b5c0e2c59e547874c24_tuple;
extern PyObject *const_int_pos_7;
extern PyObject *const_int_pos_4;
extern PyObject *const_int_pos_5;
extern PyObject *const_int_pos_2;
extern PyObject *const_int_pos_3;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_str;
static PyObject *const_str_plain_thickarrow_strings;
extern PyObject *const_int_pos_8;
static PyObject *const_tuple_8a2c567399eef9112249151a8c62caa3_tuple;
extern PyObject *const_int_pos_6;
static PyObject *const_tuple_cb92914d270a4f6b6d6d7e626220d263_tuple;
static PyObject *const_str_plain_white;
static PyObject *const_str_plain_readlines;
static PyObject *const_str_digest_c6ea53ac2684e039b757c7a331688c7d;
static PyObject *const_str_plain_black;
static PyObject *const_str_space;
extern PyObject *const_str_chr_44;
extern PyObject *const_str_plain_replace;
static PyObject *const_str_plain_ball;
extern PyObject *const_str_dot;
extern PyObject *const_str_plain_height;
static PyObject *const_str_plain_sizer_x_strings;
extern PyObject *const_str_plain_join;
static PyObject *const_str_digest_0ade19f25d59df28699eb1ae849f19fb;
static PyObject *const_str_digest_12aed46edafdc1d5b5c211c83e9bf8bb;
extern PyObject *const_str_plain_info;
static PyObject *const_str_digest_5d8f7f3a2bdf9954219128ed2b89c78a;
static PyObject *const_str_plain_num;
static PyObject *const_str_digest_18974878984e918362c04f56becf265d;
static PyObject *const_str_plain_strings;
extern PyObject *const_str_plain_compile;
static PyObject *const_str_plain_xor;
extern PyObject *const_tuple_74cd8b3e9a15d638f032c79ff4818f63_tuple;
static PyObject *const_tuple_b5efab4462fd34b9188076b876531ff5_tuple;
static PyObject *const_tuple_str_digest_7336a9dd343cfb1bed1a3fce43bb0bc3_tuple;
extern PyObject *const_str_plain_b;
extern PyObject *const_str_plain_c;
extern PyObject *const_str_plain_line;
extern PyObject *const_str_plain_x;
static PyObject *const_str_digest_7086b80ed05082b2d4c11d4bd1a64b7d;
extern PyObject *const_str_plain_s;
static PyObject *const_str_plain_hotx;
static PyObject *const_str_plain_hoty;
static PyObject *const_tuple_2f560d6b6d76c7251a2b5a9cabe326ff_tuple;
static PyObject *const_str_plain_load_xbm;
static PyObject *const_str_plain_sizer_y_strings;
extern PyObject *const_str_plain_mask;
static PyObject *const_str_digest_0f5748902608737e1fdb2ef20fee5302;
static PyObject *const_tuple_str_digest_b8b5aead6e09884da3f6dd805c61d13b_tuple;
static PyObject *const_tuple_286ccaf39c99eff01ddd0b3f99d77ecf_tuple;
static PyObject *const_str_digest_8d1ef6fdfce6c23e6c7099119e19709a;
static PyObject *const_tuple_c69d0ea490cb72cadfbecd20104d0bcf_tuple;
static PyObject *const_str_plain_bitswap;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_digest_bbb1fa0d3f443e599b5b8653f0c382f3;
static PyObject *const_tuple_str_plain_X_str_dot_str_plain_o_tuple;
extern PyObject *const_int_0;
static PyObject *const_str_digest_7915ecd5ff0aa7b34be6436dcafaabbb;
static PyObject *const_str_digest_436dcad3629c6b56c661d5c450115dc0;
static PyObject *const_str_digest_af9c4620b6a67dd2ca195108627d4405;
static PyObject *const_str_plain_maskitem;
extern PyObject *const_str_empty;
static PyObject *const_str_digest_d01eb588f4f55c8f6985b90c18212bda;
extern PyObject *const_str_plain_append;
static PyObject *const_str_digest_fd729b720765171991723e1c2f58e688;
static PyObject *const_str_plain_X;
static PyObject *module_filename_obj;

static bool constants_created = false;

static void createModuleConstants( void )
{
    const_str_plain_filldata = UNSTREAM_STRING( &constant_bin[ 42147 ], 8, 1 );
    const_str_digest_8a244d4f06cfd47791362860096740cb = UNSTREAM_STRING( &constant_bin[ 42155 ], 50, 0 );
    const_tuple_str_chr_44_str_space_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_chr_44_str_space_tuple, 0, const_str_chr_44 ); Py_INCREF( const_str_chr_44 );
    const_str_space = UNSTREAM_CHAR( 32, 0 );
    PyTuple_SET_ITEM( const_tuple_str_chr_44_str_space_tuple, 1, const_str_space ); Py_INCREF( const_str_space );
    const_str_digest_632e992f0216721e4402932436d128b8 = UNSTREAM_STRING( &constant_bin[ 42205 ], 24, 0 );
    const_str_digest_b8b5aead6e09884da3f6dd805c61d13b = UNSTREAM_STRING( &constant_bin[ 42229 ], 11, 0 );
    const_tuple_str_digest_0f5748902608737e1fdb2ef20fee5302_tuple = PyTuple_New( 1 );
    const_str_digest_0f5748902608737e1fdb2ef20fee5302 = UNSTREAM_STRING( &constant_bin[ 42240 ], 20, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_0f5748902608737e1fdb2ef20fee5302_tuple, 0, const_str_digest_0f5748902608737e1fdb2ef20fee5302 ); Py_INCREF( const_str_digest_0f5748902608737e1fdb2ef20fee5302 );
    const_tuple_96db87ae5af861479adab30eb4bf579d_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 42260 ], 365 );
    const_str_digest_2a648bfbc67de22c9b95d257cdaf98d3 = UNSTREAM_STRING( &constant_bin[ 42625 ], 24, 0 );
    const_str_digest_6375e56f5fcdd0a4a1c5333f06107399 = UNSTREAM_STRING( &constant_bin[ 42649 ], 24, 0 );
    const_str_plain_tri_left = UNSTREAM_STRING( &constant_bin[ 42673 ], 8, 1 );
    const_str_digest_ee6982adf6363a5b5f480384c2e084d2 = UNSTREAM_STRING( &constant_bin[ 42681 ], 8, 0 );
    const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple = PyTuple_New( 16 );
    const_str_digest_af9c4620b6a67dd2ca195108627d4405 = UNSTREAM_STRING( &constant_bin[ 42630 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 0, const_str_digest_af9c4620b6a67dd2ca195108627d4405 ); Py_INCREF( const_str_digest_af9c4620b6a67dd2ca195108627d4405 );
    const_str_digest_fd729b720765171991723e1c2f58e688 = UNSTREAM_STRING( &constant_bin[ 42689 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 1, const_str_digest_fd729b720765171991723e1c2f58e688 ); Py_INCREF( const_str_digest_fd729b720765171991723e1c2f58e688 );
    const_str_digest_18974878984e918362c04f56becf265d = UNSTREAM_STRING( &constant_bin[ 42713 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 2, const_str_digest_18974878984e918362c04f56becf265d ); Py_INCREF( const_str_digest_18974878984e918362c04f56becf265d );
    const_str_digest_f76a964d55ae5f4933f8f8b38accc642 = UNSTREAM_STRING( &constant_bin[ 42737 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 3, const_str_digest_f76a964d55ae5f4933f8f8b38accc642 ); Py_INCREF( const_str_digest_f76a964d55ae5f4933f8f8b38accc642 );
    const_str_digest_bbb1fa0d3f443e599b5b8653f0c382f3 = UNSTREAM_STRING( &constant_bin[ 42761 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 4, const_str_digest_bbb1fa0d3f443e599b5b8653f0c382f3 ); Py_INCREF( const_str_digest_bbb1fa0d3f443e599b5b8653f0c382f3 );
    const_str_digest_a5f3bdf7042a2c94726acc36d30aa47f = UNSTREAM_STRING( &constant_bin[ 42785 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 5, const_str_digest_a5f3bdf7042a2c94726acc36d30aa47f ); Py_INCREF( const_str_digest_a5f3bdf7042a2c94726acc36d30aa47f );
    const_str_digest_4ab6627b8404f8e1cb5bb8163c347985 = UNSTREAM_STRING( &constant_bin[ 42809 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 6, const_str_digest_4ab6627b8404f8e1cb5bb8163c347985 ); Py_INCREF( const_str_digest_4ab6627b8404f8e1cb5bb8163c347985 );
    const_str_digest_fc21ef38dce58b5b585a7f48e4278526 = UNSTREAM_STRING( &constant_bin[ 42833 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 7, const_str_digest_fc21ef38dce58b5b585a7f48e4278526 ); Py_INCREF( const_str_digest_fc21ef38dce58b5b585a7f48e4278526 );
    const_str_digest_caa0eb53245ef3ef5d9e4e558083b2a6 = UNSTREAM_STRING( &constant_bin[ 42857 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 8, const_str_digest_caa0eb53245ef3ef5d9e4e558083b2a6 ); Py_INCREF( const_str_digest_caa0eb53245ef3ef5d9e4e558083b2a6 );
    const_str_digest_3b868d2a2ac3b9e7e7ad0de4b49b5250 = UNSTREAM_STRING( &constant_bin[ 42729 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 9, const_str_digest_3b868d2a2ac3b9e7e7ad0de4b49b5250 ); Py_INCREF( const_str_digest_3b868d2a2ac3b9e7e7ad0de4b49b5250 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 10, const_str_digest_3b868d2a2ac3b9e7e7ad0de4b49b5250 ); Py_INCREF( const_str_digest_3b868d2a2ac3b9e7e7ad0de4b49b5250 );
    const_str_digest_7086b80ed05082b2d4c11d4bd1a64b7d = UNSTREAM_STRING( &constant_bin[ 42706 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 11, const_str_digest_7086b80ed05082b2d4c11d4bd1a64b7d ); Py_INCREF( const_str_digest_7086b80ed05082b2d4c11d4bd1a64b7d );
    const_str_digest_c347bb4ad121826969f6165741a4f107 = UNSTREAM_STRING( &constant_bin[ 42881 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 12, const_str_digest_c347bb4ad121826969f6165741a4f107 ); Py_INCREF( const_str_digest_c347bb4ad121826969f6165741a4f107 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 13, const_str_digest_2a648bfbc67de22c9b95d257cdaf98d3 ); Py_INCREF( const_str_digest_2a648bfbc67de22c9b95d257cdaf98d3 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 14, const_str_digest_6375e56f5fcdd0a4a1c5333f06107399 ); Py_INCREF( const_str_digest_6375e56f5fcdd0a4a1c5333f06107399 );
    PyTuple_SET_ITEM( const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple, 15, const_str_digest_6375e56f5fcdd0a4a1c5333f06107399 ); Py_INCREF( const_str_digest_6375e56f5fcdd0a4a1c5333f06107399 );
    const_str_plain_fillitem = UNSTREAM_STRING( &constant_bin[ 42905 ], 8, 1 );
    const_str_digest_ac9f1045f8ca0fed745ab4c3ae5c23f9 = UNSTREAM_STRING( &constant_bin[ 42913 ], 24, 0 );
    const_str_plain_step = UNSTREAM_STRING( &constant_bin[ 4193 ], 4, 1 );
    const_str_plain_tri_right = UNSTREAM_STRING( &constant_bin[ 42937 ], 9, 1 );
    const_tuple_str_digest_c6ea53ac2684e039b757c7a331688c7d_str_empty_tuple = PyTuple_New( 2 );
    const_str_digest_c6ea53ac2684e039b757c7a331688c7d = UNSTREAM_STRING( &constant_bin[ 42946 ], 2, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_c6ea53ac2684e039b757c7a331688c7d_str_empty_tuple, 0, const_str_digest_c6ea53ac2684e039b757c7a331688c7d ); Py_INCREF( const_str_digest_c6ea53ac2684e039b757c7a331688c7d );
    PyTuple_SET_ITEM( const_tuple_str_digest_c6ea53ac2684e039b757c7a331688c7d_str_empty_tuple, 1, const_str_empty ); Py_INCREF( const_str_empty );
    const_str_plain_arrow = UNSTREAM_STRING( &constant_bin[ 42948 ], 5, 1 );
    const_str_plain_diamond = UNSTREAM_STRING( &constant_bin[ 42953 ], 7, 1 );
    const_tuple_922ba2f432159266b6122aae41c64070_tuple = PyTuple_New( 16 );
    const_str_digest_7915ecd5ff0aa7b34be6436dcafaabbb = UNSTREAM_STRING( &constant_bin[ 42960 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 0, const_str_digest_7915ecd5ff0aa7b34be6436dcafaabbb ); Py_INCREF( const_str_digest_7915ecd5ff0aa7b34be6436dcafaabbb );
    const_str_digest_8d1ef6fdfce6c23e6c7099119e19709a = UNSTREAM_STRING( &constant_bin[ 42984 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 1, const_str_digest_8d1ef6fdfce6c23e6c7099119e19709a ); Py_INCREF( const_str_digest_8d1ef6fdfce6c23e6c7099119e19709a );
    const_str_digest_2f54e52824c029c1e44de8b5a174c37a = UNSTREAM_STRING( &constant_bin[ 43008 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 2, const_str_digest_2f54e52824c029c1e44de8b5a174c37a ); Py_INCREF( const_str_digest_2f54e52824c029c1e44de8b5a174c37a );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 3, const_str_digest_632e992f0216721e4402932436d128b8 ); Py_INCREF( const_str_digest_632e992f0216721e4402932436d128b8 );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 4, const_str_digest_ac9f1045f8ca0fed745ab4c3ae5c23f9 ); Py_INCREF( const_str_digest_ac9f1045f8ca0fed745ab4c3ae5c23f9 );
    const_str_digest_6b6df9ef410005bd837fc89125c22573 = UNSTREAM_STRING( &constant_bin[ 43032 ], 24, 0 );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 5, const_str_digest_6b6df9ef410005bd837fc89125c22573 ); Py_INCREF( const_str_digest_6b6df9ef410005bd837fc89125c22573 );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 6, const_str_digest_ac9f1045f8ca0fed745ab4c3ae5c23f9 ); Py_INCREF( const_str_digest_ac9f1045f8ca0fed745ab4c3ae5c23f9 );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 7, const_str_digest_632e992f0216721e4402932436d128b8 ); Py_INCREF( const_str_digest_632e992f0216721e4402932436d128b8 );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 8, const_str_digest_2f54e52824c029c1e44de8b5a174c37a ); Py_INCREF( const_str_digest_2f54e52824c029c1e44de8b5a174c37a );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 9, const_str_digest_8d1ef6fdfce6c23e6c7099119e19709a ); Py_INCREF( const_str_digest_8d1ef6fdfce6c23e6c7099119e19709a );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 10, const_str_digest_7915ecd5ff0aa7b34be6436dcafaabbb ); Py_INCREF( const_str_digest_7915ecd5ff0aa7b34be6436dcafaabbb );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 11, const_str_digest_6375e56f5fcdd0a4a1c5333f06107399 ); Py_INCREF( const_str_digest_6375e56f5fcdd0a4a1c5333f06107399 );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 12, const_str_digest_6375e56f5fcdd0a4a1c5333f06107399 ); Py_INCREF( const_str_digest_6375e56f5fcdd0a4a1c5333f06107399 );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 13, const_str_digest_6375e56f5fcdd0a4a1c5333f06107399 ); Py_INCREF( const_str_digest_6375e56f5fcdd0a4a1c5333f06107399 );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 14, const_str_digest_6375e56f5fcdd0a4a1c5333f06107399 ); Py_INCREF( const_str_digest_6375e56f5fcdd0a4a1c5333f06107399 );
    PyTuple_SET_ITEM( const_tuple_922ba2f432159266b6122aae41c64070_tuple, 15, const_str_digest_6375e56f5fcdd0a4a1c5333f06107399 ); Py_INCREF( const_str_digest_6375e56f5fcdd0a4a1c5333f06107399 );
    const_str_plain_cursdata = UNSTREAM_STRING( &constant_bin[ 43056 ], 8, 1 );
    const_str_plain_sizer_xy_strings = UNSTREAM_STRING( &constant_bin[ 43064 ], 16, 1 );
    const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple = PyTuple_New( 16 );
    const_str_digest_d01eb588f4f55c8f6985b90c18212bda = UNSTREAM_STRING( &constant_bin[ 43080 ], 8, 0 );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 0, const_str_digest_d01eb588f4f55c8f6985b90c18212bda ); Py_INCREF( const_str_digest_d01eb588f4f55c8f6985b90c18212bda );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 1, const_str_digest_ee6982adf6363a5b5f480384c2e084d2 ); Py_INCREF( const_str_digest_ee6982adf6363a5b5f480384c2e084d2 );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 2, const_str_digest_ee6982adf6363a5b5f480384c2e084d2 ); Py_INCREF( const_str_digest_ee6982adf6363a5b5f480384c2e084d2 );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 3, const_str_digest_ee6982adf6363a5b5f480384c2e084d2 ); Py_INCREF( const_str_digest_ee6982adf6363a5b5f480384c2e084d2 );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 4, const_str_digest_ee6982adf6363a5b5f480384c2e084d2 ); Py_INCREF( const_str_digest_ee6982adf6363a5b5f480384c2e084d2 );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 5, const_str_digest_ee6982adf6363a5b5f480384c2e084d2 ); Py_INCREF( const_str_digest_ee6982adf6363a5b5f480384c2e084d2 );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 6, const_str_digest_ee6982adf6363a5b5f480384c2e084d2 ); Py_INCREF( const_str_digest_ee6982adf6363a5b5f480384c2e084d2 );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 7, const_str_digest_ee6982adf6363a5b5f480384c2e084d2 ); Py_INCREF( const_str_digest_ee6982adf6363a5b5f480384c2e084d2 );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 8, const_str_digest_ee6982adf6363a5b5f480384c2e084d2 ); Py_INCREF( const_str_digest_ee6982adf6363a5b5f480384c2e084d2 );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 9, const_str_digest_ee6982adf6363a5b5f480384c2e084d2 ); Py_INCREF( const_str_digest_ee6982adf6363a5b5f480384c2e084d2 );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 10, const_str_digest_ee6982adf6363a5b5f480384c2e084d2 ); Py_INCREF( const_str_digest_ee6982adf6363a5b5f480384c2e084d2 );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 11, const_str_digest_d01eb588f4f55c8f6985b90c18212bda ); Py_INCREF( const_str_digest_d01eb588f4f55c8f6985b90c18212bda );
    const_str_digest_12aed46edafdc1d5b5c211c83e9bf8bb = UNSTREAM_STRING( &constant_bin[ 42221 ], 8, 0 );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 12, const_str_digest_12aed46edafdc1d5b5c211c83e9bf8bb ); Py_INCREF( const_str_digest_12aed46edafdc1d5b5c211c83e9bf8bb );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 13, const_str_digest_12aed46edafdc1d5b5c211c83e9bf8bb ); Py_INCREF( const_str_digest_12aed46edafdc1d5b5c211c83e9bf8bb );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 14, const_str_digest_12aed46edafdc1d5b5c211c83e9bf8bb ); Py_INCREF( const_str_digest_12aed46edafdc1d5b5c211c83e9bf8bb );
    PyTuple_SET_ITEM( const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple, 15, const_str_digest_12aed46edafdc1d5b5c211c83e9bf8bb ); Py_INCREF( const_str_digest_12aed46edafdc1d5b5c211c83e9bf8bb );
    const_tuple_str_plain_num_str_plain_val_str_plain_x_str_plain_b_tuple = PyTuple_New( 4 );
    const_str_plain_num = UNSTREAM_STRING( &constant_bin[ 6824 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_num_str_plain_val_str_plain_x_str_plain_b_tuple, 0, const_str_plain_num ); Py_INCREF( const_str_plain_num );
    PyTuple_SET_ITEM( const_tuple_str_plain_num_str_plain_val_str_plain_x_str_plain_b_tuple, 1, const_str_plain_val ); Py_INCREF( const_str_plain_val );
    PyTuple_SET_ITEM( const_tuple_str_plain_num_str_plain_val_str_plain_x_str_plain_b_tuple, 2, const_str_plain_x ); Py_INCREF( const_str_plain_x );
    PyTuple_SET_ITEM( const_tuple_str_plain_num_str_plain_val_str_plain_x_str_plain_b_tuple, 3, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    const_str_plain_textmarker_strings = UNSTREAM_STRING( &constant_bin[ 43088 ], 18, 1 );
    const_str_plain_curs = UNSTREAM_STRING( &constant_bin[ 6491 ], 4, 1 );
    const_str_plain_maskdata = UNSTREAM_STRING( &constant_bin[ 43106 ], 8, 1 );
    const_tuple_c502d6295ca78f7c883cc52991623ead_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 43114 ], 509 );
    const_str_digest_7336a9dd343cfb1bed1a3fce43bb0bc3 = UNSTREAM_STRING( &constant_bin[ 43623 ], 7, 0 );
    const_str_digest_8d3cab125ca9976d324a4961419d4bea = UNSTREAM_STRING( &constant_bin[ 43630 ], 255, 0 );
    const_tuple_2c1d6d232dd3e0e3e167fc51ca591929_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 43885 ], 365 );
    const_str_plain_broken_x = UNSTREAM_STRING( &constant_bin[ 44250 ], 8, 1 );
    const_str_digest_d303224accee291ce3fe897e8af818d3 = UNSTREAM_STRING( &constant_bin[ 44258 ], 45, 0 );
    const_tuple_10848a3ccffd6b5c0e2c59e547874c24_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 44303 ], 365 );
    const_str_plain_thickarrow_strings = UNSTREAM_STRING( &constant_bin[ 44668 ], 18, 1 );
    const_tuple_8a2c567399eef9112249151a8c62caa3_tuple = PyTuple_New( 13 );
    PyTuple_SET_ITEM( const_tuple_8a2c567399eef9112249151a8c62caa3_tuple, 0, const_str_plain_curs ); Py_INCREF( const_str_plain_curs );
    PyTuple_SET_ITEM( const_tuple_8a2c567399eef9112249151a8c62caa3_tuple, 1, const_str_plain_mask ); Py_INCREF( const_str_plain_mask );
    const_str_plain_bitswap = UNSTREAM_STRING( &constant_bin[ 44686 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_8a2c567399eef9112249151a8c62caa3_tuple, 2, const_str_plain_bitswap ); Py_INCREF( const_str_plain_bitswap );
    PyTuple_SET_ITEM( const_tuple_8a2c567399eef9112249151a8c62caa3_tuple, 3, const_str_plain_line ); Py_INCREF( const_str_plain_line );
    PyTuple_SET_ITEM( const_tuple_8a2c567399eef9112249151a8c62caa3_tuple, 4, const_str_plain_width ); Py_INCREF( const_str_plain_width );
    PyTuple_SET_ITEM( const_tuple_8a2c567399eef9112249151a8c62caa3_tuple, 5, const_str_plain_height ); Py_INCREF( const_str_plain_height );
    const_str_plain_hotx = UNSTREAM_STRING( &constant_bin[ 44693 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_8a2c567399eef9112249151a8c62caa3_tuple, 6, const_str_plain_hotx ); Py_INCREF( const_str_plain_hotx );
    const_str_plain_hoty = UNSTREAM_STRING( &constant_bin[ 44697 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_8a2c567399eef9112249151a8c62caa3_tuple, 7, const_str_plain_hoty ); Py_INCREF( const_str_plain_hoty );
    PyTuple_SET_ITEM( const_tuple_8a2c567399eef9112249151a8c62caa3_tuple, 8, const_str_plain_info ); Py_INCREF( const_str_plain_info );
    PyTuple_SET_ITEM( const_tuple_8a2c567399eef9112249151a8c62caa3_tuple, 9, const_str_plain_data ); Py_INCREF( const_str_plain_data );
    PyTuple_SET_ITEM( const_tuple_8a2c567399eef9112249151a8c62caa3_tuple, 10, const_str_plain_cursdata ); Py_INCREF( const_str_plain_cursdata );
    PyTuple_SET_ITEM( const_tuple_8a2c567399eef9112249151a8c62caa3_tuple, 11, const_str_plain_x ); Py_INCREF( const_str_plain_x );
    PyTuple_SET_ITEM( const_tuple_8a2c567399eef9112249151a8c62caa3_tuple, 12, const_str_plain_maskdata ); Py_INCREF( const_str_plain_maskdata );
    const_tuple_cb92914d270a4f6b6d6d7e626220d263_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 44701 ], 365 );
    const_str_plain_white = UNSTREAM_STRING( &constant_bin[ 19438 ], 5, 1 );
    const_str_plain_readlines = UNSTREAM_STRING( &constant_bin[ 43807 ], 9, 1 );
    const_str_plain_black = UNSTREAM_STRING( &constant_bin[ 29441 ], 5, 1 );
    const_str_plain_ball = UNSTREAM_STRING( &constant_bin[ 907 ], 4, 1 );
    const_str_plain_sizer_x_strings = UNSTREAM_STRING( &constant_bin[ 45066 ], 15, 1 );
    const_str_digest_0ade19f25d59df28699eb1ae849f19fb = UNSTREAM_STRING( &constant_bin[ 45081 ], 529, 0 );
    const_str_digest_5d8f7f3a2bdf9954219128ed2b89c78a = UNSTREAM_STRING( &constant_bin[ 45610 ], 39, 0 );
    const_str_plain_strings = UNSTREAM_STRING( &constant_bin[ 43073 ], 7, 1 );
    const_str_plain_xor = UNSTREAM_STRING( &constant_bin[ 45649 ], 3, 1 );
    const_tuple_b5efab4462fd34b9188076b876531ff5_tuple = PyTuple_New( 12 );
    PyTuple_SET_ITEM( const_tuple_b5efab4462fd34b9188076b876531ff5_tuple, 0, const_str_plain_strings ); Py_INCREF( const_str_plain_strings );
    PyTuple_SET_ITEM( const_tuple_b5efab4462fd34b9188076b876531ff5_tuple, 1, const_str_plain_black ); Py_INCREF( const_str_plain_black );
    PyTuple_SET_ITEM( const_tuple_b5efab4462fd34b9188076b876531ff5_tuple, 2, const_str_plain_white ); Py_INCREF( const_str_plain_white );
    PyTuple_SET_ITEM( const_tuple_b5efab4462fd34b9188076b876531ff5_tuple, 3, const_str_plain_xor ); Py_INCREF( const_str_plain_xor );
    PyTuple_SET_ITEM( const_tuple_b5efab4462fd34b9188076b876531ff5_tuple, 4, const_str_plain_size ); Py_INCREF( const_str_plain_size );
    PyTuple_SET_ITEM( const_tuple_b5efab4462fd34b9188076b876531ff5_tuple, 5, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    PyTuple_SET_ITEM( const_tuple_b5efab4462fd34b9188076b876531ff5_tuple, 6, const_str_plain_maskdata ); Py_INCREF( const_str_plain_maskdata );
    PyTuple_SET_ITEM( const_tuple_b5efab4462fd34b9188076b876531ff5_tuple, 7, const_str_plain_filldata ); Py_INCREF( const_str_plain_filldata );
    const_str_plain_maskitem = UNSTREAM_STRING( &constant_bin[ 45652 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_b5efab4462fd34b9188076b876531ff5_tuple, 8, const_str_plain_maskitem ); Py_INCREF( const_str_plain_maskitem );
    PyTuple_SET_ITEM( const_tuple_b5efab4462fd34b9188076b876531ff5_tuple, 9, const_str_plain_fillitem ); Py_INCREF( const_str_plain_fillitem );
    PyTuple_SET_ITEM( const_tuple_b5efab4462fd34b9188076b876531ff5_tuple, 10, const_str_plain_step ); Py_INCREF( const_str_plain_step );
    PyTuple_SET_ITEM( const_tuple_b5efab4462fd34b9188076b876531ff5_tuple, 11, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    const_tuple_str_digest_7336a9dd343cfb1bed1a3fce43bb0bc3_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_7336a9dd343cfb1bed1a3fce43bb0bc3_tuple, 0, const_str_digest_7336a9dd343cfb1bed1a3fce43bb0bc3 ); Py_INCREF( const_str_digest_7336a9dd343cfb1bed1a3fce43bb0bc3 );
    const_tuple_2f560d6b6d76c7251a2b5a9cabe326ff_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 45660 ], 701 );
    const_str_plain_load_xbm = UNSTREAM_STRING( &constant_bin[ 43645 ], 8, 1 );
    const_str_plain_sizer_y_strings = UNSTREAM_STRING( &constant_bin[ 46361 ], 15, 1 );
    const_tuple_str_digest_b8b5aead6e09884da3f6dd805c61d13b_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_b8b5aead6e09884da3f6dd805c61d13b_tuple, 0, const_str_digest_b8b5aead6e09884da3f6dd805c61d13b ); Py_INCREF( const_str_digest_b8b5aead6e09884da3f6dd805c61d13b );
    const_tuple_286ccaf39c99eff01ddd0b3f99d77ecf_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 46376 ], 365 );
    const_tuple_c69d0ea490cb72cadfbecd20104d0bcf_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 46741 ], 365 );
    const_tuple_str_plain_X_str_dot_str_plain_o_tuple = PyTuple_New( 3 );
    const_str_plain_X = UNSTREAM_CHAR( 88, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_X_str_dot_str_plain_o_tuple, 0, const_str_plain_X ); Py_INCREF( const_str_plain_X );
    PyTuple_SET_ITEM( const_tuple_str_plain_X_str_dot_str_plain_o_tuple, 1, const_str_dot ); Py_INCREF( const_str_dot );
    PyTuple_SET_ITEM( const_tuple_str_plain_X_str_dot_str_plain_o_tuple, 2, const_str_plain_o ); Py_INCREF( const_str_plain_o );
    const_str_digest_436dcad3629c6b56c661d5c450115dc0 = UNSTREAM_STRING( &constant_bin[ 47106 ], 596, 0 );

    constants_created = true;
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pygame$cursors( void )
{
    // The module may not have been used at all.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_38fdeeb0bd241bff1b8b1a1ddcdaf972;
static PyCodeObject *codeobj_c191f7a2cd00ab9aa8f7bf02ca6d68ac;
static PyCodeObject *codeobj_5913fbe0aabe7522b79a941a96a7d4e9;

static void createModuleCodeObjects(void)
{
    module_filename_obj = const_str_digest_8a244d4f06cfd47791362860096740cb;
    codeobj_38fdeeb0bd241bff1b8b1a1ddcdaf972 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_bitswap, 252, const_tuple_str_plain_num_str_plain_val_str_plain_x_str_plain_b_tuple, 1, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c191f7a2cd00ab9aa8f7bf02ca6d68ac = MAKE_CODEOBJ( module_filename_obj, const_str_plain_compile, 189, const_tuple_b5efab4462fd34b9188076b876531ff5_tuple, 4, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5913fbe0aabe7522b79a941a96a7d4e9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_load_xbm, 244, const_tuple_8a2c567399eef9112249151a8c62caa3_tuple, 2, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_pygame$cursors$$$function_1_compile( PyObject *defaults );


static PyObject *MAKE_FUNCTION_pygame$cursors$$$function_2_load_xbm(  );


static PyObject *MAKE_FUNCTION_pygame$cursors$$$function_2_load_xbm$$$function_1_bitswap(  );


// The module function definitions.
static PyObject *impl_pygame$cursors$$$function_1_compile( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_strings = python_pars[ 0 ];
    PyObject *par_black = python_pars[ 1 ];
    PyObject *par_white = python_pars[ 2 ];
    PyObject *par_xor = python_pars[ 3 ];
    PyObject *var_size = NULL;
    PyObject *var_s = NULL;
    PyObject *var_maskdata = NULL;
    PyObject *var_filldata = NULL;
    PyObject *var_maskitem = NULL;
    PyObject *var_fillitem = NULL;
    PyObject *var_step = NULL;
    PyObject *var_c = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_for_loop_3__for_iterator = NULL;
    PyObject *tmp_for_loop_3__iter_value = NULL;
    PyObject *tmp_assign_unpack_2__assign_source = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    int tmp_cmp_Eq_1;
    int tmp_cmp_Eq_2;
    int tmp_cmp_Eq_3;
    int tmp_cmp_NotEq_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_left_4;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    PyObject *tmp_compare_right_4;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_frame_locals;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iter_arg_2;
    PyObject *tmp_iter_arg_3;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_left_name_3;
    PyObject *tmp_left_name_4;
    PyObject *tmp_left_name_5;
    PyObject *tmp_left_name_6;
    PyObject *tmp_left_name_7;
    PyObject *tmp_left_name_8;
    PyObject *tmp_left_name_9;
    PyObject *tmp_left_name_10;
    PyObject *tmp_len_arg_1;
    PyObject *tmp_len_arg_2;
    PyObject *tmp_len_arg_3;
    PyObject *tmp_make_exception_arg_1;
    PyObject *tmp_make_exception_arg_2;
    PyObject *tmp_next_source_1;
    PyObject *tmp_next_source_2;
    PyObject *tmp_next_source_3;
    int tmp_or_left_truth_1;
    PyObject *tmp_or_left_value_1;
    PyObject *tmp_or_right_value_1;
    PyObject *tmp_raise_type_1;
    PyObject *tmp_raise_type_2;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_right_name_3;
    PyObject *tmp_right_name_4;
    PyObject *tmp_right_name_5;
    PyObject *tmp_right_name_6;
    PyObject *tmp_right_name_7;
    PyObject *tmp_right_name_8;
    PyObject *tmp_right_name_9;
    PyObject *tmp_right_name_10;
    Py_ssize_t tmp_slice_index_upper_1;
    PyObject *tmp_slice_source_1;
    Py_ssize_t tmp_sliceslicedel_index_lower_1;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscribed_name_2;
    PyObject *tmp_subscribed_name_3;
    PyObject *tmp_subscribed_name_4;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_subscript_name_2;
    PyObject *tmp_subscript_name_3;
    PyObject *tmp_subscript_name_4;
    PyObject *tmp_tuple_arg_1;
    PyObject *tmp_tuple_arg_2;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_c191f7a2cd00ab9aa8f7bf02ca6d68ac, module_pygame$cursors );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_assign_source_1 = PyTuple_New( 2 );
    tmp_subscribed_name_1 = par_strings;

    tmp_subscript_name_1 = const_int_0;
    tmp_len_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
    if ( tmp_len_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_1 );

        exception_lineno = 208;
        goto frame_exception_exit_1;
    }
    tmp_tuple_element_1 = BUILTIN_LEN( tmp_len_arg_1 );
    Py_DECREF( tmp_len_arg_1 );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_1 );

        exception_lineno = 208;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_assign_source_1, 0, tmp_tuple_element_1 );
    tmp_len_arg_2 = par_strings;

    tmp_tuple_element_1 = BUILTIN_LEN( tmp_len_arg_2 );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_1 );

        exception_lineno = 208;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_assign_source_1, 1, tmp_tuple_element_1 );
    assert( var_size == NULL );
    var_size = tmp_assign_source_1;

    tmp_subscribed_name_2 = var_size;

    tmp_subscript_name_2 = const_int_0;
    tmp_left_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
    if ( tmp_left_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 209;
        goto frame_exception_exit_1;
    }
    tmp_right_name_1 = const_int_pos_8;
    tmp_or_left_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_left_name_1 );
    if ( tmp_or_left_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 209;
        goto frame_exception_exit_1;
    }
    tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
    if ( tmp_or_left_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_or_left_value_1 );

        exception_lineno = 209;
        goto frame_exception_exit_1;
    }
    if ( tmp_or_left_truth_1 == 1 )
    {
        goto or_left_1;
    }
    else
    {
        goto or_right_1;
    }
    or_right_1:;
    Py_DECREF( tmp_or_left_value_1 );
    tmp_subscribed_name_3 = var_size;

    tmp_subscript_name_3 = const_int_pos_1;
    tmp_left_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
    if ( tmp_left_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 209;
        goto frame_exception_exit_1;
    }
    tmp_right_name_2 = const_int_pos_8;
    tmp_or_right_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
    Py_DECREF( tmp_left_name_2 );
    if ( tmp_or_right_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 209;
        goto frame_exception_exit_1;
    }
    tmp_cond_value_1 = tmp_or_right_value_1;
    goto or_end_1;
    or_left_1:;
    tmp_cond_value_1 = tmp_or_left_value_1;
    or_end_1:;
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        exception_lineno = 209;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_left_name_3 = const_str_digest_d303224accee291ce3fe897e8af818d3;
    tmp_right_name_3 = var_size;

    tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
    if ( tmp_make_exception_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 210;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 210;
    {
        PyObject *call_args[] = { tmp_make_exception_arg_1 };
        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
    }

    Py_DECREF( tmp_make_exception_arg_1 );
    assert( tmp_raise_type_1 != NULL );
    exception_type = tmp_raise_type_1;
    exception_lineno = 210;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    goto frame_exception_exit_1;
    branch_no_1:;
    tmp_sliceslicedel_index_lower_1 = 1;
    tmp_slice_index_upper_1 = PY_SSIZE_T_MAX;
    tmp_slice_source_1 = par_strings;

    tmp_iter_arg_1 = LOOKUP_INDEX_SLICE( tmp_slice_source_1, tmp_sliceslicedel_index_lower_1, tmp_slice_index_upper_1 );
    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 212;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 212;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_2;

    // Tried code:
    loop_start_1:;
    tmp_next_source_1 = tmp_for_loop_1__for_iterator;

    tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
    if ( tmp_assign_source_3 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_1;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 212;
            goto try_except_handler_2;
        }
    }

    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_3;
        Py_XDECREF( old );
    }

    tmp_assign_source_4 = tmp_for_loop_1__iter_value;

    {
        PyObject *old = var_s;
        var_s = tmp_assign_source_4;
        Py_INCREF( var_s );
        Py_XDECREF( old );
    }

    tmp_len_arg_3 = var_s;

    tmp_compare_left_1 = BUILTIN_LEN( tmp_len_arg_3 );
    if ( tmp_compare_left_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 213;
        goto try_except_handler_2;
    }
    tmp_subscribed_name_4 = var_size;

    tmp_subscript_name_4 = const_int_0;
    tmp_compare_right_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
    if ( tmp_compare_right_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_1 );

        exception_lineno = 213;
        goto try_except_handler_2;
    }
    tmp_cmp_NotEq_1 = RICH_COMPARE_BOOL_NE( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_cmp_NotEq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_1 );
        Py_DECREF( tmp_compare_right_1 );

        exception_lineno = 213;
        goto try_except_handler_2;
    }
    Py_DECREF( tmp_compare_left_1 );
    Py_DECREF( tmp_compare_right_1 );
    if ( tmp_cmp_NotEq_1 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_make_exception_arg_2 = const_str_digest_5d8f7f3a2bdf9954219128ed2b89c78a;
    frame_function->f_lineno = 214;
    {
        PyObject *call_args[] = { tmp_make_exception_arg_2 };
        tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
    }

    assert( tmp_raise_type_2 != NULL );
    exception_type = tmp_raise_type_2;
    exception_lineno = 214;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    goto try_except_handler_2;
    branch_no_2:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 212;
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_assign_source_5 = PyList_New( 0 );
    assert( var_maskdata == NULL );
    var_maskdata = tmp_assign_source_5;

    tmp_assign_source_6 = PyList_New( 0 );
    assert( var_filldata == NULL );
    var_filldata = tmp_assign_source_6;

    tmp_assign_source_7 = const_int_0;
    assert( var_maskitem == NULL );
    Py_INCREF( tmp_assign_source_7 );
    var_maskitem = tmp_assign_source_7;

    tmp_assign_source_8 = const_int_0;
    assert( var_fillitem == NULL );
    Py_INCREF( tmp_assign_source_8 );
    var_fillitem = tmp_assign_source_8;

    tmp_assign_source_9 = const_int_pos_8;
    assert( var_step == NULL );
    Py_INCREF( tmp_assign_source_9 );
    var_step = tmp_assign_source_9;

    tmp_iter_arg_2 = par_strings;

    tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_2 );
    if ( tmp_assign_source_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 222;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_2__for_iterator == NULL );
    tmp_for_loop_2__for_iterator = tmp_assign_source_10;

    // Tried code:
    loop_start_2:;
    tmp_next_source_2 = tmp_for_loop_2__for_iterator;

    tmp_assign_source_11 = ITERATOR_NEXT( tmp_next_source_2 );
    if ( tmp_assign_source_11 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_2;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 222;
            goto try_except_handler_3;
        }
    }

    {
        PyObject *old = tmp_for_loop_2__iter_value;
        tmp_for_loop_2__iter_value = tmp_assign_source_11;
        Py_XDECREF( old );
    }

    tmp_assign_source_12 = tmp_for_loop_2__iter_value;

    {
        PyObject *old = var_s;
        var_s = tmp_assign_source_12;
        Py_INCREF( var_s );
        Py_XDECREF( old );
    }

    tmp_iter_arg_3 = var_s;

    tmp_assign_source_13 = MAKE_ITERATOR( tmp_iter_arg_3 );
    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 223;
        goto try_except_handler_3;
    }
    {
        PyObject *old = tmp_for_loop_3__for_iterator;
        tmp_for_loop_3__for_iterator = tmp_assign_source_13;
        Py_XDECREF( old );
    }

    // Tried code:
    loop_start_3:;
    tmp_next_source_3 = tmp_for_loop_3__for_iterator;

    tmp_assign_source_14 = ITERATOR_NEXT( tmp_next_source_3 );
    if ( tmp_assign_source_14 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_3;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 223;
            goto try_except_handler_4;
        }
    }

    {
        PyObject *old = tmp_for_loop_3__iter_value;
        tmp_for_loop_3__iter_value = tmp_assign_source_14;
        Py_XDECREF( old );
    }

    tmp_assign_source_15 = tmp_for_loop_3__iter_value;

    {
        PyObject *old = var_c;
        var_c = tmp_assign_source_15;
        Py_INCREF( var_c );
        Py_XDECREF( old );
    }

    tmp_left_name_4 = var_maskitem;

    if ( tmp_left_name_4 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "local variable '%s' referenced before assignment", "maskitem" );
        exception_tb = NULL;

        exception_lineno = 224;
        goto try_except_handler_4;
    }

    tmp_right_name_4 = const_int_pos_1;
    tmp_assign_source_16 = BINARY_OPERATION( PyNumber_Lshift, tmp_left_name_4, tmp_right_name_4 );
    if ( tmp_assign_source_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 224;
        goto try_except_handler_4;
    }
    {
        PyObject *old = var_maskitem;
        var_maskitem = tmp_assign_source_16;
        Py_XDECREF( old );
    }

    tmp_left_name_5 = var_fillitem;

    if ( tmp_left_name_5 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "local variable '%s' referenced before assignment", "fillitem" );
        exception_tb = NULL;

        exception_lineno = 225;
        goto try_except_handler_4;
    }

    tmp_right_name_5 = const_int_pos_1;
    tmp_assign_source_17 = BINARY_OPERATION( PyNumber_Lshift, tmp_left_name_5, tmp_right_name_5 );
    if ( tmp_assign_source_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 225;
        goto try_except_handler_4;
    }
    {
        PyObject *old = var_fillitem;
        var_fillitem = tmp_assign_source_17;
        Py_XDECREF( old );
    }

    tmp_left_name_6 = var_step;

    if ( tmp_left_name_6 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "local variable '%s' referenced before assignment", "step" );
        exception_tb = NULL;

        exception_lineno = 226;
        goto try_except_handler_4;
    }

    tmp_right_name_6 = const_int_pos_1;
    tmp_assign_source_18 = BINARY_OPERATION_SUB( tmp_left_name_6, tmp_right_name_6 );
    if ( tmp_assign_source_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 226;
        goto try_except_handler_4;
    }
    {
        PyObject *old = var_step;
        var_step = tmp_assign_source_18;
        Py_XDECREF( old );
    }

    tmp_compare_left_2 = var_c;

    tmp_compare_right_2 = par_black;

    tmp_cmp_Eq_1 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_2, tmp_compare_right_2 );
    if ( tmp_cmp_Eq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 227;
        goto try_except_handler_4;
    }
    if ( tmp_cmp_Eq_1 == 1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_left_name_7 = var_maskitem;

    tmp_right_name_7 = const_int_pos_1;
    tmp_assign_source_19 = BINARY_OPERATION( PyNumber_Or, tmp_left_name_7, tmp_right_name_7 );
    if ( tmp_assign_source_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 228;
        goto try_except_handler_4;
    }
    {
        PyObject *old = var_maskitem;
        assert( old != NULL );
        var_maskitem = tmp_assign_source_19;
        Py_DECREF( old );
    }

    tmp_left_name_8 = var_fillitem;

    tmp_right_name_8 = const_int_pos_1;
    tmp_assign_source_20 = BINARY_OPERATION( PyNumber_Or, tmp_left_name_8, tmp_right_name_8 );
    if ( tmp_assign_source_20 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 229;
        goto try_except_handler_4;
    }
    {
        PyObject *old = var_fillitem;
        assert( old != NULL );
        var_fillitem = tmp_assign_source_20;
        Py_DECREF( old );
    }

    goto branch_end_3;
    branch_no_3:;
    tmp_compare_left_3 = var_c;

    tmp_compare_right_3 = par_white;

    tmp_cmp_Eq_2 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_3, tmp_compare_right_3 );
    if ( tmp_cmp_Eq_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 230;
        goto try_except_handler_4;
    }
    if ( tmp_cmp_Eq_2 == 1 )
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_left_name_9 = var_maskitem;

    tmp_right_name_9 = const_int_pos_1;
    tmp_assign_source_21 = BINARY_OPERATION( PyNumber_Or, tmp_left_name_9, tmp_right_name_9 );
    if ( tmp_assign_source_21 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 231;
        goto try_except_handler_4;
    }
    {
        PyObject *old = var_maskitem;
        assert( old != NULL );
        var_maskitem = tmp_assign_source_21;
        Py_DECREF( old );
    }

    goto branch_end_4;
    branch_no_4:;
    tmp_compare_left_4 = var_c;

    tmp_compare_right_4 = par_xor;

    tmp_cmp_Eq_3 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_4, tmp_compare_right_4 );
    if ( tmp_cmp_Eq_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 232;
        goto try_except_handler_4;
    }
    if ( tmp_cmp_Eq_3 == 1 )
    {
        goto branch_yes_5;
    }
    else
    {
        goto branch_no_5;
    }
    branch_yes_5:;
    tmp_left_name_10 = var_fillitem;

    tmp_right_name_10 = const_int_pos_1;
    tmp_assign_source_22 = BINARY_OPERATION( PyNumber_Or, tmp_left_name_10, tmp_right_name_10 );
    if ( tmp_assign_source_22 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 233;
        goto try_except_handler_4;
    }
    {
        PyObject *old = var_fillitem;
        assert( old != NULL );
        var_fillitem = tmp_assign_source_22;
        Py_DECREF( old );
    }

    branch_no_5:;
    branch_end_4:;
    branch_end_3:;
    tmp_cond_value_2 = var_step;

    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 234;
        goto try_except_handler_4;
    }
    if ( tmp_cond_truth_2 == 1 )
    {
        goto branch_no_6;
    }
    else
    {
        goto branch_yes_6;
    }
    branch_yes_6:;
    tmp_source_name_1 = var_maskdata;

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 235;
        goto try_except_handler_4;
    }
    tmp_args_element_name_1 = var_maskitem;

    if ( tmp_args_element_name_1 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "local variable '%s' referenced before assignment", "maskitem" );
        exception_tb = NULL;

        exception_lineno = 235;
        goto try_except_handler_4;
    }

    frame_function->f_lineno = 235;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_called_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 235;
        goto try_except_handler_4;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_2 = var_filldata;

    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_append );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 236;
        goto try_except_handler_4;
    }
    tmp_args_element_name_2 = var_fillitem;

    if ( tmp_args_element_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "local variable '%s' referenced before assignment", "fillitem" );
        exception_tb = NULL;

        exception_lineno = 236;
        goto try_except_handler_4;
    }

    frame_function->f_lineno = 236;
    {
        PyObject *call_args[] = { tmp_args_element_name_2 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
    }

    Py_DECREF( tmp_called_name_2 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 236;
        goto try_except_handler_4;
    }
    Py_DECREF( tmp_unused );
    tmp_assign_source_23 = const_int_0;
    {
        PyObject *old = tmp_assign_unpack_2__assign_source;
        tmp_assign_unpack_2__assign_source = tmp_assign_source_23;
        Py_INCREF( tmp_assign_unpack_2__assign_source );
        Py_XDECREF( old );
    }

    tmp_assign_source_24 = const_int_0;
    {
        PyObject *old = var_maskitem;
        var_maskitem = tmp_assign_source_24;
        Py_INCREF( var_maskitem );
        Py_XDECREF( old );
    }

    tmp_assign_source_25 = const_int_0;
    {
        PyObject *old = var_fillitem;
        var_fillitem = tmp_assign_source_25;
        Py_INCREF( var_fillitem );
        Py_XDECREF( old );
    }

    CHECK_OBJECT( (PyObject *)tmp_assign_unpack_2__assign_source );
    Py_DECREF( tmp_assign_unpack_2__assign_source );
    tmp_assign_unpack_2__assign_source = NULL;

    tmp_assign_source_26 = const_int_pos_8;
    {
        PyObject *old = var_step;
        assert( old != NULL );
        var_step = tmp_assign_source_26;
        Py_INCREF( var_step );
        Py_DECREF( old );
    }

    branch_no_6:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 223;
        goto try_except_handler_4;
    }
    goto loop_start_3;
    loop_end_3:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( tmp_for_loop_3__iter_value );
    tmp_for_loop_3__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
    Py_DECREF( tmp_for_loop_3__for_iterator );
    tmp_for_loop_3__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_3;
    // End of try:
    try_end_2:;
    Py_XDECREF( tmp_for_loop_3__iter_value );
    tmp_for_loop_3__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
    Py_DECREF( tmp_for_loop_3__for_iterator );
    tmp_for_loop_3__for_iterator = NULL;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 222;
        goto try_except_handler_3;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    tmp_return_value = PyTuple_New( 2 );
    tmp_tuple_arg_1 = var_filldata;

    tmp_tuple_element_2 = PySequence_Tuple( tmp_tuple_arg_1 );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );

        exception_lineno = 239;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_2 );
    tmp_tuple_arg_2 = var_maskdata;

    tmp_tuple_element_2 = PySequence_Tuple( tmp_tuple_arg_2 );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );

        exception_lineno = 239;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_2 );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto try_return_handler_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();
            if ( par_strings )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_strings,
                    par_strings
                );

                assert( res == 0 );
            }

            if ( par_black )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_black,
                    par_black
                );

                assert( res == 0 );
            }

            if ( par_white )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_white,
                    par_white
                );

                assert( res == 0 );
            }

            if ( par_xor )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_xor,
                    par_xor
                );

                assert( res == 0 );
            }

            if ( var_size )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_size,
                    var_size
                );

                assert( res == 0 );
            }

            if ( var_s )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_s,
                    var_s
                );

                assert( res == 0 );
            }

            if ( var_maskdata )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_maskdata,
                    var_maskdata
                );

                assert( res == 0 );
            }

            if ( var_filldata )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_filldata,
                    var_filldata
                );

                assert( res == 0 );
            }

            if ( var_maskitem )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_maskitem,
                    var_maskitem
                );

                assert( res == 0 );
            }

            if ( var_fillitem )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_fillitem,
                    var_fillitem
                );

                assert( res == 0 );
            }

            if ( var_step )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_step,
                    var_step
                );

                assert( res == 0 );
            }

            if ( var_c )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_c,
                    var_c
                );

                assert( res == 0 );
            }



            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$cursors$$$function_1_compile );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_strings );
    Py_DECREF( par_strings );
    par_strings = NULL;

    CHECK_OBJECT( (PyObject *)par_black );
    Py_DECREF( par_black );
    par_black = NULL;

    CHECK_OBJECT( (PyObject *)par_white );
    Py_DECREF( par_white );
    par_white = NULL;

    CHECK_OBJECT( (PyObject *)par_xor );
    Py_DECREF( par_xor );
    par_xor = NULL;

    CHECK_OBJECT( (PyObject *)var_size );
    Py_DECREF( var_size );
    var_size = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    CHECK_OBJECT( (PyObject *)var_maskdata );
    Py_DECREF( var_maskdata );
    var_maskdata = NULL;

    CHECK_OBJECT( (PyObject *)var_filldata );
    Py_DECREF( var_filldata );
    var_filldata = NULL;

    Py_XDECREF( var_maskitem );
    var_maskitem = NULL;

    Py_XDECREF( var_fillitem );
    var_fillitem = NULL;

    Py_XDECREF( var_step );
    var_step = NULL;

    Py_XDECREF( var_c );
    var_c = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    CHECK_OBJECT( (PyObject *)par_strings );
    Py_DECREF( par_strings );
    par_strings = NULL;

    CHECK_OBJECT( (PyObject *)par_black );
    Py_DECREF( par_black );
    par_black = NULL;

    CHECK_OBJECT( (PyObject *)par_white );
    Py_DECREF( par_white );
    par_white = NULL;

    CHECK_OBJECT( (PyObject *)par_xor );
    Py_DECREF( par_xor );
    par_xor = NULL;

    Py_XDECREF( var_size );
    var_size = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    Py_XDECREF( var_maskdata );
    var_maskdata = NULL;

    Py_XDECREF( var_filldata );
    var_filldata = NULL;

    Py_XDECREF( var_maskitem );
    var_maskitem = NULL;

    Py_XDECREF( var_fillitem );
    var_fillitem = NULL;

    Py_XDECREF( var_step );
    var_step = NULL;

    Py_XDECREF( var_c );
    var_c = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$cursors$$$function_1_compile );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_pygame$cursors$$$function_2_load_xbm( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_curs = python_pars[ 0 ];
    PyObject *par_mask = python_pars[ 1 ];
    PyObject *var_bitswap = NULL;
    PyObject *var_line = NULL;
    PyObject *var_width = NULL;
    PyObject *var_height = NULL;
    PyObject *var_hotx = NULL;
    PyObject *var_hoty = NULL;
    PyObject *var_info = NULL;
    PyObject *var_data = NULL;
    PyObject *var_cursdata = NULL;
    PyObject *var_x = NULL;
    PyObject *var_maskdata = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_for_loop_3__for_iterator = NULL;
    PyObject *tmp_for_loop_3__iter_value = NULL;
    PyObject *tmp_for_loop_4__for_iterator = NULL;
    PyObject *tmp_for_loop_4__iter_value = NULL;
    PyObject *tmp_for_loop_5__for_iterator = NULL;
    PyObject *tmp_for_loop_5__iter_value = NULL;
    PyObject *tmp_for_loop_6__for_iterator = NULL;
    PyObject *tmp_for_loop_6__iter_value = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_element_name_6;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    PyObject *tmp_assign_source_28;
    PyObject *tmp_assign_source_29;
    PyObject *tmp_assign_source_30;
    PyObject *tmp_assign_source_31;
    PyObject *tmp_assign_source_32;
    PyObject *tmp_assign_source_33;
    PyObject *tmp_assign_source_34;
    PyObject *tmp_assign_source_35;
    PyObject *tmp_assign_source_36;
    PyObject *tmp_assign_source_37;
    PyObject *tmp_assign_source_38;
    PyObject *tmp_base_name_1;
    PyObject *tmp_base_name_2;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_instance_2;
    PyObject *tmp_called_instance_3;
    PyObject *tmp_called_instance_4;
    PyObject *tmp_called_instance_5;
    PyObject *tmp_called_instance_6;
    PyObject *tmp_called_instance_7;
    PyObject *tmp_called_instance_8;
    PyObject *tmp_called_instance_9;
    PyObject *tmp_called_instance_10;
    PyObject *tmp_called_instance_11;
    PyObject *tmp_called_instance_12;
    PyObject *tmp_called_instance_13;
    PyObject *tmp_called_instance_14;
    PyObject *tmp_called_instance_15;
    PyObject *tmp_called_instance_16;
    PyObject *tmp_called_instance_17;
    PyObject *tmp_called_instance_18;
    PyObject *tmp_called_instance_19;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_called_name_5;
    PyObject *tmp_called_name_6;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    int tmp_cond_truth_3;
    int tmp_cond_truth_4;
    int tmp_cond_truth_5;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_cond_value_3;
    PyObject *tmp_cond_value_4;
    PyObject *tmp_cond_value_5;
    PyObject *tmp_frame_locals;
    PyObject *tmp_int_arg_1;
    PyObject *tmp_int_arg_2;
    PyObject *tmp_int_arg_3;
    PyObject *tmp_int_arg_4;
    bool tmp_is_1;
    bool tmp_is_2;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iter_arg_2;
    PyObject *tmp_iter_arg_3;
    PyObject *tmp_iter_arg_4;
    PyObject *tmp_iter_arg_5;
    PyObject *tmp_iter_arg_6;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_len_arg_1;
    PyObject *tmp_len_arg_2;
    PyObject *tmp_len_arg_3;
    PyObject *tmp_len_arg_4;
    PyObject *tmp_next_source_1;
    PyObject *tmp_next_source_2;
    PyObject *tmp_next_source_3;
    PyObject *tmp_next_source_4;
    PyObject *tmp_next_source_5;
    PyObject *tmp_next_source_6;
    PyObject *tmp_open_filename_1;
    PyObject *tmp_open_filename_2;
    int tmp_or_left_truth_1;
    int tmp_or_left_truth_2;
    PyObject *tmp_or_left_value_1;
    PyObject *tmp_or_left_value_2;
    PyObject *tmp_or_right_value_1;
    PyObject *tmp_or_right_value_2;
    PyObject *tmp_range_arg_1;
    PyObject *tmp_range_arg_2;
    PyObject *tmp_range_arg_3;
    PyObject *tmp_range_arg_4;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    Py_ssize_t tmp_slice_index_upper_1;
    Py_ssize_t tmp_slice_index_upper_2;
    PyObject *tmp_slice_lower_1;
    PyObject *tmp_slice_lower_2;
    PyObject *tmp_slice_lower_3;
    PyObject *tmp_slice_lower_4;
    PyObject *tmp_slice_source_1;
    PyObject *tmp_slice_source_2;
    PyObject *tmp_slice_source_3;
    PyObject *tmp_slice_source_4;
    PyObject *tmp_slice_source_5;
    PyObject *tmp_slice_source_6;
    Py_ssize_t tmp_sliceslicedel_index_lower_1;
    Py_ssize_t tmp_sliceslicedel_index_lower_2;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscribed_name_2;
    PyObject *tmp_subscribed_name_3;
    PyObject *tmp_subscribed_name_4;
    PyObject *tmp_subscribed_name_5;
    PyObject *tmp_subscribed_name_6;
    PyObject *tmp_subscribed_name_7;
    PyObject *tmp_subscribed_name_8;
    PyObject *tmp_subscribed_name_9;
    PyObject *tmp_subscribed_name_10;
    PyObject *tmp_subscribed_name_11;
    PyObject *tmp_subscribed_name_12;
    PyObject *tmp_subscribed_name_13;
    PyObject *tmp_subscribed_name_14;
    PyObject *tmp_subscribed_name_15;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_subscript_name_2;
    PyObject *tmp_subscript_name_3;
    PyObject *tmp_subscript_name_4;
    PyObject *tmp_subscript_name_5;
    PyObject *tmp_subscript_name_6;
    PyObject *tmp_subscript_name_7;
    PyObject *tmp_subscript_name_8;
    PyObject *tmp_subscript_name_9;
    PyObject *tmp_subscript_name_10;
    PyObject *tmp_subscript_name_11;
    PyObject *tmp_subscript_name_12;
    PyObject *tmp_subscript_name_13;
    PyObject *tmp_subscript_name_14;
    PyObject *tmp_subscript_name_15;
    PyObject *tmp_tuple_arg_1;
    PyObject *tmp_tuple_arg_2;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    PyObject *tmp_type_arg_1;
    PyObject *tmp_type_arg_2;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    PyObject *tmp_value_name_1;
    PyObject *tmp_value_name_2;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = MAKE_FUNCTION_pygame$cursors$$$function_2_load_xbm$$$function_1_bitswap(  );
    assert( var_bitswap == NULL );
    var_bitswap = tmp_assign_source_1;

    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_5913fbe0aabe7522b79a941a96a7d4e9, module_pygame$cursors );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_type_arg_1 = par_curs;

    tmp_compare_left_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
    assert( tmp_compare_left_1 != NULL );
    tmp_compare_right_1 = LOOKUP_BUILTIN( const_str_plain_str );
    assert( tmp_compare_right_1 != NULL );
    tmp_is_1 = ( tmp_compare_left_1 == tmp_compare_right_1 );
    Py_DECREF( tmp_compare_left_1 );
    if ( tmp_is_1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_open_filename_1 = par_curs;

    tmp_assign_source_2 = BUILTIN_OPEN( tmp_open_filename_1, NULL, NULL );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 258;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = par_curs;
        assert( old != NULL );
        par_curs = tmp_assign_source_2;
        Py_DECREF( old );
    }

    branch_no_1:;
    tmp_type_arg_2 = par_mask;

    tmp_compare_left_2 = BUILTIN_TYPE1( tmp_type_arg_2 );
    assert( tmp_compare_left_2 != NULL );
    tmp_compare_right_2 = LOOKUP_BUILTIN( const_str_plain_str );
    assert( tmp_compare_right_2 != NULL );
    tmp_is_2 = ( tmp_compare_left_2 == tmp_compare_right_2 );
    Py_DECREF( tmp_compare_left_2 );
    if ( tmp_is_2 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_open_filename_2 = par_mask;

    tmp_assign_source_3 = BUILTIN_OPEN( tmp_open_filename_2, NULL, NULL );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 259;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = par_mask;
        assert( old != NULL );
        par_mask = tmp_assign_source_3;
        Py_DECREF( old );
    }

    branch_no_2:;
    tmp_called_instance_1 = par_curs;

    frame_function->f_lineno = 260;
    tmp_assign_source_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_readlines );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 260;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = par_curs;
        assert( old != NULL );
        par_curs = tmp_assign_source_4;
        Py_DECREF( old );
    }

    tmp_called_instance_2 = par_mask;

    frame_function->f_lineno = 261;
    tmp_assign_source_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_readlines );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 261;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = par_mask;
        assert( old != NULL );
        par_mask = tmp_assign_source_5;
        Py_DECREF( old );
    }

    tmp_len_arg_1 = par_curs;

    tmp_range_arg_1 = BUILTIN_LEN( tmp_len_arg_1 );
    if ( tmp_range_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 263;
        goto frame_exception_exit_1;
    }
    tmp_iter_arg_1 = BUILTIN_RANGE( tmp_range_arg_1 );
    Py_DECREF( tmp_range_arg_1 );
    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 263;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 263;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_6;

    // Tried code:
    loop_start_1:;
    tmp_next_source_1 = tmp_for_loop_1__for_iterator;

    tmp_assign_source_7 = ITERATOR_NEXT( tmp_next_source_1 );
    if ( tmp_assign_source_7 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_1;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 263;
            goto try_except_handler_2;
        }
    }

    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_7;
        Py_XDECREF( old );
    }

    tmp_assign_source_8 = tmp_for_loop_1__iter_value;

    {
        PyObject *old = var_line;
        var_line = tmp_assign_source_8;
        Py_INCREF( var_line );
        Py_XDECREF( old );
    }

    tmp_subscribed_name_1 = par_curs;

    tmp_subscript_name_1 = var_line;

    tmp_called_instance_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
    if ( tmp_called_instance_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 264;
        goto try_except_handler_2;
    }
    frame_function->f_lineno = 264;
    tmp_cond_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_7336a9dd343cfb1bed1a3fce43bb0bc3_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_3 );
    if ( tmp_cond_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 264;
        goto try_except_handler_2;
    }
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        exception_lineno = 264;
        goto try_except_handler_2;
    }
    Py_DECREF( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_slice_source_1 = par_curs;

    tmp_slice_lower_1 = var_line;

    tmp_assign_source_9 = LOOKUP_SLICE( tmp_slice_source_1, tmp_slice_lower_1, Py_None );
    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 265;
        goto try_except_handler_2;
    }
    {
        PyObject *old = par_curs;
        assert( old != NULL );
        par_curs = tmp_assign_source_9;
        Py_DECREF( old );
    }

    goto loop_end_1;
    branch_no_3:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 263;
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_len_arg_2 = par_mask;

    tmp_range_arg_2 = BUILTIN_LEN( tmp_len_arg_2 );
    if ( tmp_range_arg_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 267;
        goto frame_exception_exit_1;
    }
    tmp_iter_arg_2 = BUILTIN_RANGE( tmp_range_arg_2 );
    Py_DECREF( tmp_range_arg_2 );
    if ( tmp_iter_arg_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 267;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_2 );
    Py_DECREF( tmp_iter_arg_2 );
    if ( tmp_assign_source_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 267;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_2__for_iterator == NULL );
    tmp_for_loop_2__for_iterator = tmp_assign_source_10;

    // Tried code:
    loop_start_2:;
    tmp_next_source_2 = tmp_for_loop_2__for_iterator;

    tmp_assign_source_11 = ITERATOR_NEXT( tmp_next_source_2 );
    if ( tmp_assign_source_11 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_2;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 267;
            goto try_except_handler_3;
        }
    }

    {
        PyObject *old = tmp_for_loop_2__iter_value;
        tmp_for_loop_2__iter_value = tmp_assign_source_11;
        Py_XDECREF( old );
    }

    tmp_assign_source_12 = tmp_for_loop_2__iter_value;

    {
        PyObject *old = var_line;
        var_line = tmp_assign_source_12;
        Py_INCREF( var_line );
        Py_XDECREF( old );
    }

    tmp_subscribed_name_2 = par_mask;

    tmp_subscript_name_2 = var_line;

    tmp_called_instance_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
    if ( tmp_called_instance_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 268;
        goto try_except_handler_3;
    }
    frame_function->f_lineno = 268;
    tmp_cond_value_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_7336a9dd343cfb1bed1a3fce43bb0bc3_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_4 );
    if ( tmp_cond_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 268;
        goto try_except_handler_3;
    }
    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_2 );

        exception_lineno = 268;
        goto try_except_handler_3;
    }
    Py_DECREF( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == 1 )
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_slice_source_2 = par_mask;

    tmp_slice_lower_2 = var_line;

    tmp_assign_source_13 = LOOKUP_SLICE( tmp_slice_source_2, tmp_slice_lower_2, Py_None );
    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 269;
        goto try_except_handler_3;
    }
    {
        PyObject *old = par_mask;
        assert( old != NULL );
        par_mask = tmp_assign_source_13;
        Py_DECREF( old );
    }

    goto loop_end_2;
    branch_no_4:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 267;
        goto try_except_handler_3;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    tmp_subscribed_name_4 = par_curs;

    tmp_subscript_name_3 = const_int_0;
    tmp_called_instance_5 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_3 );
    if ( tmp_called_instance_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 272;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 272;
    tmp_subscribed_name_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_split );
    Py_DECREF( tmp_called_instance_5 );
    if ( tmp_subscribed_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 272;
        goto frame_exception_exit_1;
    }
    tmp_subscript_name_4 = const_int_neg_1;
    tmp_int_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_4 );
    Py_DECREF( tmp_subscribed_name_3 );
    if ( tmp_int_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 272;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_14 = PyNumber_Int( tmp_int_arg_1 );
    Py_DECREF( tmp_int_arg_1 );
    if ( tmp_assign_source_14 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 272;
        goto frame_exception_exit_1;
    }
    assert( var_width == NULL );
    var_width = tmp_assign_source_14;

    tmp_subscribed_name_6 = par_curs;

    tmp_subscript_name_5 = const_int_pos_1;
    tmp_called_instance_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_6, tmp_subscript_name_5 );
    if ( tmp_called_instance_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 273;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 273;
    tmp_subscribed_name_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_6, const_str_plain_split );
    Py_DECREF( tmp_called_instance_6 );
    if ( tmp_subscribed_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 273;
        goto frame_exception_exit_1;
    }
    tmp_subscript_name_6 = const_int_neg_1;
    tmp_int_arg_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_6 );
    Py_DECREF( tmp_subscribed_name_5 );
    if ( tmp_int_arg_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 273;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_15 = PyNumber_Int( tmp_int_arg_2 );
    Py_DECREF( tmp_int_arg_2 );
    if ( tmp_assign_source_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 273;
        goto frame_exception_exit_1;
    }
    assert( var_height == NULL );
    var_height = tmp_assign_source_15;

    tmp_subscribed_name_7 = par_curs;

    tmp_subscript_name_7 = const_int_pos_2;
    tmp_called_instance_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_7, tmp_subscript_name_7 );
    if ( tmp_called_instance_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 275;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 275;
    tmp_cond_value_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_7336a9dd343cfb1bed1a3fce43bb0bc3_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_7 );
    if ( tmp_cond_value_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 275;
        goto frame_exception_exit_1;
    }
    tmp_cond_truth_3 = CHECK_IF_TRUE( tmp_cond_value_3 );
    if ( tmp_cond_truth_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_3 );

        exception_lineno = 275;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_3 );
    if ( tmp_cond_truth_3 == 1 )
    {
        goto branch_yes_5;
    }
    else
    {
        goto branch_no_5;
    }
    branch_yes_5:;
    tmp_subscribed_name_9 = par_curs;

    tmp_subscript_name_8 = const_int_pos_2;
    tmp_called_instance_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_9, tmp_subscript_name_8 );
    if ( tmp_called_instance_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 276;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 276;
    tmp_subscribed_name_8 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_split );
    Py_DECREF( tmp_called_instance_8 );
    if ( tmp_subscribed_name_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 276;
        goto frame_exception_exit_1;
    }
    tmp_subscript_name_9 = const_int_neg_1;
    tmp_int_arg_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_8, tmp_subscript_name_9 );
    Py_DECREF( tmp_subscribed_name_8 );
    if ( tmp_int_arg_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 276;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_16 = PyNumber_Int( tmp_int_arg_3 );
    Py_DECREF( tmp_int_arg_3 );
    if ( tmp_assign_source_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 276;
        goto frame_exception_exit_1;
    }
    assert( var_hotx == NULL );
    var_hotx = tmp_assign_source_16;

    tmp_subscribed_name_11 = par_curs;

    tmp_subscript_name_10 = const_int_pos_3;
    tmp_called_instance_9 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_11, tmp_subscript_name_10 );
    if ( tmp_called_instance_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 277;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 277;
    tmp_subscribed_name_10 = CALL_METHOD_NO_ARGS( tmp_called_instance_9, const_str_plain_split );
    Py_DECREF( tmp_called_instance_9 );
    if ( tmp_subscribed_name_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 277;
        goto frame_exception_exit_1;
    }
    tmp_subscript_name_11 = const_int_neg_1;
    tmp_int_arg_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_10, tmp_subscript_name_11 );
    Py_DECREF( tmp_subscribed_name_10 );
    if ( tmp_int_arg_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 277;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_17 = PyNumber_Int( tmp_int_arg_4 );
    Py_DECREF( tmp_int_arg_4 );
    if ( tmp_assign_source_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 277;
        goto frame_exception_exit_1;
    }
    assert( var_hoty == NULL );
    var_hoty = tmp_assign_source_17;

    goto branch_end_5;
    branch_no_5:;
    tmp_assign_source_18 = const_int_0;
    assert( var_hotx == NULL );
    Py_INCREF( tmp_assign_source_18 );
    var_hotx = tmp_assign_source_18;

    tmp_assign_source_19 = const_int_0;
    assert( var_hoty == NULL );
    Py_INCREF( tmp_assign_source_19 );
    var_hoty = tmp_assign_source_19;

    branch_end_5:;
    tmp_assign_source_20 = PyTuple_New( 4 );
    tmp_tuple_element_1 = var_width;

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_assign_source_20, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = var_height;

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_assign_source_20, 1, tmp_tuple_element_1 );
    tmp_tuple_element_1 = var_hotx;

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_assign_source_20, 2, tmp_tuple_element_1 );
    tmp_tuple_element_1 = var_hoty;

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_assign_source_20, 3, tmp_tuple_element_1 );
    assert( var_info == NULL );
    var_info = tmp_assign_source_20;

    tmp_len_arg_3 = par_curs;

    tmp_range_arg_3 = BUILTIN_LEN( tmp_len_arg_3 );
    if ( tmp_range_arg_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 283;
        goto frame_exception_exit_1;
    }
    tmp_iter_arg_3 = BUILTIN_RANGE( tmp_range_arg_3 );
    Py_DECREF( tmp_range_arg_3 );
    if ( tmp_iter_arg_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 283;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_21 = MAKE_ITERATOR( tmp_iter_arg_3 );
    Py_DECREF( tmp_iter_arg_3 );
    if ( tmp_assign_source_21 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 283;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_3__for_iterator == NULL );
    tmp_for_loop_3__for_iterator = tmp_assign_source_21;

    // Tried code:
    loop_start_3:;
    tmp_next_source_3 = tmp_for_loop_3__for_iterator;

    tmp_assign_source_22 = ITERATOR_NEXT( tmp_next_source_3 );
    if ( tmp_assign_source_22 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_3;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 283;
            goto try_except_handler_4;
        }
    }

    {
        PyObject *old = tmp_for_loop_3__iter_value;
        tmp_for_loop_3__iter_value = tmp_assign_source_22;
        Py_XDECREF( old );
    }

    tmp_assign_source_23 = tmp_for_loop_3__iter_value;

    {
        PyObject *old = var_line;
        var_line = tmp_assign_source_23;
        Py_INCREF( var_line );
        Py_XDECREF( old );
    }

    tmp_subscribed_name_12 = par_curs;

    tmp_subscript_name_12 = var_line;

    tmp_called_instance_10 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_12, tmp_subscript_name_12 );
    if ( tmp_called_instance_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 284;
        goto try_except_handler_4;
    }
    frame_function->f_lineno = 284;
    tmp_or_left_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_10, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_b8b5aead6e09884da3f6dd805c61d13b_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_10 );
    if ( tmp_or_left_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 284;
        goto try_except_handler_4;
    }
    tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
    if ( tmp_or_left_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_or_left_value_1 );

        exception_lineno = 284;
        goto try_except_handler_4;
    }
    if ( tmp_or_left_truth_1 == 1 )
    {
        goto or_left_1;
    }
    else
    {
        goto or_right_1;
    }
    or_right_1:;
    Py_DECREF( tmp_or_left_value_1 );
    tmp_subscribed_name_13 = par_curs;

    tmp_subscript_name_13 = var_line;

    tmp_called_instance_11 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_13, tmp_subscript_name_13 );
    if ( tmp_called_instance_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 284;
        goto try_except_handler_4;
    }
    frame_function->f_lineno = 284;
    tmp_or_right_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_11, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_0f5748902608737e1fdb2ef20fee5302_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_11 );
    if ( tmp_or_right_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 284;
        goto try_except_handler_4;
    }
    tmp_cond_value_4 = tmp_or_right_value_1;
    goto or_end_1;
    or_left_1:;
    tmp_cond_value_4 = tmp_or_left_value_1;
    or_end_1:;
    tmp_cond_truth_4 = CHECK_IF_TRUE( tmp_cond_value_4 );
    if ( tmp_cond_truth_4 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_4 );

        exception_lineno = 284;
        goto try_except_handler_4;
    }
    Py_DECREF( tmp_cond_value_4 );
    if ( tmp_cond_truth_4 == 1 )
    {
        goto branch_yes_6;
    }
    else
    {
        goto branch_no_6;
    }
    branch_yes_6:;
    goto loop_end_3;
    branch_no_6:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 283;
        goto try_except_handler_4;
    }
    goto loop_start_3;
    loop_end_3:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( tmp_for_loop_3__iter_value );
    tmp_for_loop_3__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
    Py_DECREF( tmp_for_loop_3__for_iterator );
    tmp_for_loop_3__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_3__iter_value );
    tmp_for_loop_3__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
    Py_DECREF( tmp_for_loop_3__for_iterator );
    tmp_for_loop_3__for_iterator = NULL;

    tmp_source_name_1 = const_str_space;
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_join );
    assert( tmp_called_name_1 != NULL );
    tmp_slice_source_3 = par_curs;

    tmp_left_name_1 = var_line;

    if ( tmp_left_name_1 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "local variable '%s' referenced before assignment", "line" );
        exception_tb = NULL;

        exception_lineno = 286;
        goto frame_exception_exit_1;
    }

    tmp_right_name_1 = const_int_pos_1;
    tmp_slice_lower_3 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_1 );
    if ( tmp_slice_lower_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );

        exception_lineno = 286;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_1 = LOOKUP_SLICE( tmp_slice_source_3, tmp_slice_lower_3, Py_None );
    Py_DECREF( tmp_slice_lower_3 );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );

        exception_lineno = 286;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 286;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_called_instance_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_element_name_1 );
    if ( tmp_called_instance_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 286;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 286;
    tmp_called_instance_12 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_13, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_digest_c6ea53ac2684e039b757c7a331688c7d_str_empty_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_13 );
    if ( tmp_called_instance_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 286;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 286;
    tmp_assign_source_24 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_12, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_chr_44_str_space_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_12 );
    if ( tmp_assign_source_24 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 286;
        goto frame_exception_exit_1;
    }
    assert( var_data == NULL );
    var_data = tmp_assign_source_24;

    tmp_assign_source_25 = PyList_New( 0 );
    assert( var_cursdata == NULL );
    var_cursdata = tmp_assign_source_25;

    tmp_called_instance_14 = var_data;

    frame_function->f_lineno = 288;
    tmp_iter_arg_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_14, const_str_plain_split );
    if ( tmp_iter_arg_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 288;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_26 = MAKE_ITERATOR( tmp_iter_arg_4 );
    Py_DECREF( tmp_iter_arg_4 );
    if ( tmp_assign_source_26 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 288;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_4__for_iterator == NULL );
    tmp_for_loop_4__for_iterator = tmp_assign_source_26;

    // Tried code:
    loop_start_4:;
    tmp_next_source_4 = tmp_for_loop_4__for_iterator;

    tmp_assign_source_27 = ITERATOR_NEXT( tmp_next_source_4 );
    if ( tmp_assign_source_27 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_4;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 288;
            goto try_except_handler_5;
        }
    }

    {
        PyObject *old = tmp_for_loop_4__iter_value;
        tmp_for_loop_4__iter_value = tmp_assign_source_27;
        Py_XDECREF( old );
    }

    tmp_assign_source_28 = tmp_for_loop_4__iter_value;

    {
        PyObject *old = var_x;
        var_x = tmp_assign_source_28;
        Py_INCREF( var_x );
        Py_XDECREF( old );
    }

    tmp_source_name_2 = var_cursdata;

    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_append );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 289;
        goto try_except_handler_5;
    }
    tmp_called_name_3 = var_bitswap;

    tmp_value_name_1 = var_x;

    tmp_base_name_1 = const_int_pos_16;
    tmp_args_element_name_3 = TO_INT2( tmp_value_name_1, tmp_base_name_1 );
    if ( tmp_args_element_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_2 );

        exception_lineno = 289;
        goto try_except_handler_5;
    }
    frame_function->f_lineno = 289;
    {
        PyObject *call_args[] = { tmp_args_element_name_3 };
        tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
    }

    Py_DECREF( tmp_args_element_name_3 );
    if ( tmp_args_element_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_2 );

        exception_lineno = 289;
        goto try_except_handler_5;
    }
    frame_function->f_lineno = 289;
    {
        PyObject *call_args[] = { tmp_args_element_name_2 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
    }

    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_args_element_name_2 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 289;
        goto try_except_handler_5;
    }
    Py_DECREF( tmp_unused );
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 288;
        goto try_except_handler_5;
    }
    goto loop_start_4;
    loop_end_4:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( tmp_for_loop_4__iter_value );
    tmp_for_loop_4__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_4__for_iterator );
    Py_DECREF( tmp_for_loop_4__for_iterator );
    tmp_for_loop_4__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    Py_XDECREF( tmp_for_loop_4__iter_value );
    tmp_for_loop_4__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_4__for_iterator );
    Py_DECREF( tmp_for_loop_4__for_iterator );
    tmp_for_loop_4__for_iterator = NULL;

    tmp_tuple_arg_1 = var_cursdata;

    tmp_assign_source_29 = PySequence_Tuple( tmp_tuple_arg_1 );
    if ( tmp_assign_source_29 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 290;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_cursdata;
        assert( old != NULL );
        var_cursdata = tmp_assign_source_29;
        Py_DECREF( old );
    }

    tmp_len_arg_4 = par_mask;

    tmp_range_arg_4 = BUILTIN_LEN( tmp_len_arg_4 );
    if ( tmp_range_arg_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 292;
        goto frame_exception_exit_1;
    }
    tmp_iter_arg_5 = BUILTIN_RANGE( tmp_range_arg_4 );
    Py_DECREF( tmp_range_arg_4 );
    if ( tmp_iter_arg_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 292;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_30 = MAKE_ITERATOR( tmp_iter_arg_5 );
    Py_DECREF( tmp_iter_arg_5 );
    if ( tmp_assign_source_30 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 292;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_5__for_iterator == NULL );
    tmp_for_loop_5__for_iterator = tmp_assign_source_30;

    // Tried code:
    loop_start_5:;
    tmp_next_source_5 = tmp_for_loop_5__for_iterator;

    tmp_assign_source_31 = ITERATOR_NEXT( tmp_next_source_5 );
    if ( tmp_assign_source_31 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_5;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 292;
            goto try_except_handler_6;
        }
    }

    {
        PyObject *old = tmp_for_loop_5__iter_value;
        tmp_for_loop_5__iter_value = tmp_assign_source_31;
        Py_XDECREF( old );
    }

    tmp_assign_source_32 = tmp_for_loop_5__iter_value;

    {
        PyObject *old = var_line;
        var_line = tmp_assign_source_32;
        Py_INCREF( var_line );
        Py_XDECREF( old );
    }

    tmp_subscribed_name_14 = par_mask;

    tmp_subscript_name_14 = var_line;

    tmp_called_instance_15 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_14, tmp_subscript_name_14 );
    if ( tmp_called_instance_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 293;
        goto try_except_handler_6;
    }
    frame_function->f_lineno = 293;
    tmp_or_left_value_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_15, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_b8b5aead6e09884da3f6dd805c61d13b_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_15 );
    if ( tmp_or_left_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 293;
        goto try_except_handler_6;
    }
    tmp_or_left_truth_2 = CHECK_IF_TRUE( tmp_or_left_value_2 );
    if ( tmp_or_left_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_or_left_value_2 );

        exception_lineno = 293;
        goto try_except_handler_6;
    }
    if ( tmp_or_left_truth_2 == 1 )
    {
        goto or_left_2;
    }
    else
    {
        goto or_right_2;
    }
    or_right_2:;
    Py_DECREF( tmp_or_left_value_2 );
    tmp_subscribed_name_15 = par_mask;

    tmp_subscript_name_15 = var_line;

    tmp_called_instance_16 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_15, tmp_subscript_name_15 );
    if ( tmp_called_instance_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 293;
        goto try_except_handler_6;
    }
    frame_function->f_lineno = 293;
    tmp_or_right_value_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_16, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_0f5748902608737e1fdb2ef20fee5302_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_16 );
    if ( tmp_or_right_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 293;
        goto try_except_handler_6;
    }
    tmp_cond_value_5 = tmp_or_right_value_2;
    goto or_end_2;
    or_left_2:;
    tmp_cond_value_5 = tmp_or_left_value_2;
    or_end_2:;
    tmp_cond_truth_5 = CHECK_IF_TRUE( tmp_cond_value_5 );
    if ( tmp_cond_truth_5 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_5 );

        exception_lineno = 293;
        goto try_except_handler_6;
    }
    Py_DECREF( tmp_cond_value_5 );
    if ( tmp_cond_truth_5 == 1 )
    {
        goto branch_yes_7;
    }
    else
    {
        goto branch_no_7;
    }
    branch_yes_7:;
    goto loop_end_5;
    branch_no_7:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 292;
        goto try_except_handler_6;
    }
    goto loop_start_5;
    loop_end_5:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( tmp_for_loop_5__iter_value );
    tmp_for_loop_5__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_5__for_iterator );
    Py_DECREF( tmp_for_loop_5__for_iterator );
    tmp_for_loop_5__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    Py_XDECREF( tmp_for_loop_5__iter_value );
    tmp_for_loop_5__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_5__for_iterator );
    Py_DECREF( tmp_for_loop_5__for_iterator );
    tmp_for_loop_5__for_iterator = NULL;

    tmp_source_name_3 = const_str_space;
    tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_join );
    assert( tmp_called_name_4 != NULL );
    tmp_slice_source_4 = par_mask;

    tmp_left_name_2 = var_line;

    if ( tmp_left_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_4 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "local variable '%s' referenced before assignment", "line" );
        exception_tb = NULL;

        exception_lineno = 295;
        goto frame_exception_exit_1;
    }

    tmp_right_name_2 = const_int_pos_1;
    tmp_slice_lower_4 = BINARY_OPERATION_ADD( tmp_left_name_2, tmp_right_name_2 );
    if ( tmp_slice_lower_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_4 );

        exception_lineno = 295;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_4 = LOOKUP_SLICE( tmp_slice_source_4, tmp_slice_lower_4, Py_None );
    Py_DECREF( tmp_slice_lower_4 );
    if ( tmp_args_element_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_4 );

        exception_lineno = 295;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 295;
    {
        PyObject *call_args[] = { tmp_args_element_name_4 };
        tmp_called_instance_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
    }

    Py_DECREF( tmp_called_name_4 );
    Py_DECREF( tmp_args_element_name_4 );
    if ( tmp_called_instance_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 295;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 295;
    tmp_called_instance_17 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_18, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_digest_c6ea53ac2684e039b757c7a331688c7d_str_empty_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_18 );
    if ( tmp_called_instance_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 295;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 295;
    tmp_assign_source_33 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_17, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_chr_44_str_space_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_17 );
    if ( tmp_assign_source_33 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 295;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_data;
        assert( old != NULL );
        var_data = tmp_assign_source_33;
        Py_DECREF( old );
    }

    tmp_assign_source_34 = PyList_New( 0 );
    assert( var_maskdata == NULL );
    var_maskdata = tmp_assign_source_34;

    tmp_called_instance_19 = var_data;

    frame_function->f_lineno = 297;
    tmp_iter_arg_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_19, const_str_plain_split );
    if ( tmp_iter_arg_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 297;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_35 = MAKE_ITERATOR( tmp_iter_arg_6 );
    Py_DECREF( tmp_iter_arg_6 );
    if ( tmp_assign_source_35 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 297;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_6__for_iterator == NULL );
    tmp_for_loop_6__for_iterator = tmp_assign_source_35;

    // Tried code:
    loop_start_6:;
    tmp_next_source_6 = tmp_for_loop_6__for_iterator;

    tmp_assign_source_36 = ITERATOR_NEXT( tmp_next_source_6 );
    if ( tmp_assign_source_36 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_6;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 297;
            goto try_except_handler_7;
        }
    }

    {
        PyObject *old = tmp_for_loop_6__iter_value;
        tmp_for_loop_6__iter_value = tmp_assign_source_36;
        Py_XDECREF( old );
    }

    tmp_assign_source_37 = tmp_for_loop_6__iter_value;

    {
        PyObject *old = var_x;
        var_x = tmp_assign_source_37;
        Py_INCREF( var_x );
        Py_XDECREF( old );
    }

    tmp_source_name_4 = var_maskdata;

    tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_append );
    if ( tmp_called_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 298;
        goto try_except_handler_7;
    }
    tmp_called_name_6 = var_bitswap;

    tmp_value_name_2 = var_x;

    tmp_base_name_2 = const_int_pos_16;
    tmp_args_element_name_6 = TO_INT2( tmp_value_name_2, tmp_base_name_2 );
    if ( tmp_args_element_name_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_5 );

        exception_lineno = 298;
        goto try_except_handler_7;
    }
    frame_function->f_lineno = 298;
    {
        PyObject *call_args[] = { tmp_args_element_name_6 };
        tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
    }

    Py_DECREF( tmp_args_element_name_6 );
    if ( tmp_args_element_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_5 );

        exception_lineno = 298;
        goto try_except_handler_7;
    }
    frame_function->f_lineno = 298;
    {
        PyObject *call_args[] = { tmp_args_element_name_5 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
    }

    Py_DECREF( tmp_called_name_5 );
    Py_DECREF( tmp_args_element_name_5 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 298;
        goto try_except_handler_7;
    }
    Py_DECREF( tmp_unused );
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 297;
        goto try_except_handler_7;
    }
    goto loop_start_6;
    loop_end_6:;
    goto try_end_6;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( tmp_for_loop_6__iter_value );
    tmp_for_loop_6__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_6__for_iterator );
    Py_DECREF( tmp_for_loop_6__for_iterator );
    tmp_for_loop_6__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    Py_XDECREF( tmp_for_loop_6__iter_value );
    tmp_for_loop_6__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_6__for_iterator );
    Py_DECREF( tmp_for_loop_6__for_iterator );
    tmp_for_loop_6__for_iterator = NULL;

    tmp_tuple_arg_2 = var_maskdata;

    tmp_assign_source_38 = PySequence_Tuple( tmp_tuple_arg_2 );
    if ( tmp_assign_source_38 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 299;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_maskdata;
        assert( old != NULL );
        var_maskdata = tmp_assign_source_38;
        Py_DECREF( old );
    }

    tmp_return_value = PyTuple_New( 4 );
    tmp_sliceslicedel_index_lower_1 = 0;
    tmp_slice_index_upper_1 = 2;
    tmp_slice_source_5 = var_info;

    tmp_tuple_element_2 = LOOKUP_INDEX_SLICE( tmp_slice_source_5, tmp_sliceslicedel_index_lower_1, tmp_slice_index_upper_1 );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );

        exception_lineno = 300;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_2 );
    tmp_sliceslicedel_index_lower_2 = 2;
    tmp_slice_index_upper_2 = PY_SSIZE_T_MAX;
    tmp_slice_source_6 = var_info;

    tmp_tuple_element_2 = LOOKUP_INDEX_SLICE( tmp_slice_source_6, tmp_sliceslicedel_index_lower_2, tmp_slice_index_upper_2 );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );

        exception_lineno = 300;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_2 );
    tmp_tuple_element_2 = var_cursdata;

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_return_value, 2, tmp_tuple_element_2 );
    tmp_tuple_element_2 = var_maskdata;

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_return_value, 3, tmp_tuple_element_2 );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto try_return_handler_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();
            if ( par_curs )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_curs,
                    par_curs
                );

                assert( res == 0 );
            }

            if ( par_mask )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_mask,
                    par_mask
                );

                assert( res == 0 );
            }

            if ( var_bitswap )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_bitswap,
                    var_bitswap
                );

                assert( res == 0 );
            }

            if ( var_line )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_line,
                    var_line
                );

                assert( res == 0 );
            }

            if ( var_width )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_width,
                    var_width
                );

                assert( res == 0 );
            }

            if ( var_height )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_height,
                    var_height
                );

                assert( res == 0 );
            }

            if ( var_hotx )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_hotx,
                    var_hotx
                );

                assert( res == 0 );
            }

            if ( var_hoty )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_hoty,
                    var_hoty
                );

                assert( res == 0 );
            }

            if ( var_info )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_info,
                    var_info
                );

                assert( res == 0 );
            }

            if ( var_data )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_data,
                    var_data
                );

                assert( res == 0 );
            }

            if ( var_cursdata )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_cursdata,
                    var_cursdata
                );

                assert( res == 0 );
            }

            if ( var_x )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_x,
                    var_x
                );

                assert( res == 0 );
            }

            if ( var_maskdata )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_maskdata,
                    var_maskdata
                );

                assert( res == 0 );
            }



            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$cursors$$$function_2_load_xbm );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_curs );
    Py_DECREF( par_curs );
    par_curs = NULL;

    CHECK_OBJECT( (PyObject *)par_mask );
    Py_DECREF( par_mask );
    par_mask = NULL;

    CHECK_OBJECT( (PyObject *)var_bitswap );
    Py_DECREF( var_bitswap );
    var_bitswap = NULL;

    Py_XDECREF( var_line );
    var_line = NULL;

    CHECK_OBJECT( (PyObject *)var_width );
    Py_DECREF( var_width );
    var_width = NULL;

    CHECK_OBJECT( (PyObject *)var_height );
    Py_DECREF( var_height );
    var_height = NULL;

    CHECK_OBJECT( (PyObject *)var_hotx );
    Py_DECREF( var_hotx );
    var_hotx = NULL;

    CHECK_OBJECT( (PyObject *)var_hoty );
    Py_DECREF( var_hoty );
    var_hoty = NULL;

    CHECK_OBJECT( (PyObject *)var_info );
    Py_DECREF( var_info );
    var_info = NULL;

    CHECK_OBJECT( (PyObject *)var_data );
    Py_DECREF( var_data );
    var_data = NULL;

    CHECK_OBJECT( (PyObject *)var_cursdata );
    Py_DECREF( var_cursdata );
    var_cursdata = NULL;

    Py_XDECREF( var_x );
    var_x = NULL;

    CHECK_OBJECT( (PyObject *)var_maskdata );
    Py_DECREF( var_maskdata );
    var_maskdata = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( par_curs );
    par_curs = NULL;

    Py_XDECREF( par_mask );
    par_mask = NULL;

    CHECK_OBJECT( (PyObject *)var_bitswap );
    Py_DECREF( var_bitswap );
    var_bitswap = NULL;

    Py_XDECREF( var_line );
    var_line = NULL;

    Py_XDECREF( var_width );
    var_width = NULL;

    Py_XDECREF( var_height );
    var_height = NULL;

    Py_XDECREF( var_hotx );
    var_hotx = NULL;

    Py_XDECREF( var_hoty );
    var_hoty = NULL;

    Py_XDECREF( var_info );
    var_info = NULL;

    Py_XDECREF( var_data );
    var_data = NULL;

    Py_XDECREF( var_cursdata );
    var_cursdata = NULL;

    Py_XDECREF( var_x );
    var_x = NULL;

    Py_XDECREF( var_maskdata );
    var_maskdata = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$cursors$$$function_2_load_xbm );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_pygame$cursors$$$function_2_load_xbm$$$function_1_bitswap( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_num = python_pars[ 0 ];
    PyObject *var_val = NULL;
    PyObject *var_x = NULL;
    PyObject *var_b = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = -1;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_compexpr_left_1;
    PyObject *tmp_compexpr_right_1;
    PyObject *tmp_frame_locals;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_left_name_3;
    PyObject *tmp_left_name_4;
    PyObject *tmp_next_source_1;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_right_name_3;
    PyObject *tmp_right_name_4;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = const_int_0;
    assert( var_val == NULL );
    Py_INCREF( tmp_assign_source_1 );
    var_val = tmp_assign_source_1;

    tmp_iter_arg_1 = const_tuple_74cd8b3e9a15d638f032c79ff4818f63_tuple;
    tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
    assert( tmp_assign_source_2 != NULL );
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_2;

    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_38fdeeb0bd241bff1b8b1a1ddcdaf972, module_pygame$cursors );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    tmp_next_source_1 = tmp_for_loop_1__for_iterator;

    tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
    if ( tmp_assign_source_3 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_1;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 254;
            goto try_except_handler_2;
        }
    }

    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_3;
        Py_XDECREF( old );
    }

    tmp_assign_source_4 = tmp_for_loop_1__iter_value;

    {
        PyObject *old = var_x;
        var_x = tmp_assign_source_4;
        Py_INCREF( var_x );
        Py_XDECREF( old );
    }

    tmp_left_name_1 = par_num;

    tmp_left_name_2 = const_int_pos_1;
    tmp_right_name_2 = var_x;

    tmp_right_name_1 = BINARY_OPERATION( PyNumber_Lshift, tmp_left_name_2, tmp_right_name_2 );
    if ( tmp_right_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 255;
        goto try_except_handler_2;
    }
    tmp_compexpr_left_1 = BINARY_OPERATION( PyNumber_And, tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_right_name_1 );
    if ( tmp_compexpr_left_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 255;
        goto try_except_handler_2;
    }
    tmp_compexpr_right_1 = const_int_0;
    tmp_assign_source_5 = RICH_COMPARE_NE( tmp_compexpr_left_1, tmp_compexpr_right_1 );
    Py_DECREF( tmp_compexpr_left_1 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 255;
        goto try_except_handler_2;
    }
    {
        PyObject *old = var_b;
        var_b = tmp_assign_source_5;
        Py_XDECREF( old );
    }

    tmp_left_name_4 = var_val;

    if ( tmp_left_name_4 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "local variable '%s' referenced before assignment", "val" );
        exception_tb = NULL;

        exception_lineno = 256;
        goto try_except_handler_2;
    }

    tmp_right_name_3 = const_int_pos_1;
    tmp_left_name_3 = BINARY_OPERATION( PyNumber_Lshift, tmp_left_name_4, tmp_right_name_3 );
    if ( tmp_left_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 256;
        goto try_except_handler_2;
    }
    tmp_right_name_4 = var_b;

    tmp_assign_source_6 = BINARY_OPERATION( PyNumber_Or, tmp_left_name_3, tmp_right_name_4 );
    Py_DECREF( tmp_left_name_3 );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 256;
        goto try_except_handler_2;
    }
    {
        PyObject *old = var_val;
        var_val = tmp_assign_source_6;
        Py_XDECREF( old );
    }

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 254;
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = var_val;

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyString_FromFormat( "local variable '%s' referenced before assignment", "val" );
        exception_tb = NULL;

        exception_lineno = 257;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto try_return_handler_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    {
        bool needs_detach = false;

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_function, exception_lineno );
            needs_detach = true;
        }
        else if ( exception_lineno != -1 )
        {
            PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function, exception_lineno );
            traceback_new->tb_next = exception_tb;
            exception_tb = traceback_new;

            needs_detach = true;
        }

        if (needs_detach)
        {

            tmp_frame_locals = PyDict_New();
            if ( par_num )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_num,
                    par_num
                );

                assert( res == 0 );
            }

            if ( var_val )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_val,
                    var_val
                );

                assert( res == 0 );
            }

            if ( var_x )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_x,
                    var_x
                );

                assert( res == 0 );
            }

            if ( var_b )
            {
                int res = PyDict_SetItem(
                    tmp_frame_locals,
                    const_str_plain_b,
                    var_b
                );

                assert( res == 0 );
            }



            detachFrame( exception_tb, tmp_frame_locals );
        }
    }

    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygame$cursors$$$function_2_load_xbm$$$function_1_bitswap );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_num );
    Py_DECREF( par_num );
    par_num = NULL;

    Py_XDECREF( var_val );
    var_val = NULL;

    Py_XDECREF( var_x );
    var_x = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = -1;

    CHECK_OBJECT( (PyObject *)par_num );
    Py_DECREF( par_num );
    par_num = NULL;

    Py_XDECREF( var_val );
    var_val = NULL;

    Py_XDECREF( var_x );
    var_x = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygame$cursors$$$function_2_load_xbm$$$function_1_bitswap );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:

    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}



static PyObject *MAKE_FUNCTION_pygame$cursors$$$function_1_compile( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$cursors$$$function_1_compile,
        const_str_plain_compile,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_c191f7a2cd00ab9aa8f7bf02ca6d68ac,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame$cursors,
        const_str_digest_436dcad3629c6b56c661d5c450115dc0,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygame$cursors$$$function_2_load_xbm(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$cursors$$$function_2_load_xbm,
        const_str_plain_load_xbm,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_5913fbe0aabe7522b79a941a96a7d4e9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame$cursors,
        const_str_digest_8d3cab125ca9976d324a4961419d4bea,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygame$cursors$$$function_2_load_xbm$$$function_1_bitswap(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygame$cursors$$$function_2_load_xbm$$$function_1_bitswap,
        const_str_plain_bitswap,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_38fdeeb0bd241bff1b8b1a1ddcdaf972,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_pygame$cursors,
        Py_None,
        0
    );


    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pygame$cursors =
{
    PyModuleDef_HEAD_INIT,
    "pygame.cursors",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

#if PYTHON_VERSION >= 300
extern PyObject *metapath_based_loader;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineType();
extern void _initCompiledCoroutineWrapperType();
#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( pygame$cursors )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pygame$cursors );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();

#if PYTHON_VERSION >= 350
    _initCompiledCoroutineType();
    _initCompiledCoroutineWrapperType();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pygame.cursors: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pygame.cursors: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpygame$cursors" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pygame$cursors = Py_InitModule4(
        "pygame.cursors",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No __doc__ is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else
    module_pygame$cursors = PyModule_Create( &mdef_pygame$cursors );
#endif

    moduledict_pygame$cursors = (PyDictObject *)((PyModuleObject *)module_pygame$cursors)->md_dict;

    CHECK_OBJECT( module_pygame$cursors );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_digest_436e7eb61b35d39994d896416c082efa, module_pygame$cursors );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    PyObject *module_dict = PyModule_GetDict( module_pygame$cursors );

    if ( PyDict_GetItem( module_dict, const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

#ifndef __NUITKA_NO_ASSERT__
        int res =
#endif
            PyDict_SetItem( module_dict, const_str_plain___builtins__, value );

        assert( res == 0 );
    }

#if PYTHON_VERSION >= 330
    PyDict_SetItem( module_dict, const_str_plain___loader__, metapath_based_loader );
#endif

    // Temp variables if any
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_defaults_1;

    // Module code.
    tmp_assign_source_1 = const_str_digest_0ade19f25d59df28699eb1ae849f19fb;
    UPDATE_STRING_DICT0( moduledict_pygame$cursors, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = const_str_digest_8a244d4f06cfd47791362860096740cb;
    UPDATE_STRING_DICT0( moduledict_pygame$cursors, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = const_tuple_96db87ae5af861479adab30eb4bf579d_tuple;
    UPDATE_STRING_DICT0( moduledict_pygame$cursors, (Nuitka_StringObject *)const_str_plain_arrow, tmp_assign_source_3 );
    tmp_assign_source_4 = const_tuple_286ccaf39c99eff01ddd0b3f99d77ecf_tuple;
    UPDATE_STRING_DICT0( moduledict_pygame$cursors, (Nuitka_StringObject *)const_str_plain_diamond, tmp_assign_source_4 );
    tmp_assign_source_5 = const_tuple_2c1d6d232dd3e0e3e167fc51ca591929_tuple;
    UPDATE_STRING_DICT0( moduledict_pygame$cursors, (Nuitka_StringObject *)const_str_plain_ball, tmp_assign_source_5 );
    tmp_assign_source_6 = const_tuple_cb92914d270a4f6b6d6d7e626220d263_tuple;
    UPDATE_STRING_DICT0( moduledict_pygame$cursors, (Nuitka_StringObject *)const_str_plain_broken_x, tmp_assign_source_6 );
    tmp_assign_source_7 = const_tuple_10848a3ccffd6b5c0e2c59e547874c24_tuple;
    UPDATE_STRING_DICT0( moduledict_pygame$cursors, (Nuitka_StringObject *)const_str_plain_tri_left, tmp_assign_source_7 );
    tmp_assign_source_8 = const_tuple_c69d0ea490cb72cadfbecd20104d0bcf_tuple;
    UPDATE_STRING_DICT0( moduledict_pygame$cursors, (Nuitka_StringObject *)const_str_plain_tri_right, tmp_assign_source_8 );
    tmp_assign_source_9 = const_tuple_2f560d6b6d76c7251a2b5a9cabe326ff_tuple;
    UPDATE_STRING_DICT0( moduledict_pygame$cursors, (Nuitka_StringObject *)const_str_plain_thickarrow_strings, tmp_assign_source_9 );
    tmp_assign_source_10 = const_tuple_922ba2f432159266b6122aae41c64070_tuple;
    UPDATE_STRING_DICT0( moduledict_pygame$cursors, (Nuitka_StringObject *)const_str_plain_sizer_x_strings, tmp_assign_source_10 );
    tmp_assign_source_11 = const_tuple_c502d6295ca78f7c883cc52991623ead_tuple;
    UPDATE_STRING_DICT0( moduledict_pygame$cursors, (Nuitka_StringObject *)const_str_plain_sizer_y_strings, tmp_assign_source_11 );
    tmp_assign_source_12 = const_tuple_fbcb2d8a13c6eaf49b8a66c3ad298098_tuple;
    UPDATE_STRING_DICT0( moduledict_pygame$cursors, (Nuitka_StringObject *)const_str_plain_sizer_xy_strings, tmp_assign_source_12 );
    tmp_assign_source_13 = const_tuple_06fa2711c84d2a0f906a3493eedb3ed9_tuple;
    UPDATE_STRING_DICT0( moduledict_pygame$cursors, (Nuitka_StringObject *)const_str_plain_textmarker_strings, tmp_assign_source_13 );
    tmp_defaults_1 = const_tuple_str_plain_X_str_dot_str_plain_o_tuple;
    tmp_assign_source_14 = MAKE_FUNCTION_pygame$cursors$$$function_1_compile( INCREASE_REFCOUNT( tmp_defaults_1 ) );
    UPDATE_STRING_DICT1( moduledict_pygame$cursors, (Nuitka_StringObject *)const_str_plain_compile, tmp_assign_source_14 );
    tmp_assign_source_15 = MAKE_FUNCTION_pygame$cursors$$$function_2_load_xbm(  );
    UPDATE_STRING_DICT1( moduledict_pygame$cursors, (Nuitka_StringObject *)const_str_plain_load_xbm, tmp_assign_source_15 );

    return MOD_RETURN_VALUE( module_pygame$cursors );
}
