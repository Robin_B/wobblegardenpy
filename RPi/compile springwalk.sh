#!/bin/bash

echo "Compiling Springwalk, this will take a while..."

cd /home/pi/wobblegarden/wobblegardenpy/Transmitter

nuitka --recurse-to=wobbleforestunified --recurse-to=noise --recurse-to=pygame --recurse-to=serial springwalk.py
$SHELL