#!/bin/bash
echo "setting default audio"
sudo amixer cset numid=3 1

while true ; do

  echo "starting vlc..."
  cvlc --quiet --repeat --gain 0.1 ~/wobblegarden/wobblegardenpy/dreamy\ amb3_fixed\ white.wav &

  echo "Starting Wobble Garden!" 
  cd /home/pi/wobblegarden/wobblegardenpy/Transmitter

  python2.7 -u springwalk.py |& tee output.txt

  killall vlc
done

killall vlc

$SHELL